﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Clases_Globales
{
    public class Equivalencias : Dictionary<string, string>
    {
        public Equivalencias() {

            // EQUIVALENCIAS DE CREACION DEL CODIGO ORDEN A CADA PAQUETE
            // ES UTILIZADA PARA GENERAR CODIGO ORDEN 
            
            this.Add("ID_SUCURSAL", "013");
            // DESTINATARIOS
            this.Add("NDCODIGO", "028");
            this.Add("DECODIGO", "027");
            this.Add("PVCODIGO", "026");
            this.Add("CMCODIGO", "025");
            this.Add("PRCODIGO", "024");
            this.Add("TICODIGO", "023");
            this.Add("INCODIGO", "022");
            this.Add("COCODIGO", "002");


            // EQUIVALENCIA DE FUNCIONES
            // LOS PREFIJOS USADOS EN LAS FUNCIONES DEBEN TENER CADA UNO SU EQUIVALENCIA            
            // PREFIJO DE MENSAJERO
            this.Add("MENSAJERO", "101");
        }
    }
}