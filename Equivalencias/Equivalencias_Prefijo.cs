﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Clases_Globales
{
    public class Equivalencias_Prefijo : Dictionary<string, string>
    {
        public Equivalencias_Prefijo()
        {
            // CADA PREFIJO UTILIZADO EN EL CODIGO ORDEN DEBE ESTAR CONTENIDO EN ESTE OBJETO

            // PREFIJO DE DESTINATARIOS 
            this.Add("013", "SUCURSAL");
            this.Add("028", "DESTINATARIO");
            this.Add("027", "DEPENDIENTE");
            this.Add("026", "PROVEEDOR");
            this.Add("025", "CENTRO MEDICO");
            this.Add("024", "PSS");
            this.Add("023", "TITULAR");
            this.Add("022", "INTERMEDIARIO");
            this.Add("002", "CONTRATANTE");
            
            // PREFIJO DE MENSAJERO
            this.Add("101", "MENSAJERO");

            // PREFIJO DE BATCH
            this.Add("001", "BATCHTEST");


            // PREFIJO DE TRACKING - NO MODIFICAR
            this.Add("333", "CONTENEDOR");
            // PREFIJOS DE BATCHS DE DOCUMENTOS

            this.Add("055", "FACTURACION");
            this.Add("056", "EMISION");
            this.Add("057", "CARNETIZACION");
            this.Add("058", "COMUNICACIONES");
            this.Add("059", "CHEQUES");
            this.Add("060", "DEVOLUCION_REEM");
            this.Add("061", "CHEQ_CCM");
            this.Add("062", "CHEQ_COMISIONES");
            this.Add("063", "FACTURACIONBA");
            this.Add("064", "CARNETIZACIONBA");
            this.Add("065", "COMUNICACIONESBA");
            this.Add("066", "EMISIONBA");
            this.Add("067", "FACTURACIONRO");
            this.Add("068", "CARNETIZACIONRO");
            this.Add("069", "COMUNICACIONESRO");
            this.Add("070", "EMISIONRO");
            this.Add("071", "FACTURACIONSA");
            this.Add("072", "CARNETIZACIONSA");
            this.Add("073", "COMUNICACIONSA");
            this.Add("074", "EMISIONSA");
            this.Add("075", "CARTA_DEVOLUCION");
            this.Add("076", "CARTA_DEVOLUCIONBA");
            this.Add("077", "CARTA_DEVOLUCIONRO");
            this.Add("078", "CARTA_DEVOLUCIONSA");
        }
    }
}