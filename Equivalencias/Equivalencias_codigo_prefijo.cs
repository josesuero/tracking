﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tracking.Clases_Globales
{
    class Equivalencias_codigo_prefijo : Dictionary<string, string>
    {
        public Equivalencias_codigo_prefijo()
        {
            // CODIGO DE LOS TIPOS DE DESTINATARIOS

            // PREFIJO DE DESTINATARIOS 
            this.Add("028", "DESTINATARIO");
            this.Add("027", "DEPENDIENTE");
            this.Add("026", "PROVEEDOR");
            this.Add("025", "CENTRO MEDICO");
            this.Add("024", "PSS");
            this.Add("023", "TITULAR");
            this.Add("022", "INTERMEDIARIO");
            this.Add("002", "CONTRATANTE");
            
        }
    }
}
