﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jayrock.Json;
using System.Data;
using Jayrock.Json.Conversion;

namespace Tracking.Core
{
    public class Functions
    {
        cgdata cgdata;
        public Functions()
        {

            cgdata = new cgdata();

        }

        public string getcodmensajero(Dictionary<string, object> data)
        {

            Dictionary<string, string> wsDir = (Dictionary<string, string>)data["DATA_DIRECCION"];
            Parameters parameters = new Parameters();
            parameters.addIn("in_id_sector", int.Parse(wsDir["codbarrio"]), Parameters.TiposDatos.integer);
            parameters.addOut("out_id_mensajero", -1, Parameters.TiposDatos.integer);
            parameters.add("out_nomb_mensajero", " ", ParameterDirection.Output, null, 1000, Parameters.TiposDatos.strings);
            cgdata.procedureDataSet("tracking.tracking_pkg.BUSCAR_MENSAJERO", parameters.toArray());

            return "101" + parameters.getByName("out_id_mensajero").Value.ToString().Trim().PadLeft(11, '0');

        }

        public string setEmision(Dictionary<string, object> data)
        {

            Parameters parameters = new Parameters();
            parameters.addIn("In_IdObjeto", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_NumRef", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_TipoObjeto", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_SistemaOrigen", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_Sucursal", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_Usuario", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_Area", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_TipoIntermediario", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_IdIntermediario", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_NombreIntermediario", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_TipoPrestador", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_IdPrestador", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_NombrePrestador", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_IdCliente", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_NombreCliente", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_NumContrato", data["NUM_REF"], Parameters.TiposDatos.strings);
            parameters.addIn("In_NumFamilia", " ", Parameters.TiposDatos.integer);
            parameters.addIn("In_Notas", " ", Parameters.TiposDatos.strings);
            parameters.addIn("In_Caso", " ", Parameters.TiposDatos.strings);
            parameters.addOut("Out_IdCase", -1, Parameters.TiposDatos.strings);
            parameters.addOut("Out_OrdenTracking", -1, Parameters.TiposDatos.strings);
            parameters.addOut("out_scodejecucioninterface", -1, Parameters.TiposDatos.strings);
            parameters.addOut("out_scodmensaje", -1, Parameters.TiposDatos.strings);
            parameters.addOut("out_sdescmensaje", -1, Parameters.TiposDatos.strings);
            //parameters.add("out_nomb_mensajero", " ", ParameterDirection.Output, null, 1000, Parameters.TiposDatos.strings);
            cgdata.procedureDataSet("eps.websvctracking_pkg.prc_actdocori", parameters.toArray());

            return "1";
        }
    }
}