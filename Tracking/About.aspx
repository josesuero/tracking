﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="Tracking.About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#myTable").dataTable({
                "bJQueryUI": false
					, "bFilter": false
                    ,"bPaginate": false
					, "bSort": false
					, "bInfo": false
					, "bAutoWidth": false
					, "bLengthChange": false
					, "sScrollY": 100
					, "aoColumns": [
					{ "sWidth": "10%" },
					{ "sWidth": "30%" },
					{ "sWidth": "40%" },
					{ "sWidth": "20%" }
					]
            });
        });
    </script>    
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div>
    <table id="myTable" width="300px">
        <thead class="ui-widget-header">
            <tr>
                <th>
                    Test
                </th>
                <th>
                    Test
                </th>
                <th>
                    Test
                </th>
                <th>
                    Test
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div style="width:10%; word-wrap:break-word;">testaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</div>
                </td>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
            </tr>
            <tr>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
            </tr>
            <tr>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
            </tr>
            <tr>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
                <td>
                    test
                </td>
            </tr>
        </tbody>
    </table>
    </div>
    <div id="tbTest">
            <div id="toolBox">
            </div>
            <hr />
            <div id="smartTablePrint" class="alignRight"></div>         
            <div id="contTbSmart">
                <table>
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
</asp:Content>
