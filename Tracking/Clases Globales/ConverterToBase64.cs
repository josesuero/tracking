﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace Tracking.Clases_Globales
{
    public class ConverterToBase64
    {
        static char[] map = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

        public ConverterToBase64() { 

        }

        private static string Encode(long inp, IEnumerable<char> map)
        {
            Debug.Assert(inp >= 0, "not implemented for negative numbers");

            var b = map.Count();
            // value -> character
            var toChar = map.Select((v, i) => new { Value = v, Index = i }).ToDictionary(i => i.Index, i => i.Value);
            var res = "";
            if (inp == 0)
            {
                return "" + toChar[0];
            }
            while (inp > 0)
            {
                // encoded least-to-most significant
                var val = (int)(inp % b);
                inp = inp / b;
                res += toChar[val];
            }
            return res;
        }

        private static long Decode(string encoded, IEnumerable<char> map)
        {
            var b = map.Count();
            // character -> value
            var toVal = map.Select((v, i) => new { Value = v, Index = i }).ToDictionary(i => i.Value, i => i.Index);
            long res = 0;
            // go in reverse to mirror encoding
            for (var i = encoded.Length - 1; i >= 0; i--)
            {
                var ch = encoded[i];
                var val = toVal[ch];
                res = (res * b) + val;
            }
            return res;
        }

        public static string Compress(string codigo_compress)
        {
            //string codigo_compress = "00100000000018013000000000011010000000000102700001519894";

            string codigo_orden = "";
            int partLength = 14;
            for (int i = 0; i < codigo_compress.Length; i += partLength)
            {
                string part = codigo_compress.Substring(i, partLength);

                //codigo_orden += "_" + compressPart(codigo_compress.Substring(i, partLength));
                codigo_orden += ".";
                codigo_orden += ConverterToBase64.Encode(long.Parse(part.Substring(0, 3).ToString()), map);
                codigo_orden += "-";
                codigo_orden += ConverterToBase64.Encode(long.Parse(part.Substring(3).ToString()), map);

            }
            return codigo_orden;
        }

        public static string Decompress(string codigo_orden)
        {
            string[] decod = codigo_orden.Substring(1).Split('.');
            string strdecod = "";
            foreach (var t in decod)
            {
                string[] partdecod = t.Split('-');

                var decoded = ConverterToBase64.Decode(partdecod[0], map);
                strdecod += decoded.ToString().PadLeft(3, '0');
                decoded = ConverterToBase64.Decode(partdecod[1], map);
                strdecod += decoded.ToString().PadLeft(11, '0');
            }
            return strdecod;
        }
    }
}