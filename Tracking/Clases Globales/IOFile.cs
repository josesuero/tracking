﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jayrock.Json;

using System.IO;
using System.Data.OleDb;
using System.Data;

using System.Configuration;
using Tracking.Objetos_DB;
using System.Text;


namespace TrackingFile
{
        public class IOFile
        {

            // CARACTER SEPARADOR PARA ARCHIVOS GENERICOS
            private static char separator = '|';
            // TIPO DE DOCUMENTO  SELECCIONADO
            private static string type_document;
            
            // ESTRUCTURA DE ARCHIVO
            private static string[] structure_file = 
            { 
                "ID_OBJETO",                                    
                "NUM_REF",                                    
                "ID_SUCURSAL",                                    
                "ID_CONTRATO",                                    
                "ID_FAMILIA",                                    
                "NOTES",    
                "NO_CASO",
                "CODIGO",
                "NOMBRE",
                "TIPO",
                "TIPO_PERSONA",
                "SEARCH_DIRECCION"
            };

            // ESTRUCTURA DE DESTINATARIO
            private static string[] structure_dest = 
            { 
                "CODIGO",
                "NOMBRE",
                "TIPO",
                "TIPO_PERSONA"                
            };

            // ESTRUCTURA DE DIRECCION
            private static string[] structure_dir = 
            { 
                "codciudad",
                "codprovincia",
                "codbarrio",
                "codtipodireccion",
                "direccion",                
                "telefono"
            };

            // SISTEMA ORIGEN = TRACKING
            private const string id_sistema_origen = "1";

            public IOFile() { 
            
            }

            private static string[] readFile(string fileLocation, string ext)
            {
                ext = ext.ToLower();
                List<string> lines = new List<string>();
                if (ext == ".txt")
                {
                    using (StreamReader reader = new StreamReader(@fileLocation, Encoding.Default))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            lines.Add(line); // Add to list.
                        }
                    };
                }
                else {
                    lines = null;
                }
                return lines.ToArray();
            }

            public static Dictionary<string, object>[] toDictionary(string fileLocation, string ext, string type_doc)
            {
                type_document = type_doc;
                string[] lines = readFile(fileLocation, ext);
                Dictionary<string, object>[] documentos = new Dictionary<string, object>[lines.Length];
                if (type_doc == "37" || type_doc == "8")
                {                    
                    documentos = no_standard_format(lines);
                }else{
                    documentos = standard_format(lines);
                }    
                return documentos;
            }

            private static Dictionary<string, object>[] no_standard_format (string [] lines){

                List<Dictionary<string, object>> documentos = new List<Dictionary<string, object>>();
                //Dictionary<string, object>[] documentos = new Dictionary<string, object>[lines.Length];

                Dictionary<string, object> documento;
                for (int i = 0; i < lines.Length; i++)
                {
                    documento = no_standard_format_toDictionary(lines[i]);
                    // SI ES TRANSFERENCIA NO LO INSTERTA
                    if(!documento.ContainsKey("ERROR")){
                        documentos.Add(documento);
                    }                    
                }
                return documentos.ToArray();
            }

            private static Dictionary<string, object>[] standard_format(string[] lines)
            {
                Dictionary<string, object>[] documentos = new Dictionary<string, object>[lines.Length];
                for (int i = 0; i < lines.Length; i++)
                {
                    documentos[i] = standard_format_toDictionary(lines[i]);
                }
                return documentos;
            }


            private static string value_of_position(string line,int pos_separator, int next_separator)
            {
                return line.Substring(pos_separator, next_separator - (pos_separator));
            }

            private static int get_next_separator(string line, int pos_separator)
            {
                return line.IndexOf(separator, pos_separator+1);
            }

            private static Dictionary<string, object> no_standard_format_toDictionary(string line)
            {
                char[] Posiciones = line.ToCharArray();
                string tipoMovimiento = Posiciones[11].ToString();
                string NumeroCheque = line.Substring(5, 6);
                string NumeroAfiliado = line.Substring(142, 20);
                string Numrad = line.Substring(0, 11);
                string Monto = line.Substring(122, 20);
                string CodPro = line.Substring(163, 2);
                string NombreCliente = line.Substring(12, 110);


                Dictionary<string, object> documento = new Dictionary<string, object>();
                if (Posiciones[175].ToString().ToLower() == "c")
                {
                    documento.Add("ID_TIPO_OBJETO", 37);
                }
                else
                {
                    documento.Add("ERROR", 1);
                    return documento;                     
                }
                documento.Add("ID_SISTEMA_ORIGEN", id_sistema_origen);
                documento.Add("ID_SUCURSAL", "7");
                documento.Add("NUM_REF", Numrad);
                documento.Add("ID_OBJETO", NumeroCheque);
                documento.Add("ID_CONTRATO", " ");
                documento.Add("ID_FAMILIA", " ");
                documento.Add("NOTES", " ");
                documento.Add("DESTINATARIO", "PV");
                documento.Add("PVCODIGO", NumeroCheque);
                documento.Add("PVNOMBRE", NombreCliente);
                documento.Add("PVTIPO", "PV");
                documento.Add("PVTIPO_PERSONA", "N"); 
                
                
                // DESTINATARIOS

                //documento.Add("DESTINATARIO", "IN");
                //documento.Add("INCODIGO", "0");
                //documento.Add("INNOMBRE", "CENTRO DE SERVICIOS");
                //documento.Add("INTIPO", "IN");
                //documento.Add("INTIPO_PERSONA", "I");                
                //Dictionary<string, string> dest_final = new Dictionary<string, string>();
                //dest_final.Add("CODIGO", NumeroCheque);
                //dest_final.Add("NOMBRE", NombreCliente);
                //dest_final.Add("TIPO", "PV");
                //dest_final.Add("TIPO_PERSONA", "N");
                //documento.Add("DESTINATARIO_FINAL", dest_final);
                return documento;

            }

            private static Dictionary<string, object> standard_format_toDictionary(string line) {

                Dictionary<string, object> documento = new Dictionary<string, object>();
                Paquete paquete = new Paquete();
                int actual_separator = 0;
                int next_separator;
                string value;

                line += "|" + line;
                // LLENA DICIONARIO DE DATOS DEL DOCUMENTO
                for (int i = 0; i < structure_file.Length; i++) 
                {
                    next_separator = get_next_separator(line, actual_separator);
                    value = value_of_position(line, actual_separator, next_separator);
                    documento.Add(structure_file[i], value);
                    actual_separator = next_separator + 1;
                }


                if (documento["SEARCH_DIRECCION"].ToString() == "1")
                {
                    // SI EL DOCUMENTO TIENE LA DIRECCION EN EL ARCHIVO ES = 1 Y SE AGREGARA LA QUE ESTA 
                    // EN LA FILA
                    Dictionary<string, string> direccion = new Dictionary<string, string>();

                    for (int i = 0; i < structure_dir.Length; i++)
                    {
                        next_separator = get_next_separator(line, actual_separator);
                        value = value_of_position(line, actual_separator, next_separator);
                        direccion.Add(structure_dir[i], value);
                        actual_separator = next_separator + 1;
                    }

                    documento.Add("DATA_DIRECCION", direccion);
                }
                documento.Remove("SEARCH_DIRECCION");
                // TIPO DE DESTINATARIO
                string tipo = documento["TIPO"].ToString();

                for (int i = 0; i < structure_dest.Length; i++)
                {
                    documento.Add(tipo + structure_dest[i], documento[structure_dest[i]]);
                    documento.Remove(structure_dest[i]);
                }
                documento.Add("ID_TIPO_OBJETO", type_document);
                documento.Add("ID_SISTEMA_ORIGEN", id_sistema_origen);
                documento.Add("DESTINATARIO", tipo);
                return documento; 

            }          

      }
}