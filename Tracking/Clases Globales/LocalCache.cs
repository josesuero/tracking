﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;
namespace Tracking
{
    public enum MyCachePriority
    {
        Default,
        NotRemovable
    }
    public class LocalCache
    {
        // Gets a reference to the default MemoryCache instance. 
        private static ObjectCache cache = MemoryCache.Default;
        private CacheItemPolicy policy = null;
        private CacheEntryRemovedCallback callback = null;
        private int defaultTTL = 300;

        public LocalCache()
        {
        }

        public LocalCache(int TTL)
        {
            defaultTTL = TTL;
        }

        public void Add(String CacheKeyName, Object CacheItem,
            MyCachePriority MyCacheItemPriority, int seconds)
        {
            // 
            callback = new CacheEntryRemovedCallback(this.MyCachedItemRemovedCallback);
            policy = new CacheItemPolicy();
            policy.Priority = (MyCacheItemPriority == MyCachePriority.Default) ?
                    CacheItemPriority.Default : CacheItemPriority.NotRemovable;
            policy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(seconds);
            policy.RemovedCallback = callback;
            //policy.ChangeMonitors.Add(new HostFileChangeMonitor(FilePath));

            // Add inside cache 
            cache.Set(CacheKeyName, CacheItem, policy);
        }

        public void Add(String CacheKeyName, Object CacheItem)
        {
            Add(CacheKeyName, CacheItem, MyCachePriority.Default, defaultTTL);
        }
        public object Get(String CacheKeyName)
        {
            // 
            return cache[CacheKeyName];
        }

        public void Remove(String CacheKeyName)
        {
            // 
            if (cache.Contains(CacheKeyName))
            {
                cache.Remove(CacheKeyName);
            }
        }

        private void MyCachedItemRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            // Log these values from arguments list 
            String strLog = String.Concat("Reason: ", arguments.RemovedReason.ToString(), "| Key-Name: ", arguments.CacheItem.Key, " | Value-Object: ", arguments.CacheItem.Value.ToString());
        }
    }
}
