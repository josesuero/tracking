﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Clases_Globales
{
    public class RegexBuilder
    {

        private string expression;
        
        public RegexBuilder() {
            expression = "";
        }

        public void addWord(string word) {
            expression += word + " ";
        }

        public void addNotWord(string word)
        {

            expression += string.Format(@"(?!\b{0}\b)\s", word);
        }

        public string getExpression() {
            return expression;
        }
    }
}