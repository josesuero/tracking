﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CodeConverter.aspx.cs" Inherits="Tracking.CodeConverter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" action="CodeConverter.aspx" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="LBL_CODIGO_ORDEN" runat="server" Text="Codigo de Orden"></asp:Label>        
            </td>
            <td>
                <asp:TextBox ID="CODIGO_ORDEN" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:CheckBox ID="ENCRYPTADO" AutoPostBack="true" runat="server"  Text="Encriptado" />                
            </td>      
            <td>
                <asp:Button ID="CONVERTER" runat="server" Text="Convertir" />
            </td>         
        </tr>
        <tr>
            <td>
                <asp:Label ID="LBL_RESULT" runat="server" Text="Resultado"></asp:Label>        
            </td>
            <td colspan="3">
                <asp:Label ID="RESULT" runat="server" Text=""></asp:Label>        
            </td>
        </tr>
    </table>

    <Triggers> 
        <asp:ASyncPostBackTrigger ControlID="ENCRYPTADO" /> 
    </Triggers>
    </form>
</asp:Content>
