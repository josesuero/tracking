﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tracking.Clases_Globales;

namespace Tracking
{
    public partial class CodeConverter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ENCRYPTADO.Checked = true;
            CONVERTER.Click += new EventHandler(this.converterClick);
            
        }

        public void converterClick(object sender, EventArgs e)
        {
            ConverterToBase64 converter = new ConverterToBase64();
            string codigo_orden = CODIGO_ORDEN.Text.ToString();
            string result_converter = "";
            try {
                
                if (ENCRYPTADO.Checked == true)
                {
                    result_converter = ConverterToBase64.Decompress(codigo_orden);
                }
                else {
                    result_converter = ConverterToBase64.Compress(codigo_orden);
                }

            } catch (Exception ex)
            {
                result_converter = "Codigo incorrecto. Favor verificar.";
            }             
            RESULT.Text = result_converter;            
        }
    }
}