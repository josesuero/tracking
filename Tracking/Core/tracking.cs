﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Tracking.Objetos_DB;
using Jayrock.Json;
using Jayrock.Json.Conversion;
using System.Reflection;
using Tracking.Clases_Globales;
using Tracking.WsMain;
using Tracking.WsSeguridad;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace Tracking.Core
{
    public class tracking
    {
        cgdata cgdata;
        JsonObject rules;
        JsonObject notificaciones;
        Equivalencias equivalencias;
        LocalCache cache;
        Dictionary<string, object> tipos_objetos;
        Equivalencias_Prefijo eq_prefijo;
        Dictionary<string, object> destinatario_codOrden;
        Dictionary<string, string> sucursales;
        Dictionary<string, string> batchs;
        Dictionary<string, string> status;

        private readonly object _syncRoot = new Object();
        // CANTIDAD DE CARACTERES QUE SE LE SUMAN A LOS PREFIJOS DE CADA CODIGO ORDEN PARA HACER UN TOTAL DE 14
        // NOTA: EL "CODIGO DE PAQUETE" SE LE AGREGA AL FINAL DEL "CODIGO ORDEN" GENERADO, Y A ESTE NO SE LE SUMA ESTA CONSTANTE
        // POR QUE CADA "CODIGO DE PAQUETE" ES DE 14 CARACTERES
        // CANTIDAD DE CARACTERES DEL CODIGO ORDEN SIN EL PREFIJO
        public const int LEN_COD_ORDEN = 11;


        public tracking()
        {

        }

        public tracking(cgdata newcgdata)
        {
            cgdata = newcgdata;
            rules = new JsonObject();
            equivalencias = new Equivalencias();
            cache = new LocalCache(30000);
            tipos_objetos = getTiposObj();
            eq_prefijo = new Equivalencias_Prefijo();
            destinatario_codOrden = get_destinatario_codOrden();

        }

        public Dictionary<string, object> get_destinatario_codOrden()
        {
            Dictionary<string, object> destinatario_codOrden = (Dictionary<string, object>)cache.Get("destinatario_codOrden");
            if (destinatario_codOrden == null)
            {
                destinatario_codOrden = new Dictionary<string, object>();
                cache.Add("destinatario_codOrden", destinatario_codOrden);
            }
            return destinatario_codOrden;
        }

        public void set_destinatario_codOrden(Dictionary<string, object> destinatario_codOrden)
        {
            cache.Remove("destinatario_codOrden");
            cache.Add("destinatario_codOrden", destinatario_codOrden);
        }

        #region "GENERADOR DE CODIGO ORDEN"

        // APLICA LAS REGLAS A UN PAQUETE  Y RETORNA UN CODIGO DE ORDEN
        public Dictionary<string, object> applyRules(Paquete paquete, Destinatario[] destinatarios)
        {
            rules = getReglas();
            Dictionary<string, object> error = new Dictionary<string, object>();
            Dictionary<string, object> dataFinal;
            Dictionary<string, string> data = dataPkg(paquete, destinatarios);
            JsonArray rulesObject = new JsonArray();
            JsonArray criterios = new JsonArray();
            JsonObject rule = new JsonObject();
            

            try
            {
                data["COD_PAQUETE"] = genCodPackage(data["ID_TIPO_OBJETO"].ToString(), data["ID_OBJETO"].ToString());
            }
            catch (Exception ex)
            {
                error["EJECUCION"] = 0;
                error["MESSAGE"] = "Error al intentar generar el codigo del paquete. Favor verificar ID_TIPO_OBJETO = " + data["ID_TIPO_OBJETO"] +
                " o ID_OBJETO = " + data["ID_OBJETO"] + " / " + ex.Message;
                return error;
            }


            bool result = new bool();
            //NOTA: Todos los tipos de documentos deben tener una regla o una regla por defecto
            int tipo_objeto = paquete.ID_TIPO_OBJETO;
            rulesObject = (JsonArray)rules[tipo_objeto.ToString()];
            if (rulesObject == null)
            {
                error["EJECUCION"] = 0;
                error["MESSAGE"] = "No existe regla para este tipo de documento.";
                return error;
            }
            for (int i = 0; i < rulesObject.Length; i++)
            {
                rule = (JsonObject)rulesObject[i];
                try
                {
                    result = validarCriterios((JsonArray)rule["criterio"], data);
                }
                catch (Exception ex)
                {
                    error["EJECUCION"] = 0;
                    error["MESSAGE"] = "Criterio incorrecto. Favor verificar. \n \n" +
                    rule["criterio"].ToString() + "Error c#: " + ex.Message + " / " + ex.StackTrace;
                    return error;
                }
                if (result) break;
            }
            if (result)
            {
                data.Add("DESTINATARIO", rule["destinatario"].ToString());
                dataFinal = data.ToDictionary(k => k.Key, k => (object)k.Value);
                try
                {
                    dataFinal.Add("DATA_DIRECCION", wsDireccion(data));
                }
                catch (Exception ex)
                {
                    error["EJECUCION"] = 0;
                    error["MESSAGE"] = "Error al consumir webservice de direccion. Favor verificar. \n \n" +
                    "Error c#: " + ex.Message + " / " + ex.StackTrace;
                    return error;
                }

                string batch_code = "";
                try
                {
                    Dictionary<string, object> batc = (Dictionary<string, object>)getAllBatch()[rule["batch"].ToString()];
                    batch_code = batc["PREFIJO"].ToString() + batc["CONTADOR"].ToString().PadLeft(LEN_COD_ORDEN, '0');
                    // SI BATCH NO HA SIDO INICIADO ES = 0, SI HA SIDO INICIADO = 1
                    if (batc["STATUS"].ToString() == "0")
                    {
                        cambiarStatusBatch(int.Parse(batc["ID"].ToString()));
                        insertLog("-1", 1, dataFinal["ID_USUARIO"].ToString(), "Creado via Integracion", batch_code, dataFinal["ID_AREA"].ToString());
                        //removeCache("batchs");
                    }

                }
                catch (Exception ex)
                {
                    error["EJECUCION"] = 0;
                    error["MESSAGE"] = "Error al intentar crear codigo de batch para el paquete. Verifique si existe este batch " + rule["batch"] + ". \n \n" +
                    "Error c#: " + ex.Message + " / " + ex.StackTrace;
                    return error;
                }




                try
                {

                    dataFinal.Add("COD_ORDEN", applyLevels(rule, dataFinal, batch_code));

                }
                catch (Exception ex)
                {
                    error["EJECUCION"] = 0;
                    error["MESSAGE"] = "Error al intentar aplicar niveles a paquete. Verifique los niveles de esta regla " + rule.ToString() + ". \n \n" +
                    "Error c#: " + ex.Message + " / " + ex.StackTrace;
                    return error;
                }
                return dataFinal;

            }
            else
            {
                error["EJECUCION"] = 0;
                error["MESSAGE"] = "Error buscando regla para paquete. No existe ninguna regla en la que este documento cumpla con los criterios, favor verificar este conjunto de reglas. " + rule.ToString() + ".";
                return error;
            }

        }

        public Dictionary<string, object> applyRulesDocManual(Dictionary<string, object> data, JsonObject rule, string codigo_orden_batch)
        {

            data["COD_PAQUETE"] = genCodPackage(data["ID_TIPO_OBJETO"].ToString(), data["ID_OBJETO"].ToString());
            data.Add("COD_ORDEN", applyLevels(rule, data, codigo_orden_batch));
            return data;

        }


        // GENERA UN CODIGO PARA EL PAQUETE  Y LO RETORNA
        public string genCodPackage(string id_tipo_obj, string id_obj)
        {
            JsonObject tipo = (JsonObject)tipos_objetos[id_tipo_obj];
            return tipo["PREFIJO"].ToString() + id_obj.Trim().PadLeft(11, '0');
        }

        //BUSCAR DIRECCION A WS 
        public Dictionary<string, string> wsDireccion(Dictionary<string, string> data)
        {
            WebSvcTracking ws = new WebSvcTracking();
            string dest = data["DESTINATARIO"];
            WebSvcTrackingUser_prcDirtrack_Out wsDir = (WebSvcTrackingUser_prcDirtrack_Out)ws.prcDirtrack(data[dest + "CODIGO"], data[dest + "TIPO_PERSONA"], data["ID_CONTRATO"], data["ID_FAMILIA"], " ", data[dest + "TIPO"], 1);

            Dictionary<string, string> direccion = new Dictionary<string, string>();
            foreach (var prop in wsDir.outdireccionesOut[0].GetType().GetProperties())
            {
                direccion.Add(prop.Name, prop.GetValue(wsDir.outdireccionesOut[0], null).ToString());
            }
            //int id_dir = id_dir = insertDireccion(idDestTracking, wsDir.outdireccionesOut[0].codciudad, wsDir.outdireccionesOut[0].codprovincia, wsDir.outdireccionesOut[0].codbarrio, wsDir.outdireccionesOut[0].codtipodireccion, wsDir.outdireccionesOut[0].direccion);
            return direccion;
        }

        // SI RETORNA TRUE EL PAQUETE DEBE SER AGRUPADO POR LOS NIVELES DE ESA REGLA
        public bool validarCriterios(JsonArray criterios, Dictionary<string, string> data)
        {
            JsonObject criterio = new JsonObject();
            RegexBuilder regex = new RegexBuilder();

            string campo = "";
            string valor = "";
            string operador = "";
            string input = "";
            string expression = "";

            for (int i = 0; i < criterios.Length; i++)
            {
                criterio = (JsonObject)criterios[i];
                campo = criterio["campo"].ToString();
                valor = criterio["valor"].ToString();
                operador = criterio["criterio"].ToString();
                if (data.ContainsKey(campo))
                {
                    input += data[campo] + " ";
                }
                else
                {
                    input += "null" + " ";
                }
                expression += valor + " ";
            }
            return Regex.IsMatch(input, expression);
        }

        // APLICA LOS NIVELES DE UNA REGLA AL PAQUETE Y EL DESTINATARIO SELECCIONADO
        public string applyLevels(JsonObject rule, Dictionary<string, object> obj, string codigo_orden_batch)
        {
            //Dictionary<string, object> batch_ = getAllBatch();
            //Dictionary<string, object> batc = (Dictionary<string, object>)batch_[rule["batch"].ToString()];
            string codigo_orden = codigo_orden_batch;
            //string codigo_orden = batc["PREFIJO"].ToString() + batc["CONTADOR"].ToString().PadLeft(LEN_COD_ORDEN, '0');
            JsonArray levels = (JsonArray)rule["niveles"];
            JsonObject level = new JsonObject();
            for (int i = 0; i < levels.Length; i++)
            {
                level = (JsonObject)levels[i];
                switch (level["tipo"].ToString())
                {
                    case "funcion":
                        codigo_orden += callFunction(obj, level["valor"].ToString());
                        break;
                    case "campo":
                        codigo_orden += getFieldValue(obj, level["valor"].ToString());
                        break;
                }
            }
            codigo_orden += obj["COD_PAQUETE"];
            return codigo_orden;
        }

        // LLAMA UNA FUNCION PARA LOS NIVELES Y RETORNA SU VALOR
        public string callFunction(Dictionary<string, object> obj, string function)
        {

            object result = new object();
            Type t = typeof(Functions);
            Functions functions = new Functions();
            MethodInfo mi = t.GetMethod(function);
            object[] args = new object[1];
            args[0] = obj;
            result = mi.Invoke(functions, args);
            return result.ToString();

        }

        // RETORNA EL VALOR DEL CAMPO EN EL DICIONARIO OBJ
        public string getFieldValue(Dictionary<string, object> obj, string campo)
        {
            return equivalencias[campo] + obj[campo].ToString().Trim().PadLeft(LEN_COD_ORDEN, '0');
        }

        #endregion

        #region "NOTIFICACIONES"

        public void threadNotificaciones(string id_usr, string id_sub_area, JsonObject paquete, int id_status)
        {
            JsonObject dict = new JsonObject();
            Thread t = new Thread(new ParameterizedThreadStart(aplicarNotificaciones));
            dict["id_usr"] = id_usr;
            dict["id_sub_area"] = id_sub_area;
            dict["paquete"] = paquete;
            dict["id_status"] = id_status;
            t.Start(dict);
        }

        public void noThreadNotificaciones(string id_usr, string id_sub_area, JsonObject paquete, int id_status)
        {
            JsonObject dict = new JsonObject();            
            dict["id_usr"] = id_usr;
            dict["id_sub_area"] = id_sub_area;
            dict["paquete"] = paquete;
            dict["id_status"] = id_status;
            aplicarNotificaciones(dict);
        }

        // VERIFICA SI AL ESTATUS DEL DOCUMENTO LE CORRESPONDE UNA NOTIFICACION
        public void aplicarNotificaciones(object data)
        {
            JsonObject data_ = (JsonObject)data;
            string id_usr = data_["id_usr"].ToString();
            string id_sub_area = data_["id_sub_area"].ToString();
            JsonObject paquete = (JsonObject)data_["paquete"];
            int id_status = int.Parse(data_["id_status"].ToString());
            //string id_usr, string id_sub_area, Dictionary<string, string> paquete, int id_status
            bool openConection = false;

            try
            {
                if (cgdata.connection.State != ConnectionState.Open)
                {
                    cgdata.connection.Open();
                    openConection = true;
                }

                
                notificaciones = getNotificaciones();

                //paquete.Add("id_status_recibir", id_status.ToString());

                //NOTA: Todos los tipos de documentos deben tener una notificacion o una notificacion por defecto
                JsonArray notificacionesObject = new JsonArray();

                JsonArray documentos = getDocumentosPrint(id_usr, id_sub_area, paquete["CODIGO_ORDEN"].ToString());
                JsonObject documento = new JsonObject();
                JsonObject notificacion = new JsonObject();
                bool result = false;
                Dictionary<string, object> dict;
                Dictionary<string, string> dataPaquete;
                System.Web.Script.Serialization.JavaScriptSerializer json;

                Parallel.For(0, documentos.Length, i => {
                    result = false;
                    documento = (JsonObject)documentos[i];
                    documento["id_status_recibir"] = id_status.ToString();

                    notificacionesObject = (JsonArray)notificaciones[documento["ID_TIPO_OBJETO"].ToString()];
                    if (notificacionesObject == null)
                    {
                        return;
                    }

                    json = new System.Web.Script.Serialization.JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
                    dict = (Dictionary<string, object>)json.DeserializeObject(JsonConvert.ExportToString(documento));
                    dataPaquete = dict.ToDictionary(k => k.Key, k => k.Value == null ? "" : k.Value.ToString());

                    for (int j = 0; j < notificacionesObject.Length; j++)
                    {
                        notificacion = (JsonObject)notificacionesObject[j];
                        result = validarCriterios((JsonArray)notificacion["criterio"], dataPaquete);
                        if (result) break;
                    }
                    if (result == true)
                    {
                        ejecutarNotificacion(notificacion, dict);
                    }
                });
                //for (int i = 0; i < documentos.Length; i++)
                //{
                //    result = false;
                //    documento = (JsonObject)documentos[i];
                //    documento["id_status_recibir"] = id_status.ToString();

                //    notificacionesObject = (JsonArray)notificaciones[documento["ID_TIPO_OBJETO"].ToString()];
                //    if (notificacionesObject == null)
                //    {
                //        continue;
                //    }

                //    json = new System.Web.Script.Serialization.JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
                //    dict = (Dictionary<string, object>)json.DeserializeObject(JsonConvert.ExportToString(documento));
                //    dataPaquete = dict.ToDictionary(k => k.Key, k => k.Value == null ? "" : k.Value.ToString());

                //    for (int j = 0; j < notificacionesObject.Length; j++)
                //    {
                //        notificacion = (JsonObject)notificacionesObject[j];
                //        result = validarCriterios((JsonArray)notificacion["criterio"], dataPaquete);
                //        if (result) break;
                //    }
                //    if (result == true)
                //    {
                //        ejecutarNotificacion(notificacion, dict);
                //    }
                //}
            }
            catch (Exception ex)
            {
                if (cgdata.connection.State == ConnectionState.Open && openConection)
                {
                    cgdata.connection.Close();
                }
            }
            finally {
                if (cgdata.connection.State == ConnectionState.Open && openConection)
                {
                    cgdata.connection.Close();
                }
            }

            
           
        }

        // ENVIAR LA NOTIFICACION
        public void ejecutarNotificacion(JsonObject notificacion, Dictionary<string, object> paquete)
        {
            callFunction(paquete, notificacion["accion"].ToString());
        }

        #endregion

        #region"Tracking Utilities"

        // RETORNA UN DICCIONARIO CON LA INFORMACION DEL PAQUETE Y CADA DESTINATARIO EN EL.
        public Dictionary<string, string> dataPkg(Paquete paquete, Destinatario[] destinatarios)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            foreach (var prop in paquete.GetType().GetProperties())
            {
                data.Add(prop.Name, prop.GetValue(paquete, null).ToString());
            }

            for (int i = 0; i < destinatarios.Length; i++)
            {
                foreach (var prop in destinatarios[i].GetType().GetProperties())
                {
                    data.Add(destinatarios[i].TIPO + prop.Name, prop.GetValue(destinatarios[i], null).ToString());
                }
            }
            return data;
        }

        // RETORNA UN OBJETO CON LAS REGLAS DE TRACKING
        public JsonObject getReglas()
        {
            JsonObject reglas = (JsonObject)cache.Get("reglas");
            if (reglas == null)
            {
                reglas = new JsonObject();

                JsonArray reglas_;
                JsonObject regla;

                DataSet ds = cgdata.query("select ID, VALOR, NOMBRE , ID_OBJETO from tracking.reglas");
                reglas_ = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
                for (int i = 0; i < reglas_.Length; i++)
                {
                    regla = (JsonObject)reglas_[i];
                    reglas[regla["ID_OBJETO"].ToString()] = (JsonArray)JsonConvert.Import(regla["VALOR"].ToString());
                }
                cache.Add("reglas", reglas, MyCachePriority.Default, 86400);
            }
            return reglas;
        }

        // RETORNA UN OBJETO CON LAS NOTIFICACIONES DE TRACKING
        public JsonObject getNotificaciones()
        {
            JsonObject notificaciones = (JsonObject)cache.Get("notificaciones");
            if (notificaciones == null)
            {
                notificaciones = new JsonObject();

                JsonArray notificaciones_;
                JsonObject notificacion;

                DataSet ds = cgdata.query("select ID, VALOR, NOMBRE , ID_OBJETO from tracking.notificacion");
                notificaciones_ = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
                for (int i = 0; i < notificaciones_.Length; i++)
                {
                    notificacion = (JsonObject)notificaciones_[i];
                    notificaciones[notificacion["ID_OBJETO"].ToString()] = (JsonArray)JsonConvert.Import(notificacion["VALOR"].ToString());
                }
                cache.Add("notificaciones", notificaciones, MyCachePriority.Default, 86400);
            }
            return notificaciones;
        }

        // RETORNA JSON ARRAY DE TODOS LOS BATCH BY ID_SUB_AREA
        public JsonArray getJsonBatchs()
        {

            DataSet ds = cgdata.query("select ID, NOMBRE, STATUS, PREFIJO, CONTADOR from tracking.batch");
            return (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));

        }

        // RETORNA JSON ARRAY DE TODOS LOS BATCH CORRESPODIENTE CON EL PARAMETRO ID_SUB_AREA 
        public JsonArray getJsonBatchs(string id_sub_area)
        {

            DataSet ds = cgdata.query("select ID, NOMBRE, STATUS, PREFIJO, CONTADOR from tracking.batch where id_sub_area = '" + id_sub_area + "'");
            return (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));

        }



        // RETORNA UN DICCIONARIO DE DATOS DE TODOS LOS BATCH EN LA BASE DE DATOS O EN CACHE SI YA ESTAN
        public Dictionary<string, object> getAllBatch()
        {
            Dictionary<string, object> batch = new Dictionary<string, object>();
            //batch = (Dictionary<string, object>)cache.Get("batchs");
            //if (batch == null)
            //{
                DataSet ds = cgdata.query("select ID, NOMBRE, PREFIJO, CONTADOR,STATUS from tracking.batch");
                DataRow[] rows = new DataRow[ds.Tables[0].Rows.Count];
                ds.Tables[0].Rows.CopyTo(rows, 0);
                batch = batchToDictionary(rows);
                
                //cache.Add("batchs", batch);
            //}
            return batch;

        }

        // RETORNA UN DICCIONARIO DE DATOS, DE LOS REGISTROS DE LA TABLA BATCH
        public Dictionary<string, object> batchToDictionary(DataRow[] rows)
        {
            Dictionary<string, object> batchs = new Dictionary<string, object>();
            Dictionary<string, object> batch;
            DataRow row;
            for (int i = 0; i < rows.Length; i++)
            {
                batch = new Dictionary<string, object>();
                row = rows[i];
                batch.Add("ID", rows[i]["ID"]);
                batch.Add("PREFIJO", rows[i]["PREFIJO"]);
                batch.Add("CONTADOR", rows[i]["CONTADOR"]);
                batch.Add("STATUS", rows[i]["STATUS"]);
                batchs.Add(rows[i]["NOMBRE"].ToString(), batch);
            }
            return batchs;
        }

        // RETORNA UN DICCIONARIO CON LOS BATCH CON KEY = PREFIJO Y VALOR = NOMBRE
        public Dictionary<string, string> getAllBatchPref()
        {
            Dictionary<string, string> batch = new Dictionary<string, string>();
            //batch = (Dictionary<string, string>)cache.Get("batchpref");
            //if (batch == null)
            //{
                DataSet ds = cgdata.query("select NOMBRE, PREFIJO from tracking.batch");
                DataRow[] rows = new DataRow[ds.Tables[0].Rows.Count];
                ds.Tables[0].Rows.CopyTo(rows, 0);
                batch = batchPrefToDictionary(rows);

                cache.Add("batchpref", batch);
            //}
            return batch;

        }

        // RETORNA UN DICCIONARIO CON LOS BATCH CON KEY = PREFIJO Y VALOR = NOMBRE
        public Dictionary<string, string> batchPrefToDictionary(DataRow[] rows)
        {
            Dictionary<string, string> batch = new Dictionary<string, string>();
            DataRow row;
            for (int i = 0; i < rows.Length; i++)
            {
                row = rows[i];
                batch.Add(rows[i]["PREFIJO"].ToString(), rows[i]["NOMBRE"].ToString());
            }
            return batch;
        }

        public Dictionary<string, string> getStatusDictionary()
        {

            Dictionary<string, string> status;
            status = (Dictionary<string, string>)cache.Get("status");
            if (status != null)
            {
                return status;
            }

            status = new Dictionary<string, string>();
            JsonArray status_arr = new JsonArray();
            JsonObject status_obj = new JsonObject();

            status_arr = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(getStatusPkg().Tables[0].Rows));
            for (int i = 0; i < status_arr.Length; i++)
            {
                status_obj = (JsonObject)status_arr[i];
                status.Add(status_obj["ID"].ToString(), status_obj["NOMBRE"].ToString());
            }
            cache.Add("status", status);
            return status;
        }

        // RETORNA UN DICCIONARIO CON LOS TIPOS DE OBJETOS
        public Dictionary<string, object> getTiposObj()
        {

            Dictionary<string, object> tipos_obj;
            tipos_obj = (Dictionary<string, object>)cache.Get("tipos_obj");
            if (tipos_obj != null)
            {
                return tipos_obj;
            }

            tipos_obj = new Dictionary<string, object>();
            JsonArray tipos = new JsonArray();
            JsonObject tipo = new JsonObject();
            tipos = getTypesDoc();
            for (int i = 0; i < tipos.Length; i++)
            {
                tipo = (JsonObject)tipos[i];
                tipos_obj.Add(tipo["ID"].ToString(), tipo);
            }
            cache.Add("tipos_obj", tipos_obj);
            return tipos_obj;

        }
        // RETORNA UN DICCIONARIO CON LOS DATOS DE CADA AREA, CADA KEY CORRESPONDE AL ID DE LA SUB AREA
        public Dictionary<string, object> getDictionaryAreas()
        {
            Dictionary<string, object> areas_;

            areas_ = (Dictionary<string, object>)cache.Get("areas");
            if (areas_ != null)
            {
                return areas_;
            }

            areas_ = new Dictionary<string, object>();
            JsonArray arrAreas = getAreas();
            JsonObject area = new JsonObject();
            Dictionary<string, string> area_;

            for (int i = 0; i < arrAreas.Length; i++)
            {
                area = (JsonObject)arrAreas[i];
                area_ = new Dictionary<string, string>();
                area_.Add("ACCARECOD", area["ACCARECOD"].ToString());
                area_.Add("ACCARENOM", area["ACCARENOM"].ToString());
                area_.Add("ACCARESUBCOD", area["ACCARESUBCOD"].ToString());
                area_.Add("ACCARESUBNOM", area["ACCARESUBNOM"].ToString());
                areas_.Add(area["ACCARESUBCOD"].ToString(), area_);
            }
            cache.Add("areas", areas_);
            return areas_;
        }
        // RETORNA UN DICCIONARIO CON LAS SUCURSALES
        public Dictionary<string, string> getDictionarySucursales()
        {

            Dictionary<string, string> sucursales;
            sucursales = (Dictionary<string, string>)cache.Get("sucursales");
            if (sucursales != null)
            {
                return sucursales;
            }

            sucursales = new Dictionary<string, string>();
            JsonArray arrSucursales = new JsonArray();
            JsonObject sucursal = new JsonObject();
            arrSucursales = getSucursales();
            for (int i = 0; i < arrSucursales.Length; i++)
            {
                sucursal = (JsonObject)arrSucursales[i];
                sucursales.Add(sucursal["AFISUCCOD"].ToString().Trim(), sucursal["AFISUCNOM"].ToString());
            }
            cache.Add("sucursales", sucursales);
            return sucursales;

        }

        // RETORNA UN DICCIONARIO CON LOS DATOS DEL CODIGO ORDEN PASADO POR PARAMETROS
        public JsonObject data_destinatario(string codigo_orden, JsonObject paquete)
        {

            JsonArray destinatarios = new JsonArray();
            JsonObject destinatario_;
            JsonObject destinatario = new JsonObject(); ;

            int final = int.Parse(paquete["FINAL"].ToString());
            // NOTA: SI SE CAMBIA EL LARGO DEL CODIGO ORDEN, DEBE SER CAMBIADO AQUI
            int len = codigo_orden.Length / (LEN_COD_ORDEN + 3);
            string part_codigo_orden = "";
            string codigo_orden_ = "";
            long codigo = 0;
            string prefijo = "";

            for (int i = 0; i < len; i++)
            {
                destinatario = new JsonObject();
                codigo_orden_ = codigo_orden.Substring(0, (LEN_COD_ORDEN + 3) * (i + 1));
                part_codigo_orden = codigo_orden_.Substring(codigo_orden_.Length - (LEN_COD_ORDEN + 3), (LEN_COD_ORDEN + 3));

                // VERIFICA SI ESA PARTE DEL CODIGO ORDEN ESTA DISPONIBLE EN CACHE
                if (destinatario_codOrden.ContainsKey(codigo_orden_))
                {
                    destinatario = (JsonObject)destinatario_codOrden[codigo_orden_];
                    destinatarios.Add(destinatario);
                    continue;
                }

                prefijo = codigo_orden_.Substring(codigo_orden_.Length - (LEN_COD_ORDEN + 3), 3);
                codigo = long.Parse(codigo_orden_.Substring(codigo_orden_.Length - LEN_COD_ORDEN, LEN_COD_ORDEN));


                //destinatario["DEST_TIPO_COD"] = "";
                //destinatario["DEST_ID_TIPO"] = "-1";
                //destinatario["DEST_TIPO_PERSONA"] = "";                

                destinatario["DEST_COD"] = codigo;
                destinatario["DEST_CODIGO_ORDEN"] = codigo_orden_;
                destinatario["ENTREGADO"] = paquete["ENTREGADO"];                


                switch (prefijo)
                {
                    case "013":
                        destinatario["DEST_NAME"] = sucursales[codigo.ToString()];
                        destinatario["DEST_TIPO_NOM"] = eq_prefijo[prefijo];
                        destinatario["LABEL"] = eq_prefijo[prefijo] + ": " + destinatario["DEST_NAME"];
                        break;
                    case "101":
                        destinatario["DEST_NAME"] = getMensajero(codigo, true);
                        destinatario["DEST_TIPO_NOM"] = eq_prefijo[prefijo];
                        destinatario["LABEL"] = eq_prefijo[prefijo] + ": " + destinatario["DEST_NAME"];
                        break;
                    case "028":
                    case "027":
                    case "026":
                    case "025":
                    case "024":
                    case "023":
                    case "002":
                    case "022":
                        destinatario_ = getDestinatarioByCod(codigo.ToString(), eq_prefijo[prefijo], int.Parse(paquete["ID_LAST_DOCUMENTO"].ToString()));
                        destinatario["DEST_ID"] = destinatario_["DEST_ID"];
                        destinatario["DEST_NAME"] = destinatario_["DEST_NAME"];
                        destinatario["DEST_ID_TIPO"] = destinatario_["DEST_ID_TIPO"];
                        destinatario["DEST_TIPO_PERSONA"] = destinatario_["DEST_TIPO_PERSONA"];
                        destinatario["DEST_TIPO_COD"] = destinatario_["DEST_TIPO_COD"];
                        destinatario["DEST_TIPO_NOM"] = destinatario_["DEST_TIPO_NOM"];
                        destinatario["LABEL"] = eq_prefijo[prefijo] + ": " + destinatario_["DEST_NAME"].ToString();
                        break;
                    default:
                        if (final == 1 && (i + 1) == len)
                        {
                            destinatario["DEST_NAME"] = "";
                            destinatario["DEST_TIPO_NOM"] = "";
                            destinatario["LABEL"] = paquete["TIPO_OBJETO"].ToString() + ": " + paquete["ID_OBJETO"].ToString();
                        }
                        else
                        {
                            destinatario["DEST_NAME"] = batchs[prefijo];
                            destinatario["DEST_TIPO_NOM"] = "BATCH";
                            destinatario["LABEL"] = "BATCH: " + codigo;
                        }
                        break;
                }
                destinatarios.Add(destinatario);
                destinatario_codOrden.Add(codigo_orden_, destinatario);
            }

            if (final == 0)
            {
                paquete["TIPO_OBJETO"] = "PAQUETE";
                paquete["ID_OBJETO"] = part_codigo_orden;
                paquete["NUM_REF"] = part_codigo_orden;
                paquete["CODIGO"] = codigo_orden;
                paquete["ID_OBJETO"] = part_codigo_orden;
                paquete["DEST_COD"] = destinatario["DEST_COD"];
                paquete["DEST_NAME"] = destinatario["DEST_NAME"];
                paquete["DEST_ID_TIPO"] = destinatario["DEST_ID_TIPO"];
                paquete["DEST_TIPO_PERSONA"] = destinatario["DEST_TIPO_PERSONA"];
                paquete["DEST_TIPO_COD"] = destinatario["DEST_TIPO_COD"];
                paquete["DEST_TIPO_NOM"] = destinatario["DEST_TIPO_NOM"];
                // TODO: MEJORAR FORMA DE AGREGAR ESTATUS
                //paquete["STATUS_NOM"] = " ";
                paquete["STATUS_NOM"] = get_status_paquetes_virtuales(paquete["CODIGO_ORDEN"].ToString());

            }
            paquete["FECHA_CREACION"] = "";
            paquete["DATA_CODIGO_ORDEN"] = destinatarios;
            return paquete;
        }

        // AGREGA LOS DATOS QUE NO ESTAN EN EL OBJETO PAQUETE DE CADA PAQUETE VIRTUAL Y DOCUMENTO.
        public JsonArray fillDataPaquetes(JsonArray paquetes)
        {

            JsonObject paquete;
            // VARIABLES GLOBALES
            sucursales = getDictionarySucursales();
            batchs = getAllBatchPref();
            status = getStatusDictionary();

            Dictionary<string, object> areas = getDictionaryAreas();
            Dictionary<string, string> area;
            string codigo_orden;
            bool connOpen = false;

            if (cgdata.connection.State != ConnectionState.Open)
            {                
                cgdata.connection.Open();
                connOpen = true;
            }


            // OBJETOS PARA LOGICA DE CAMPO DE CREACION
            string batchs_ = "";
            List<int> indexes_ = new List<int>();            

            for (int i = 0; i < paquetes.Length; i++)
            {
                paquete = (JsonObject)paquetes[i];
                area = (Dictionary<string, string>)areas[paquete["ID_SUB_AREA"].ToString()];
                codigo_orden = paquete["CODIGO_ORDEN"].ToString();                             
                paquete = data_destinatario(codigo_orden, paquete);

                // LOGICA PARA CAMPO DE FECHA DE CREACION
                if (codigo_orden.Length == 14 && paquete["FECHA_CREACION"].ToString().Equals(""))
                {
                    batchs_ += codigo_orden + ",";
                    indexes_.Add(i);
                    
                }  

                paquete["DEPT_UBICACION"] = area["ACCARENOM"] + " - " + area["ACCARESUBNOM"];
                paquete["childrens"] = new JsonArray();
            }

            // BUSCAR Y AGREGAR FECHA DE CREACION A PAQUETES DE BATCH
            if(batchs_.Length > 0){
                batchs_ = batchs_.Substring(0, batchs_.Length - 1);
                paquetes = fill_field_datecreate(paquetes, batchs_, indexes_.ToArray());
            }                        

            if (connOpen)
            {
                cgdata.connection.Close();
            }
            // se guarda el codigo_orden en cache
            set_destinatario_codOrden(destinatario_codOrden);

            return paquetes;
        }


        public JsonArray fillDataDocumentos(JsonArray paquetes)
        {

            JsonArray destinatarios;
            JsonObject destinatario;
            JsonObject paquete;
            Dictionary<string, object> areas = getDictionaryAreas();
            Dictionary<string, string> area;

            for (int i = 0; i < paquetes.Length; i++)
            {
                paquete = (JsonObject)paquetes[i];
                area = (Dictionary<string, string>)areas[paquete["ID_SUB_AREA"].ToString()];
                destinatarios = getDestinatariosPaquete(int.Parse(paquete["ID"].ToString()));
                for (int j = 0; j < destinatarios.Length; j++)
                {
                    destinatario = (JsonObject)destinatarios[i];

                }
                paquete["DEPT_UBICACION"] = area["ACCARENOM"] + " - " + area["ACCARESUBNOM"];
            }
            // se guarda el codigo_orden en cache
            set_destinatario_codOrden(destinatario_codOrden);
            return paquetes;
        }

        // BUSCA LOS NOMBRE DE CADA CODIGO DE USUARIO PASADO EN EL PARAMETRO LISTOBJ
        public Dictionary<string, string> nameUsers(AccusrlistObjUser[] listObj)
        {

            Dictionary<string, string> users = new Dictionary<string, string>();
            WebSvcAcceso wsAcceso = new WebSvcAcceso();
            WebSvcAccesoUser_prcAccusrlistsel_Out ws_users = wsAcceso.prcAccusrlistsel(listObj);
            for (int i = 0; i < ws_users.outresultOut.Length; i++)
            {
                users[ws_users.outresultOut[i].accusrcod] = ws_users.outresultOut[i].accusrnom;
            }
            return users;
        }

        // ELIMINA UN KEY DE CACHE
        public void removeCache(string key)
        {

            cache.Remove(key);

        }

        #endregion

        #region "CRUD TRACKING"

        #region "CONSULTAS"

        // DEVUELVE LOS TIPOS DE DOCUMENTOS
        public JsonArray getTypesDoc()
        {
            DataSet ds = cgdata.query(
            "select ID, NOMBRE, PREFIJO, PROCOD, DESTINATARIO_FINAL, ID_SUB_AREA" +
            ",MANUAL, ARCHIVO " +
            "from tracking.tipo_objetos order by NOMBRE");
            return (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
        }

        // DEVUELVE LAS SUCURSALES
        public JsonArray getSucursales()
        {
            DataSet ds = cgdata.query("select AFISUCCOD, AFISUCNOM from tracking.tabafisuc order by AFISUCCOD");
            return (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
        }

        // RETORNA TODAS LAS AREAS DE ESQUEMA DE SEGURIDAD
        public JsonArray getAreas()
        {
            Parameters parameters = new Parameters();
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.areas", parameters.toArray());
            return (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
        }

        // RETORNA TODAS LAS AREAS DE ESQUEMA DE SEGURIDAD
        public JsonArray getTiposDestinatario()
        {
            Parameters parameters = new Parameters();
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.TIPOS_DETINATARIO", parameters.toArray());
            return (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
        }

        // RETORNA TODOS LOS DOCUMENTO QUE LA SUB AREA DEL USUARIO PUEDE RECIBIR
        public Dictionary<string, string> getDocumentosArea(int id_sub_area)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("in_id_sub_area ", id_sub_area, Parameters.TiposDatos.integer);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.DOCUMENTOS_X_AREA", parameters.toArray());
            JsonArray documentos = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            JsonObject documento;
            Dictionary<string, string> documentos_ = new Dictionary<string, string>();

            for (int i = 0; i < documentos.Length; i++)
            {
                documento = (JsonObject)documentos[i];
                documentos_[documento["ID_TIPO_OBJETO"].ToString()] = documento["ID_SUB_AREA"].ToString();
            }
            return documentos_;
        }

        // DEVUELVE LOS ESTATUS DE PAQUETE
        public DataSet getStatusPkg()
        {
            DataSet ds = cgdata.query("select * from tracking.status_paquete order by id");
            return ds;
        }

        // RETORNA LABEL DE PAQUETES DEL ULTIMO NIVEL( CUANDO SE ENVIA LEN = 1 ) 
        //O RETORNA LABEL DEL PAQUETE ACTUAL( CUANDO SE ENVIA LEN = 0 )
        public JsonArray getPaquetesFinal(string id_usr, string id_sub_area, string cod_orden, int len)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("in_id_usr", id_usr, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_sub_area", id_sub_area, Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
            parameters.addIn("in_len", len, Parameters.TiposDatos.integer);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.PAQUETES_FINAL", parameters.toArray());
            JsonArray paquetes = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            paquetes = fillDataPaquetes(paquetes);
            return paquetes;
        }

        // VERIFICA SI EL PROXIMO NIVEL ES UN DOCUMENTO O UN PAQUETE 
        public int Verificar_Prox_Nivel(string cod_orden, int len)
        {

            Parameters parameters = new Parameters();
            parameters.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
            parameters.addIn("in_len", len, Parameters.TiposDatos.integer);
            parameters.addOut("out_final", 0, Parameters.TiposDatos.integer);
            cgdata.procedureNoquery("tracking.tracking_pkg.NEXT_FINAL", parameters.toArray());
            return int.Parse(parameters.getByName("out_final").Value.ToString());

        }

        // DEVUELVE PAQUETES QUE ESTEN EN EL AREA DEL USUARIO  DEL PROXIMO NIVEL AL CODIGO ORDEN QUE SE PASO POR PARAMETRO 
        public JsonArray getUserPkg(string id_usr, string id_sub_area, string cod_orden, int entregado)
        {
            return getUserPkg(id_usr, id_sub_area, cod_orden, entregado, 1);
        }

        // DEVUELVE PAQUETES QUE ESTEN EN EL AREA DEL USUARIO
        public JsonArray getUserPkg(string id_usr, string id_sub_area, string cod_orden, int entregado, int len)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("in_id_usr", id_usr, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_sub_area", id_sub_area, Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
            parameters.addIn("in_len", len, Parameters.TiposDatos.integer);
            parameters.addIn("in_entregado", entregado, Parameters.TiposDatos.integer);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.paquetes_usr", parameters.toArray());
            JsonArray paquetes = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            paquetes = fillDataPaquetes(paquetes);
            return paquetes;
        }

        // DEVUELVE LISTA DE DOCUMENTOS PARA EL PROCESO DE IMPRESION
        public JsonArray getDocumentosPrint(string id_usr, string id_sub_area, string cod_orden)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_sub_area", id_sub_area, Parameters.TiposDatos.strings);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.DOCUMENTOS_PRINT", parameters.toArray());
            JsonArray paquetes = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            paquetes = fillDataPaquetes(paquetes);
            return paquetes;
        }

        // DEVUELVE EL PAQUETE SOLICITADO
        public JsonObject getFromWsPkg(string id_usr, string cod_orden, int entregado)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("in_id_usr", id_usr, Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_paquete", cod_orden, Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
            parameters.addIn("in_entregado", entregado, Parameters.TiposDatos.integer);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.PAQUETES_WS_REPORT ", parameters.toArray());
            JsonArray paquetes = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            paquetes = fillDataPaquetes(paquetes);
            return (JsonObject)paquetes[0];
        }

        public JsonArray getgetPkgFromOthers(string id_usr, string codigo)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("in_id_usr", id_usr, Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_paquete", codigo, Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_orden", codigo, Parameters.TiposDatos.strings);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.paquetes_otros_usr", parameters.toArray());
            return (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
        }

        // RETORNA PAQUETES DE CUALQUIER AREA
        public JsonObject getPkgFromOthers(string id_usr, string codigo, Dictionary<string, string> documentos_area)
        {
            JsonArray paquetes = getgetPkgFromOthers(id_usr, codigo);
            JsonArray paquetes_ = new JsonArray();
            if (paquetes.Length != 0)
            {
                paquetes_.Add(verificarPaquete(paquetes, documentos_area));
                if (((JsonObject)paquetes_[0])["ERROR"].ToString() == "0")
                {
                    paquetes_ = fillDataPaquetes(paquetes_);
                }
                return (JsonObject)paquetes_[0];
            }
            else
            {
                JsonObject error = new JsonObject();
                error["ERROR"] = 1;
                error["MENSAJE"] = "Documento o Paquete no existe o ya fue entregado y no permite realizar transacciones sobre el.";
                return error;
            }
        }

        public JsonObject verificarPaquete(JsonArray paquetes, Dictionary<string, string> documentos_area) {
            return verificarPaquete(paquetes, documentos_area, true);
        }

        public JsonObject verificarPaquete(JsonArray paquetes, Dictionary<string, string> documentos_area, bool accion_recibir)
        {
            // SI ACCION RECIBIR ES TRUE, ENTONCES TOMARA EN CUENTA TODAS LAS CONDICIONES DURANTE EL FOR
            // SI ES FALSE ENTONCES NO TOMARA EN CUENTA LA CONDICION DE QUE EL PAQUETE PUEDA SER RECIBIDO EN EL AREA(PARA PROCESO DE CREACION DE PAQUETE)


            JsonObject paquete;
            JsonObject paqueteFinal = new JsonObject();
            bool stop = false;
            paqueteFinal.Put("ERROR", 0);
            paqueteFinal.Put("MENSAJE", "");
            paqueteFinal.Put("DOCUMENTOS", 0);
            paqueteFinal.Put("ID_SUB_AREA", ((JsonObject)paquetes[0])["ID_SUB_AREA"]);
            paqueteFinal.Put("ID_TIPO_OBJETO", ((JsonObject)paquetes[0])["ID_TIPO_OBJETO"]);

            for (int i = 0; i < paquetes.Length; i++)
            {
                paquete = (JsonObject)paquetes[i];
                foreach (string key in paquete.Names)
                {
                    if (key == "ID_SUB_AREA")
                    {
                        if (paqueteFinal[key].ToString() != paquete[key].ToString())
                        {
                            paqueteFinal["ERROR"] = 1;
                            paqueteFinal["MENSAJE"] = "No se puede recibir este paquete o documento "
                            + paquete["CODIGO_ORDEN"] + ", existen documentos en mas de un area.";
                            stop = true;
                            break;
                        }
                    }
                    else if (key == "ID_TIPO_OBJETO" && accion_recibir)
                    {

                        if (documentos_area.ContainsKey(paquete[key].ToString()) == false)
                        {
                            paqueteFinal["ERROR"] = 1;
                            paqueteFinal["MENSAJE"] = "No se puede recibir este tipo de documento en su area.";
                            stop = true;
                            break;
                        }
                    }
                    else if (key == "DOCUMENTOS")
                    {
                        paqueteFinal[key] = int.Parse(paquete[key].ToString()) + int.Parse(paqueteFinal[key].ToString());
                    }
                    else
                    {
                        paqueteFinal[key] = paquete[key];
                    }
                }
                if (stop)
                {
                    break;
                }

            }

            return paqueteFinal;
        }

        // RETORNA LOS PAQUETES QUE CONCUERDEN CON EL TERMINO ESCRITO POR EL USUARIO

        public JsonArray buscarDocumento(string term, string codigo_orden, string id_sub_area)
        {
            DataSet ds = new DataSet();
            Parameters parameters = new Parameters();
            parameters.addIn("term", term, Parameters.TiposDatos.strings);
            parameters.addIn("in_codigo_orden", codigo_orden, Parameters.TiposDatos.strings);
            //parameters.addIn("in_id_sub_area", id_sub_area, Parameters.TiposDatos.strings);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            ds = cgdata.procedureDataSet("tracking.tracking_pkg.SEARCH_DOCUMENTOS", parameters.toArray());
            JsonArray paquetes = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            paquetes = fillDataPaquetes(paquetes);
            return paquetes;
        }

        public JsonArray busquedadAvanzada(JsonObject data, string id_user, string id_sub_area)
        {

            string filter = "";
            string filter_destinatario = "";
            string query_destinatario = "";
            string filter_fecha = "";
            string query_fecha = "";

            DataSet ds = new DataSet();
            Parameters parameters = new Parameters();
            //parameters.addIn("term", term, Parameters.TiposDatos.strings);
            //parameters.addIn("in_codigo_orden", codigo_orden, Parameters.TiposDatos.strings);
            ////parameters.addIn("in_id_sub_area", id_sub_area, Parameters.TiposDatos.strings);
            //parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);

            string valor = "";
            bool band = false;
            bool band_dest = false;
            bool band_fecha = false;
            string key_fecha = "";
            string key_codigo = "";

            foreach (string key in data.Names)
            {
                valor = data[key].ToString();
                if (valor != "")
                {
                    // NOTA: SI CAMBIAN ID DE LA PANTALLA BUSQUEDAD AVANZADA
                    // AQUI TAMBIEN DEBEN SER REMPLAZADOS EN CASO DE SER ALGUNO
                    // DE ESTOS QUE ESTAN EN LA CONDICION
                    if (key == "d.CODIGO" || key == "td.CODIGO")
                    {
                        if (band_dest == true)
                        {
                            filter_destinatario += " and ";
                            key_codigo = "O";
                        }
                        parameters.addIn(":" + key.Substring(key.IndexOf(".") + 1) + key_codigo, valor, Parameters.TiposDatos.strings);
                        filter_destinatario += key + " = " + ":" + key.Substring(key.IndexOf(".") + 1) + key_codigo + " ";
                        band_dest = true;
                        continue;
                    }

                    if (key == "FECHA_DESDE" || key == "FECHA_HASTA" || key == "h.STATUS")
                    {

                        if (band_fecha == true)
                        {
                            filter_fecha += " and ";
                        }

                        if (key == "FECHA_DESDE")
                        {   
                            key_fecha = "trunc(h.FECHA) >= to_date(:FECHA_DESDE, 'DD/MM/YYYY') ";
                            filter_fecha += key_fecha;
                            parameters.addIn(":FECHA_DESDE", valor, Parameters.TiposDatos.strings);
                        }
                        else if (key == "FECHA_HASTA")
                        {
                            key_fecha = "trunc(h.FECHA) <= to_date(:FECHA_HASTA, 'DD/MM/YYYY') ";
                            filter_fecha += key_fecha;
                            parameters.addIn(":FECHA_HASTA", valor, Parameters.TiposDatos.strings);
                        }
                        else
                        {
                            key_fecha = "h.status = :status";
                            filter_fecha += key_fecha;
                            parameters.addIn(":status", valor, Parameters.TiposDatos.strings);
                        }
                        band_fecha = true;
                        continue;
                    }

                    if (band == true)
                    {
                        filter += " and ";
                    }

                    if (key == "p.ID_SUCURSAL")
                    {
                        //parameters.addIn(":ID_SUC", valor, Parameters.TiposDatos.strings);
                        //filter += "trim(" + key + ") = trim(:ID_SUC) ";
                        filter += key + " = " + valor + " ";
                        band = true;
                    }
                    else
                    {
                        parameters.addIn(":" + key.Substring(key.IndexOf(".") + 1), valor, Parameters.TiposDatos.strings);
                        filter += key + " = " + ":" + key.Substring(key.IndexOf(".") + 1) + " ";
                        band = true;
                    }
                }

            }


            if (filter_destinatario != "")
            {
                query_destinatario =
                " p.ID in (select " +
                "ID_PAQUETE " +
                "from " +
                "tracking.paquete_destinatarios pd " +
                "inner join tracking.destinatarios d on pd.ID_DESTINATARIO = d.ID " +
                "inner join tracking.tipo_destinatarios td on td.ID = d.ID_TIPO where " + filter_destinatario + " )";
            }

            if (filter_fecha != "")
            {
                //query_fecha =
                //" p.ID in (select h.ID_PAQUETE " +
                //"from tracking.historial h where " +
                //" h.STATUS = 1 " + filter_fecha + " )";
                query_fecha =
                " p.ID in (SELECT a.ID FROM " +
                "(SELECT h.codigo_orden, h.fecha_modificado FROM tracking.historial h where " +
                filter_fecha + " ) " +
                "paq inner join tracking.paquete a on a.codigo_orden like paq.codigo_orden||'%') ";
            }

            if (filter != "")
            {
                filter = " where " + filter;
                if (filter_destinatario != "")
                {
                    filter = filter + " and ";
                    if (filter_fecha != "")
                    {
                        query_destinatario = query_destinatario + " and ";
                    }
                }
                else
                {
                    if (filter_fecha != "")
                    {
                        query_destinatario = " and ";
                    }
                }
            }
            else
            {
                if (filter_destinatario != "")
                {
                    filter = " where ";
                    if (filter_fecha != "")
                    {
                        query_destinatario = query_destinatario + " and ";
                    }
                }
                else
                {
                    if (filter_fecha != "")
                    {
                        filter = " where ";
                    }
                }
            }



            ds = cgdata.query(
            "select " +
            "p.CODIGO_ORDEN" +
            ",1 AS FINAL" +
            ",0 AS DOCUMENTOS" +
            ",p.ID as ID_LAST_DOCUMENTO" +
            ",1 AS AREAS" +
            ",p.ID_SUB_AREA" +
            ",p.ID" +
            ",p.CODIGO" +
            ",p.STATUS" +
            ",p.ENTREGADO" +
            ",p.ID_OBJETO" +
            ",p.ID_TIPO_OBJETO" +
            ",p.ID_SISTEMA_ORIGEN" +
            ",p.ID_SUCURSAL" +
            ",p.ID_DIRECCION" +
            ",p.ID_DESTINATARIO" +
            ",p.NUM_REF" +
            ",p.NO_CASO" +
            ",p.NUM_CONTRATO" +
            ",p.CAMPO1" +
            ",p.CAMPO2" +
            ",tp.NOMBRE AS TIPO_OBJETO " +
            ",d.ID AS DEST_ID" +
            ",d.CODIGO AS DEST_COD" +
            ",d.NOMBRE AS DEST_NAME" +
            ",d.ID_TIPO AS DEST_ID_TIPO" +
            ",td.CODIGO as DEST_TIPO_COD" +
            ",td.NOMBRE as DEST_TIPO_NOM" +
            ",td.TIPO_PERSONA as DEST_TIPO_PERSONA" +
            ",sp.NOMBRE as STATUS_NOM" +
            ",' ' as DEPT_ORIGEN" +
            ",' ' as DEPT_UBICACION " +
            "from tracking.paquete p " +
            "inner join tracking.status_paquete sp on sp.ID = p.STATUS " +
            "inner join tracking.destinatarios d on d.ID = p.ID_DESTINATARIO " +
            "inner join tracking.tipo_destinatarios td on td.ID = d.ID_TIPO " +
            "inner join tracking.tipo_objetos tp on tp.id = p.ID_TIPO_OBJETO " + filter
            + " " + query_destinatario + " " + query_fecha
            , parameters.toArray());

            JsonArray paquetes = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            paquetes = fillDataPaquetes(paquetes);

            return paquetes;
        }

        //RECIBE EL PARAMETRO ID DE LA TABLA DE DIRECCIONES Y LA RETORNA
        public JsonObject getDirPkg(int id)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("in_id_dest", DBNull.Value, Parameters.TiposDatos.integer);
            parameters.addIn("in_id_dir", id, Parameters.TiposDatos.integer);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.buscar_direccion", parameters.toArray());
            JsonObject dir = (JsonObject)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows[0]));
            return dir;
        }

        //RECIBE EL PARAMETRO ID_DESTINATARIO Y BUSCA LAS DIRECCIONES QUE LE PERTENESCAN Y LA RETORNA A LA PAGINA
        public JsonArray getDirDest(string codDestinatario, string tipo, string tipopersona)
        {
            Parameters parameters = new Parameters();
            parameters.addIn(":cod", codDestinatario, Parameters.TiposDatos.strings);
            parameters.addIn(":tipo", tipo, Parameters.TiposDatos.strings);
            parameters.addIn(":tipo_persona", tipopersona, Parameters.TiposDatos.strings);
            object id_dest = cgdata.scalar(
             "select d.id from tracking.destinatarios d \n"
            + " inner join tracking.tipo_destinatarios tp on tp.ID = d.ID_TIPO \n"
            + " where d.codigo = :cod and tp.codigo = :tipo and tp.tipo_persona = :tipo_persona"
            , parameters.toArray());
            parameters = new Parameters();
            parameters.addIn("in_id_dest", id_dest, Parameters.TiposDatos.integer);
            parameters.addIn("in_id_dir", DBNull.Value, Parameters.TiposDatos.integer);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.buscar_direccion", parameters.toArray());
            JsonArray dir = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            return dir;
        }

        // OBTENER STATUS DE HISTORIAL PAQUETES VIRTUALES
        // TODO: MEJORAR LOGICA DE CONSEGUIR STATUS DEL PAQUETE VIRTUAL
        public string get_status_paquetes_virtuales(string cod_orden)
        {
            //Parameters parameters = new Parameters();
            //string procedure;
            //DataSet ds;
            //parameters.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
            //parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            //procedure = "tracking.tracking_pkg.paquete_virtual_hist";
            //ds = cgdata.procedureDataSet(procedure, parameters.toArray());
            DataSet ds;
            string query = " select " +
                //"h.ID " +
                //",h.ID_PAQUETE " +
                //",h.ID_SUB_AREA " +
                //",h.STATUS " +
                //",to_char(h.FECHA,'DD/MM/YYYY HH:MI:SS AM') FECHA " +
                //",h.ACTIVO " +
                //",h.ID_LOCATION " +
                //",to_char(h.FECHA_MODIFICADO,'DD/MM/YYYY HH:MI:SS AM') FECHA_MODIFICADO " +
                // SIN LA COMA 
                //"to_char(h.FECHA_MODIFICADO,'DD/MM/YYYY HH:MI:SS AM') FECHA_MODIFICADO " +
            " h.STATUS as STATUS_COD " +
                //",sp.NOMBRE as STATUS_NOM " +
            "from tracking.historial h " +
                //"inner join tracking.status_paquete sp on h.STATUS = sp.ID " +
            "where h.CODIGO_ORDEN in " +
            "(" + get_part_codOrden(cod_orden) + ")" +
            "and rownum = 1 " +
            "order by h.FECHA_MODIFICADO desc ";
            object result = cgdata.scalar(query);
            string resultSt = "";
            if (result != null && status.ContainsKey(result.ToString()))
            {
                resultSt = status[result.ToString()];
            }
            return resultSt;
            //JsonObject log = (JsonObject)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows[0]));
            //return log["STATUS_NOM"].ToString();
        }

        // RETORNA ARREGLO CON LA FECHA DE CREACION DE LOS PAQUETES VIRTUALES DE BACTH 
        public JsonArray fill_field_datecreate(JsonArray paquetes, string batchs, int[] indexes)
        {
            // CONSTRUYE QUERY
            string sqlstr = "select to_char(FECHA,'DD/MM/YYYY') as FECHA_CREACION, CODIGO_ORDEN from tracking.historial " 
            + "where codigo_orden IN (" + batchs + ") and status = 1";

            JsonArray results = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(cgdata.query(sqlstr).Tables[0].Rows));
            string[] codigos = batchs.Split(',');

            JsonObject result;
            string cod_orden = "";
            int index = -1;
            
            for (int i = 0; i < results.Length; i++) { 
                result = (JsonObject)results[i];
                cod_orden = result["CODIGO_ORDEN"].ToString();
                index = Array.IndexOf(codigos, cod_orden);
                if (index != -1)
                {
                    ((JsonObject)paquetes[indexes[index]])["FECHA_CREACION"] = result["FECHA_CREACION"].ToString();
                }            
            }

            return paquetes;
        }

        // RETORNA EL CODIGO ORDEN DIVIDIDO EN PARTES
        public string get_part_codOrden(string codigo_orden) {
            string codigo_orden_ = "";
            int len = codigo_orden.Length / (LEN_COD_ORDEN + 3);
            for (byte i = 0; i < len; i++)
            {
                codigo_orden_ += "'" + codigo_orden.Substring(0, (LEN_COD_ORDEN + 3) * (i + 1)) + "',";
            }
            return codigo_orden_.Substring(0, codigo_orden_.Length - 1);
        }

        // RETORNA HISTORIAL DE UN PAQUETE
        public JsonArray getLogPkg(int id, string cod_orden)
        {

            // SI ID ES -1 SE EJECUTARA EL PROCEDIMIENTO PARA PAQUETES VIRTUALES
            // EN CASO CONTRARIO EJECUTARA EL PROCEDIMIENTO PARA DOCUMENTOS
            Parameters parameters = new Parameters();
            string procedure;
            DataSet ds;

            if (id == -1)
            {

                string query =
                " select " +
                "h.ID " +
                ",h.ID_PAQUETE " +
                ",h.ID_SUB_AREA " +
                ",h.STATUS " +
                ",h.fecha as orden_fecha " +
                ",to_char(h.FECHA,'DD/MM/YYYY HH:MI:SS AM') FECHA " +
                ",h.ACTIVO " +
                ",h.ID_LOCATION " +
                ",to_char(h.FECHA_MODIFICADO,'DD/MM/YYYY HH:MI:SS AM') FECHA_MODIFICADO " +
                ",h.STATUS as STATUS_COD " +
                ",sp.NOMBRE as STATUS_NOM " +
                "from tracking.historial h " +
                "inner join tracking.status_paquete sp on h.STATUS = sp.ID " +
                "where h.CODIGO_ORDEN in " +
                "(" + get_part_codOrden(cod_orden) + ")" +
                "ORDER BY orden_fecha DESC ";
                //parameters.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
                //parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
                //procedure = "tracking.tracking_pkg.paquete_virtual_hist";
                //ds = cgdata.procedureDataSet(procedure, parameters.toArray());
                ds = cgdata.query(query);

            }
            else
            {

                parameters.addIn("in_pkg", id, Parameters.TiposDatos.integer);
                parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
                procedure = "tracking.tracking_pkg.paquete_hist";
                ds = cgdata.procedureDataSet(procedure, parameters.toArray());

                JsonArray list_paquete_cod = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
                JsonObject paquete_cod;
                string query = "";

                for (int i = 0; i < list_paquete_cod.Length; i++)
                {
                    paquete_cod = (JsonObject)list_paquete_cod[i];
                    if (i > 0)
                    {
                        query += " union ";
                    }
                    query +=
                    string.Format(
                    "select historial.ID, '{2}' as ID_PAQUETE, historial.STATUS as STATUS_COD, historial.fecha as orden_fecha, " +
                    "to_char(historial.FECHA,'DD/MM/YYYY HH:MI:SS AM') as FECHA, " +
                    "historial.ACTIVO, historial.ID_LOCATION,historial.FECHA_MODIFICADO, status_paquete.NOMBRE as STATUS_NOM, historial.CODIGO_ORDEN, " +
                    "historial.ID_SUB_AREA  " +
                    "from tracking.historial " +
                    "inner join tracking.status_paquete on historial.status = status_paquete.id " +
                    "where historial.codigo_orden in " +
                    "(" + get_part_codOrden(paquete_cod["CODIGO_ORDEN"].ToString()) + ")" +
                        //"(select * from table(CAST (tracking.tracking_pkg.get_tb_codorden('{0}') as tracking.t_cod_orden))) " +
                    "and historial.fecha_modificado >= to_date('{0}','DD/MM/YYYY HH:MI:SS AM')  " +
                    "and (historial.fecha_modificado <= to_date('{1}','DD/MM/YYYY HH:MI:SS AM') or to_date('{1}','DD/MM/YYYY HH:MI:SS AM') is null)"
                        //, paquete_cod["CODIGO_ORDEN"]
                    , paquete_cod["FECHA_DESDE"]
                    , paquete_cod["FECHA_HASTA"]
                    , paquete_cod["ID_PAQUETE"]
                    );
                }

                query = "select ID, STATUS_COD, ID_PAQUETE, orden_fecha, FECHA, ACTIVO, ID_LOCATION, FECHA_MODIFICADO, STATUS_NOM, CODIGO_ORDEN, ID_SUB_AREA"
                + " from (" + query + ")  order by orden_fecha DESC";
                ds = cgdata.query(query);

            }

            Dictionary<string, object> areas = getDictionaryAreas();
            Dictionary<string, string> area = new Dictionary<string, string>();

            JsonArray logs = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            JsonObject log = new JsonObject();
            AccusrlistObjUser[] list_name_User = new AccusrlistObjUser[logs.Length];
            AccusrlistObjUser obj_name_user;

            for (int i = 0; i < logs.Length; i++)
            {
                log = (JsonObject)logs[i];

                obj_name_user = new AccusrlistObjUser();
                obj_name_user.accusrcod = log["ID_LOCATION"].ToString();
                list_name_User[i] = obj_name_user;
                log.Put("NOTAS", getNotas(int.Parse(log["ID"].ToString())));
                area = (Dictionary<string, string>)areas[log["ID_SUB_AREA"].ToString()];
                log.Put("AREA", area["ACCARENOM"] + " - " + area["ACCARESUBNOM"]);
                logs[i] = log;
            }
            Dictionary<string, string> name_users = new Dictionary<string, string>();
            if (list_name_User.Length != 0)
            {
                name_users = nameUsers(list_name_User);
            }
            for (int i = 0; i < logs.Length; i++)
            {
                log = (JsonObject)logs[i];
                log["NOMBRE_USR"] = name_users[log["ID_LOCATION"].ToString().ToUpper()];
                logs[i] = log;
            }

            return logs;

        }



        // RETORNA LAS NOTAS DE UN HISTORIAL DE UN PAQUETE
        public JsonArray getNotas(int id)
        {
            //usrname key de json
            DataSet ds = cgdata.query(
            "select no.ID, no.ID_PAQUETE, no.ID_USUARIO, to_char(FECHA,'DD/MM/YYYY HH:MI AM') as FECHA, no.NOTA, no.ID_HISTORIAL "
            + " from tracking.nota no "
            + " where no.id_historial = " + id + " order by FECHA DESC");

            JsonArray notas = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));

            JsonObject nota = new JsonObject();
            AccusrlistObjUser[] list_name_User = new AccusrlistObjUser[notas.Length];
            AccusrlistObjUser obj_name_user;

            for (int i = 0; i < notas.Length; i++)
            {
                nota = (JsonObject)notas[i];
                obj_name_user = new AccusrlistObjUser();
                obj_name_user.accusrcod = nota["ID_USUARIO"].ToString();
                list_name_User[i] = obj_name_user;
            }

            Dictionary<string, string> name_users = new Dictionary<string, string>();
            if (list_name_User.Length != 0)
            {
                name_users = nameUsers(list_name_User);
            }
            for (int i = 0; i < notas.Length; i++)
            {
                nota = (JsonObject)notas[i];
                nota["USRNAME"] = name_users[nota["ID_USUARIO"].ToString().ToUpper()];
                notas[i] = nota;
            }


            return notas;
        }

        // RETORNA DETALLE DEL PAQUETE, EL HISTORIAL Y LA DIRECCION
        public JsonObject getDetPkg(JsonObject pkg, string id_user, string id_sub_area)
        {
            JsonObject detalle = new JsonObject();

            detalle["direccion"] = -1;
            string codigo = pkg["CODIGO_ORDEN"].ToString();
            int id_paquete = int.Parse(pkg["ID"].ToString());
            detalle["historial"] = getLogPkg(id_paquete, codigo);
            detalle["cambiar_direccion"] = 0;

            if (pkg["FINAL"].ToString() == "0")
            {
                detalle["dest_id"] = -1;
                detalle["dest_id_tipo"] = -1;
                detalle["destinatarios"] = new JsonArray();
                detalle["childrens"] = getUserPkg(id_user, id_sub_area, codigo, int.Parse(pkg["ENTREGADO"].ToString()));
                if (((JsonArray)detalle["childrens"]).Length != 0)
                {
                    JsonObject obj = (JsonObject)((JsonArray)detalle["childrens"])[0];
                    if (obj["FINAL"].ToString() == "1")
                    {
                        try
                        {
                            detalle["direccion"] = getDirPkg(int.Parse(obj["ID_DIRECCION"].ToString()));
                        }
                        catch
                        {
                            JsonObject error = new JsonObject();
                            error.Put("error", 1);
                            return error;
                        }

                        detalle["cambiar_direccion"] = 1;
                        detalle["dest_id"] = obj["DEST_ID"];
                        detalle["dest_id_tipo"] = obj["DEST_ID_TIPO"];
                        detalle["dest_cod"] = obj["DEST_COD"];

                    }
                }
            }
            else
            {
                detalle["destinatarios"] = getDestinatariosPaquete(id_paquete);
                try
                {
                    detalle["direccion"] = getDirPkg(int.Parse(pkg["ID_DIRECCION"].ToString()));
                }
                catch
                {
                    JsonObject error = new JsonObject();
                    error.Put("error", 1);
                    return error;
                }
                detalle["childrens"] = new JsonArray();
            }
            return detalle;
        }

        // CODIGO DE DESTINATARIO NUEVO
        public long get_codigo_nuevo_dest()
        {
            Parameters parameters = new Parameters();
            parameters.addOut("out_id_mensajero", -1, Parameters.TiposDatos.integer);
            cgdata.procedureDataSet("tracking.tracking_pkg.CODIGO_NUEVO_DESTINATARIO", parameters.toArray());
            return long.Parse(parameters.getByName("out_id_mensajero").Value.ToString());
        }

        //RETORNA UN OBJETO DE DESTINATARIO, RECIBIENDO COMO PARAMETRO EL ID UNICO DE LA TABLA
        public JsonObject getDestinatario(int id_dest)
        {
            return getDestinatario(id_dest, null, null);
        }

        // RETORNA UN OBJETO DE DESTINATARIO
        public JsonObject getDestinatario(int id_dest, string cod, string tipodest)
        {
            JsonObject dest = new JsonObject();
            Parameters parameters = new Parameters();
            // SI ID_DEST ES DIFERENTE DE -1, EL CUAL ES EL ID DE LA TABLA _
            // CONSULTA CON ESTE UNICO PARAMETRO Y IGNORA LOS DEMAS.
            if (id_dest != -1)
            {
                parameters.addIn(":id_dest", id_dest, Parameters.TiposDatos.integer);
                cgdata.query("select * from tracking.destinatarios where id = :id_dest", parameters.toArray());
            } // CONSULTA CON LOS PARAMETROS COD Y TIPODEST.
            else
            {
                parameters.addIn(":cod", cod, Parameters.TiposDatos.strings);
                parameters.addIn(":id_tipo", tipodest, Parameters.TiposDatos.strings);
                cgdata.query("select * from tracking.destinatarios where codigo = :cod and id_tipo = :id_tipo", parameters.toArray());
            }
            return dest;
        }

        // RETORNA UN DESTINATARIO DE UN PAQUETE, ( CREADO ESPECIALMENTE PARA LOS PAQUETES VIRTUALES )
        public JsonObject getDestinatarioByCod(string codigo, string nombre_tipo, int id_paquete)
        {

            Parameters parameters = new Parameters();
            parameters.addIn("in_codigo", codigo, Parameters.TiposDatos.integer);
            parameters.addIn("in_nombre_tipo", nombre_tipo, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_paquete", id_paquete, Parameters.TiposDatos.integer);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.CONS_DESTINATARIO_BY_CODIGO", parameters.toArray());
            return (JsonObject)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows[0]));
        }

        // RETORNA TODOS LOS DESTINATARIO DE UN PAQUETE, ( CREADO ESPECIALMENTE PARA LOS DOCUMENTOS )
        public JsonArray getDestinatariosPaquete(int id_paquete)
        {

            Parameters parameters = new Parameters();
            parameters.addIn("in_id_paquete", id_paquete, Parameters.TiposDatos.integer);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.CONS_DESTINATARIOS_PAQUETE", parameters.toArray());
            return (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));

        }

        // NOTA: SI NAME = TRUE, EL VALOR DEL PARAMNETRO IN_ID_SECTOR, ES EL DEL CODIGO DEL MENSAJERO
        public string getMensajero(long in_id_sector, bool name)
        {
            Parameters parameters = new Parameters();
            if (name == true)
            {
                // AQUI EL ID_SECTOR ES EL CODIGO DE MENSAJERO
                parameters.addIn("in_id_mensajero", in_id_sector, Parameters.TiposDatos.integer);
                parameters.add("out_nomb_mensajero", " ", ParameterDirection.Output, null, 1000, Parameters.TiposDatos.strings);
                cgdata.procedureDataSet("tracking.tracking_pkg.BUSCAR_MENSAJERO_BY_ID", parameters.toArray());
                return parameters.getByName("out_nomb_mensajero").Value.ToString();
            }
            else
            {
                parameters.addIn("in_id_sector", in_id_sector, Parameters.TiposDatos.integer);
                parameters.addOut("out_id_mensajero", -1, Parameters.TiposDatos.integer);
                parameters.add("out_nomb_mensajero", " ", ParameterDirection.Output, null, 1000, Parameters.TiposDatos.strings);
                cgdata.procedureDataSet("tracking.tracking_pkg.BUSCAR_MENSAJERO", parameters.toArray());
                return parameters.getByName("out_id_mensajero").Value.ToString();
            }

        }

        // EJECUTA PROCEDIMIENTO PROVINCIAS, CONSIGUE Y LUEGO RETORNA TODAS LAS PROVINCIAS

        public JsonArray getProvincias()
        {
            Parameters parameters = new Parameters();
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.provincias", parameters.toArray());
            JsonArray provincias = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            return provincias;
        }

        public JsonArray getCiudades(string codprovincia)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("codprovincia", codprovincia, Parameters.TiposDatos.strings);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.ciudades", parameters.toArray());
            JsonArray ciudades = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            return ciudades;
        }

        public JsonArray getSectores(string codciudad)
        {
            Parameters parameters = new Parameters();
            parameters.addIn("codprovincia", codciudad, Parameters.TiposDatos.strings);
            parameters.addOut("recordset", DBNull.Value, Parameters.TiposDatos.tabla);
            DataSet ds = cgdata.procedureDataSet("tracking.tracking_pkg.barrios", parameters.toArray());
            JsonArray sectores = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(ds.Tables[0].Rows));
            return sectores;
        }

        #endregion

        #region "INSERTS"

        //INSERTA UN NUEVO DESTINATARIO
        public int insertDestinatario(string cod, string name, string cod_tipo, string tipo_persona)
        {
            Parameters parameters = new Parameters();
            // out_id_dest = id generado cuando se halla creado o actualizado el destinatario
            parameters.addOut("out_id_dest", null, Parameters.TiposDatos.integer);
            parameters.addIn("in_nombre", name.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_codigo", cod.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_tipo", cod_tipo.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_tipo_persona", tipo_persona.Trim(), Parameters.TiposDatos.strings);
            // EJECUTA EL PROCEDIMIENTO PARA CREAR O ACTUALIZAR DESTINATARIO
            cgdata.procedureNoquery("tracking.tracking_pkg.insert_destinatario", parameters.toArray());
            // RETORNA EL ID CREADO O ACTUALIZADO
            return int.Parse(parameters.getByIndex(0).Value.ToString());
        }
        // INSERTA NUEVA DIRECCION
        public int insertDireccion(int id_dest, string ciudad, string provincia, string sector, string tipo, string calle, string telefono)
        {
            // PARAMETROS
            Parameters parameters = new Parameters();
            // INSERTA Y DEVUELVE EL ID DE LA DIRECCION
            parameters.addIn("in_id_dest", id_dest, Parameters.TiposDatos.integer);
            parameters.addIn("in_status", 1, Parameters.TiposDatos.integer);
            parameters.addIn("in_cod_ciudad", ciudad.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_provincia", provincia.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_sector", sector.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_tipo", tipo.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_calle", calle.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_telefono", telefono.Trim(), Parameters.TiposDatos.strings);
            parameters.addOut("out_id_dir", null, Parameters.TiposDatos.integer);
            cgdata.procedureNoquery("tracking.tracking_pkg.insert_direccion", parameters.toArray());
            // RETORNA EL VALOR EN EL PARAMETRO 8 ( ID_DIRECCION )
            return int.Parse(parameters.getByName("out_id_dir").Value.ToString());
        }

        // AGRUPA PAQUETES EN UN NUEVO BATCH 
        public JsonObject crearPaquete(string id_usr, string id_sub_area, string notes, JsonArray paquetes, Dictionary<string, string> documentos_area)
        {

            JsonObject result = new JsonObject();
            result["batch_cod"] = "-1";
            result["ERROR"] = "0";
            result["MENSAJE"] = "";

            // BUSCA LOS PAQUETES EN LA BASE DE DATOS
            JsonObject pos = new JsonObject();
            for (int i = 0; i < paquetes.Length; i++)
            {
                pos = (JsonObject)paquetes[i];
                pos = ((JsonObject)((JsonArray)getgetPkgFromOthers(id_usr, pos["CODIGO_ORDEN"].ToString()))[0]);
                paquetes[i] = pos;
            }

            // VERIFICA PAQUETES QUE SE DESEAN AGRUPAR
            JsonArray paquetes_ = new JsonArray();
            paquetes_.Add(verificarPaquete(paquetes, documentos_area, false));
            if (((JsonObject)paquetes_[0])["ERROR"].ToString() != "0")
            {
                result["ERROR"] = 1;
                result["MENSAJE"] = "No es posible agrupar documentos, porque uno de los paquetes o documentos pertenece a otra area. Actualice su bandeja.";
                return result;
            }

            // CREA PAQUETE NUEVO
            string name_batch = ConfigurationManager.AppSettings["BATCH_CONTENEDOR"];
            Dictionary<string, object> batch_cont = (Dictionary<string, object>)getAllBatch()[name_batch];
            // AUMENTA EL CONTADOR            
            batch_cont["CONTADOR"] = aumentarBatch(int.Parse(batch_cont["ID"].ToString()));
            string batch_cod = batch_cont["PREFIJO"].ToString() + batch_cont["CONTADOR"].ToString().PadLeft(LEN_COD_ORDEN, '0');
            //notes = "Agrupados en el batch " + batch_cod;
            Parameters parameters;
            JsonObject paquete;
            for (int i = 0; i < paquetes.Length; i++)
            {
                paquete = (JsonObject)paquetes[i];
                parameters = new Parameters();
                parameters.addInOut("in_cod_orden", paquete["CODIGO_ORDEN"].ToString(), Parameters.TiposDatos.strings);
                parameters.addIn("in_batch", batch_cod, Parameters.TiposDatos.strings);
                parameters.addIn("in_id_usr", id_usr, Parameters.TiposDatos.strings);
                parameters.addIn("in_id_sub_area", id_sub_area.Trim(), Parameters.TiposDatos.strings);
                parameters.addIn("in_notes", notes, Parameters.TiposDatos.strings);
                cgdata.procedureNoquery("tracking.tracking_pkg.AGRUPAR_DOCUMENTOS", parameters.toArray());
            }

            //notes = "Se creo agrupador de paquetes.";
            insertLog("-1", 1, id_usr, notes, batch_cod, id_sub_area);
            // RETORNA EL MISMO CODIGO ORDEN QUE SE PASO COMO PARAMETRO EN EL PROCEDIMIENTO DE LA BASE DE DATOS
            result["batch_cod"] = batch_cod;
            return result;

        }
        // SOBRECARGA INSERTPACKAGE SIN LOS PARAMETROS:  NO_CASO, NUM_CONTRATO, CAMPO1, CAMPO2
        public JsonObject insertPackage(string cod_orden, string cod_pkg, string numRef, string id_sub_area, string numDoc, int idTipoObj
           , string idSistOri, string codsucursal, int id_dir, int id_dest, string l_destinatarios, string id_user, string nota)
        {
            return insertPackage(cod_orden, cod_pkg, numRef, id_sub_area, numDoc, idTipoObj, idSistOri, codsucursal, id_dir, id_dest
                , l_destinatarios, id_user, nota, "", "", "", "");
        }


        // SOBRECARGA INSERTPACKAGE SIN LOS PARAMETROS:  NUM_CONTRATO, CAMPO1, CAMPO2
        public JsonObject insertPackage(string cod_orden, string cod_pkg, string numRef, string id_sub_area, string numDoc, int idTipoObj
           , string idSistOri, string codsucursal, int id_dir, int id_dest, string l_destinatarios, string id_user, string nota
           , string no_caso)
        {
            return insertPackage(cod_orden, cod_pkg, numRef, id_sub_area, numDoc, idTipoObj, idSistOri, codsucursal, id_dir, id_dest
                , l_destinatarios, id_user, nota, no_caso, "", "", "");
        }

        // SOBRECARGA INSERTPACKAGE SIN LOS PARAMETROS:  CAMPO1, CAMPO2
        public JsonObject insertPackage(string cod_orden, string cod_pkg, string numRef, string id_sub_area, string numDoc, int idTipoObj
            , string idSistOri, string codsucursal, int id_dir, int id_dest, string l_destinatarios, string id_user, string nota
            , string no_caso, string num_contrato)
        { 
            return insertPackage(cod_orden, cod_pkg, numRef, id_sub_area, numDoc, idTipoObj, idSistOri, codsucursal, id_dir, id_dest
                , l_destinatarios, id_user, nota, no_caso, num_contrato, "", "");
        }

        // INSERTA PAQUETE EN LA BD, Y DEVUELVE EL ID DEL PAQUETE
        public JsonObject insertPackage(string cod_orden, string cod_pkg, string numRef, string id_sub_area, string numDoc, int idTipoObj
            ,string idSistOri, string codsucursal, int id_dir, int id_dest,string l_destinatarios , string id_user, string nota
            ,string no_caso, string num_contrato, string campo1, string campo2)
        {

            Parameters parameters = new Parameters();

            parameters.addOut("out_id_pkg", null, Parameters.TiposDatos.integer);
            parameters.add("out_cod_orden", cod_orden, ParameterDirection.InputOutput, null, 2000, Parameters.TiposDatos.strings);
            parameters.add("cod_pkg", cod_pkg, ParameterDirection.InputOutput, null, 2000, Parameters.TiposDatos.strings);
            parameters.addIn("in_num_ref", numRef.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_id_sub_area", id_sub_area.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_id_obj", numDoc.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_id_tipo_obj", idTipoObj, Parameters.TiposDatos.integer);
            parameters.addIn("in_id_sist_ori", idSistOri, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_sucursal", codsucursal, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_dir", id_dir, Parameters.TiposDatos.integer);
            parameters.addIn("in_id_dest", id_dest, Parameters.TiposDatos.integer);
            parameters.addIn("in_dest_list", l_destinatarios, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_usr", id_user, Parameters.TiposDatos.strings);
            parameters.addIn("in_notes", nota, Parameters.TiposDatos.strings);
            parameters.addIn("in_no_caso", no_caso, Parameters.TiposDatos.strings);
            parameters.addIn("num_contrato", num_contrato, Parameters.TiposDatos.strings);
            parameters.addIn("campo1", campo1, Parameters.TiposDatos.strings);
            parameters.addIn("campo2", campo2, Parameters.TiposDatos.strings);
            cgdata.procedureNoquery("tracking.tracking_pkg.crear_paquete", parameters.toArray());
            JsonObject paq = new JsonObject();
            paq.Put("id_pkg", parameters.getByName("out_id_pkg").Value.ToString());
            paq.Put("cod_orden", parameters.getByName("out_cod_orden").Value.ToString());
            paq.Put("codigo", parameters.getByName("cod_pkg").Value.ToString());
            return paq;
        }

        // REGISTRA HISTORIAL PAQUETE
        public void insertLog(string id_pkg, int status, string id_user_, string nota, string cod_orden, string id_sub_area)
        {
            // data del paquete y data a agregar a log
            Parameters parameters = new Parameters();
            parameters.addIn("in_id_pkg", id_pkg, Parameters.TiposDatos.strings);
            parameters.addIn("in_status", status, Parameters.TiposDatos.integer);
            parameters.addIn("in_id_location", id_user_, Parameters.TiposDatos.strings);
            parameters.addIn("in_notes", nota, Parameters.TiposDatos.strings);
            parameters.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_sub_area", id_sub_area.Trim(), Parameters.TiposDatos.strings);
            parameters.addIn("in_fecha", Convert.ToDateTime("01/01/1900"), Parameters.TiposDatos.fecha);
            cgdata.procedureNoquery("tracking.tracking_pkg.insert_historial", parameters.toArray());
            //parameters.addIn("codigo", informacionDoc["numRefDoc"], Parameters.TiposDatos.strings);            
        }

        // INSERTA EN TABLA TEMPORAL DE REPORTE
        public string insert_tmp_report(JsonObject objeto, string id_report, string USUARIO)
        {
            Parameters parameters = new Parameters();
            parameters.addOut("OUT_ID_TMP_TBL", null, Parameters.TiposDatos.integer);
            parameters.addIn("IN_AREAS", int.Parse(objeto["AREAS"].ToString()), Parameters.TiposDatos.integer);
            parameters.addIn("IN_CODIGO", objeto["CODIGO"], Parameters.TiposDatos.strings);
            // SE COMPRIME EL CODIGO ORDEN            
            parameters.addIn("IN_CODIGO_ORDEN", ConverterToBase64.Compress(objeto["CODIGO_ORDEN"].ToString()), Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEPT_ORIGEN", objeto["DEPT_ORIGEN"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEPT_UBICACION", objeto["DEPT_ORIGEN"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_COD", objeto["DEST_COD"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_ID", objeto["DEST_ID"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_ID_TIPO", objeto["DEST_ID_TIPO"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_NAME", objeto["DEST_NAME"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_TIPO_COD", objeto["DEST_TIPO_COD"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_TIPO_NOM", objeto["DEST_TIPO_NOM"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_TIPO_PERSONA", objeto["DEST_TIPO_PERSONA"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DOCUMENTOS", objeto["DOCUMENTOS"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ENTREGADO", objeto["ENTREGADO"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_DIRECCION", objeto["ID_DIRECCION"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_LAST_DOCUMENTO", objeto["ID_LAST_DOCUMENTO"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_LOCATION", objeto["ID_LOCATION"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_OBJETO", objeto["ID_OBJETO"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_PAQUETE", objeto["ID"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_REPORTE", id_report, Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_SISTEMA_ORIGEN", objeto["ID_SISTEMA_ORIGEN"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_SUB_AREA", objeto["ID_SUB_AREA"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_SUCURSAL", objeto["ID_SUCURSAL"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_TIPO_OBJETO", objeto["ID_TIPO_OBJETO"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_NUM_REF", objeto["NUM_REF"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_STATUS", objeto["STATUS"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_STATUS_NOM", objeto["STATUS_NOM"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_TIPO_OBJETO", objeto["TIPO_OBJETO"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_USUARIO", USUARIO, Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_DIR_LAST_DOC", objeto["ID_DIR_LAST_DOC"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_ID_FINAL", objeto["DEST_ID_FINAL"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_COD_FINAL", objeto["DEST_COD_FINAL"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_NAME_FINAL", objeto["DEST_NAME_FINAL"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_ID_TIPO_FINAL", objeto["DEST_ID_TIPO_FINAL"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_TIPO_COD_FINAL", objeto["DEST_TIPO_COD_FINAL"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_TIPO_NOM_FINAL", objeto["DEST_TIPO_NOM_FINAL"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_DEST_TIPO_PERSONA_FINAL", objeto["DEST_TIPO_PERSONA_FINAL"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_ACUSE", objeto["ID_ACUSE"], Parameters.TiposDatos.strings);
            parameters.addIn("IN_ID_SUB_AREA_ORIGEN", objeto["ID_SUB_AREA_ORIGEN"], Parameters.TiposDatos.strings);
            cgdata.procedureNoquery("tracking.tracking_pkg.INSERT_REPORT_TBL_TMP", parameters.toArray());
            return parameters.getByName("OUT_ID_TMP_TBL").Value.ToString();

        }

        #endregion

        #region "UPDATES"

        // REGISTRA NUEVA NOTA A HISTORIAL PAQUETE
        public void newNote(JsonObject data, string id_user_)
        {
            // data del paquete
            Parameters parameters = new Parameters();
            JsonObject pkg = (JsonObject)data["package"];
            //parameters.addIn(":codigo_orden", pkg["CODIGO_ORDEN"], Parameters.TiposDatos.strings);
            // BUSCA EN LA BD LA NOTA ACTUAL

            //object id = cgdata.scalar("select id from tracking.historial where codigo_orden = :codigo_orden and activo = 1", parameters.toArray());
            object id = data["id_last_hist"];

            // CONSTRUYE LA NOTA NUEVA MAS LA ANADE ARRIBA DEL ANTERIOR
            parameters = new Parameters();
            parameters.addIn("in_id_pkg", pkg["ID"], Parameters.TiposDatos.integer);
            parameters.addIn("in_usr", id_user_, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_hist", id, Parameters.TiposDatos.integer);
            parameters.addIn("in_nota", data["note"], Parameters.TiposDatos.strings);
            parameters.addIn("in_nota_doc", data["note_doc"], Parameters.TiposDatos.integer);
            
            // INSERTA NUEVO REGISTRO NOTA
            cgdata.procedureNoquery("tracking.tracking_pkg.insert_note", parameters.toArray());
            //parameters.addIn("codigo", informacionDoc["numRefDoc"], Parameters.TiposDatos.strings);            
        }

        // CAMBIA LA DIRECCION DEL PAQUETE POR UNA NUEVA
        public void cambioDireccion(JsonObject data, string id_usr, string id_sub_area)
        {
            JsonObject direccion = (JsonObject)data["direccion"];
            JsonObject pkg = (JsonObject)data["pkg"];
            int id_dir = insertDireccion(int.Parse(pkg["DEST_ID"].ToString()), direccion["codciudad"].ToString(), direccion["codprovincia"].ToString(), direccion["codbarrio"].ToString(), direccion["codtipodireccion"].ToString(), direccion["direccion"].ToString(), direccion["telefono"].ToString());
            // ESTATUS 4 = CAMBIO DIRECCION
            updatePkg(int.Parse(pkg["ID"].ToString()), pkg["CODIGO_ORDEN"].ToString(), pkg["CODIGO"].ToString(), id_sub_area, id_dir, id_usr, int.Parse(pkg["ENTREGADO"].ToString()), 4, direccion["nota"].ToString());
        }

        public void updatePkg(int id_pkg, string codOrden, string cod, string id_sub_area, int id_dir, string id_usr, int entregado, int statusLog, string notes)
        {
            Parameters parameter = new Parameters();
            parameter.addIn("in_id_pkg", id_pkg, Parameters.TiposDatos.integer);
            parameter.addIn("in_cod_orden", codOrden, Parameters.TiposDatos.strings);
            parameter.addIn("in_id_sub_area", id_sub_area.Trim(), Parameters.TiposDatos.strings);
            parameter.addIn("in_id_dir", id_dir, Parameters.TiposDatos.integer);
            parameter.addIn("in_id_usr", id_usr, Parameters.TiposDatos.strings);
            parameter.addIn("in_status_log ", statusLog, Parameters.TiposDatos.integer);
            parameter.addIn("in_notes ", notes, Parameters.TiposDatos.strings);
            cgdata.procedureNoquery("tracking.tracking_pkg.mod_pkg", parameter.toArray());
        }

        // CAMBIA EL DESTINATARIO DEL PAQUETE POR UNA NUEVO
        public void cambioDestinatario(JsonObject data, int id_dest, string id_dest_list, int id_dir, string part_cod_orden, string notes, string id_usr, string id_sub_area)
        {

            Parameters parameters = new Parameters();
            parameters.addIn("in_id_pkg", data["ID"], Parameters.TiposDatos.integer);
            parameters.addIn("in_cod_orden", data["CODIGO_ORDEN"], Parameters.TiposDatos.strings);
            parameters.addIn("in_new_codigo_orden", part_cod_orden, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_sub_area", id_sub_area, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_dest", id_dest, Parameters.TiposDatos.integer);
            parameters.addIn("in_dest_list", id_dest_list, Parameters.TiposDatos.strings);
            parameters.addIn("in_id_dir", id_dir, Parameters.TiposDatos.integer);
            parameters.addIn("in_id_usr", id_usr, Parameters.TiposDatos.strings);
            parameters.addIn("in_status_log", 8, Parameters.TiposDatos.integer);
            parameters.addIn("in_notes", notes, Parameters.TiposDatos.strings);
            cgdata.procedureNoquery("tracking.tracking_pkg.CAMBIO_DESTINATARIO", parameters.toArray());
        }

        // ACTUALIZA UN PAQUETE EN LA BD Y LE ASIGNA UN NUEVO ESTATUS
        public void recibirPaquete(string cod_orden, string id_usr, string id_sub_area, int id_status, string notes)
        {
            Parameters parameter = new Parameters();
            parameter.addIn("in_cod_orden", cod_orden, Parameters.TiposDatos.strings);
            parameter.addIn("in_id_usr", id_usr, Parameters.TiposDatos.strings);
            parameter.addIn("in_status", id_status, Parameters.TiposDatos.integer);
            parameter.addIn("in_id_sub_area", id_sub_area.Trim(), Parameters.TiposDatos.strings);
            parameter.addIn("in_notes", notes, Parameters.TiposDatos.strings);
            cgdata.procedureNoquery("tracking.tracking_pkg.recibir_paquete", parameter.toArray());
        }

        // AUMENTA EL CONTADOR DE UN BATCH 
        public int aumentarBatch(int id_batch)
        {

            lock (_syncRoot)
            {

                Parameters parameters = new Parameters();
                parameters.addIn("in_id_batch", id_batch, Parameters.TiposDatos.integer);
                parameters.addInOut("in_out_counter", 0, Parameters.TiposDatos.integer);
                cgdata.procedureNoquery("tracking.tracking_pkg.batch_counter", parameters.toArray());
                //cache.Remove("batchs");
                return int.Parse(parameters.getByName("in_out_counter").Value.ToString());
            }
            // RETORNA EL CONTADOR CON EL NUEVO VALOR           
        }

        // CAMBIA EL STATUS DE UN BATCH
        public void cambiarStatusBatch(int id_batch)
        {
            lock (_syncRoot)
            {
                Parameters parameters = new Parameters();
                parameters.addIn("in_id_batch", id_batch, Parameters.TiposDatos.integer);
                cgdata.procedureNoquery("tracking.tracking_pkg.BATCH_STATUS", parameters.toArray());
                //cache.Remove("batchs");
            }
        }


        #endregion

        #region "DELETE"
        #endregion

        #endregion
    }
}