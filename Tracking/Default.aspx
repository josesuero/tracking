﻿<%@ Page Title="Tracking" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Tracking._Default" %>

<%@ Register TagPrefix="Formularios" TagName="Paquete" Src="Formularios/Paquete.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="NuevoDocumento" Src="Formularios/NuevoDocumento.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="AnadirComentario" Src="Formularios/AnadirComentario.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="MainDatosInfoAfiliado" Src="Formularios/InfoAfiliado.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="MainDatosCartera" Src="Formularios/DatosCartera.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="CrearPaquete" Src="Formularios/CrearPaquete.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="CambioDireccion" Src="Formularios/CambioDireccion.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="BusqAvanzada" Src="Formularios/BusqAvanzada.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="Print" Src="Formularios/Print.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="Batch" Src="Formularios/Batch.ascx" %>
<%@ Register TagPrefix="Formularios" TagName="CambioDestinatario" Src="Formularios/CambioDestinatario.ascx" %>

<%--<%@ Register TagPrefix="Formularios" TagName="MainPaquete" src="Formularios/MainPaquete.ascx" %>--%>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        // variable global, contiene todos los paquetes de la tabla principales, mas se hacen interaciones con esta.
        var paquetes = [];
        // VARIABLE PARA PAQUETES DE TABLA DE RECIBIR
        var otrosPaquetes = [];
        // VARIABLE DE CACHE QUE ES UTILIZADA PARA LOS AUTOCOMPLETE DE DESTINATARIOS
        var cache = {};

        $(document).ready(function () {

            // llenar dropdowns tipos de documentos
            var objs = $(".control.dTypesDoc");
            var pos = null;
            for (var i = 0; i < objs.length; i++) {
                objs.eq(i).append("<option value=" + -1 + ">Seleccionar</option>");
                for (var j = 0; j < dataTracking.typesDoc.length; j++) {
                    pos = dataTracking.typesDoc[j];
                    if (($.inArray(dataTracking.user.outsubareacodOut, pos.ID_SUB_AREA.split(",")) != -1 && pos.MANUAL == 1)
                    || (pos.ID_SUB_AREA == -1 && pos.MANUAL == 1)) {
                        objs.eq(i).append("<option value=" + pos.ID + ">" + pos.NOMBRE + "</option>");
                    }
                }
            }
            i = null; j = null;
            // CARGA LOS DATOS A CADA DROPDOWN DE ESTATUS DE TRACKING
            objs = $(".control#statuspackage");
            pos = null;
            for (var i = 0; i < objs.length; i++) {
                objs.eq(i).append("<option value=" + -1 + ">Seleccionar</option>");
                for (var j = 0; j < dataTracking.status.length; j++) {
                    pos = dataTracking.status[j];
                    if (pos.TIPO == 2) {
                        objs.eq(i).append("<option value=" + pos.ID + ">" + pos.NOMBRE + "</option>");
                    }
                }
            }
            i = null; j = null;
            pos = null;
            objs = null; pos = null; option = null;

            // BOTON DE CERRAR BATCH
            $("#btn_close_batch").click(function () {

                $("#Batch").dialog("open");
            });

            // VARIABLE GLOBAL QUE CONTIENE PAQUETES DE LA TABLA PRINCIPAL
            paquetes = dataTracking.packages;
            // width = 10 10 8 15 5 8 8 15 = 79%

            // COLUMNAS DE LA TABLA PRINCIPAL
            var columns = [
            {
                key: "CODIGO",
                label: "Cod. Paquete",
                width: "10%",
                events: []
            },{
                key: "FECHA_CREACION",
                label: "Fecha Creacion",
                width: "6%",
                events: []
            }, {
                key: "TIPO_OBJETO",
                label: "Tipo Documento",
                width: "10%",
                events: []
            }, {
                key: "ID_OBJETO",
                label: "No. Referencia",
                width: "8%",
                events: []
            }, {
                key: "DEPT_UBICACION",
                label: "Area",
                width: "15%",
                events: []
            },
            {
                key: "STATUS_NOM",
                label: "Estado",
                width: "5%",
                events: []
            },
            {
                key: "DEST_TIPO_NOM",
                label: "Tipo Destinatario",
                width: "8%",
                events: []
            },
            {
                key: "DEST_COD",
                label: "Cod. Destinatario",
                width: "8%",
                events: []
            },
            {
                key: "DEST_NAME",
                label: "Nombre Destinatario",
                width: "15%",
                events: []
            },
            {
                key: "DOCUMENTOS",
                label: "Cant. Documentos",
                width: "3%",
                events: []
            }
        ];

            var toolBox = [{
                type: "SEARCH",
                event: ""
            }, {
                type: "CREAR",
                event: ""
            }];

            // QUITA LA COLUMNA DE FECHA DE CREACION
            

            var mainTab = $("#tabsMain");
            var div = mainTab.find("#divPaqActuales");
            div.find("#tbPaquetes")
            .smartTable(
                { columns: columns,
                    toolBox: toolBox, height: 400,
                    print: false,
                    dataTable: false, searchAdvanced: true,
                    rowClick: function (evt, smartb) {
                        // CUANDO SE HACE CLICK SOBRE CADA FILA
                        // SI LA FILA DE LA TABLA SOBRE LA QUE SE HIZO CLICK NO ESTA SELECCIONADA, LA SELECCIONA
                        // , LIMPIA EL HISTORIAL Y CARGA LA DATA Y MUESTRA LA TABLA DE HISTORIAL
                        // Y EN CASO DE QUE LA FILA DE LA TABLA HUBIESE ESTADO SELECCIONADA
                        // QUITA LA SELECCION DE LA FILA Y OCULTA LA TABLA DE HISTORIAL
                        var div = $('#divPaqActuales');
                        cleanHistorial(div.find('#tbHistorial'));
                        var tb = div.find('#tbPaquetes');

                        if (smartb.row.hasClass('rowSelected')) {
                            div.find('#divHistorialPaqAct').show();
                            var data = tb.smartTable('getPackByIndex', smartb.row.attr('index'));
                            fillHistorial(div.find('#tbHistorial'), getHistorialPkg(data), smartb.row.attr('index'));
                            tb.smartTable('resize', 200);
                            data = null;
                        }
                        else {
                            tb.smartTable('resize', 400);
                            div.find('#divHistorialPaqAct').hide();
                        }
                        div = null; data = null; tb = null;
                    },
                    rowDblClick: function (evt, smartb) {

                        // OCULTA LA TABLA DE HISTORIAL Y CARGA EL DIALOG DE PAQUETE
                        $('#divPaqActuales').find('#divHistorialPaqAct').hide();
                        createDialogDetPaq(smartb.row);
                    }
                }
            );
            //SE ESCONDE ESTE DIV AL CARGAR LA PAGINA
            div.find('#divHistorialPaqAct').hide();

            mainTab.attr("refresh", "false").tabs({
                create: function (event, ui) {
                    // SE CARGA LA DATA A LA TABLA PRINCIPAL LUEGO DE QUE YA SE RENDERIZO EL CONTROL TAB DE JQUERY
                    $(this).find("#tbPaquetes").smartTable("fillTbody", paquetes);
                    $(this).find("#tbPaquetes").smartTable("addDataTable");
                },
                activate: function (event, ui) {
                    var table;
                    switch ($(this).tabs("option", "active")) {
                        case 0:
                            // ACTUALMENTE ESTE ATRIBUTO SOLO CAMBIA PARA CUANDO SE RECIBE UN PAQUETE
                            if ($(this).attr("refresh") == "true") {
                                // REFRESCA LA TABLA PRINCIPAL
                                loadPkgs();
                            }
                            break;
                        case 1:

                            break;
                    }
                }
            });
            div.find("#btnAddComment").click(function () {
                var div = $(this).parent().parent();
                anadirComentario(
                getPackByIndex(div.find("#tbHistorial").attr("pkgIndex")),
                 'divPaqActuales',
                 div.find("#tbHistorial").attr("ID_Lasthist")
                 );
                div = null;
            }).button({
                text: false,
                icons: {
                    primary: "ui-icon-plus"
                }
            });

            //$('#divRecibirPaq').find('#tbHistorial');

            // FORMULARIO PARA RECIBIR PAQUETE (divRecibirPaq)

            //$("#divRecibirPaq").find("#tbRecibirPaq").smartTable({ columns: columns, events: events });

            div = mainTab.find("#divRecibirPaq");

            // LLENA DROPDOWN DE SUPERVISADOR
            fillDropDownRecibir(-1);

            // TABLA DE RECIBIR PAQUETES  
            columns.splice(1, 1);          
            div.find("#tbRecPaq").smartTable({
                columns: columns, onlyView: true, deleteRow: true
                , height: 150, classMouseView: "mouseEnter"
                , rowDblClickView: function (evt, smartb) {
                    createDialogDetOtherPaq(smartb.row);
                }
            });

            // CAMPO DE CODIGO DE PAQUETE
            div.find("#CODIGO").keyup(function (e) {
                // VERIFICA QUE EL CAMPO ESTE CON DATOS Y SE HALLA PRESIONADO ENTER
                if (e.which == 13 && $(this).val() != "") {
                    var data = { CODIGO: $(this).val(), AGRUPAR: 0 };
                    if ($("#divRecibirPaq").find("#agruparPaquetes").prop("checked")) {
                        data["AGRUPAR"] = 1
                    }
                    var tbPaq = $("#divRecibirPaq").find("#tbRecPaq");
                    if (verificadorPaquetes(tbPaq.smartTable("option", "data"), data) == 0) {
                        tbPaq = null;
                        data = null;
                        return;
                    }
                    data = getPkgFromOthers(data);
                    if (data["ERROR"] == 1) {
                        messageBox(data["MENSAJE"]);
                        tbPaq = null;
                        data = null;
                        return;
                    }

                    // SI EL PAQUETE LUEGO DE SER VERIFICADO NO PRESENTA NINGUN ERROR LO AGREGA A LA TABLA 
                    // Y CAMBIA ESTATUS DE VARIABLE VERIFICADORA DE CAMBIOS
                    existenCambios = true;
                    tbPaq.smartTable("addRowToBody", data, true);
                    data = null;
                    tbPaq = null;
                    $(this).val("");
                }
            });

            // BOTON DE RECIBIR PAQUETE
            div.find("#btnRecibirPkg").click(function () {
                var form = $("#divRecibirPaq");
                // VERIFICA QUE ESTE SELECCIONADO UN ESTATUS Y QUE LA TABLA TENGA AL MENOS UN PAQUETE EL CUAL RECIBIR
                if (!form.find("#agruparPaquetes").prop("checked") && (form.find("#tbRecPaq").smartTable("option", "data").length <= 0 || form.find("#statuspackage").val() == -1)) {
                    form = null;
                    return;
                }

                if (form.find("#agruparPaquetes").prop("checked")) {
                    var data_ = {};
                    data_["childrens"] = form.find("#tbRecPaq").smartTable("option", "data");
                    data_["nota"] = form.find("#nota").val();
                    var result = ajaxrequest("crearPaquete", data_);
                    if (result) {
                        registroCompletado(form);
                    }
                    result = null; data_ = null;
                } else {
                    recibirPaquetes(form);
                }
                // RECIBE CADA PAQUETE EN LA TABLA CON EL ESTATUS SELECCIONADO Y LA NOTA ESCRITA

                form.find("#agruparPaquetes").prop("checked", false);
                form.find(".campos_recibir_paq").show();
                form = null;
            });

            div.find("#agruparPaquetes").change(function () {
                var form = $("#divRecibirPaq");
                if (form.find("#tbRecPaq").smartTable("option", "data").length > 0) {
                    if (confirm("Si cambia el tipo de acción seran descartados los paquetes. Esta seguro que desea hacerlo?")) {
                        comenzarNuevamente(form);
                        existenCambios = false;
                    }
                }
                if ($(this).prop("checked")) {
                    form.find(".campos_recibir_paq").hide();
                } else {
                    form.find(".campos_recibir_paq").show();
                }
                form = null;
            });
            // LIMPIA DATA DE PANTALLA DE RECIBIR, EN CASO DE NO HABER DATA NO HACE NADA
            div.find("#btnCancelar").click(function () {
                var form = $("#divRecibirPaq");
                if (form.find("#tbRecPaq").smartTable("option", "data").length <= 0) {
                    form = null;
                    return;
                }
                if (!confirm("Esta seguro que desea comenzar de nuevo?")) {
                    form = null;
                    return;
                }
                comenzarNuevamente(form);
                existenCambios = false;
                form = null;

            });

            // SE AGREGA EVENTO CHANGE A DROPDOWN DE ESTATUS DE TRACKING
            div.find("#statuspackage")
            //            .focus(function () {
            //                if ($(this).val() == 3 && $(this).val() != "") {
            //                    $(this).val($(this).val() + " ");
            //                }
            //            })
            .change(function () {
                // DEPENDIENDO DEL ESTATUS SELECCIONADO
                // CAMBIAN LOS USUARIOS CON LOS QUE SE REALIZAN TRANSACCIONES
                fillDropDownRecibir($(this).val());
                if ($(this).val() == 5) {
                    $("#superUser").val(dataTracking.user.id);
                } else if ($(this).val() == 3) {
                    var nota = $("#divRecibirPaq").find("#nota");
                    if (nota.val() == "") {
                        nota.val("Recibido por: .");
                        SetCaretAtEnd(nota[0], 13);
                        //nota.focus().val("").val("Recibido por: ");
                        //nota[0].value = nota[0].value.replace(/^\s*|\s*$/g, '');
                        //SetCaretAtEnd(nota[0]);
                    }
                    nota = null;
                }
            });
            // SE ELIMINA VARIABLES
            events = null; columns = null; div = null; mainTab = null; i = null; j = null;
            dataTracking.user.outpermisosOut = null;
            delete dataTracking.user.outpermisosOut;
        });
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div id="mainDefault">
        <div id="div_close_draft">
            <input type="button" id="btn_close_batch" class="modificacion" value="Cerrar Batch" />
        </div>
        <div id="tabsMain" class="ui-tabs">
            <ul>
                <li><a href="#divPaqActuales">Paquetes Actuales</a></li>
                <li><a href="#divRecibirPaq">Registro</a></li>
            </ul>
            <div id="divPaqActuales">
                <div id="tbPaquetes">
                    <div id="toolBox">
                    </div>
                    <hr />
                    <div id="smartTablePrint" class="alignRight">
                    </div>
                    <div id="contTbSmart">
                        <table style="width: 100%">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="contentDivHistorial">
                    <div id="divHistorialComment" class="hide">
                        <p>
                            <label>
                                Observaci&oacute;n</label>
                        </p>
                        <%--<textarea id="NOTES" rows="11" cols="48" readonly="readonly" class="textAreaJustify control allSpace"></textarea>--%>
                        <div id="divNotes">
                            <table id="tbNotes" class="control allWidth">
                                <thead class="ui-widget-header">
                                    <tr>
                                        <th>
                                            Observaci&oacute;n
                                        </th>
                                        <th>
                                            Usuario
                                        </th>
                                        <th>
                                            Fecha
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="divHistorialPaqAct">
                        <p>
                            <label>
                                Historial</label>
                            <button id="btnAddComment">
                                Añadir Observaci&oacute;n</button>
                        </p>
                        <div id="divTbHistorialPaqAct">
                            <table id="tbHistorial" class="control allWidth">
                                <thead class="ui-widget-header">
                                    <tr>
                                        <td>
                                            Estado
                                        </td>
                                        <td>
                                            Area
                                        </td>
                                        <td>
                                            Usuario
                                        </td>
                                        <td>
                                            Fecha
                                        </td>
                                        <td>
                                            Notas
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divRecibirPaq">
                <div id="divTbRecPaq">
                    <input type="button" id="btnCancelar" value="Comenzar de nuevo" />
                    <div id="tbRecPaq">
                        <div id="toolBox">
                        </div>
                        <hr />
                        <div id="smartTablePrint" class="alignRight">
                        </div>
                        <div id="contTbSmart">
                            <table>
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <%--<table id="copyTbSmart">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>--%>
                    </div>
                </div>
                <div id="dataRecPaq">
                    <table>
                        <tr>
                            <td>
                                <label for="agruparPaquetes">Agrupar paquetes</label>
                            </td>
                            <td>
                                <input type="checkbox" id="agruparPaquetes"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Codigo
                            </td>
                            <td>
                                <input type="text" class="control" id="CODIGO" />
                            </td>
                        </tr>
                        <tr class="campos_recibir_paq">
                            <td>
                                Usuario
                            </td>
                            <td>
                                <select id="superUser" class="control">
                                </select>
                            </td>
                        </tr>
                        <tr class="campos_recibir_paq">
                            <td>
                                Status
                            </td>
                            <td>
                                <select class="control" id="statuspackage">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span>Comentarios</span>
                                <br />
                                <textarea id="nota" cols="30" rows="8" class="textAreaJustify"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <input type="button" value="Aceptar" id="btnRecibirPkg" class="modificacion" />
            </div>
        </div>
    </div>
    <%--FORMULARIOS TRACKING--%>
    <Formularios:CrearPaquete ID="crearPaquete" runat="server" />
    <Formularios:AnadirComentario ID="anadirComentario" runat="server" />
    <Formularios:NuevoDocumento ID="nuevoDocumento" runat="server" />
    <Formularios:MainDatosInfoAfiliado ID="mainDatosInfoAfiliado" runat="server" />
    <Formularios:MainDatosCartera ID="mainDatosCartera" runat="server" />
    <Formularios:CambioDireccion ID="mainCambioDireccion" runat="server" />
    <Formularios:BusqAvanzada ID="BusqAvanzada" runat="server" />
    <Formularios:Print ID="Print" runat="server" />    
    <Formularios:Batch ID="Batch" runat="server" />
    <Formularios:CambioDestinatario ID="cambioDestinatario" runat="server" />
    <%--<Formularios:MainPaquete id="mainPaquete" runat="server" />--%>
    <div id="contentMainPaquete">
        <Formularios:Paquete ID="mainPaquete" runat="server" />
    </div>
    <div id="printPdfReport">
        <iframe id="iFramePdf"></iframe>
    </div>
    <%--<object id="pdfDiv" type="application/pdf" width="0" height="0"></object>--%>
    <%--------------------------------%>
</asp:Content>
