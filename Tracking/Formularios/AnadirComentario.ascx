﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnadirComentario.ascx.cs" Inherits="Tracking.Formularios.AñadirCommentario" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#anadirComentario");
        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 420,
            minHeight: 200,
            open: function () {
                var pkg = JSON.parse($(this).attr("pkg"));
                var divNoteDoc = $(this).find("#divNoteDoc");
                divNoteDoc.find("#noteDoc").prop("checked", true);
                if (pkg.FINAL != 1) {
                    divNoteDoc.hide();
                } else {
                    divNoteDoc.show();
                }
                cleanForm($(this));
                existenCambios = true;
                divNoteDoc = null;
                pkg = null;
            },
            buttons: {
                "Aceptar": function () {
                    // SE ENVIA EL PAQUETE DENTRO DE UN ARREGLO DE UNA POSICION POR QUE LA FUNCION FILLHISTORIAL LO ESPERA ASI
                    var pkg = JSON.parse($(this).attr("pkg"));
                    // EJECUTA LA FUNCION LO INSERT PARA INSERTA EN LA BD LA NUEVA NOTA
                    var note_doc = 0;
                    if ($(this).find("#noteDoc").prop("checked")) {
                        note_doc = 1;
                    }
                    logInsert(pkg, $(this).find("#nota").val(), $(this).attr("ID_Lasthist"), note_doc);
                    // CONSIGUE EL HISTORIAL DEL PAQUETE Y LO GUARDA EN LA MISMA VARIABLE DE PKG
                    pkg = getHistorialPkg(pkg);
                    var div = $('#' + $(this).attr('from'));
                    // CARGA EL HISTORIAL DEL PAQUETE A LA TABLA NUEVAMENTE
                    cleanHistorial(div.find('#tbHistorial'));
                    fillHistorial(div.find('#tbHistorial'), pkg);
                    div = null; pkg = null; note_doc = null;
                    $(this).dialog("close");
                    existenCambios = false;
                },
                "Cancel": function () {
                    $(this).dialog("close");
                    existenCambios = false;
                }
            }
        });
        lengthfield(divForm.find("#nota"), 4000);
        divForm = null;

    });
</script>
<div id='anadirComentario' title="Añadir Observaci&oacute;n">
    <p>
        <textarea id="nota" rows="5" cols="53" class="control" style="width:95%; height:95%;"></textarea> 
    </p>
    <p id="divNoteDoc">        
        <input type="checkbox" id="noteDoc" />
        <label for="noteDoc">Agregar solo al documento</label>
    </p>    
</div>