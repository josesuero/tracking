﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Batch.ascx.cs" Inherits="Tracking.Formularios.Batch" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#Batch");
        divForm.dialog({
            modal: true,
            autoOpen: false,
            width: 300,
            height: 200,
            open: function () {
                cleanForm($(this));
                existenCambios = true;
                $(this).find("#dropdown_batchs").val(-1);

                // CARGA BATCHS EN DROPDOWN DE CERRAR BATCH
                dataTracking.batch = ajaxrequest("getJsonBatch");
                var pos;
                var objs = $("#dropdown_batchs");
                objs.children().remove();
                objs.append("<option value=" + -1 + ">Seleccionar</option>");
                for (var j = 0; j < dataTracking.batch.length; j++) {
                    pos = dataTracking.batch[j];
                    if (pos.STATUS == 1) {
                        objs.append("<option value='" + pos.ID + "' data='" + JSON.stringify(pos) + "'>" + pos.NOMBRE + "</option>");
                    }
                }
                objs = null; pos = null;

            },
            buttons: {
                "Aceptar": function () {
                    if ($(this).find("#dropdown_batchs").val() == -1) {
                        $(this).dialog("close");
                        return;
                    }
                    var data = ajaxrequest("aumentarBatch", { ID: $(this).find("#dropdown_batchs").val() });
                    if (data != false) {
                        dataTracking.batch = data;
                        $(this).dialog("close");
                    }
                    data = null;
                },

                "Cancel": function () {

                    $(this).dialog("close");

                }
            }, close: function () {
                existenCambios = false;
            }
        });

        divForm.find("#dropdown_batchs").change(function () {
            var div = $("#Batch");
            if ($(this).val() == -1) {
                cleanForm($(this));
            } else {
                fillForm(JSON.parse($(this).find("option:selected").attr("data")), div);
            }
            div = null;
        });

        divForm = null;
    });
</script>
<div id='Batch' title="Cerrar Batch">
    <table>
        <tr>
            <td>
                Batch
            </td>
            <td>&nbsp;</td>
            <td>
                <select id="dropdown_batchs">
                </select>
            </td>
        </tr>
        <tr>
            <td>
                Prefijo
            </td>
            <td>&nbsp;</td>
            <td>
                <input type="text" class="control" id="PREFIJO"  readonly="readonly"/>
            </td>               
        </tr>
    </table>
</div>
