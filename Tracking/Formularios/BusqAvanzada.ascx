﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusqAvanzada.ascx.cs" Inherits="Tracking.Formularios.BusqAvanzada" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#divAdvSearch");
        divForm.dialog(
		    { autoOpen: false, width: 800, buttons: {
		        Buscar: function () {
		            var searchObj = {};
		            $(".advSearch").each(function () {
		                searchObj[$(this).attr("key")] = $(this).val();
		            });
		            searchObj = busquedadAvanzada(searchObj);
		            paquetes = searchObj;
		            var tb = $("#tabsMain").find("#divPaqActuales").find("#tbPaquetes");
		            tb.smartTable("reCreateTbl");
		            tb.smartTable("fillTbody", searchObj);
		            tb.smartTable("addDataTable");
		            $('#divPaqActuales').find('#divHistorialPaqAct').hide();
		            searchObj = null; tb = null;
		        }
		    }
		    });
        divForm.find("#FECHA_DESDE").datepicker({ dateFormat: 'dd/mm/yy' });
        divForm.find("#FECHA_HASTA").datepicker({ dateFormat: 'dd/mm/yy' });

        var objs = divForm.find("#ID_TIPO_OBJETO");
        var pos = null;
        for (var i = 0; i < objs.length; i++) {
            for (var j = 0; j < dataTracking.typesDoc.length; j++) {
                pos = dataTracking.typesDoc[j];
                objs.eq(i).append("<option value=" + pos.ID + ">" + pos.NOMBRE + "</option>");
            }
        }

        objs = divForm.find("#ID_STATUS");
        pos = null; i = null; j = null;

        for (var i = 0; i < objs.length; i++) {
            //objs.eq(i).append("<option value=" + -1 + ">Seleccionar</option>");
            for (var j = 0; j < dataTracking.status.length; j++) {
                pos = dataTracking.status[j];
                objs.eq(i).append("<option value=" + pos.ID + ">" + pos.NOMBRE + "</option>");
            }
        }
        i = null; j = null;
        objs = divForm.find("#ID_SUB_AREA");
        pos = null;
        for (var i = 0; i < objs.length; i++) {

            for (var j = 0; j < dataTracking.areas.length; j++) {
                pos = dataTracking.areas[j];
                objs.eq(i).append("<option value=" + pos.ACCARESUBCOD + ">" + pos.ACCARENOM + " - " + pos.ACCARESUBNOM + "</option>");
            }
        }
        i = null; j = null;
        objs = divForm.find("#ID_SUCURSAL");
        pos = null;
        for (var i = 0; i < objs.length; i++) {

            for (var j = 0; j < dataTracking.sucursales.length; j++) {
                pos = dataTracking.sucursales[j];
                objs.eq(i).append("<option value=" + pos.AFISUCCOD + ">" + pos.AFISUCNOM + "</option>");
            }
        }
        i = null; j = null;
        objs = divForm.find("#DEST_TIPO_COD");
        pos = null;
        for (var i = 0; i < objs.length; i++) {

            for (var j = 0; j < dataTracking.tipos_destinatarios.length; j++) {
                pos = dataTracking.tipos_destinatarios[j];
                objs.eq(i).append("<option value=" + pos.CODIGO + ">" + pos.NOMBRE + "</option>");
            }
        }
        i = null;
        pos = null;
        objs = null;
        divForm = null;
        dataTracking.sucursales = null;
        delete dataTracking.sucursales;
        dataTracking.tipos_destinatarios = null;
        delete dataTracking.tipos_destinatarios;
        dataTracking.status = null;
        delete dataTracking.status;
        dataTracking.typesDoc = null;
        delete dataTracking.typesDoc;
        dataTracking.areas = null;
        delete dataTracking.areas;
    });
</script>

<div id="divAdvSearch" title="Busqueda Avanzada">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Tipo Documento</td>
			<td>
				<select id="ID_TIPO_OBJETO"  key="p.ID_TIPO_OBJETO" class="advSearch">
					<option value="">Todos</option>
				</select>
			</td>
			<td style="width:20px;">&nbsp;</td>
			<td>Estado</td>
			<td>
				<select id="ID_STATUS" key="h.STATUS" class="advSearch">
					<option value="">Todos</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Tipo Destinatario</td>
			<td>
				<select id="DEST_TIPO_COD" key="td.CODIGO" class="advSearch">
					<option value="">Todos</option> 
				</select>
			</td>
			<td>&nbsp;</td>
			<td>Codigo Destinatario</td>
			<td>
				<input type="text" id="CODIGO_DESTINATARIO" key="d.CODIGO"  class="advSearch" />
			</td>
		</tr>
		<tr>
			<td>Fecha Paquete Desde</td>
			<td>
				<input type="text" id="FECHA_DESDE" key="FECHA_DESDE" class="advSearch">
			</td>
			<td>&nbsp;</td>
			<td>Fecha Paquete Hasta</td>
			<td>
				<input type="text" id="FECHA_HASTA" key="FECHA_HASTA" class="advSearch" />
			</td>
		</tr>
		<tr>
			<td>Area</td>
			<td>
				<select id="ID_SUB_AREA" key="p.ID_SUB_AREA" class="advSearch">
					<option value="">Todos</option>
				</select>
			</td>
			<td>&nbsp;</td>
			<td>Usuario</td>
			<td>
				<input type="text" id="ID_USUARIO"  key="p.ID_LOCATION" class="advSearch" />
			</td>
		</tr>
		<tr>
			<td>Sucursal</td>
			<td>
				<select id="ID_SUCURSAL" key="p.ID_SUCURSAL" class="advSearch">
					<option value="">Todos</option>
				</select>
			</td>
            <td>&nbsp;</td>
            <td>Numero de Contrato</td>
			<td>
				<input type="text" id="NUM_CONTRATO" key="p.NUM_CONTRATO"  class="advSearch" />
			</td>
		</tr>
        <tr>
            <td>Numero de Caso</td>
            <td>
				<input type="text" id="NO_CASO" key="p.NO_CASO"  class="advSearch" />
			</td>
        </tr>
	</table>
</div>

