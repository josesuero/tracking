﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CambioDestinatario.ascx.cs" Inherits="Tracking.Formularios.CambioDestinatario" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#cambioDestinatario");
        // CONFIGURACION DE DIALOGO
        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 700,
            minHeight: 455,
            open: function () {
                existenCambios = true;
                cleanForm($(this));
                $(this).attr("receiver", "");
                $(this).removeAttr("destinatario_via");
                $(this).attr("direction", "");
                var data = JSON.parse($(this).attr("data"));
                $(this).find("#nombre").val(data.DEST_NAME);
                $(this).find("#doc_indirecto").prop("checked", true);
                fillForm(data, $(this));
                fillForm(data["direccion"], $(this));
                data = null;
            },
            close: function () {
                existenCambios = false;
            },
            buttons: {
                "Aceptar": function () {

                    if ($(this).attr("receiver") == "") {
                        messageBox("Debe escoger un destinatario.");
                        return;
                    }

                    if ($(this).attr("direction") == "") {
                        messageBox("No existe una direccion seleccionada para este documento.");
                        return;
                    }

                    var data = JSON.parse($(this).attr("data"));
                    var index = $("#cambioDestinatario").attr("divINDEX");
                    var idfrom = $("#cambioDestinatario").attr("divIdFrom");
                    var divPaq = $("#" + idfrom);

                    var result = cambiarDestinatario($(this), data);
                    if (result["ERROR"] == 0) {

                        $(".mainPaqueteOpen").each(function () {
                            if ($(this).prop("id") != idfrom) {
                                $(this).find("#tbDetPaquetes").smartTable("destroy");
                                $(this).find("#tbDetPaquetes").remove();
                                $(this).dialog("destroy");
                                $(this).remove();
                            }
                        });


                        data = getFromWsPkg(result["CODIGO_ORDEN"], 0);
                        var detalle = getDetPkg(data);
                        data.DEST_ID = detalle["dest_id"];
                        data.DEST_ID_TIPO = detalle["dest_id_tipo"];
                        data.childrens = detalle["childrens"];
                        data.direccion = detalle["direccion"];

                        var data_ = getPackByIndex(index);
                        for (var x in data) {
                            data_[x] = data[x];
                        }
                        divPaq.attr("CODIGO_ORDEN", data["CODIGO_ORDEN"]);

                        //createTbDetPaqSmartTable($(this), index, , data["CODIGO_ORDEN"], data["ID_SUB_AREA"]);

                        divPaq.find("#tbDetPaquetes").smartTable("option", "codigo", data["CODIGO_ORDEN"]);
                        divPaq.find("#tbDetPaquetes").smartTable("fillTbody", data.childrens);
                        buildOpenMainPaquete(divPaq, data, detalle, index, false);

                        cleanHistorial(divPaq.find('#tbHistorial'));
                        fillHistorial(divPaq.find('#tbHistorial'), getHistorialPkg(data), divPaq.prop("id"));


                        data_ = null;
                        detalle = null;

                        $(this).dialog("close");

                    } else {
                        messageBox(result["MESSAGE"]);
                    }
                    result = null; data = null; divPaq = null; idfrom = null; index = null;
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

        divForm.find("#searchIdnuevDoc").bind("keydown", function (e) {
            var keyCode = e.keyCode || e.which;

            if (keyCode == 9 && $(this).val().indexOf("  ") == -1) {
                e.preventDefault();

                $(this).val($(this).val() + "  ");
                $(this).trigger("keydown");
            } else if (keyCode == 9 && $(this).val().indexOf("  ") != -1) {
                e.isDefaultPrevented();
            }
            keyCode = null;
        });


        // AUTOCOMPLETE
        divForm.find("#searchIdnuevDoc").autocomplete({
            minLength: 3,
            delay: 500,
            source: searchID,
            //            source: function (request, response) {
            //                var term = request.term;
            //                response(searchReceiver(term));
            //                return;
            //            },
            focus: function (event, ui) {
                return false;
            },
            select: function (event, ui) {
                var div = $("#cambioDestinatario");
                div.removeAttr("destinatario_via");
                var value = $.trim($(this).val());

                if (ui.item.nuevocte) {
                    // NUEVO DESTINATARIO
                    ui.item = nuevo_destinatario();
                } else if (ui.item.desc == "PBS" || ui.item.desc == "MPP") {
                    value = ui.item.value;
                    ui.item = WebSvcTrackingSoapHttpPort_prcBusquedainf(value, "CRT");
                    if (ui.item.outinformantesOut.length > 0) {
                        ui.item = ui.item.outinformantesOut[0];
                    } else {
                        return;
                    }

                }


                if (ui.item.codtipoinformante && ($.trim(ui.item.codtipoinformante).toUpperCase() == "PR" || $.trim(ui.item.codtipoinformante).toUpperCase() == "IN")) {
                    ui.item.consecutivo = ui.item.codigo;
                } else if (
                    (value.substring(0, 2) == "03" || value.substring(0, 2) == "04") && ((value.length == 8 || value.length == 14 || value.indexOf('-') != -1))
                ) {
                    // CONSULTA DE SERVICIO SI ES INDIRECTO = 1
                    var destinatario;
                    if (div.find(".tipo_envio:checked").attr("via") == 1) {
                        destinatario = WebSvcTrackingSoapHttpPort_prcBusquedainf(value, "INT").outinformantesOut;
                        if (destinatario.length > 0) {
                            destinatario = destinatario[0];
                            if (destinatario.codtipoinformante && ($.trim(destinatario.codtipoinformante).toUpperCase() == "PR" || $.trim(destinatario.codtipoinformante).toUpperCase() == "IN")) {
                                destinatario.consecutivo = destinatario.codigo;
                            }
                            div.attr("destinatario_via",
                                JSON.stringify(destinatario)
                            );
                        }
                    }
                    destinatario = null;
                }
                $(this).val(ui.item.nombre + "(" + $.trim(ui.item.identificacion) + ")");
                div.find(".control#nombre").val(ui.item.nombre);
                div.attr("receiver", JSON.stringify(ui.item));
                cambioDireccionPaq("cambioDestinatario", ui.item.consecutivo, ui.item.codtipoinformante, ui.item.tipopersona, 1);

                div = null; value = null;
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            if (item.desc && (item.desc == "PBS") || item.desc == "MPP") {
                return $("<li></li>")
				    .data("item.autocomplete", item)
				    .append("<a>" + item.label + " (" + item.desc + ")" + "</a>")
				    .appendTo(ul);
            } else if (item.nuevocte) {
                return $("<li>").append("<a><hr><br>Nuevo Destinatario</a>").appendTo(ul).css("font-size", "8pt");
            } else {
                return $("<li>")
                .append("<a>" + item.nombre + "(" + item.tipoinformante + ")<br>" + $.trim(item.tipoid) + " - " + $.trim(item.identificacion) + "</a>")
                .appendTo(ul).css("font-size", "8pt");
            }
        };
        divForm.find("#btnDirecciones").click(function () {
            var receiver = $("#cambioDestinatario").attr("receiver");
            if (receiver) {
                receiver = JSON.parse(receiver);
                $("#mainCambioDireccion").attr("openFrom", "cambioDestinatario").attr("directions", JSON.stringify(searchDirections(receiver))).dialog("open");
            }
        });
        lengthfield(divForm.find("#NOTES"), 4000);
        divForm = null;
    });    
</script>
<div id='cambioDestinatario' title="Cambiar Destinatario">
    <p>
        <label>
            Destinatario: 
        </label>
        <label for="doc_directo">
            Directo
        </label>        
        <input id="doc_directo" class="control tipo_envio" via="0" type="radio" name="tipo_envio"/>
         | 
         <label for="doc_indirecto">
            Indirecto
        </label>
         <input id="doc_indirecto" class="control tipo_envio" via="1" type="radio" name="tipo_envio"/>
    </p>
    <hr />
    <p>
        <label>
            Buscar
        </label>
        <input id="searchIdnuevDoc" class="control" type="text" />
    </p>
    <hr />
    <div id="datosPrincipales">
        <p>
            <label>
                Tipo de Documento
            </label>
            <select id="ID_TIPO_OBJETO" class="control dTypesDoc" disabled="disabled">
            </select>
        </p>        
        <p>
            <label>
                N&uacute;mero de Referencia
            </label>
            <input type="text" class="control" id="NUM_REF" readonly="readonly" />
        </p>
        <p>
            <label>
                N&uacute;mero de Documento
            </label>
            <input type="text" class="control" id="ID_OBJETO" readonly="readonly" />
        </p>
        <p>
            <label>
                Nombre de la Persona
            </label>
            <input type="text" class="control" readonly="readonly" id="nombre"/>
        </p>
        <p>
            <label>
                Sucursal
            </label>
            <input type="text" class="control" readonly="readonly" id="sucursal"/>
        </p>
    </div>    
    <h3>
        Datos de direcci&oacute;n
        <button id="btnDirecciones">Direcciones</button>
    </h3>
    <div id="datosDireccion">
        <div class="labelDivFloat">
            <p><span>Provincia</span></p>
            <p><span>Ciudad</span></p>
                        
        </div>
        <div class="camposDivFloat">
            <p><input id="provincia" type="text" class="control" readonly="readonly" /></p>
            <p><input id="ciudad" type="text" class="control" readonly="readonly" /></p>  
                                
        </div>
        <div class="labelDivFloat">  
            <p><span>Sector</span></p>          
            <p><span>Calle</span></p>              
        </div>
        <div class="camposDivFloat">             
            <p><input id="barrio" type="text" class="control" readonly="readonly" /></p> 
            <p><input id="direccion" type="text" class="control" readonly="readonly" /></p>
        </div>
    </div>
    <br />
    <div id="divObservacion">
        <h3>
            Observaci&oacute;n 
        </h3>
        <textarea id="NOTES" rows="2" cols="77" class="control"></textarea> 
            
    </div>
     
</div>