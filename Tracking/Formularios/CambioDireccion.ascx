﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CambioDireccion.ascx.cs" Inherits="Tracking.Formularios.CambioDireccion" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#mainCambioDireccion");
        // DIALOGO
        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 680,
            width: 700,
            height: 425,
            minHeight: 420,
            close: function () {
                $(this).find("#btnMoreDirecciones").removeAttr("loaded");
                existenCambios = false;
            },
            open: function () {
                if (!dataTracking.provincias) {
                    dataTracking.provincias = ajaxrequest("getProvincias");
                }

                $(this).find("#envNuevaDir").prop("checked", false);
                cleanForm($(this));
                switch ($(this).attr("openFrom")) {
                    case 'crearPaquete':
                    case 'nuevoDocumento':
                        $(this).find("#divObservacion").hide();
                        break;
                }
                wizardBackDir();
                var data = searchDirections($(this).attr("codigo"), $(this).attr("codtipo"), $(this).attr("tipopersona"), "tracking");
                $(this).attr("direction", JSON.stringify(data));
                $(this).find("#nuevDirChk").prop('checked', false);

                $(this).find("#tbMasDirecciones").smartTable("reCreateTbl");
                $(this).find("#tbDirecciones").smartTable("reCreateTbl");
                $(this).find("#tbDirecciones").smartTable("fillTbody", data);
                $(this).find("#tbDirecciones").smartTable("addDataTable");

                $(this).find("#datosDireccion").find(".control").each(function () {
                    if (!$(this).prop("disabled")) {
                        $(this).attr("disabled", "disabled");
                    } else {
                        return;
                    }
                });
                if ($(this).attr("cambiar_direccion") == 1) {
                    existenCambios = true;
                }

                data = null;
            }

            //            ,buttons: {
            //                "Aceptar": function () {

            //                },
            //                "Cancel": function () {
            //                    $(this).dialog("close");
            //                    existenCambios = false;
            //                }
            //            }
        });

        var columns = [
                {
                    key: "direccion",
                    label: "Calle",
                    width: "12%",
                    events: []
                },
                {
                    key: "barrio",
                    label: "Sector",
                    width: "13%",
                    events: []
                },
                {
                    key: "provincia",
                    label: "Provincia",
                    width: "11%",
                    events: []
                },
                {
                    key: "ciudad",
                    label: "Ciudad",
                    width: "11%",
                    events: []
                }
            ];

        // CREANDO INSTANCIA DE SMART TABLE PARA LA TABLA DE "tbDirecciones"
        divForm.find("#tbDirecciones")
        .smartTable({
            columns: columns, index: "-2", object: 1
            , height: 150, dataTable: false
            , rowClick: function (evt, smartb) {
                var div = $('#mainCambioDireccion').find('#datosDireccion');
                div.find('.control').each(function () {
                    $(this).attr('disabled', true);
                });
                div.find('#nuevDirChk');
                if ($(this).attr('evt') == 'false') {
                    $(this).attr('evt', 'true');
                    return;
                }
                $(this).parent().find('input:radio').each(function () {
                    $(this).prop('checked', false);
                    $(this).parent().parent().removeClass('rowSelected');
                });
                $(this).addClass('rowSelected');
                $(this).find('input:radio').prop('checked', true);
            }
        });

        // CREANDO INSTANCIA DE SMART TABLE PARA LA TABLA DE "TBMASDIRECCIONES"
        divForm.find("#tbMasDirecciones").smartTable({
            columns: columns, index: "-2", object: 1
            , height: 150, dataTable: false
            , rowClick: function (evt, smartb) {
                smartb.row.parent().find('input:radio').each(function () {
                    $(this).prop('checked', false);
                });
                smartb.row.find('input:radio').prop('checked', true);
            }
        });

        divForm.find("#btnAceptar").click(function () {
            if ($("#mainCambioDireccion").attr("cambiar_direccion") == 0) {
                $("#mainCambioDireccion").dialog("close");
                return;
            }
            var data = chosenDirection($("#mainCambioDireccion"));
            if (data == null) {
                $("#mainCambioDireccion").dialog("close");
                existenCambios = false;
                return;
            } else if (data.error) {
                messageBox(data.message);
                return;
            }
            var div;
            switch ($("#mainCambioDireccion").attr("openFrom")) {
                case 'crearPaquete':
                    div = $("#crearPaquete");
                    div.attr("direction", JSON.stringify(data));
                    fillForm(data, div);
                    break;
                case 'cambioDestinatario':
                    div = $("#cambioDestinatario");
                    div.attr("direction", JSON.stringify(data));
                    fillForm(data, div);
                    var arr = [];
                    // SERVICIO DE BOLIVAR
                    if (data["send_dir"] == 1) {

                        arr[arr.length] = data["direccion_obj"];

                        WebSvcTrackingSoapHttpPort_prcActdireccion(
                            dataTracking.user.id, // string inCodigousuario
                            " ", // string inAppkey
                            " ", // string inIdioma
                            $("#mainCambioDireccion").attr("codigo"), // decimal inInumide
                            $("#mainCambioDireccion").attr("tipopersona"), // string inStrtipper
                            arr// data["direccion_obj"] // DireccionesObjUser[] inDirecciones
                        );
                        arr = null;
                    }
                    break;
                case 'nuevoDocumento':
                    div = $("#nuevoDocumento");
                    div.attr("direction", JSON.stringify(data));
                    fillForm(data, div);
                    // SERVICIO DE BOLIVAR
                    var arr = [];
                    if (data["send_dir"] == 1) {

                        arr[arr.length] = data["direccion_obj"];

                        WebSvcTrackingSoapHttpPort_prcActdireccion(
                            dataTracking.user.id, // string inCodigousuario
                            " ", // string inAppkey
                            " ", // string inIdioma
                            $("#mainCambioDireccion").attr("codigo"), // decimal inInumide
                            $("#mainCambioDireccion").attr("tipopersona"), // string inStrtipper
                            arr//data["direccion_obj"] // DireccionesObjUser[] inDirecciones
                        );
                        arr = null;
                    }
                    break;
                case 'tbSmart':
                    // EL JSON DEL PKG ESTA EN EL ATRIBUTO JSON DATA CUANDO VIENE PROVIENE DESDE UNA TBSMART
                    var pkg = JSON.parse($("#mainCambioDireccion").attr("jsonData"));
                    if (pkg["FINAL"] == 0) {
                        $("#mainCambioDireccion").dialog("close");
                        pkg = null;
                        existenCambios = false;
                        return;
                    }
                    ajaxrequest("cambioDireccion", { direccion: data, pkg: pkg });
                    pkg = null;
                    break;
                case 'mainPaquete':
                    div = $("#" + $("#mainCambioDireccion").attr("divIdFrom"));
                    var index = div.attr("INDEX");
                    var pkg = getPackByIndex(index);
                    //pkg.childrens = detalle["childrens"];
                    //pkg.direccion = detalle["direccion"];

                    var arr = [];

                    // SERVICIO DE BOLIVAR

                    if (data["send_dir"] == 1) {
                        arr[arr.length] = data["direccion_obj"];

                        WebSvcTrackingSoapHttpPort_prcActdireccion(
                            dataTracking.user.id, // string inCodigousuario
                            " ", // string inAppkey 
                            " ", // string inIdioma
                            pkg["DEST_COD"], // decimal inInumide
                            pkg["DEST_TIPO_PERSONA"], // string inStrtipper
                            arr // DireccionesObjUser[] inDirecciones
                        );
                        arr = null;
                    }

                    ajaxrequest("cambioDireccion", { direccion: data, pkg: pkg });
                    fillForm(data, div);
                    cleanHistorial(div.find('#tbHistorial'));
                    var historial = getHistorialPkg(pkg);
                    if (div.find("#tabs").tabs("option", "active") == 0) {
                        fillHistorial(div.find('#tbHistorial'), historial, div.prop("id"));
                    } else {
                        div.find("#tabs").attr("tbexist", "false").attr("historial", JSON.stringify(historial));
                    }
                    pkg = null; index = null; historial = null;
                    break;
            }
            $("#mainCambioDireccion").dialog("close");
            existenCambios = false; div = null; data = null;
        });

        divForm.find("#btnCancelar").click(function () {
            $("#mainCambioDireccion").dialog("close");
            existenCambios = false;
        });

        divForm.find("#btnBack").click(function () {
            var div = $("#mainCambioDireccion");
            if (div.find("#divNueDir").css("display") == "none") {
                wizardBackDir();
            } else {
                wizardMoreDir();
            }
            div = null;
        });

        divForm.find("#btnNew").click(function () {
            wizardNew();
        });
        divForm.find("#btnMoreDirecciones").click(function () {
            wizardMoreDir();
            if ($("#mainCambioDireccion").attr("cambiar_direccion") == 0) {
                return;
            }
            if ($(this).attr("loaded")) {
                return;
            }
            $(this).attr("loaded", true);
            var div = $("#mainCambioDireccion");
            var data = searchDirections(div.attr("codigo"), div.attr("codtipo"), div.attr("tipopersona"), "ws");
            // SERVICIO DEVUELVE CODTIPODIRECCION = " ", CUANDO NO ENCUENTRA NINGUNA DIRECCION
            if (data.length == 0 || data[0].codtipodireccion == " ") {
                data = null;
                div = null;
                return;
            }
            data = moreDirections(JSON.parse(div.attr("direction")), data);

            div.find("#tbMasDirecciones").smartTable("reCreateTbl");
            div.find("#tbMasDirecciones").smartTable("fillTbody", data);
            div.find("#tbMasDirecciones").smartTable("addDataTable");

            data = null;
            div = null;
        });

        divForm.find("#nuevDirChk").change(function () {
            if ($(this).prop('checked')) {
                return;
            }
            var div = $("#mainCambioDireccion").find("#datosDireccion");
            div.find(".control").each(function () {
                $(this).attr("disabled", true);
            });
            div = null;
        });
        divForm.find("#nuevDirChk").click(function () {
            var div = $("#mainCambioDireccion").find("#datosDireccion");
            if ($(this).prop('checked')) {
                $("#mainCambioDireccion").find("#tbDirecciones").smartTable("unSelectAllRows");
                $("#mainCambioDireccion").find("#tbMasDirecciones").smartTable("unSelectAllRows");
                //$("#mainCambioDireccion").find("#tbDirecciones").hide("fold", { horizFirst: true }, 2000);
                div.find(".control").each(function () {
                    $(this).removeAttr("disabled");
                });
            } else {

                //$("#mainCambioDireccion").find("#tbDirecciones").show("fold", { horizFirst: true }, 2000);
                div.find(".control").find("#tbDirecciones").each(function () {
                    $(this).attr("disabled", "disabled");
                });
            }
            div = null;
        });

        lengthfield(divForm.find("#nota"), 4000);

        var obj = divForm.find("#divNueDir").find("#provincia");
        var pos = null;
        var option = null;
        obj.append("<option value=" + -1 + ">Seleccionar</option>");
        for (var j = 0; j < dataTracking.provincias.length; j++) {
            pos = dataTracking.provincias[j];
            option = $("<option value=" + pos.codprovincia + ">" + pos.provincia + "</option>");
            obj.append(option);
        }

        obj.change(function () {
            var div = $("#mainCambioDireccion").find("#divNueDir");

            // NOTA: SE RESTA EL INDEX DE SELECCIONAR
            var index = $(this).find("option:selected").index() - 1;
            var ciudades;
            if (index >= 0) {
                dataTracking.provincias[index] = ajaxrequest("getProvincia", dataTracking.provincias[index]);
                ciudades = dataTracking.provincias[index].ciudades;
            } else {
                ciudades = [];
            }

            var pos = null;
            var option = null;

            var obj = div.find("#ciudad").attr("index", index);
            // LIMPIAR CAMPO CIUDAD Y SECTOR
            obj.children().remove();
            div.find("#barrio").children().remove();
            obj.append("<option value=" + -1 + ">Seleccionar</option>");
            for (var j = 0; j < ciudades.length; j++) {
                pos = ciudades[j];
                option = $("<option value=" + pos.codciudad + ">" + pos.ciudad + "</option>");
                obj.append(option);
            }
            option = null; pos = null; obj = null; ciudades = null;
            index = null; div = null;
        });

        obj = divForm.find("#divNueDir").find("#ciudad");
        obj.change(function () {

            var div = $("#mainCambioDireccion").find("#divNueDir");

            var indexpro = $(this).attr("index");
            // NOTA: SE RESTA EL INDEX DE SELECCIONAR
            var index = $(this).find("option:selected").index() - 1;
            var sectores;
            if (index >= 0) {
                sectores = dataTracking.provincias[indexpro].ciudades[index].sectores;
            } else {
                sectores = [];
            }
            var pos = null;
            var option = null;


            var obj = div.find("#barrio");
            // LIMPIAR CAMPO SECTOR
            obj.children().remove();

            obj.append("<option value=" + -1 + ">Seleccionar</option>");
            for (var j = 0; j < sectores.length; j++) {
                pos = sectores[j];
                option = $("<option value=" + pos.codbarrio + ">" + pos.barrio + "</option>");
                obj.append(option);
            }
            option = null; pos = null; obj = null; sectores = null; index = null; indexpro = null;
            div = null;
        });

        divForm = null; columns = null;
        option = null; pos = null; obj = null;
    });
</script>

<div id='mainCambioDireccion' title="Direcci&oacute;n">
    <h3 id="titleDir">
        Existente
    </h3>
    <div id="divTbDirecciones">
        <div id="tbDirecciones">
            <div id="toolBox">
            </div>
            <hr />
            <div id="smartTablePrint" class="alignRight"></div>         
            <div id="contTbSmart">
                <table >
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div> 
    </div>
    <div id="divMasDirecciones">
        <div id="tbMasDirecciones">
            <div id="toolBox">
            </div>
            <hr />
            <div id="smartTablePrint" class="alignRight"></div>         
            <div id="contTbSmart">
                <table >
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div> 
    </div> 
    <div id="divNueDir">
        <h3>
            <input id="nuevDirChk" type="radio" name="radio" /><label id="nuevDirchkLbl" for="nuevDirChk">Nueva direcci&oacute;n</label>        
        </h3>   
        <div id="datosDireccion">
            <div class="labelDivCambioDireccionFloat">
                <p><span>Provincia</span></p>
                <p><span>Ciudad</span></p>          
                <p><span>N&uacute;mero</span></p>
                <p><span>Apartamento</span></p>
            </div>
            <div class="camposDivCambioDireccionFloat">            
                <p><select id="provincia" class="control"></select></p>
                <p><select id="ciudad" class="control"></select></p>  
                <p><input id="numero" type="text" class="control" /></p>
                <p><input id="apartamento" type="text" class="control" /></p>
            </div>
            <div class="labelDivCambioDireccionFloat">  
                <p><span>Sector</span></p>         
                <p><span>Calle</span></p>
                <p><span>Edificio</span></p>
                <p><span>Tel&eacute;fono</span></p>
            </div>
            <div class="camposDivCambioDireccionFloat">            
                <p><select id="barrio" class="control"></select></p> 
                <p><input id="direccion" type="text" class="control" /></p>     
                <p><input id="edificio" type="text" class="control" /></p>
                <p><input id="telefono" type="text" class="control" /></p>
            </div>
        </div>   
        <div id="divEnvNuevaDir">
            <input id="envNuevaDir" type="checkbox" class="control"/>
            <label for="envNuevaDir">Cambio sera temporal.</label>
        </div>
    </div>
    <div id="divObservacion">
        <h3>
            Observaci&oacute;n 
        </h3>
        <textarea id="nota" rows="5" cols="77" class="control"></textarea>     
    </div>   
    <br />
    <hr />
    <div id="divWizard">
        <button id="btnBack">Volver Atras</button>
        <button id="btnNew">Nueva Direcci&oacute;n</button>        
        <button id="btnMoreDirecciones">Cargar mas direcciones...</button>
        <button id="btnAceptar">Aceptar</button>
        <button id="btnCancelar">Cancelar</button>
    </div>
    <%--<div>
        <button id="btnMoreDirecciones">Cargar mas direcciones...</button>
    </div>--%>     
</div>