﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrearPaquete.ascx.cs"
    Inherits="Tracking.Formularios.CrearPaquete" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#crearPaquete");
        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 960,
            height: 600,
            open: function (event, ui) {
                //                var oTable = $(this).find('div.dataTables_scrollBody>#tbSmart').dataTable();
                //                if (oTable.length > 0) {
                //                    oTable.fnAdjustColumnSizing();
                //                }
                //                oTable = null;
                //cleanForm($(this));
                //$(this).removeAttr("receiver");
                //$(this).find(".control#nombre").removeAttr("readonly");
                //$(this).find("#tipodocumento").val(5);
                existenCambios = true;
            },

            close: function () {
                existenCambios = false;
            },

            buttons: {
                "Aceptar": function () {
                    var result = crearPaquete($(this));
                    if (result["ERROR"] == 0) {
                        $(this).dialog("close");
                        creacionPaquete = 1;

                        if ($(".mainPaqueteOpen").length == 0) {
                            loadPkgs();
                            $("#divPaqActuales").find(".searchSmartTable").val("");
                        } else {
                            // ACTUALIZAR PANTALLA DE PAQUETE
                            var index = $(this).attr("index");
                            var div = $("#mainPaquete" + index.replace(/,/g, ''));
                            var data = getPackByIndex(index);
                            var detalle = getDetPkg(data);
                            data.DEST_ID = detalle["dest_id"];
                            data.DEST_ID_TIPO = detalle["dest_id_tipo"];
                            data.childrens = detalle["childrens"];
                            data.direccion = detalle["direccion"];
                            if (detalle["childrens"].length == 0) {
                                $(".mainPaqueteOpen").each(function () {
                                    $(this).find("#tbDetPaquetes").smartTable("destroy");
                                    $(this).find("#tbDetPaquetes").remove();
                                    $(this).dialog("destroy");
                                    $(this).remove();
                                });
                            }
                            div.find("#tbDetPaquetes").smartTable("option", "codigo", data["CODIGO_ORDEN"]);
                            div.find("#tbDetPaquetes").smartTable("fillTbody", data.childrens);
                            buildOpenMainPaquete(div, data, detalle, index, false);

                            detalle = null; data = null;
                        }
                    } else if (result["ERROR"] == 1) {
                        messageBox(result["MENSAJE"]);
                    }
                    result = null;

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

        var columns = [
            {
                key: "CODIGO",
                label: "Codigo Paquete",
                width: "13%",
                events: []
            }, {
                key: "TIPO_OBJETO",
                label: "Tipo Documento",
                width: "10%",
                events: []
            }, {
                key: "ID_OBJETO",
                label: "No. Referencia",
                width: "10%",
                events: []
            }, {
                key: "DEPT_UBICACION",
                label: "Area",
                width: "15%",
                events: []
            },
            {
                key: "STATUS_NOM",
                label: "Estado",
                width: "10%",
                events: []
            },
            {
                key: "DEST_TIPO_NOM",
                label: "Tipo Destinatario",
                width: "15%",
                events: []
            },
            {
                key: "DEST_COD",
                label: "Codigo Destinatario",
                width: "10%",
                events: []
            },
            {
                key: "DEST_NAME",
                label: "Nombre Destinatario",
                width: "15%",
                events: []
            }
        ];
        divForm.find("#tbDetPaquetes").smartTable({ columns: columns, onlyView: true, height: 380, print: false, dataTable: false });
        //lengthfield($(this).find("#nota"), 4000);

        divForm = null; columns = null;
    });
</script>
<div id='crearPaquete' title="Crear Paquete">
<%--    <p>
        <label>
            Buscar
        </label>
        <input id="searchIdnuevPaq" class="control" type="text" />
    </p>
    <hr />
    <div id="datosPrincipales" class="ui-widget-content">
        <p>
            <label>
                Contenedor
            </label>
            <select id="tipodocumento" class="control dTypesDoc">
            </select>
        </p>
        <p>
            <label>
                N&uacute;mero de Referencia
            </label>
            <input type="text" class="control" id="numRefDoc" />
        </p>
        <p>
            <label>
                N&uacute;mero de Documento
            </label>
            <input type="text" class="control" id="numDoc" />
        </p>
        <p>
            <label>
                Nombre de la Persona
            </label>
            <input type="text" class="control" id="nombre" />
        </p>
        <p>
            <label>
                Sucursal
            </label>
            <input type="text" class="control" id="sucursal" />
        </p>
    </div>
    <div id="datosDireccion" class="ui-widget-content">
        <h3 id="labelDir">
            <span>Datos de direcci&oacute;n</span>
            <button id="btnDirecciones">
                Direcciones</button>
        </h3>
        <div class="labelDivCrearPaqFloat">
            <p>
                <span>Provincia</span></p>
            <p>
                <span>Ciudad</span></p>
        </div>
        <div class="camposDivCrearPaqFloat">
            <p>
                <input id="provincia" type="text" class="control" readonly="readonly" /></p>
            <p>
                <input id="ciudad" type="text" class="control" readonly="readonly" /></p>
        </div>
        <div class="labelDivCrearPaqFloat">
            <p>
                <span>Sector</span></p>
            <p>
                <span>Calle</span></p>
        </div>
        <div class="camposDivCrearPaqFloat">
            <p>
                <input id="barrio" type="text" class="control" readonly="readonly" /></p>
            <p>
                <input id="direccion" type="text" class="control" readonly="readonly" /></p>
        </div>
    </div>
    <br />--%>
    <div id="tbDetPaquetes" class="allWidth">
        <div id="toolBox">
        </div>
        <hr />
        <div id="smartTablePrint" class="alignRight">
        </div>
        <div id="contTbSmart">
            <table>
                <thead>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <hr />
    <div id="divObservacion">
        <h3 id="labelObservacion">
            Observaci&oacute;n
        </h3>
        <textarea id="nota" rows="2" cols="100" class="control"></textarea>
    </div>
</div>
