﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InfoAfiliado.ascx.cs" Inherits="Tracking.Formularios.InfoAfiliado" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#mainDatosInfoAfiliado");
        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 650,
            minHeight: 450,
            open: function () {
                cleanForm($(this));
            },
            buttons: {
                "Aceptar": function () {
                    $(this).dialog("close");
                }
            }
        });
    });
</script>
<div id="mainDatosInfoAfiliado" title="Cliente">
    <div id="accordion">
        <h3>
            <a href="#">Informaci&oacute;n General</a></h3>
        <div id="informaciongeneral">
            <table>
                <tr>
                    <td>
                        <span>N&uacute;mero de Afiliado</span>
                    </td>
                    <td>
                        <input type="text" id="outnumafiliado" />
                    </td>
                    <td style="width: 75px">
                    </td>
                    <td>
                        Contrato
                    </td>
                    <td>
                        <input type="text" id="outcontrato" style="width: 100px" readonly="readonly" />
                        <input type="text" id="outfamilia" style="width: 40px" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Nombre
                    </td>
                    <td>
                        <input type="text" id="outnombre" />
                    </td>
                </tr>
            </table>
        </div>
        <h3>
            <a href="#">Direcci&oacute;n y Tel&eacute;fono</a></h3>
        <div id="direccionTelefonoAfiliado">
            <table>
                <tr>
                    <td>
                        <span>Direcci&oacute;n</span>
                    </td>
                    <td>
                        <input type="text" id="outdireccion" readonly="readonly" style="width: 355px" />
                    </td>
                    <td style="width: 40px">
                    </td>
                    <td>
                        <span>Tel&eacute;fono Residencial</span>
                    </td>
                    <td>
                        <input type="text" id="outtelefonoresidencial" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Ciudad
                    </td>
                    <td>
                        <input type="text" id="outciudad" readonly="readonly" style="width: 355px" />
                    </td>
                    <td style="width: 40px">
                    </td>
                    <td>
                        <span>Tel&eacute;fono M&oacute;vil</span>
                    </td>
                    <td>
                        <input type="text" id="outtelefonomovil" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Sector
                    </td>
                    <td>
                        <input type="text" id="outsector" readonly="readonly" style="width: 355px" />
                    </td>
                    <td style="width: 40px">
                    </td>
                    <td>
                        <span>Tel&eacute;fono Oficina</span>
                    </td>
                    <td>
                        <input type="text" id="outtelefonooficina" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td style="width: 40px">
                    </td>
                    <td>
                        Email
                    </td>
                    <td>
                        <input type="text" id="outemail" readonly="readonly" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="tabs">
        <ul>
            <li><a href="#asesor">Asesor</a></li>
            <li><a href="#empleador">Empleador</a></li>
        </ul>
        <div id="asesor">
        </div>
        <div id="empleador">
        </div>
    </div>
</div>