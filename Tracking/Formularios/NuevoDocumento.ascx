﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NuevoDocumento.ascx.cs" Inherits="Tracking.Formularios.NuevoDocumento" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#nuevoDocumento");

        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 700,
            minHeight: 455,
            open: function () {
                existenCambios = true;
                cleanForm($(this));
                $(this).attr("receiver", "");
                $(this).removeAttr("destinatario_via");
                $(this).attr("direction", "");
                $(this).find(".control#nombre").removeAttr("readonly");
                // COLOCA LA OPCION DE DESTINATARIO INDIRECTO SELECCIONADA SIEMPRE QUE ABRE LA PANTALLA
                $(this).find("#doc_indirecto").prop("checked", true);
            },
            close: function () {
                existenCambios = false;
            },
            buttons: {
                "Aceptar": function () {

                    if ($(this).attr("receiver") == "") {
                        messageBox("Debe escoger un destinatario.");
                        return;
                    }

                    if ($(this).attr("direction") == "") {
                        messageBox("No existe una direccion seleccionada para este documento.");
                        return;
                    }

                    if ($(this).find("#ID_TIPO_OBJETO").val() == -1) {
                        messageBox("Debe seleccionar un tipo de documento para continuar.");
                        return;
                    }
                    if ($(this).find("#ID_OBJETO").val() == "") {
                        messageBox("El campo N&uacute;mero de Documento es requerido. Favor completar.");
                        return;
                    }

                    if (!/^\d+$/.test($(this).find("#ID_OBJETO").val())) {
                        messageBox("El campo N&uacute;mero de Documento solo permite numeros enteros.");
                        return;
                    }

                    var result = crearDocumento($(this));
                    if (result == 1) {
                        $(this).dialog("close");
                        loadPkgs();
                    }
                    result = null;
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

        divForm.find("#searchIdnuevDoc").bind("keydown", function (e) {
            var keyCode = e.keyCode || e.which;

            if (keyCode == 9 && $(this).val().indexOf("  ") == -1) {
                e.preventDefault();

                $(this).val($(this).val() + "  ");
                $(this).trigger("keydown");
            } else if (keyCode == 9 && $(this).val().indexOf("  ") != -1) {
                e.isDefaultPrevented();
            }
            keyCode = null;
        });
        divForm.find("#searchIdnuevDoc").autocomplete({
            minLength: 3,
            delay: 500,
            source: searchID,
            //            source: function (request, response) {
            //                var term = request.term;
            //                response(searchReceiver(term));
            //                return;
            //            },
            focus: function (event, ui) {
                return false;
            },
            select: function (event, ui) {

                var div = $("#nuevoDocumento");
                div.removeAttr("destinatario_via");
                var value = $.trim($(this).val());

                if (ui.item.nuevocte) {
                    // NUEVO DESTINATARIO
                    ui.item = nuevo_destinatario();
                } else if (ui.item.desc == "PBS" || ui.item.desc == "MPP") {
                    value = ui.item.value;
                    ui.item = WebSvcTrackingSoapHttpPort_prcBusquedainf(value, "CRT");
                    if (ui.item.outinformantesOut.length > 0) {
                        ui.item = ui.item.outinformantesOut[0];
                    } else {
                        return;
                    }
                }
                $(this).val(ui.item.nombre + "(" + $.trim(ui.item.identificacion) + ")");
                div.find(".control#nombre").val(ui.item.nombre);
                if (ui.item.codtipoinformante && ($.trim(ui.item.codtipoinformante).toUpperCase() == "PR" || $.trim(ui.item.codtipoinformante).toUpperCase() == "IN")) {
                    ui.item.consecutivo = ui.item.codigo;
                } else if (
                    (value.substring(0, 2) == "03" || value.substring(0, 2) == "04") && ((value.length == 8 || value.length == 14 || value.indexOf('-') != -1))
                ) {
                    // CONSULTA DE SERVICIO SI ES INDIRECTO = 1
                    var destinatario;
                    if (div.find(".tipo_envio:checked").attr("via") == 1) {
                        destinatario = WebSvcTrackingSoapHttpPort_prcBusquedainf(value, "INT").outinformantesOut;
                        if (destinatario.length > 0) {
                            destinatario = destinatario[0];
                            if (destinatario.codtipoinformante && ($.trim(destinatario.codtipoinformante).toUpperCase() == "PR" || $.trim(destinatario.codtipoinformante).toUpperCase() == "IN")) {
                                destinatario.consecutivo = destinatario.codigo;
                            }
                            div.attr("destinatario_via",
                                JSON.stringify(destinatario)
                            );
                        }
                    }
                    destinatario = null;
                }

                div.attr("receiver", JSON.stringify(ui.item));
                cambioDireccionPaq("nuevoDocumento", ui.item.consecutivo, ui.item.codtipoinformante, ui.item.tipopersona, 1);
                div = null; value = null;
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            if (item.desc && (item.desc == "PBS") || item.desc == "MPP") {
                return $("<li></li>")
				    .data("item.autocomplete", item)
				    .append("<a>" + item.label + " (" + item.desc + ")" + "</a>")
				    .appendTo(ul);
            } else if (item.nuevocte) {
                return $("<li>").append("<a><hr><br>Nuevo Destinatario</a>").appendTo(ul).css("font-size", "8pt");
            } else {
                return $("<li>")
                .append("<a>" + item.nombre + "(" + item.tipoinformante + ")<br>" + $.trim(item.tipoid) + " - " + $.trim(item.identificacion) + "</a>")
                .appendTo(ul).css("font-size", "8pt");
            }
        };
        divForm.find("#btnDirecciones").click(function () {
            var receiver = $("#nuevoDocumento").attr("receiver");
            if (receiver) {
                receiver = JSON.parse(receiver);
                $("#mainCambioDireccion").attr("openFrom", "nuevoDocumento").attr("directions", JSON.stringify(searchDirections(receiver))).dialog("open");
            }
            receiver = null;

        });
        lengthfield(divForm.find("#NOTES"), 4000);
        divForm = null;
    });    
</script>
<div id='nuevoDocumento' title="Crear Nuevo Documento">
    <p>
        <label>
            Destinatario: 
        </label>
        <label for="doc_directo">
            Directo
        </label>        
        <input id="doc_directo" class="control tipo_envio" via="0" type="radio" name="tipo_envio"/>
         | 
         <label for="doc_indirecto">
            Indirecto
        </label>
         <input id="doc_indirecto" class="control tipo_envio" via="1" type="radio" name="tipo_envio"/>
    </p>
    <hr />
    <p>
        <label>
            Buscar
        </label>
        <input id="searchIdnuevDoc" class="control" type="text" />
    </p>
    <hr />
    <div id="datosPrincipales">
        <p>
            <label>
                Tipo de Documento
            </label>
            <select id="ID_TIPO_OBJETO" class="control dTypesDoc">
            </select>
        </p>        
        <p>
            <label>
                N&uacute;mero de Referencia
            </label>
            <input type="text" class="control" id="NUM_REF" />
        </p>
        <p>
            <label>
                N&uacute;mero de Documento
            </label>
            <input type="text" class="control" id="ID_OBJETO" />
        </p>
        <p>
            <label>
                Nombre de la Persona
            </label>
            <input type="text" class="control" readonly="readonly" id="nombre"/>
        </p>
        <p>
            <label>
                Sucursal
            </label>
            <input type="text" class="control" readonly="readonly" id="sucursal"/>
        </p>
    </div>    
    <h3>
        Datos de direcci&oacute;n
        <button id="btnDirecciones">Direcciones</button>
    </h3>
    <div id="datosDireccion">
        <div class="labelDivFloat">
            <p><span>Provincia</span></p>
            <p><span>Ciudad</span></p>
            <p><span>Tel&eacute;fono</span></p>
        </div>
        <div class="camposDivFloat">
            <p><input id="provincia" type="text" class="control" readonly="readonly" /></p>
            <p><input id="ciudad" type="text" class="control" readonly="readonly" /></p>  
            <p><input id="telefono" type="text" class="control" readonly="readonly" /></p>                                 
        </div>
        <div class="labelDivFloat">  
            <p><span>Sector</span></p>          
            <p><span>Calle</span></p>              
        </div>
        <div class="camposDivFloat">             
            <p><input id="barrio" type="text" class="control" readonly="readonly" /></p> 
            <p><input id="direccion" type="text" class="control" readonly="readonly" /></p>
        </div>
    </div>
    <br />
    <div id="divObservacion">
        <h3>
            Observaci&oacute;n 
        </h3>
        <textarea id="NOTES" rows="2" cols="90" class="control"></textarea> 
            
    </div>
     
</div>