﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Paquete.ascx.cs" Inherits="Tracking.Formularios.Paquete" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#mainPaquete");

        var toolBar = divForm.find("#paqueteTools");
        toolBar.find("#verCliente").button({
            text: false,
            icons: {
                primary: "ui-icon-contact"
            }
        });
        toolBar.find("#verDocumento").button({
            text: false,
            icons: {
                primary: "ui-icon-document"
            }
        });
        toolBar.find("#btnPrint").button({
            text: false,
            icons: {
                primary: "ui-icon-print"
            }
        });

        toolBar.find("#btnHome").button({
            text: false,
            icons: {
                primary: "ui-icon-home"
            }
        });

        divForm.find("#btnAddComment").attr("div", $(this).prop("id")).button({
            text: false,
            icons: {
                primary: "ui-icon-plus"
            }
        });
        toolBar = null; divForm = null;

    });    
</script>
<div id='mainPaquete'>
    <div id="paqueteTools">
      <span>Consultas: </span>
      <button id="verCliente" style="font-size: 7.7pt;">Ver Cliente</button>
      <button id="verDocumento" style="font-size: 7.7pt;">Ver Documento</button>
      <span>Transacciones:</span>
      <button id="solcCambioDir" style="font-size: 7.7pt;">Cambiar Direcci&oacute;n</button>
      <button id="solcCambioDest" style="font-size: 7.7pt;">Cambiar Destinatario</button>
      <button id="btnHome" style="font-size: 7.7pt;">Inicio</button>
      <button id="btnPrint" style="font-size: 7.7pt;">Imprimir</button>
    </div>

    <hr />
    <div id="divNavigation">
        
    </div>
    <div>
        <span id="count_paquetes"></span>
         |
        <span id="count_documents"></span>
    </div>
    <div class="mainDataPaquete">
        <span class="fontDecorate">Datos Principales</span>
        
    </div>
    <div id="mainDataPaquete" class="mainDataPaquete">
        <div id="#contentDataPaq">
            <div class="labelDivPaqFloat">
                <p><span>Codigo</span></p>
                <p><span>No. Documento</span></p>
                <p><span>ID Cliente</span></p>
                <p><span>No. Contrato</span></p>
                <p><span>Codigo Intermediario</span></p>
                <p><span>Codigo Prestador</span></p>
                <p><span>ID Proveedor</span></p>
                <p><span>Codigo Centro Medico</span></p>
                <p><span>No. Caso</span></p>
            </div>
            <div class="camposDivPaqFloat">
                <p><input id="CODIGO" type="text" class="control" readonly="readonly"/></p>
                <p><input id="ID_OBJETO" type="text" class="control" readonly="readonly" /></p>
                <p><input id="DEST_COD" type="text" class="control" readonly="readonly"/></p>
                <p><input id="NUM_CONTRATO" type="text" class="control" readonly="readonly"/></p>
                <p><input id="INDEST_COD" type="text" class="control" readonly="readonly"/></p>
                <p><input id="PRDEST_COD" type="text" class="control" readonly="readonly"/></p>
                <p><input id="PVDEST_COD" type="text" class="control" readonly="readonly"/></p>
                <p><input id="CEDEST_COD" type="text" class="control" readonly="readonly"/></p>
                <p><input id="NO_CASO" type="text" class="control" readonly="readonly"/></p>
            </div>
            <div class="labelDivPaqFloat">
                <p><span>Tipo Documento</span></p>                
                <p><span>No. Referencia</span></p>
                <p><span>Nombre Cliente</span></p>
                <p><span>Nombre Contratante</span></p>
                <%--<p><span>Sucursal</span></p>--%>
                <p><span>Nombre Intermediario</span></p>
                <p><span>Nombre Prestador</span></p>
                <p><span>Nombre Proveedor</span></p>
                <p><span>Centro Medico</span></p>
            </div>
            <div class="camposDivPaqFloat">
                <p><input id="TIPO_OBJETO" type="text" class="control" readonly="readonly" /></p>                 
                <p><input id="NUM_REF" type="text" class="control" readonly="readonly" /></p> 
                <p><input id="DEST_NAME" type="text" class="control" readonly="readonly"/></p>                 
                <p><input id="CODEST_NAME" type="text" class="control" readonly="readonly"/></p>
                <p><input id="INDEST_NAME" type="text" class="control" readonly="readonly"/></p>
                <p><input id="PRDEST_NAME" type="text" class="control" readonly="readonly"/></p>
                <p><input id="PVDEST_NAME" type="text" class="control" readonly="readonly"/></p>
                <p><input id="CEDEST_NAME" type="text" class="control" readonly="readonly"/></p>
            </div>
        </div>
    </div>    
    <h3 id="toggleDireccion"  class="contentDatosDireccion" >Datos de direcci&oacute;n</h3>
    <div id="contentDatosDireccion" class="contentDatosDireccion">
        <div id="datosDireccion">
            <div class="labelDivPaqFloat">
                <p><span>Provincia</span></p>
                <p><span>Ciudad</span></p>               
                <p><span>Tel&eacute;fono</span></p>
            </div>
            <div class="camposDivPaqFloat">
                <p><input id="provincia" type="text" class="control" readonly="readonly" /></p>
                <p><input id="ciudad" type="text" class="control" readonly="readonly" /></p>  
                <p><input id="telefono" type="text" class="control" readonly="readonly" /></p>                                                
            </div>
            <div class="labelDivPaqFloat">  
                <p><span>Sector</span></p>              
                <p><span>Calle</span></p>
            </div>
            <div class="camposDivPaqFloat">           
                <p><input id="barrio" type="text" class="control" readonly="readonly" /></p>    
                <p><input id="direccion" type="text" class="control" readonly="readonly" /></p>  
            </div>
        </div>
    </div>                                
    <div id="tabs">
      <ul>
        <li><a href="#divContentDivHistorial">Historial</a></li>
        <li><a href="#tbDetPaquetes">Detalle Documentos</a></li>
      </ul>
      <div id="divContentDivHistorial">
        <div id="contentDivHistorial" class="allWidth">
            <div id="divHistorialComment" class="hide">
                <p>
                    <label>Observaci&oacute;n</label>                    
                </p>
                <%--<textarea id="NOTES" rows="11" cols="48" readonly="readonly" class="textAreaJustify control allSpace"></textarea>--%>
                <div id="divNotes">
                    <table id="tbNotes" class="control allWidth">
                        <thead class="ui-widget-header">
                            <tr>
                                <th>Observaci&oacute;n</th>
                                <th>Usuario</th>                       
                                <th>Fecha</th>                                                                
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div> 
            </div>
            <div id="divHistorialPaqAct" class="allWidth">
                <p>
                    <label>Historial</label>
                    <button id="btnAddComment" style="font-size: 7.7pt;">Añadir Observaci&oacute;n</button>
                </p>
                <div id="divTbHistorialPaqAct">
                    <table id="tbHistorial" class="control allWidth">
                        <thead class="ui-widget-header">
                            <tr>
                                <td>Estado</td>
                                <td>Area</td>                       
                                <td>Usuario</td>
                                <td>Fecha</td>
                                <td>Notas</td>                                                                                                     
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>                     
            </div>           
        </div>
      </div>
      
      <div id="tbDetPaquetes">
        <div id="toolBox">
        </div>
        <hr />
        <div id="smartTablePrint" class="alignRight"></div>         
        <div id="contTbSmart">
            <table  style="width:100%">
                <thead>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>        
    </div>    
</div>
