﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Print.ascx.cs" Inherits="Tracking.Formularios.Print" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#divPrintTracking");
        //divForm.find("#trInvisible").hide();
        divForm.dialog(
		    { autoOpen: false, width: 380, modal: true,
		        open: function () {
		            $(this).find("#paqueteRadioPrint").prop("checked", true);
		            $(this).find("#nivel1RadioPrint").prop("checked", true);
		        }
            , buttons: {
                Imprimir: function () {
                    var data = {};
                    data["CODIGO_ORDEN"] = $(this).attr("codigo_orden");
                    // SI ES 0 = ACUSE, Y SI ES 1 = LABEL
                    data["IMPRESION"] = $(this).attr("impresion");
                    data["TIPO_IMPRESION"] = $("input[name=tipo_impresion]:checked", "#divPrintTracking").attr("key");
                    data["NIVEL"] = $("input[name=nivel_impresion]:checked", "#divPrintTracking").attr("key");
                    var id_report = ajaxrequest("process_Print", data);
                    if (id_report["ERROR"] == 1) {
                        messageBox(id_report["MESSAGE"]);
                    } else {
                        // SI ES 0 = ACUSE, Y SI ES 1 = LABEL
                        if (data["IMPRESION"] == 0) {
                            callReportPdf(reporteAcuseUrl, id_report["id_report"]);

                        } else {
                            callReportPdf(reporteLabelUrl, id_report["id_report"]);
                        }
                    }
                    data = null; id_report = null;
                }
            }
		    });
        divForm = null;
    });
</script>

<div id="divPrintTracking" title="Imprimir">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr id="trInvisible">
			<td><label for="documentoRadioPrint">Paquete</label></td>
			<td>
                <input type="radio" name="tipo_impresion" class="tipoPrint" id="paqueteRadioPrint" key="0"/>
			</td>
			<td>&nbsp;</td>
			<td>
                <label for="documentoRadioPrint">Documentos</label>
            </td>
			<td>
                <input type="radio" name="tipo_impresion" class="tipoPrint" id="documentoRadioPrint" key="1"/>
			</td>
		</tr>
        <tr>
            <td>                
                <input type="radio" name="nivel_impresion"  class="nivelPrint" id="nivel1RadioPrint" key="1"/>
                <label for="nivel1RadioPrint">Paquete</label>
            </td>
            <td>                
                <input type="radio" name="nivel_impresion" class="nivelPrint" id="nivel2RadioPrint" key="2"/>
                <label for="nivel2RadioPrint">Detalle</label>
            </td>
        </tr>
	</table>
</div>
