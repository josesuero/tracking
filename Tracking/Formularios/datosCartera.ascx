﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatosCartera.ascx.cs" Inherits="Tracking.Formularios.DatosCartera" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#mainDatosCartera");
        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 600,
            minHeight: 400,
            open: function () {
                cleanForm($(this));
            },
            buttons: {
                "Aceptar": function () {
                    $(this).dialog("close");
                }
            }
        });
    });
</script>
<div id='mainDatosCartera' title="Datos Cartera">
    <div id="datosCartera">
        <table>
            <tr>
                <td>
                    <span>Balance de la factura /&nbsp;cr&eacute;dito</span>
                </td>
                <td>
                    <input type="text" id="outbalance" readonly="readonly" />
                </td>
                <td style="width: 75px">
                </td>
                <td>
                    <span>Vigencia factura / cr&eacute;dito</span>
                </td>
                <td>
                    <input type="text" id="outvigencia" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td>
                    <span>Concepto de factura / cr&eacute;dito</span>
                </td>
                <td>
                    <input type="text" id="outconcepto" readonly="readonly" />
                </td>
                <td style="width: 75px">
                </td>
                <td>
                    Tipo de documento
                </td>
                <td>
                    <input type="text" id="outtipodocumento" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td>
                    <span>Fecha emisi&oacute;n de la<br />
                        factura / cr&eacute;dito</span>
                </td>
                <td>
                    <input type="text" id="outfechaemision" readonly="readonly" />
                </td>
                <td style="width: 75px">
                </td>
                <td>
                    <span>Fecha l&iacute;mite de pago</span>
                </td>
                <td>
                    <input type="text" id="outfechalimite" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td>
                    <span>Monto de la factura /<br />
                        cr&eacute;dito</span>
                </td>
                <td>
                    <input type="text" id="outmonto" readonly="readonly" />
                </td>
                <td style="width: 75px">
                </td>
                <td>
                    NCF
                </td>
                <td>
                    <input type="text" id="outncf" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td>
                    <span>N&uacute;mero de la factura / cr&eacute;dito</span>
                </td>
                <td>
                    <input type="text" id="outnumero" readonly="readonly" />
                </td>
                <td style="width: 75px">
                </td>
                <td>
                    ¿Imprime reporte?
                </td>
                <td>
                    <input type="text" id="outimprimereporte" readonly="readonly" style="width: 20px" />
                </td>
            </tr>
            <tr>
                <td>
                    <span>Periodo de facturaci&oacute;n<br />
                        del contrato</span>
                </td>
                <td>
                    <input type="text" id="outperiodo" readonly="readonly" />
                </td>
                <td style="width: 75px">
                </td>
                <td>
                    ¿Correos inscritos?
                </td>
                <td>
                    <input type="text" id="outcorreosinscritos" readonly="readonly" style="width: 20px" />
                </td>
            </tr>
            <tr>
                <td>
                    <span>Fecha motor env&iacute;o</span>
                </td>
                <td>
                    <input type="text" id="outfechamotorenvio" readonly="readonly" />
                </td>
                <td style="width: 75px">
                </td>
                <td>
                    <span>Pagos autom&aacute;ticos?</span>
                </td>
                <td>
                    <input type="text" id="outpagosautomaticos" readonly="readonly" style="width: 20px" />
                </td>
            </tr>
            <tr>
                <td>
                    <span>Correo motor env&iacute;o</span>
                </td>
                <td>
                    <input type="text" id="outcorreomotorenvio" readonly="readonly" />
                </td>
                <td style="width: 75px">
                </td>
                <td>
                    <input type="hidden" id="hiddenNumFact" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <span>Observaci&oacute;n</span>
                </td>
                <td colspan="4">
                    <textarea cols="50" rows="4" id="outobservacion" readonly="readonly">Enter text</textarea>
                </td>
            </tr>
        </table>
    </div>
    <div id="tabs">
        <ul>
            <li><a href="#detalleFacturas">Detalle de Facturas</a></li>
            <li><a href="#pagos">Pagos</a></li>
            <li><a href="#creditos">Cr&eacute;ditos</a></li>
            <li><a href="#interaccionesGestor">Interacciones del Gestor</a></li>
            <li><a href="#cantidad">Cantidad</a></li>
        </ul>
        <div id="detalleFacturas">
            <table id="tbdetalleFacturas" border="1">
                <thead>
                    <tr>
                        <td abbr="valorbruto">
                            Valor Bruto
                        </td>
                        <td abbr="concepto">
                            Concepto
                        </td>
                        <td abbr="cuotainscripcion">
                            Cuota Inscripci&oacute;n
                        </td>
                        <td abbr="valorneto">
                            Valor Neto
                        </td>
                        <td abbr="factura">
                            Factura
                        </td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="pagos">
            <table id="tbpagos" border="1">
                <thead>
                    <tr>
                        <td abbr="fechaaplicacion">
                            Fecha de Aplicaci&oacute;n
                        </td>
                        <td abbr="montopago">
                            Monto Pago
                        </td>
                        <td abbr="abonocapital">
                            Abono Capital
                        </td>
                        <td abbr="abonointeres">
                            Abono Inter&eacute;s
                        </td>
                        <td abbr="numfolio">
                            N&uacute;mero de Folio
                        </td>
                        <td abbr="tipopago">
                            Tipo Pago
                        </td>
                        <td abbr="fechapago">
                            Fecha de Pago
                        </td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="creditos">
            <table id="tbcreditos" border="1">
                <thead>
                    <tr>
                        <td abbr="fechaemision">
                            Fecha de Emisi&oacute;n
                        </td>
                        <td abbr="monto">
                            Monto
                        </td>
                        <td abbr="folio">
                            Folio
                        </td>
                        <td abbr="numerocredito">
                            N&uacute;mero cr&eacute;dito
                        </td>
                        <td abbr="tiponcf">
                            Tipo N&uacute;mero Factura
                        </td>
                        <td abbr="numncf">
                            N&uacute;mero Factura
                        </td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="interaccionesGestor" style="overflow: auto">
            <table id="tbinteraccionesGestor" border="1">
                <thead>
                    <tr>
                        <td abbr="numgescontacto">
                            No.
                        </td>
                        <td abbr="fecrespuesta">
                            Fecha Respuesta
                        </td>
                        <td abbr="horacontacto">
                            Hora Contacto
                        </td>
                        <td abbr="mtoprometido">
                            Monto Prometido
                        </td>
                        <td abbr="feccontacto">
                            Fecha Contacto
                        </td>
                        <td abbr="fecpromesapago">
                            Fecha Promesa de Pago
                        </td>
                        <td abbr="saldofact">
                            Saldo Factura
                        </td>
                        <td abbr="montofact">
                            Monto Factura
                        </td>
                        <td abbr="diasatraso">
                            D&iacute;as Atraso
                        </td>
                        <td abbr="gestor">
                            Gestor
                        </td>
                        <td abbr="observaciones">
                            Observaciones
                        </td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="cantidad">
        </div>
    </div>
    <p>
        <span>Cantidad de registros</span>
        <select id="cantregistrostab">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
        </select>
    </p>
</div>