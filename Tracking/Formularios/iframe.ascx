﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="iframe.ascx.cs" Inherits="Tracking.Formularios.iframe" %>
<script type="text/javascript">
    $(document).ready(function () {
        var divForm = $("#mainIframe");
        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 800,
            minHeight: 500,
            open: function () {

                var iframe = $(this).find("#infoframe");
                iframe.prop('src', 'about:blank');
                var data = JSON.parse($(this).attr("jsonData"));
                var params = "codPersona=" + data.DEST_COD;
                params += "&tipoDestinatario=" + data.DEST_TIPO_COD;
                params += "&tipoPersona=" + data.DEST_TIPO_PERSONA;
                params += "&tipoDocumento=1"
                params += "&codigoDocumento=" + data.ID_OBJETO;

                iframe.prop("src", "Consultas/InfoAfiliado.html?" + params);
                params = null;
                iframe = null;
                data = null;
            }
        });

        divForm = $("#mainIframeDocumento");
        divForm.dialog({
            modal: true,
            autoOpen: false,
            minWidth: 800,
            minHeight: 500,
            open: function () {
                var iframe = $(this).find("#infoframe");
                iframe.prop('src', 'about:blank');
                var data = JSON.parse($(this).attr("jsonData"));
                delete data.childrens;
                delete data.DATA_CODIGO_ORDEN;
                var params = "";
                for (var x in data) {
                    params += "&" + x + "=" + data[x];
                }
                $(this).find("#infoframe").prop("src", "Consultas/InfoDocumento.html?" + encodeURI(params.substr(1)));
                params = null; iframe = null; data = null;
            }
        });

        divForm = null;
    });
</script>
<div id="mainIframe" title="Destinatario">
    <div style="width:100%; height:500px;">
        <iframe id="infoframe"  src="" height="500px" width="100%"></iframe>
    </div>    
</div>

<div id="mainIframeDocumento" title="Documento">
    <div style="width:100%; height:500px;">
        <iframe id="infoframe"  src="" height="500px" width="100%"></iframe>
    </div>    
</div>