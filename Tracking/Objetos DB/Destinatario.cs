﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Objetos_DB
{
    public class Destinatario
    {
        public string CODIGO { get; set; }
        public string NOMBRE { get; set; }        
        public string TIPO { get; set; }
        public string TIPO_PERSONA { get; set; }

    }
}