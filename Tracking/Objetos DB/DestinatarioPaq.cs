﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Objetos_DB
{
    public class DestinatarioPaq
    {
        public string DEST_ID { get; set; }
        public string DEST_COD { get; set; }
        public string DEST_NAME { get; set; }
        public string DEST_ID_TIPO { get; set; }
        public string DEST_TIPO_COD { get; set; }
        public string DEST_TIPO_NOM { get; set; }
        public string DEST_TIPO_PERSONA { get; set; }
    }
}