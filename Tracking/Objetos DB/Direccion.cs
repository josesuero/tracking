﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Objetos_DB
{
    public class Direccion
    {
        public string id { get; set; }
        public string id_destinatario { get; set; }
        public string codtipodireccion { get; set; }
        public string status { get; set; }
        public string fecha { get; set; }
        public string codprovincia { get; set; }
        public string provincia { get; set; }
        public string codciudad { get; set; }
        public string ciudad { get; set; }
        public string codbarrio { get; set; }
        public string barrio { get; set; }
        public string codsucursal { get; set; }
        public string sucursal { get; set; }
        public string direccion { get; set; } 
    }
}