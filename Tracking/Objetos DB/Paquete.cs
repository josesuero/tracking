﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Objetos_DB
{
    public class Paquete
    {
        public string ID_OBJETO { get; set; }
        public string NUM_REF{ get; set; }
        public int ID_TIPO_OBJETO { get; set; }
        public string ID_SISTEMA_ORIGEN { get; set; }
        public string ID_SUCURSAL { get; set; }
        public string ID_USUARIO { get; set; }
        public string ID_AREA { get; set; }
        public string ID_CONTRATO { get; set; }
        public string ID_FAMILIA { get; set; }
        public string NOTES { get; set; }
        public string NO_CASO { get; set; } 
    }
}