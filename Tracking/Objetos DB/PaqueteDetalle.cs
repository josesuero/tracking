﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Objetos_DB
{
    public class PaqueteDetalle
    {
        public string CODIGO_ORDEN { get; set; }
        public string FINAL { get; set; }
        public string DOCUMENTOS { get; set; }
        public string ID_LAST_DOCUMENTO { get; set; }
        public string AREAS { get; set; }
        public string ID_SUB_AREA { get; set; }
        public string ID { get; set; }
        public string CODIGO { get; set; }
        public string STATUS { get; set; }
        public string ENTREGADO { get; set; }
        public string ID_OBJETO { get; set; }
        public string ID_TIPO_OBJETO { get; set; }
        public string ID_SISTEMA_ORIGEN { get; set; }
        public string ID_SUCURSAL { get; set; }
        public string ID_DIRECCION { get; set; }
        public string NUM_REF { get; set; }
        public string TIPO_OBJETO { get; set; }
        public string DEST_ID { get; set; }
        public string DEST_COD { get; set; }
        public string DEST_NAME { get; set; }
        public string DEST_ID_TIPO { get; set; }
        public string DEST_TIPO_COD { get; set; }
        public string DEST_TIPO_NOM { get; set; }
        public string DEST_TIPO_PERSONA { get; set; }
        public string STATUS_NOM { get; set; }
        public string DEPT_ORIGEN { get; set; }
        public string DEPT_UBICACION { get; set; }
        public DestinatarioPaq[] DESTINATARIOS { get; set; }
        public Direccion DIRECCION { get; set; } 

    }
}