﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracking.Objetos_DB
{
    public class PaqueteOut
    {
        public string CODIGO_ORDEN { get; set; }
        public string CODIGO { get; set; }
        public int EJECUCION { get; set; }
        public string MESSAGE { get; set; }
        public string TIME { get; set; }
    }
}