﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reportes.aspx.cs" Inherits="Tracking.Reportes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script src="Scripts/TimePicker.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#reporte1').hide();
        var destinatario;
        //var urlreporte = <!--<urlreport%>;-->
        //var urlreporte = 'http://su01sisacdb/ReportServer/Pages/ReportViewer.aspx?%2ftracking%2fpaquetes_v2&amp;rc:Parameters=false';
        var urlreporte = 'http://su01sisacdb/ReportServer/Pages/ReportViewer.aspx?%2ftracking%2fpaquetes_v2'
        var urlreportemas = 'http://su01sisacdb/ReportServer/Pages/ReportViewer.aspx?%2ftracking%2fpaquetes_v3';
        $('#ftfecdesde').datepicker({ dateFormat: 'dd/mm/yy' });
        $('#ftfechasta').datepicker({ dateFormat: 'dd/mm/yy' });
        var objs = $("#fttipdoc");
        var pos = null;
        for (var i = 0; i < objs.length; i++) {
            objs.eq(i).append("<option value=" + -1 + ">Seleccionar</option>");
            for (var j = 0; j < dataTracking.typesDoc.length; j++) {
                pos = dataTracking.typesDoc[j];
                objs.eq(i).append("<option value=" + pos.ID + ">" + pos.NOMBRE + "</option>");
            }
        }
        objs = $("#ftarea");
        pos = null;
        for (var i = 0; i < objs.length; i++) {
            objs.eq(i).append("<option value=" + -1 + ">Seleccionar</option>");
            for (var j = 0; j < dataTracking.areas.length; j++) {
                pos = dataTracking.areas[j];
                objs.eq(i).append("<option value=" + pos.ACCARECOD + "," + pos.ACCARESUBCOD + ">" + pos.ACCARENOM + ": " + pos.ACCARESUBNOM + "</option>");
            }
        }
        objs = $("#ftestpaquete");
        pos = null;
        for (var i = 0; i < objs.length; i++) {
            objs.eq(i).append("<option value=" + -1 + ">Seleccionar</option>");
            for (var j = 0; j < dataTracking.status.length; j++) {
                pos = dataTracking.status[j];
                objs.eq(i).append("<option value=" + pos.ID + ">" + pos.NOMBRE + "</option>");
            }
        }

        objs = $("#ftsucursal");
        pos = null;
        for (var i = 0; i < objs.length; i++) {
            objs.eq(i).append("<option value=" + -1 + ">Seleccionar</option>");
            for (var j = 0; j < dataTracking.sucursales.length; j++) {
                pos = dataTracking.sucursales[j];
                objs.eq(i).append("<option value=" + pos.AFISUCCOD + ">" + pos.AFISUCNOM + "</option>");
            }
        }



        //Funcion para generar el reporte de acuerdo a los parametros del formulario
        $("#btngenreport").click(function () {
            var query = "";

            var querymain = "";

            //var starterq = "AND PAQ.ID IN (select his.ID_PAQUETE from tracking.historial his where ";

            var starterq = "AND PAQ.ID IN (SELECT a.ID FROM (SELECT his.codigo_orden, his.fecha_modificado FROM tracking.historial his where ";

            var starterquerymain = " AND ";

            var fecdesde = $("#ftfecdesde").val();
            var fechasta = $("#ftfechasta").val();
            var tipdoc = $("#fttipdoc option:selected").val();
            var mensajeros = $("#ftmensajeros option:selected").val();
            var estpaquete = $("#ftestpaquete option:selected").val();
            var tipprod = $("#fttipprod option:selected").val();
            var area = $("#ftarea option:selected").val().split(",")[0];
            var subarea = $("#ftarea option:selected").val().split(",")[1];
            var sucursal = $("#ftsucursal option:selected").val();
            var user = $("#ftuser").val();
            var destinatario = $("#ftdestinatario").val();

            query += starterq;

            querymain += starterquerymain;

            if ((!!fecdesde) || (!!fechasta) ||
					(!!user) || (!!estpaquete) ||
					(!!subarea)
						) {
                if (!!fecdesde) {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "trunc(HIS.FECHA_MODIFICADO) >= TO_DATE('" + fecdesde + "','DD/MM/YYYY')";
                }

                if (!!fechasta) {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "trunc(HIS.FECHA_MODIFICADO) <= TO_DATE('" + fechasta + "','DD/MM/YYYY')";
                }

                if (!!user) {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "trim(upper(HIS.ID_LOCATION)) = TRIM(UPPER('" + user + "'))";
                }

                if (estpaquete != "-1") {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "HIS.STATUS = '" + estpaquete + "'";
                }

                if (!!subarea) {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "HIS.ID_SUB_AREA = '" + subarea + "'";
                }
            }

            if (tipdoc != "-1") {
                if (querymain != starterquerymain) {
                    querymain += " AND ";
                }
                querymain += "PAQ.ID_TIPO_OBJETO = " + tipdoc;
            }

            if (sucursal != "-1") {
                if (querymain != starterquerymain) {
                    querymain += " AND ";
                }
                querymain += "PAQ.ID_SUCURSAL = RPAD('" + sucursal + "',5)";
            }
            //TODO BUSQUEDA DE DESTINATARIO
            if (!!destinatario) {
                if (querymain != starterquerymain) {
                    querymain += " AND ";
                }
                querymain += "paq.id_destinatario = (select id from tracking.destinatarios des where trim(des.codigo) = '" +
						$("#ftdestinatario").attr("codigo") + "' and ((des.id_tipo = (select id from tracking.tipo_destinatarios tip where tip.codigo = '"
						+ $("#ftdestinatario").attr("tipodest") + "' and tip.tipo_persona = '"
						+ $("#ftdestinatario").attr("tipopersona") + "')) or ('" + $("#ftdestinatario").attr("tipodest") + "' IN ('PR','IN'))))";
            }

            if (query != starterq) {
                query += ") p left outer join tracking.paquete a on a.codigo_orden like p.codigo_orden||'%')";
            } else {
                query = "";
            }

            if (querymain == starterquerymain) {
                querymain = "";
            }
            /*
            if (tipdoc != nul && tipdoc != "") {
            query += "X = " + tipdoc;
            }

            if (mensajeros != nul && mensajeros != "") {
            query += "X = " + mensajeros;
            }

            if (estpaquete != nul && estpaquete != "") {
            query += "X = " + estpaquete;
            }

            if (tipprod != nul && tipprod != "") {
            query += "X = " + tipprod;
            }

            if (area != nul && area != "") {
            query += "X = " + area;
            }

            if (subarea != nul && subarea != "") {
            query += "X = " + subarea;
            }

            if (sucursal != nul && sucursal != "") {
            query += "X = " + sucursal;
            }

            if (user != nul && user != "") {
            query += "X = " + user;
            }

            if (destinatario != nul && destinatario != "") {
            query += "X = " + destinatario;
            }
            */

            var stringurl = urlreporte;
            if (!!query) {
                stringurl += "&FILTER= " + query;
            } else {
                stringurl += "&FILTER= ";
            }
            if (!!querymain) {
                stringurl += "&FILTERMAIN=%20" + querymain;
            } else {
                stringurl += "&FILTERMAIN=%20";
            }
            //var stringurl = urlreporte +  "&FILTER= " + query + "&FILTERMAIN= " + querymain;// +  "&FILTER=and " + filter;  
            $("#frameReport").attr('src', stringurl);
        });

        $("#btngenmasreport").click(function () {

            var query = "";

            var querymain = "";

            //var starterq = "AND PAQ.ID IN (select his.ID_PAQUETE from tracking.historial his where ";

            var starterq = "AND PAQ.ID IN (SELECT a.ID FROM (SELECT his.codigo_orden, his.fecha_modificado FROM tracking.historial his where ";

            var starterquerymain = " AND ";

            var fecdesde = $("#ftfecdesde").val();
            var fechasta = $("#ftfechasta").val();
            var tipdoc = $("#fttipdoc option:selected").val();
            var mensajeros = $("#ftmensajeros option:selected").val();
            var estpaquete = $("#ftestpaquete option:selected").val();
            var tipprod = $("#fttipprod option:selected").val();
            var area = $("#ftarea option:selected").val().split(",")[0];
            var subarea = $("#ftarea option:selected").val().split(",")[1];
            var sucursal = $("#ftsucursal option:selected").val();
            var user = $("#ftuser").val();
            var destinatario = $("#ftdestinatario").val();

            query += starterq;

            querymain += starterquerymain;

            if ((!!fecdesde) || (!!fechasta) ||
					(!!user) || (!!estpaquete) ||
					(!!subarea)
						) {
                if (!!fecdesde) {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "trunc(HIS.FECHA_MODIFICADO) >= TO_DATE('" + fecdesde + "','DD/MM/YYYY')";
                }

                if (!!fechasta) {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "trunc(HIS.FECHA_MODIFICADO) <= TO_DATE('" + fechasta + "','DD/MM/YYYY')";
                }

                if (!!user) {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "trim(upper(HIS.ID_LOCATION)) = TRIM(UPPER('" + user + "'))";
                }

                if (estpaquete != "-1") {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "HIS.STATUS = '" + estpaquete + "'";
                }

                if (!!subarea) {
                    if (query != starterq) {
                        query += " AND ";
                    }
                    query += "HIS.ID_SUB_AREA = '" + subarea + "'";
                }
            }

            if (tipdoc != "-1") {
                if (querymain != starterquerymain) {
                    querymain += " AND ";
                }
                querymain += "PAQ.ID_TIPO_OBJETO = " + tipdoc;
            }

            if (sucursal != "-1") {
                if (querymain != starterquerymain) {
                    querymain += " AND ";
                }
                querymain += "PAQ.ID_SUCURSAL = RPAD('" + sucursal + "',5)";
            }
            //TODO BUSQUEDA DE DESTINATARIO
            if (!!destinatario) {
                if (querymain != starterquerymain) {
                    querymain += " AND ";
                }
                querymain += "paq.id_destinatario = (select id from tracking.destinatarios des where trim(des.codigo) = '" +
						$("#ftdestinatario").attr("codigo") + "' and ((des.id_tipo = (select id from tracking.tipo_destinatarios tip where tip.codigo = '"
						+ $("#ftdestinatario").attr("tipodest") + "' and tip.tipo_persona = '"
						+ $("#ftdestinatario").attr("tipopersona") + "')) or ('" + $("#ftdestinatario").attr("tipodest") + "' IN ('PR','IN'))))";
            }

            if (query != starterq) {
                query += ") p left outer join tracking.paquete a on a.codigo_orden like p.codigo_orden||'%')";
            } else {
                query = "";
            }

            if (querymain == starterquerymain) {
                querymain = "";
            }
            /*
            if (tipdoc != nul && tipdoc != "") {
            query += "X = " + tipdoc;
            }

            if (mensajeros != nul && mensajeros != "") {
            query += "X = " + mensajeros;
            }

            if (estpaquete != nul && estpaquete != "") {
            query += "X = " + estpaquete;
            }

            if (tipprod != nul && tipprod != "") {
            query += "X = " + tipprod;
            }

            if (area != nul && area != "") {
            query += "X = " + area;
            }

            if (subarea != nul && subarea != "") {
            query += "X = " + subarea;
            }

            if (sucursal != nul && sucursal != "") {
            query += "X = " + sucursal;
            }

            if (user != nul && user != "") {
            query += "X = " + user;
            }

            if (destinatario != nul && destinatario != "") {
            query += "X = " + destinatario;
            }
            */

            var stringurl = urlreportemas;
            if (!!query) {
                stringurl += "&FILTER= " + query;
            } else {
                stringurl += "&FILTER= ";
            }
            if (!!querymain) {
                stringurl += "&FILTERMAIN=%20" + querymain;
            } else {
                stringurl += "&FILTERMAIN=%20";
            }
            //var stringurl = urlreporte +  "&FILTER= " + query + "&FILTERMAIN= " + querymain;// +  "&FILTER=and " + filter;  
            $("#frameReport").attr('src', stringurl);
        });

        //Funcion para limpiar el formulario
        $("#btncleanreport").click(function () {
            $("#ftfecdesde").val("");
            $("#ftfechasta").val("");
            $("#fttipdoc").val("-1");
            $("#ftmensajeros").val("-1");
            $("#ftestpaquete").val("-1");
            $("#fttipprod").val("-1");
            $("#ftarea").val("-1");
            $("#ftsubarea").val("-1");
            $("#ftsucursal").val("-1");
            $("#ftuser").val("");
            $("#ftdestinatario").val("");
        });
        $("#ftdestinatario").autocomplete({
            minLength: 3,
            delay: 600,
            source: function (request, response) {
                var term = request.term;
                response(searchReceiver(term));
                return;
            },
            focus: function (event, ui) {
                return false;
            },
            select: function (event, ui) {
                //var div = $("#crearPaquete");
                $(this).val(ui.item.nombre + " (" + $.trim(ui.item.codigo) + ")");
                if (($.trim(ui.item.codtipoinformante) == 'DE') || ($.trim(ui.item.codtipoinformante) == 'TI') || ($.trim(ui.item.codtipoinformante) == 'CO')) {
                    $(this).attr('codigo', $.trim(ui.item.consecutivo));
                    $(this).attr('tipodest', $.trim(ui.item.codtipoinformante));
                    $(this).attr('tipopersona', $.trim(ui.item.tipopersona));
                } else if (($.trim(ui.item.codtipoinformante) == 'PR') || ($.trim(ui.item.codtipoinformante) == 'IN') || ($.trim(ui.item.codtipoinformante) == 'PV') || ($.trim(ui.item.codtipoinformante) == 'CM')) {
                    $(this).attr('codigo', $.trim(ui.item.codigo));
                    $(this).attr('tipodest', $.trim(ui.item.codtipoinformante));
                    $(this).attr('tipopersona', $.trim(ui.item.tipopersona));
                }
                //div.find(".control#nombre").val(ui.item.nombre);
                //div.attr("receiver", JSON.stringify(ui.item));
                //cambioDireccionPaq("crearPaquete", ui.item.consecutivo, ui.item.codtipoinformante, ui.item.tipopersona);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            if (item.nuevocte) {
                return $("<li>").append("<a><br><hr>NUEVO CLIENTE</a>").appendTo(ul).css({ "font-size": "8pt", "font-weight": "bold", "text-align": "center" });
            } else {
                return $("<li>")
                .append("<a>" + item.nombre + "(" + item.tipoinformante + ")<br>" + $.trim(item.tipoid) + " - " + $.trim(item.identificacion) + "</a>")
                .appendTo(ul).css("font-size", "8pt");
            }
        };
        $('#reporte1').show();
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="reporte1">
			<table id="tabreporte1">
				<tr>
					<td>
						Fecha Desde
					</td>
					<td>
						<input type="text" id="ftfecdesde" dbvalue="ftfecdesde" />
					</td>
					<td class="cellSeparator">
					</td>
					<td>
						Fecha Hasta
					</td>
					<td>
						<input type="text" id="ftfechasta" dbvalue="ftfechasta" />
					</td>
				</tr>
				<tr>
					<td>
						Tipo Documento
					</td>
					<td>
						<select id="fttipdoc" dbvalue="fttipdoc">
							<!--<option value=" ">Seleccionar</option>-->
						</select>
					</td>
					<td class="cellSeparator">
					</td>
					<td>
						Usuario
					</td>
					<td>
						<input type="text" id="ftuser" dbvalue="ftuser" />
					</td>
					<!--<td>
						Mensajeros
					</td>
					<td>
						<select id="ftmensajeros" dbvalue="ftmensajeros">
							<option value=" ">Seleccionar</option>
						</select>
					</td>-->
				</tr>
				<tr>
					<td>
						&Aacute;rea
					</td>
					<td>
						<select id="ftarea" dbvalue="ftarea">
							<!--<option value=" ">Seleccionar</option>-->
						</select>
					</td>
					<td class="cellSeparator">
					<td>
						Estado Paquete
					</td>
					<td>
						<select id="ftestpaquete" dbvalue="ftestpaquete">
							<!--<option value=" ">Seleccionar</option>-->
						</select>
					</td>
				</tr>
				<tr>
					<!--<td>
						Sub-&aacute;rea
					</td>
					<td>
						<select id="ftsubarea" dbvalue="ftsubarea">
							<option value=" ">Seleccionar</option>
						</select>
					</td>
					<td class="cellSeparator">
					</td>-->
					<td>
						Destinatario
					</td>
					<td>	
						<input type="text" id="ftdestinatario" dbvalue="ftdestinatario" style="width:500px;" />
					</td>
					</td>
					<td class="cellSeparator">
					<td>
						Sucursal
					</td>
					<td>
						<select id="ftsucursal" dbvalue="ftsucursal">
							<!--<option value=" ">Seleccionar</option>-->
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<button id="btngenreport">Detalle</button>
					</td>
					<td>
						<button id="btngenmasreport">Estadistico</button>
					</td>
					<td>
						<button id="btncleanreport">Limpiar</button>
					</td>
					<td class="cellSeparator">
					</td>
					<td>
					</td>
				</tr>
			</table>
		</div>
<div>
    <iframe id="frameReport" src="" width="900px" height="300px"></iframe>
</div>
</asp:Content>
