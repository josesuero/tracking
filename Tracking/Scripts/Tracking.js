﻿
// MUESTRA UN MENSAJE( EL QUE SE ENVIE COMO PARAMETRO ) DE INFORMACION AL USUARIO
function messageBox(mensaje) {
    $("#messageBox").find("#mensaje").html(mensaje);
    $("#messageBox").dialog("open");
}

// LA FUNCION INTERACTUA CON UN AUTOCOMPLETE. RECIBE LO ESCRITO EN EL, Y RETORNA DATA CORRESPONDIENTE A ESO
function searchReceiver(filter) {
    var data = [];
    data = WebSvcTrackingSoapHttpPort_prcBusquedainf(filter, "").outinformantesOut;
    // SE LIMITA A QUE SOLO SEA UN ARREGLO DE 10 POSICIONES
    data = data.slice(0, 10);    
    data[data.length] = { nuevocte: true };
    return data;
}

// RECIBE EL CODIGO DE DESTINATARIO Y EL TIPO PERSONA Y APARTIR DE AHI RETORNA LA DIRECCION QUE TRAE EL WS DE DIRECCIONES
function searchDirections(codigo, cod_tipo, tipopersona, source) {

    switch (source) {
        case 'ws':
            return WebSvcTrackingSoapHttpPort_prcDirtrack(codigo, tipopersona, 0, 0, 0, cod_tipo, 0).outdireccionesOut;
            break;
        case 'tracking':
            return ajaxrequest("getDirDest", { cod: codigo, tipo: cod_tipo, tipo_persona: tipopersona });
            break;
    }
}

// COMPARA LAS DIRECCIONES RETORNADAS DEL WEBSERVICES DE BOLIVAR LA COMPARA Y SOLO COLOCA LAS DIFERENTES A LA QUE
// YA ESTAN EN TRACKING
function moreDirections(dirTracking, dirWs) {

    if (dirTracking.length <= 0) {
        return dirWs;
    }
    
    var pos1;
    var pos2;
    var data = [];
    
    //    for (var i = 0; i < dirTracking.length; i++) {
    //        data[data.length] = dirTracking[i];
    //    } 

    for (var i = 0; i < dirTracking.length; i++) {
        pos1 = dirTracking[i];
        for (var j = 0; j < dirWs.length; j++) {
            pos2 = dirWs[j];
            if (compareDirection(pos1, pos2) == 0) {
                data[data.length] = pos2;
            }
            dirWs.splice(j, 1);
            j = j - 1;
        }
    }
    pos1 = null;pos2 = null;
    return data;
}

// COMPARA UNA DIRECCION DE TRACKING CON UNA EXTERNA
function compareDirection(dirTracking, dirWs) {
    if (
        dirTracking["codbarrio"].toString() == dirWs["codbarrio"] &&
        dirTracking["direccion"] == dirWs["direccion"]
    ) {
        // SI NO HAY DIFERENCIA RETORNA 1
        return 1;
    } else {
        // SI HAY DIFERENCIA RETORNA 0
        return 0;
    }
}

// VERIFICA EL PAQUETE QUE SE DESEA AGREGAR A LA TABLA DE PAQUETES QUE SE VAN A RECIBIR
// ENTONCES SI EL PAQUETE YA ESTA EN LA LISTA RETORNA UN MENSAJE Y NO LO AGREGA
function verificadorPaquetes(data, paquete) { 
    for(var i = 0; i < data.length; i++){
        if (data[i].CODIGO_ORDEN == paquete.CODIGO || data[i].codigo == paquete.CODIGO) {
            messageBox("Este paquete o documento " + data[i].CODIGO_ORDEN + " ya existe en la lista de los paquetes que seran recibidos.");
            return 0;
        }
    }
    return 1;
}

// RETORNA UN JSON CON LA DIRECCION DEL PAQUETE
function chosenDirection(div) {
    var data = {};
    
    // SI ES NUEVO CREA UN JSON CON LOS DATOS DEL FORM, SINO CONSIGUE EL JSON DE LA FILA DE LA TABLA
    if (div.find("#nuevDirChk").prop("checked")) {

        var result;
        var index = div.find("#provincia option:selected").index() - 1;
        result = validate_fields(index, "provincia");
        if(result.error){
            return result;
        }
        var dir = dataTracking.provincias[index];
        data["codprovincia"] = dir["codprovincia"];
        data["codsucursal"] = dir["codsucursal"];

        data["provincia"] = dir["provincia"];
        data["sucursal"] = dir["sucursal"];

        index = div.find("#ciudad option:selected").index() - 1;
        result = validate_fields(index, "ciudad");
        if (result.error) {
            return result;
        }
        dir = dir.ciudades[index];

        data["ciudad"] = dir["ciudad"];
        data["codciudad"] = dir["codciudad"];

        index = div.find("#barrio option:selected").index() - 1;
        result = validate_fields(index, "barrio");
        if (result.error) {
            return result;
        }
        dir = dir.sectores[index];

        data["barrio"] = dir["barrio"];
        data["codbarrio"] = dir["codbarrio"];
        var calle = "";
        if(div.find("#numero").val() != ""){
            calle += " #" + div.find("#numero").val();
        }
        if(div.find("#edificio").val() != ""){
            calle += " EDIF. " + div.find("#edificio").val();
            if(div.find("#apartamento").val() != ""){
                calle += " APTO. " + div.find("#apartamento").val();
            }
        }
        data["direccion"] = div.find("#direccion").val() + calle;
        result = validate_fields(data["direccion"], "calle, numero, edificio y apartamento");
        if (result.error) {
            return result;
        }
        data["codtipodireccion"] = "-1";
        debugger;
        data["telefono"] = div.find("#telefono").val();

        calle = null;

        var direccionObj = {};
        data["send_dir"] = 0;
        
        if (!div.find("#envNuevaDir").is(":checked")) {

            direccionObj["stelefono"] = data["telefono"];

            direccionObj["sdireccion"] = data["direccion"];
            direccionObj["scodsucursal"] = data["codsucursal"];
            direccionObj["scodciudad"] = data["codciudad"];
            direccionObj["scodbarrio"] = data["codbarrio"];
            //direccionesObj["scodprovincia"] = data["codprovincia"];         

            direccionObj["scodtipoubicacion"] = " ";
            direccionObj["scodareatelefono1"] = " ";
            direccionObj["semailfacturas"] = " ";
            direccionObj["semailcobros"] = " ";
            direccionObj["scodareatelefono2"] = " ";
            direccionObj["sbeeper"] = " ";
            direccionObj["scodareatelefono3"] = " ";
            direccionObj["semailcomision"] = " ";
            direccionObj["snombreubicacion"] = " ";
            direccionObj["scelular"] = " ";
            direccionObj["scodtipodireccion"] = "1";
            direccionObj["sdecripcion"] = " ";
            data["send_dir"] = 1;
        }

        data.nota = div.find(".control#nota").val();
        data.direccion_obj = direccionObj  
        index = null; dir = null;
    } else if (div.find("input:checked").length > 0) {
        data = JSON.parse(div.find("input:checked").parent().parent().attr("data"));
        data.nota = div.find(".control#nota").val(); 
    } else {
        data = null;
    }
    return data;
}

// INSERTA NOTA AL ULTIMO HISTORIAL
function logInsert(pkg, data, id_last_hist, note_doc) {
    ajaxrequest("newNote", { package: pkg, note: data, id_last_hist: id_last_hist, note_doc: note_doc });
}

// RECIBE COMO PARAMETRO JSON DE PAQUETE, Y RETORNA TODO EL HISTORIAL DE ESE PAQUETE
function getHistorialPkg(pkg) {
    return ajaxrequest("getLogPkg", { package: pkg });
}

// RECIBE COMO PARAMETRO EL ID_HISTORIAL Y RETORNA LAS NOTAS
function getNotas(id_hist) {
    return ajaxrequest("getNotas", { id_hist: id_hist });
}

// LIMPIA LA TABLA DE HISTORIAL ENVIADA QUE SE ENVIA COMO PARAMETRO A LA FUNCION
function cleanHistorial(tbl) {
    tbl.removeAttr();
    tbl.children("tbody").children().remove();
    var div = tbl.parent().parent().parent();
    var divNote = div.find("#divHistorialComment");
    divNote.addClass("hide");
    div.find("#divHistorialPaqAct").removeClass("histToLeft");    
    divNote.find("#tbNotes").children("tbody").children().remove();
    divNote = null;
    div = null;
}

// CARGA DATA DE ESTATUS AL DROPDOWN DE LA PANTALLA DE RECIBIR
function fillDropDownRecibir(estatus) {

    var dropdown = $("#divRecibirPaq").find("#superUser");
    var options = "<option value=" + dataTracking.user.id + ">" + dataTracking.user.outnombreOut + "</option>";
    if (estatus == 5) {
        dropdown.html(options);
    } else {
        for (var i = 0; i < dataTracking.user.outcolaboradoresOut.length; i++) {
            options += "<option value=" + dataTracking.user.outcolaboradoresOut[i].accusrcod + ">" + dataTracking.user.outcolaboradoresOut[i].accusrnom + "</option>";    
        }
        dropdown.html(options);
    }
    dropdown = null; options = null;
}
// CARGA EL HISTORIAL DE UN PAQUETE
// PARAMETROS TBL = LA TABLA DONDE SE CARGARA EL HISTORIAL, smartTb = tbsmart donde se encuentra los paquete, div = div contenedor de las notas.
function fillHistorial(tbl, data, indexPkg) {

    tbl.attr("pkgIndex", indexPkg);
    if (data.length != 0) {
        tbl.attr("ID_Lasthist", data[0].ID);            
    }else{
        tbl.attr("ID_Lasthist", -1);
    }    
    var div = tbl.parent().parent().parent();
    var tbody = tbl.children("tbody");
    if (!tbl.parent().parent().parent().find("#divHistorialPaqAct").hasClass("histToLeft")) {
        tbl.css("width", tbl.parent().width() - 20 + "px");
    }
    tbody.empty();
    var pos;
    var tr;
    // EN ESTE FOR SE AGREGA LOS REGISTRO DE HISTORIAL PERTENECIENTE AL PAQUETE
    for (var i = 0; i < data.length; i++) {
        pos = data[i];
        tr = $("<tr></tr>");
        tr.append($("<td></td>").append(pos.STATUS_NOM));
        tr.append($("<td></td>").append(pos.AREA));
        //tr.append($("<td></td>").append(pos.FECHA_MODIFICADO));
        tr.append($("<td></td>").append(pos.NOMBRE_USR));
        tr.append($("<td></td>").append(pos.FECHA));
        tr.append($("<td></td>").append(pos.NOTAS.length));

        //        if (pos.ACTIVO == 1) {
        //            div.find("#NOTES").val(pos.NOTES);
        //            tr.addClass("rowSelected");
        //        }
        tr.mouseenter(function () {
            $(this).addClass('mouseEnter');
        });
        tr.mouseleave(function () {
            $(this).removeClass('mouseEnter');
        });
        // CUANDO SE HACE CLICK A LA FILA SE AGREGAN LAS NOTAS AL CAMPO
        // IMPORTANTE: PARA QUE SE AGREGEN LAS NOTAS CUANDO SE SELECCIONE LA FILA, DEBE EXISTIR UN TXTAREA CON ID='NOTES' DENTRO DEL PARAMETRO DIV
        tr.attr("data", JSON.stringify(pos)).click(function () {
            var tbody = $(this).parent();
            // IMPORTANTE: NO CAMBIAR ESTA ESTRUCTURA EN HTML
            var div = tbody.parent().parent().parent().parent();
            var divNote = div.find("#divHistorialComment");
            if (!$(this).hasClass("rowSelected")) {
                tbody.children().each(function () {
                    $(this).removeClass("rowSelected");
                });
                $(this).addClass("rowSelected");

                //var notes = getNotas(JSON.parse($(this).attr("data")).ID);
                fillNote(divNote.find("#tbNotes"), JSON.parse($(this).attr("data")).NOTAS);
                div.find("#divHistorialPaqAct").addClass("histToLeft");
                div.find("#divHistorialComment").removeClass("hide");
            } else {
                $(this).removeClass("rowSelected");
                div.find("#divHistorialPaqAct").removeClass("histToLeft");
                divNote.addClass("hide");
            }

        });
        tbody.append(tr);
    }
    tr = null; tbody = null; pos = null;div = null;
}

// CARGA LAS NOTAS DE UN HISTORIAL
function fillNote(tbl, data) {
    var tbody = tbl.children("tbody");
    tbody.children().remove();
    var pos;
    var tr;
    // EN ESTE FOR SE AGREGA LAS NOTAS PERTENECIENTE AL HISTORIAL SELECCIONADO
    for (var i = 0; i < data.length; i++) {
        pos = data[i];
        tr = $("<tr></tr>");
        tr.append($("<td></td>").append(pos.NOTA));
        tr.append($("<td></td>").append(pos.USRNAME));
        tr.append($("<td></td>").append(pos.FECHA));
        tr.mouseenter(function () {
            $(this).addClass('mouseEnterView');
        });
        tr.mouseleave(function () {
            $(this).removeClass('mouseEnterView');
        });
        tr.attr("data", JSON.stringify(pos));
        tbody.append(tr);
    }
    tr = null; tbody = null; pos = null;
}

// NO SE ESTA USANDO LA FUNCION
//RETORNA UN ARREGLO DE LA JERARQUIA DE PAQUETES CORRESPONDIENTE AL INDEX ENVIADO A LA FUNCION
function getPackByIndex(index) {
    var indexArray = index.replace("-1,", "").split(",");
    var pos = paquetes[indexArray[0]];
    //EMPIESA EN 1 PORQUE LA POSICION 0 ES -1
    for (var i = 1; i < indexArray.length; i++) {
        pos = pos.childrens[indexArray[i]];
    }
    indexArray = null;
    return pos;
}


// RETORNA UN PAQUETE DE LA VARIABLE DE otrosPaquetes
// LA CUAL SOLO ES UTILIZADA PARA LA PANTALLA DE RECIBIR
function getOtherPackByIndex(index) {
    var indexArray = index.replace("-1,", "").split(",");
    var pos = otrosPaquetes[indexArray[0]];
    //EMPIESA EN 1 PORQUE LA POSICION 0 ES -1
    for (var i = 1; i < indexArray.length; i++) {
        pos = pos.childrens[indexArray[i]];
    }
    indexArray = null;
    return pos;
}


// PANTALLA PRINCIPAL

// REFRESCA LA TABLA PRINCIPAL DE PAQUETES
function loadPkgs() {
    paquetes = ajaxrequest("getPkg", { CODIGO_ORDEN: "" });
    var tbl = $("#divPaqActuales").find("#tbPaquetes");
    tbl.smartTable("reCreateTbl", paquetes);
    tbl.smartTable("fillTbody", paquetes);
    tbl.smartTable("addDataTable", paquetes);
    tbl = null;
}

// PROCESO DE RECIBIR PAQUETES
function recibirPaquetes(div) {
    var data = {};
    data["paquetes"] = div.find("#tbRecPaq").smartTable("option", "data");
    data["id_status"] = div.find("#statuspackage").val();
    data["id_user"] = div.find("#superUser").val();
    data["nota"] = div.find("#nota").val();
    
    var result = ajaxrequest("recibirPaquete", data);
    if (result) {
        registroCompletado(div)
    } 
    //loadPkgs();    
    data = null;result = null;
}

function registroCompletado(div) {
    comenzarNuevamente(div);
    // CAMBIA VALOR A VARIABLE VERIFICADORA DE CAMBIOS
    existenCambios = false;
    // CAMBIA VALOR A ATRIBUTO VERIFICADOR DEL CONTROL TAB,
    // PARA QUE CUANDO EL USUARIO SE COLOQUE EN EL TAB PRINCIPAL SE ACTUALIZEN LOS DATOS
    // NOTA: SE HACE DE ESTA MANERA PARA EVITAR PROBLEMA DE RENDIRIZACION DEL CONTROL SMARTTABLE, 
    // CAUSADO POR TABS DE JQUERY
    $("#tabsMain").attr("refresh", "true");
}

function comenzarNuevamente(form) {
    form.find("#CODIGO").val("");
    form.find("#tbRecPaq").smartTable("reCreateTbl");
    form.find("#tbRecPaq").smartTable("addDataTable");
    form.find("#nota").val("");
    form = null;
}

// FORMULARIO DE CREAR DOCUMENTO
function crearNuevoDocumento() {
    $("#nuevoDocumento").dialog("open");
}

// FORMULARIO DE CREAR PAQUETE
function openCrearPackage(data, index) {
    var div = $("#crearPaquete");
    div.attr("index", index);
    div.find("#tbDetPaquetes").smartTable("reCreateTbl");
    div.find("#tbDetPaquetes").smartTable("fillTbody", data);
    div.dialog("open");
    div.find("#tbDetPaquetes").smartTable("addDataTable");
    div = null;
}

// ENVIA DATOS DE DOCUMENTO NUEVO A SERVIDOR PARA EL PROCESO DE CREACION DE UN DOCUMENTO
function crearDocumento(div) {
    var data = {};
    data["controles"] = getDataForm(div.find("#datosPrincipales"));
    //SI EL PARAMETRO ID DIRECCION ES -1 SE ENVIARA LA DIRECCION PARA QUE SEA INSERTADA, EN CASO CONTRARIO SE VA A ENVIAR SOLO EL ID.
    var direction = JSON.parse(div.attr("direction"));
    if (!direction.id) {
        direction["id"] = -1;
    }
    data["receiver"] = JSON.parse(div.attr("receiver"));
    if (div.attr("destinatario_via")) {
        data["destinatario_via"] = JSON.parse(div.attr("destinatario_via"));
    }
    data["direction"] = direction;
    data.controles.NOTES = div.find(".control#NOTES").val();
    direction = null;
    return ajaxrequest("crearDocumento", data);

}

// CAMBIAR DESTINATARIO
function cambiarDestinatario(div, data) {
    //SI EL PARAMETRO ID DIRECCION ES -1 SE ENVIARA LA DIRECCION PARA QUE SEA INSERTADA, EN CASO CONTRARIO SE VA A ENVIAR SOLO EL ID.
    data["receiver"] = JSON.parse(div.attr("receiver"));
    if (div.attr("destinatario_via")) {
        data["destinatario_via"] = JSON.parse(div.attr("destinatario_via"));
    }    
    data["direction"] = JSON.parse(div.attr("direction"));
    data["notes"] = div.find(".control#NOTES").val();
    return ajaxrequest("cambioDestinatario", data);
}

// AGRUPA DOCUMENTOS O PAQUETES
function crearPaquete(div) {

    return ajaxrequest("crearPaquete", { childrens: div.find("#tbDetPaquetes").smartTable("option", "data"), nota: div.find("#nota").val() });   

}

// MAIN PAQUETE, PANTALLA DE DETALLE DE UN PAQUETE

// BUSCA LA DATA DE DETALLE AL SERVIDOR
function getDetPkg(data) {
    return ajaxrequest("getDetPkg", data);    
}

// FUNCION 
function getPkgFromOthers(data) {
    return ajaxrequest("getPkgFromOthers", data);
}

// FORMULARIO DE PAQUETE
function createDialogDetOtherPaq(row) {
    // SE CREA UN ID CON EL INDEX Y SIN LAS COMAS DEL INDEX DEL PAQUETE PARA EL DIALOG 
    var index = row.attr('INDEX');
    var div = $("<div id='mainPaquete" + index.replace(/,/g, '') + "' class='mainPaqueteOpen'></div>").append($('#mainPaquete').clone());
    div.attr("INDEX", index);
    div.dialog({
        title: "Paquete",
        autoOpen: false,
        modal: true,
        minWidth: 850,
        width: 930,
        minHeight: 745,
        height: 755,
        create: function () {
            var index = $(this).attr("INDEX");
            var toolBar = $(this).find("#paqueteTools");

            if (getOtherPackByIndex(index)["FINAL"] == 0) {
                $(this).find("#tabs").attr("tbexist", "false").attr("div", $(this).prop("id")).tabs({
                    active: 1
                    , activate: function (event, ui) {

                        if ($(this).attr("tbexist") == "true") {
                            return;
                        }
                        if ($(this).tabs("option", "active") != 0) {
                            return;
                        }
                        var div = $("#" + $(this).attr("div"));
                        // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                        fillHistorial(div.find('#tbHistorial'), JSON.parse($(this).attr("historial")), div.prop("id"));

                        $(this).attr("tbexist", "true");
                        data = null;
                        div = null; index = null;
                    }
                });
            } else {
                $(this).find("#tabs").attr("tbexist", "false").attr("div", $(this).prop("id")).tabs({
                    active: 0
                    , activate: function (event, ui) {

                        if ($(this).attr("tbexist") == "true") {
                            return;
                        }

                        var div = $("#" + $(this).attr("div"));
                        // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                        var index = div.attr("INDEX");
                        var data = getOtherPackByIndex(index);
                        createTbDetOtherPaqSmartTable(div, index, data.childrens, data["CODIGO_ORDEN"], data["ID_SUB_AREA"]);
                        div.find("#tbDetPaquetes").smartTable("addDataTable");
                        $(this).attr("tbexist", "true");
                        data = null;
                        div = null; index = null;

                    }
                });

            }


            toolBar.find("#verCliente").attr("div", $(this).prop("id")).click(function () {
                var div = $("#" + $(this).attr("div"));
                // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                var index = div.attr("INDEX");
                var data = getOtherPackByIndex(index)
                verCliente(data);
                data = null; index = null; div = null;
            });
            toolBar.find("#verDocumento").attr("div", $(this).prop("id")).click(function () {
                var div = $("#" + $(this).attr("div"));
                // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                var index = div.attr("INDEX");
                var data = getOtherPackByIndex(index)
                verDocumento(data);
                data = null; index = null; div = null;
            });

            toolBar.find("#solcCambioDir").hide();
            toolBar.find("#solcCambioDest").hide();

            toolBar.find("#btnPrint").hide();

            toolBar.find("#btnHome").click(function () {
                $(".mainPaqueteOpen").each(function () {
                    $(this).find("#tbDetPaquetes").smartTable("destroy");
                    $(this).find("#tbDetPaquetes").remove();
                    $(this).dialog("destroy");                           
                    $(this).remove();
                });
            });

            $(this).find("#btnAddComment").attr("div", $(this).prop("id")).hide();

            // TOGGLE DE H3 DE DATOS DE DIRECCION
            $(this).find('#toggleDireccion')
            .mouseenter(function () {
                // Handle mouseenter...
                $(this).addClass('mouseOverTag');
            })
            .mouseleave(function () {
                // Handle mouseleave...
                $(this).removeClass('mouseOverTag');
            })
            .attr("div", $(this).prop("id")).click(function () {
                var div = $("#" + $(this).attr("div"));
                if (!div.find("#contentDatosDireccion").is(":visible")) {
                    div.find("#contentDatosDireccion").show();
                    div.find("#tbDetPaquetes").smartTable('resize', $(this).attr("size_tabla"));
                    div.find("#divTbHistorialPaqAct").css("height", $(this).attr("size_historial") + "px");
                    div.find("#divNotes").css("height", $(this).attr("size_historial") + "px");

                } else {
                    div.find("#contentDatosDireccion").hide();
                    div.find("#tbDetPaquetes").smartTable('resize', parseInt($(this).attr("size_tabla")) + 100);
                    div.find("#divTbHistorialPaqAct").css("height", (parseInt($(this).attr("size_historial")) + 100) + "px");
                    div.find("#divNotes").css("height", $(this).attr("size_historial") + "px");

                }
                div = null;

            });

            index = null;
            toolBar = null;
        },
        open: function () {

            var index = $(this).attr("INDEX");
            var data = getOtherPackByIndex(index);
            var detalle = getDetPkg(data);

            data.DEST_ID = detalle["dest_id"];
            data.DEST_ID_TIPO = detalle["dest_id_tipo"];
            data.DEST_COD = detalle["dest_cod"];
            data.childrens = detalle["childrens"];
            data.direccion = detalle["direccion"];
            $(this).attr("CODIGO_ORDEN", data["CODIGO_ORDEN"])

            createTbDetOtherPaqSmartTable($(this), index, data.childrens, data["CODIGO_ORDEN"], data["ID_SUB_AREA"]);
            $(this).find("#tbDetPaquetes").smartTable("addDataTable");

            buildOpenMainPaquete($(this), data, detalle, index, true);

            index = null;
            data = null;
            detalle = null;

        },
        close: function (event, ui) {
            $(this).find("#tbDetPaquetes").smartTable("destroy");
            $(this).find("#tbDetPaquetes").remove();
            $(this).dialog("destroy");
            $(this).remove();
        }
    });
    div.dialog('open');
    index = null; div = null;
}

// FORMULARIO DE PAQUETE
function createDialogDetPaq(row, indexPaq) {
    // SE CREA UN ID CON EL INDEX Y SIN LAS COMAS DEL INDEX DEL PAQUETE PARA EL DIALOG
    var index = null;
    if (indexPaq == undefined) {
        indexPaq = -1;
        index = row.attr('INDEX');
    } else {
        index = row;
    }    
    //var data = getPackByIndex(index);
    var div = $("<div id='mainPaquete" + index.replace(/,/g, '') + "' class='mainPaqueteOpen'></div>").append($('#mainPaquete').clone());
    div.attr("INDEX", index).attr("INDEX_PAQ", indexPaq);
    div.dialog({
        title: "Paquete",
        autoOpen: false,
        modal: true,
        minWidth: 850,
        width: 930,
        minHeight: 740,
        height: 755,
        create: function () {
            var index = $(this).attr("INDEX");
            var toolBar = $(this).find("#paqueteTools");

            if (getPackByIndex(index)["FINAL"] == 0) {
                $(this).find("#tabs").attr("tbexist", "false").attr("div", $(this).prop("id")).tabs({
                    active: 1
                    , activate: function (event, ui) {

                        if ($(this).attr("tbexist") == "true") {
                            return;
                        }
                        if ($(this).tabs("option", "active") != 0) {
                            return;
                        }
                        var div = $("#" + $(this).attr("div"));
                        // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                        fillHistorial(div.find('#tbHistorial'), JSON.parse($(this).attr("historial")), div.prop("id"));
                        $(this).attr("tbexist", "true");
                        data = null;
                        div = null; index = null;
                    }
                });
            } else {
                $(this).find("#tabs").attr("tbexist", "false").attr("div", $(this).prop("id")).tabs({
                    active: 0
                    , activate: function (event, ui) {

                        if ($(this).attr("tbexist") == "true") {
                            return;
                        }
                        var div = $("#" + $(this).attr("div"));
                        // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                        var index = div.attr("INDEX");
                        var data = getPackByIndex(index);
                        createTbDetPaqSmartTable(div, index, data.childrens, data["CODIGO_ORDEN"], data["ID_SUB_AREA"]);
                        div.find("#tbDetPaquetes").smartTable("addDataTable");
                        $(this).attr("tbexist", "true");
                        data = null;
                        div = null; index = null;

                    }
                });

            }

            toolBar.find("#verCliente").attr("div", $(this).prop("id")).click(function () {
                var div = $("#" + $(this).attr("div"));
                // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                var index = div.attr("INDEX");
                var data = getPackByIndex(index);
                verCliente(data);
                data = null; index = null; div = null;
            });
            toolBar.find("#verDocumento").attr("div", $(this).prop("id")).click(function () {
                var div = $("#" + $(this).attr("div"));
                // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                var index = div.attr("INDEX");
                var data = getPackByIndex(index);
                verDocumento(data);
                data = null; index = null; div = null;
            });
            toolBar.find("#solcCambioDir").attr("div", $(this).prop("id")).click(function () {
                // SE LE AGREGA EL ID DEL DIALOG DINAMICO COMO PARAMETRO AL DIALOG DE DIRECCIONES                
                $("#mainCambioDireccion").attr("divIdFrom", $(this).attr("div"));
                // BUSCA EL DIV CREADO DINAMICAMENTE
                var div = $("#" + $(this).attr("div"));
                // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                var index = div.attr("INDEX");
                var data = getPackByIndex(index);
                cambioDireccionPaq("mainPaquete", data.DEST_COD, data.DEST_TIPO_COD, data.DEST_TIPO_PERSONA, data.cambiar_direccion);
                data = null; index = null; div = null;
            });

            toolBar.find("#solcCambioDest").attr("div", $(this).prop("id")).click(function () {
                // SE LE AGREGA EL ID DEL DIALOG DINAMICO COMO PARAMETRO AL DIALOG DE DESTINATARIO                
                $("#cambioDestinatario").attr("divIdFrom", $(this).attr("div"));
                // BUSCA EL DIV CREADO DINAMICAMENTE
                var div = $("#" + $(this).attr("div"));
                // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                $("#cambioDestinatario").attr("divINDEX", div.attr("INDEX"));
                var index = div.attr("INDEX");
                var data = getPackByIndex(index);
                cambioDestinatario("mainPaquete", data);
                data = null; index = null; div = null;

            });


            // EVENTO CLICK DE BOTON DE IMPRIMIR
            toolBar.find("#btnPrint").attr("div", $(this).prop("id")).click(function () {
                var div = $("#" + $(this).attr("div"));
                var index = div.attr("INDEX");
                var data_ = getPackByIndex(index);
                var data = {};
                if (data_["ENTREGADO"] == 1 || data_.AREAS != 1 || (data_.ID_SUB_AREA != dataTracking.user.outsubareacodOut && data_.AREAS == 1)) {
                    messageBox("No es posible realizar la impresion del paquete o documento, por que ha sido entregado o no esta en su departamento.");
                    return;
                }
                data["CODIGO_ORDEN"] = data_.CODIGO_ORDEN;
                data["IMPRESION"] = 1;
                data["TIPO_IMPRESION"] = 0;
                data["NIVEL"] = 0;
                var id_report = ajaxrequest("process_Print", data);
                if (id_report["ERROR"] == 1) {
                    messageBox(id_report["MESSAGE"]);
                } else {
                    callReportPdf(reporteLabelUrl, id_report["id_report"]);
                }

                data = null; data_ = null; index = null; div = null; id_report = null;
            });

            // EVENTO CLICK DE BOTON DE INICIO
            toolBar.find("#btnHome").click(function () {
                // BUSCA TODAS LAS PANTALLAS ABIERTAS DE PAQUETE Y LA CIERRA
                $(".mainPaqueteOpen").each(function () {
                    $(this).find("#tbDetPaquetes").smartTable("destroy");
                    $(this).find("#tbDetPaquetes").remove();
                    $(this).dialog("destroy");
                    $(this).remove();
                });

                // CARGA LOS PAQUETES DE LA PANTALLA PRINCIPAL
                //loadPkgs();
            });

            // AGREGA FUNCION CLICK A BOTON DE ANADIR COMENTARIO EN LA NOTA
            $(this).find("#btnAddComment").attr("div", $(this).prop("id")).click(function () {
                // BUSCA EL DIV CREADO DINAMICAMENTE
                var div = $("#" + $(this).attr("div"));
                // CONSIGUE EL INDEX Y BUSCA EL PAQUETE CORRESPODIENTE A EL
                // BUSCA LA FILA QUE EN ESE MOMENTO ESTA SELECCIONADA
                anadirComentario(
                getPackByIndex(div.attr("INDEX")),
                 $(this).attr("div")
                 , div.find("#tbHistorial").attr("ID_Lasthist")
                );
                data = null;
                index = null;
                div = null;
            });

            // TOGGLE DE H3 DE DATOS DE DIRECCION
            $(this).find('#toggleDireccion')
            .mouseenter(function () {
                // Handle mouseenter...
                $(this).addClass('mouseOverTag');
            })
            .mouseleave(function () {
                // Handle mouseleave...
                $(this).removeClass('mouseOverTag');
            })
            .attr("div", $(this).prop("id")).click(function () {
                var div = $("#" + $(this).attr("div"));
                if (!div.find("#contentDatosDireccion").is(":visible")) {
                    div.find("#contentDatosDireccion").show();
                    div.find("#tbDetPaquetes").smartTable('resize', $(this).attr("size_tabla"));
                    div.find("#divTbHistorialPaqAct").css("height", $(this).attr("size_historial") + "px");
                    div.find("#divNotes").css("height", $(this).attr("size_historial") + "px");

                } else {
                    div.find("#contentDatosDireccion").hide();
                    div.find("#tbDetPaquetes").smartTable('resize', parseInt($(this).attr("size_tabla")) + 100);
                    div.find("#divTbHistorialPaqAct").css("height", (parseInt($(this).attr("size_historial")) + 100) + "px");
                    div.find("#divNotes").css("height", $(this).attr("size_historial") + "px");

                }
                div = null;

            });
            toolBar = null; index = null;
        },
        open: function () {

            var index = $(this).attr("INDEX");
            var data = getPackByIndex(index);
            var detalle = getDetPkg(data);

            data.DEST_ID = detalle["dest_id"];
            data.DEST_ID_TIPO = detalle["dest_id_tipo"];
            data.DEST_COD = detalle["dest_cod"];
            data.childrens = detalle["childrens"];
            data.direccion = detalle["direccion"];
            $(this).attr("CODIGO_ORDEN", data["CODIGO_ORDEN"]);
            createTbDetPaqSmartTable($(this), index, data.childrens, data["CODIGO_ORDEN"], data["ID_SUB_AREA"], data["ENTREGADO"]);
            $(this).find("#tbDetPaquetes").smartTable("addDataTable");

            buildOpenMainPaquete($(this), data, detalle, index, false);
            index = null;
            data = null;
            detalle = null;

        },
        close: function (event, ui) {
            $(this).dialog("destroy");
            $(this).empty();

            if ($(".mainPaqueteOpen").length == 0 && creacionPaquete == 1) {
                loadPkgs();
                $("#divPaqActuales").find(".searchSmartTable").val("");
                creacionPaquete = 0;
            }
            var indexpaq = $(this).attr("INDEX_PAQ");
            if (indexpaq != -1) {
                paquetes.slice(indexpaq, indexpaq + 1);
            }
            indexpaq = null; $(this).remove();
        }
    });
    div.dialog('open');
    index = null; div = null;
}

// CREA TABLA CON LOS PAQUETES HIJOS EN LA PANTALLA DE DETALLE DE PAQUETE
function createTbDetPaqSmartTable(div, index, data, codigo, id_sub_area, entregado) {
    var columns = [
            {
                key: "CODIGO",
                label: "Codigo Paquete",
                width: "13%",
                events: []
            }, {
                key: "TIPO_OBJETO",
                label: "Tipo Documento",
                width: "10%",
                events: []
            }, {
                key: "NUM_REF",
                label: "No. Referencia",
                width: "10%",
                events: []
            }, {
                key: "DEPT_UBICACION",
                label: "Area",
                width: "15%",
                events: []
            },
            {
                key: "STATUS_NOM",
                label: "Estado",
                width: "10%",
                events: []
            },
            {
                key: "DEST_TIPO_NOM",
                label: "Tipo Destinatario",
                width: "15%",
                events: []
            },
            {
                key: "DEST_COD",
                label: "Codigo Destinatario",
                width: "10%",
                events: []
            },
            {
                key: "DEST_NAME",
                label: "Nombre Destinatario",
                width: "15%",
                events: []
            },
            {
                key: "DOCUMENTOS",
                label: "Cantidad de Documentos",
                width: "10%",
                events: []
            }
        ];

    var toolBox = [{
        type: "IMPRIMIR",
        event: ""
    }, {
        type: "SEARCH",
        event: ""
    }];

    if (entregado == 0) {
        div.find("#tbDetPaquetes")
        .smartTable(
            { columns: columns,
                toolBox: toolBox, height: 80,
                index: index, data: data,
                id_sub_area: id_sub_area,
                dataTable: false,
                searchVar: true, codigo: codigo,
                rowDblClick: function (evt, smartb) {
                    createDialogDetPaq(smartb.row);
                }
            }
        );
    } else {
            columns = [
            {
                key: "CODIGO",
                label: "Codigo Paquete",
                width: "13%",
                events: []
            }, {
                key: "TIPO_OBJETO",
                label: "Tipo Documento",
                width: "10%",
                events: []
            }, {
                key: "ID_OBJETO",
                label: "No. Referencia",
                width: "10%",
                events: []
            }, {
                key: "DEPT_UBICACION",
                label: "Area",
                width: "15%",
                events: []
            },
            {
                key: "STATUS_NOM",
                label: "Estado",
                width: "10%",
                events: []
            },
            {
                key: "DEST_TIPO_NOM",
                label: "Tipo Destinatario",
                width: "15%",
                events: []
            },
            {
                key: "DEST_COD",
                label: "Codigo Destinatario",
                width: "10%",
                events: []
            },
            {
                key: "DEST_NAME",
                label: "Nombre Destinatario",
                width: "15%",
                events: []
            },
            {
                key: "DOCUMENTOS",
                label: "Cantidad de Documentos",
                width: "10%",
                events: []
            }
        ];

        toolBox = [{
            type: "IMPRIMIR",
            event: ""
        }, {
            type: "SEARCH",
            event: ""
        }];
        div.find("#tbDetPaquetes")
        .smartTable({
            columns: columns, onlyView: true
                    , toolBox: toolBox, height: 80
                    , index: index, data: data
                    , id_sub_area: id_sub_area
                    , dataTable: false, classMouseView: "mouseEnter"
                    , searchView: true
                    , searchVar: true, codigo: codigo
                    , rowDblClickView: function (evt, smartb) {
                        createDialogDetPaq(smartb.row);
                    }
        });
    }
    toolBox = null;columns = null;
}

// CREA TABLA DEL PAQUETE QUE SE ESTA RECIBIENDO
// CON LOS PAQUETES HIJOS EN LA PANTALLA DE DETALLE DE PAQUETE
function createTbDetOtherPaqSmartTable(div, index, data, codigo, id_sub_area) {
    var columns = [
            {
                key: "CODIGO",
                label: "Codigo Paquete",
                width: "13%",
                events: []
            }, {
                key: "TIPO_OBJETO",
                label: "Tipo Documento",
                width: "10%",
                events: []
            }, {
                key: "ID_OBJETO",
                label: "No. Referencia",
                width: "10%",
                events: []
            }, {
                key: "DEPT_UBICACION",
                label: "Area",
                width: "15%",
                events: []
            },
            {
                key: "STATUS_NOM",
                label: "Estado",
                width: "10%",
                events: []
            },
            {
                key: "DEST_TIPO_NOM",
                label: "Tipo Destinatario",
                width: "15%",
                events: []
            },
            {
                key: "DEST_COD",
                label: "Codigo Destinatario",
                width: "10%",
                events: []
            },
            {
                key: "DEST_NAME",
                label: "Nombre Destinatario",
                width: "15%",
                events: []
            },
            {
                key: "DOCUMENTOS",
                label: "Cantidad de Documentos",
                width: "10%",
                events: []
            }
        ];

    var toolBox = [{
        type: "IMPRIMIR",
        event: ""
    }, {
        type: "SEARCH",
        event: ""
    }];
    div.find("#tbDetPaquetes")
    .smartTable({
        columns: columns, onlyView: true
                , toolBox: toolBox, height: 80
                , index: index, data: data
                , id_sub_area: id_sub_area
                , dataTable: false, classMouseView: "mouseEnter"
                , searchView: true
                ,searchVar: true, codigo: codigo
                , rowDblClickView: function (evt, smartb) {
                    createDialogDetOtherPaq(smartb.row);
                }
    });
    toolBox = null; columns = null;
}


//VER DOCUMENTO
function verCliente(data) {
    $("#mainIframe").attr("jsonData", JSON.stringify(data)).dialog("open");
}

//VER DOCUMENTO
function verDocumento(data) {
    $("#mainIframeDocumento").attr("jsonData", JSON.stringify(data)).dialog("open");
}

// FORMULARIO DE CAMBIO DE DIRECCION
function cambioDireccionPaq(from, codigoDest, codTipoDest, tipoDest, cambiar_direccion) {
    $("#mainCambioDireccion")
    .attr("openFrom", from)
    .attr("codigo", codigoDest)
    .attr("codtipo", codTipoDest)
    .attr("tipopersona", tipoDest)
    .attr("cambiar_direccion", cambiar_direccion)
    .dialog("open");
}

// FORMULARIO DE CAMBIO DE DESTINATARIO
function cambioDestinatario(from, data) {
    $("#cambioDestinatario")
    .attr("openFrom", from)
    .attr("data", JSON.stringify(data))
    .dialog("open");
}


// FORMULARIO DE ANADIR COMENTARIO
function anadirComentario(data, from, ID_LAST_HIST) {
    $("#anadirComentario").attr("pkg", JSON.stringify(data)) // data del paquete
    .attr("from", from) // ID del div que contiene la tabla de historial
    .attr("ID_Lasthist", ID_LAST_HIST) // ID del ultimo historial del paquete
    .dialog("open");
}

// LIMITA CAMPO A UNA CANTIDAD ESPECIFICA DE CARACTERES
// SI AUN SE PUEDE ESCRIBIR RETORNA TRUE, EN CASO CONTRARIO FALSE
function lengthfield(field, length) {
    field.attr("lengthfield", length).keydown(function (e) {
        if ($(this).val().length <= $(this).attr("lengthfield") || e.keyCode == 8) {
            return true;
        } else {
            return false;
        }
    });
}

// RENDERIZA PANTALLA DE DIRECCION EXTERNAS
function wizardMoreDir() {
    var div = $("#mainCambioDireccion");
    div.find("#btnMoreDirecciones").hide();
    div.find("#btnBack").show();
    div.find("#btnNew").show();
    div.find("#titleDir").html("Mas Direcciones");
    div.find("#divMasDirecciones").show();
    div.find("#divTbDirecciones").hide();
    div.find("#divNueDir").hide();
    div = null;
}

// RENDERIZA PANTALLA DE DIRECCION EN TRACKING
function wizardBackDir() {
    var div = $("#mainCambioDireccion");
    div.find("#btnMoreDirecciones").show();
    div.find("#btnBack").hide();
    div.find("#btnNew").hide();
    div.find("#titleDir").html("Existente");

    div.find("#divNueDir").hide();

    div.find("#divTbDirecciones").show();
    div.find("#divMasDirecciones").hide();
    div = null;
}

// RENDERIZA PANTALLA DE NUEVA DIRECCION
function wizardNew() {

    var div = $("#mainCambioDireccion");
    div.find("#btnMoreDirecciones").hide();
    div.find("#btnNew").hide();
    div.find("#btnBack").show();
    div.find("#divTbDirecciones").hide();
    div.find("#divMasDirecciones").hide();
    div.find("#divNueDir").show();
    div.find("#titleDir").html("");
    div = null;

}

// VERIFICA SI UN DOCUMENTO PUEDE SER RECIBIDO EN EL AREA DEL USUARIO
function verificador_documento_area(data) {
    var band = false;
    for(var i = 0; i < dataTracking.user.documentos_area.length; i++){
        if (dataTracking.user.documentos_area[i].ID_TIPO_OBJETO == data.ID_TIPO_OBJETO) {
            band = true;
            break;
        }
    }
    return band;
}

// REALIZA CONSULTA AVANZADA DE DOCUMENTO Y RETORNA LOS DATOS QUE CUMPLAS LAS CONDICIONES
function busquedadAvanzada(data) {
    return ajaxrequest("busquedadAvanzada", data);
}

// CONSULTA UN PAQUETE IGUAL AL CODIGO PASADO POR PARAMETRO Y QUE ESTE EN EL ESTATUS DE ENTREGADO ENVIADO POR PARAMETRO 
function getFromWsPkg(codigo, entregado) { 
    return ajaxrequest("getFromWsPkg", { CODIGO: codigo, ENTREGADO: entregado });
}

// CONSTRUYE PANTALLA DE VER DETALLE DE PAQUETE
function buildOpenMainPaquete($this, data, detalle, index, onlyview) {


    genNavigation($this.find("#divNavigation"), data["DATA_CODIGO_ORDEN"]);

    $this.find("#count_paquetes").html("Paquetes: " + data.childrens.length);
    $this.find("#count_documents").html("Documentos: " + data.DOCUMENTOS);

    if (data["FINAL"] == 0) {
        // DATOS PARA TABLA DE HISTORIAL
        $this.find("#tabs").attr("historial", JSON.stringify(detalle["historial"]));

        // OCULTA LOS DATOS PRINCIPALES Y CAMBIA EL HEIGHT DE LA TABLA DE PAQUETES
        $this.find(".mainDataPaquete").hide();
        $this.find("#tbDetPaquetes").smartTable('resize', 335);
        $this.find("#toggleDireccion").attr("size_tabla", 335);

        if (detalle["direccion"] == -1) {
            // OCULTA LOS DATOS DE DIRECCION Y CAMBIA EL HEIGHT DE LA TABLA DE PAQUETES
            $this.find(".contentDatosDireccion").hide();
            $this.find("#tbDetPaquetes").smartTable('resize', 440);
            $this.find("#toggleDireccion").attr("size_tabla", 440);

        } else {
            // CARGA LOS DATOS DE DIRECCION
            fillForm(detalle["direccion"], $this);
        }
        $this.find("#divTbHistorialPaqAct").css("height", $this.find("#tbDetPaquetes").height() - 40 + "px");
        $this.find("#divNotes").css("height", $this.find("#tbDetPaquetes").height() - 40 + "px");

        $this.find("#toggleDireccion").attr("size_historial", $this.find("#tbDetPaquetes").height() - 40);

    } else {

        // SE LE AGREGA INFORMACCIONES AL TAB, Y SE CARGA LA DATA DE LA TABLA DE HISTORIAL
        $this.find("#tabs")/*.attr("childrens", JSON.stringify(detalle["childrens"]))*/.attr("codigo_orden", data["CODIGO_ORDEN"]);
        fillHistorial($this.find('#tbHistorial'), detalle["historial"], $this.prop("id"));

        $this.find("#toggleDireccion")
                .attr("size_historial", $this.find("#tbDetPaquetes").height() - 40)
                .attr("size_tabla", 80);

        // CARGA LOS DATOS DEL DOCUMENTO
        fillDocumentoFields(data, $this);
        fillForm(detalle["direccion"], $this);

    }

    if (data["ENTREGADO"] == 1 || data.AREAS != 1 || (data.ID_SUB_AREA != dataTracking.user.outsubareacodOut && data.AREAS == 1)) {
        $this.find("#tbDetPaquetes").find(".print").hide();
    }

    if (detalle["cambiar_direccion"] == 0 || data["ENTREGADO"] == 1) { 
        $this.find("#solcCambioDest").hide();
        $this.find("#solcCambioDir").hide();
    }

    
    if (onlyview == true || data.AREAS != 1 || (data.ID_SUB_AREA != dataTracking.user.outsubareacodOut && data.AREAS == 1)) {
        $this.find("#solcCambioDest").hide();
        $this.find("#solcCambioDir").hide();
        $this.find("#btnAddComment").hide();
        $this.find("#tbDetPaquetes").find(".print").hide();
    }

    //$this.attr("tbexist", "true");

    //AGREGA EL NOMBRE DEL CTE Y EL ID CONCATENADO
    fillDestinatariosFields(detalle["destinatarios"], $this);

    index = null;
    data = null;
    detalle = null;
}
// NUEVO DESTINATARIO
function nuevo_destinatario() {
    var data = {};
    data.nombre = prompt("Introdusca nombre del destinatario: ", "Nombre de destinatario");
    var date_ = new Date();
    data.consecutivo = ajaxrequest("get_codigo_nuevo_dest");
    data.codtipoinformante = "ND";
    data.tipopersona = "N";
    date_ = null;
    return data;
}

// BORRAR PAQUETE O DOCUMENTO DE LISTA QUE SE ESTA RECIBIENDO
function deleteRow($this) {
    debugger;
    alert("test");
}