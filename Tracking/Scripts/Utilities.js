﻿var to;
var timeout = 800000;

function TimedOut() {
    $.post("refresh_session.aspx", null,
    function (data) {
        if (data == "success") {
            to = setTimeout("TimedOut()", timeout);
        }
        else { $("#timeout").slideDown('fast'); }
    }
  );
}

function ajaxrequest(command, rqdata, rqdataType) {
    var rtn;
    var dataType = "json"
    if (rqdataType) {
        dataType = rqdataType;
    }
    if (typeof rqdata === "object") {
        rqdata = JSON.stringify(rqdata);
    }
    var data = "requesttype=" + command + "&data=" + encodeURIComponent(rqdata) + "&rnd=" + Math.floor(Math.random() * 1000);
    $.ajax({
        type: "POST",
        url: "ajaxserver.ashx",
        dataType: dataType,
        async: false,
        data: data,
        success: function (msg) {
            rtn = msg;
        },
        error: function (error) {
            alert(error.responseText);
            rtn = false;
        }
    });
    dataType = null;
    data = null;
    return rtn;
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
};

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}

function fillFieldById(data, div, field) {
    // Llena un campo de un formulario buscando por el ID con la informacion de la variable data
    var fieldObj = div.find(".control#" + field);
    if (fieldObj.is(":checkbox")) {
        if (data[field] == "s") {
            fieldObj.attr('checked', true);
        } else {
            fieldObj.attr('checked', false);
        }
    } else {
        fieldObj.val(data[field]);
    }
    fieldObj = null;
}

function fillForm(data, div) {
    // Llenar los campos de un formulario con la informacion de la variable data
    var fieldObj;
    for (field in data) {
        fieldObj = div.find(".control#" + field);
        if (fieldObj.is("checkbox")) {
            if (data[field] == "s") {
                fieldObj.attr('checked', true);
            } else {
                fieldObj.attr('checked', false);
            }
        } else {
            fieldObj.val(data[field]);
        }
    }
    fieldObj = null;

}

function fillDocumentoFields(data, div) {
    // Llenar los campos de un formulario con la informacion de la variable data 
    fillForm(data, div);
    div.find(".control#DEST_COD").val("");
    div.find(".control#DEST_NAME").val("");
    div.find(".control#" + data["DEST_TIPO_COD"] + "DEST_COD").val(data.DEST_COD);
    div.find(".control#" + data["DEST_TIPO_COD"] + "DEST_NAME").val(data.DEST_NAME);


}

function fillDestinatariosFields(data, div) {
    // Llenar los campos de un formulario con la informacion de la variable data
    for (var i = 0; i < data.length; i++) {
        switch (data[i].DEST_TIPO_COD) {
            case 'TI':
            case 'DE':
                div.find(".control#DEST_COD").val(data[i].DEST_COD);
                div.find(".control#DEST_NAME").val(data[i].DEST_NAME);
                break;
            default:
                div.find(".control#" + data[i].DEST_TIPO_COD + "DEST_COD").val(data[i].DEST_COD);
                div.find(".control#" + data[i].DEST_TIPO_COD + "DEST_NAME").val(data[i].DEST_NAME);
                break;
        }
    }
    i = 0;
}

function cleanForm(div) {
    // Limpia un formulario
    div.find(".control").each(function () {
        if ($(this).is("table")) {
            $(this).children("tbody").children().remove();
        } else {
            $(this).val("");
        }
    });

}

function getDataForm(form) {
    var data = {};
    var objs = form.find(".control");
    var pos;
    for (var i = 0; i < objs.length; i++) {
        pos = objs.eq(i);
        if (pos.is(":checkbox")) {

        } else if (pos.is(":text") || pos.is("select")) {
            data[pos.prop("id")] = pos.val();
        }
    }
    pos = null;objs = null;i = 0;
    return data;
}
function aplicarExcepciones() {
    var excepciones = dataTracking.user.outexcepcionesOut;
    var pos;
    // SI EXCEPCIONES LLEGAN NULL = SE RETORNA
    // NOTA: EXCEPCIONES NO DEBERIA LLEGAR UNDEFINED, SI SE CORRIGE QUITAR ESTA CONDICION
    if(excepciones == null){
        return;
    }
    for (var i = 0; i < excepciones.length; i++) {
        pos = excepciones[i];
        switch (pos.accexecod) {
            case '1    ':
                motorVisibilidad("#" + pos.accexeide, pos.acctipviscod);
                break;
            case '2    ':
                motorVisibilidad("#" + pos.accexeide, pos.acctipviscod);
                break;
            case '3    ':
                motorVisibilidad("#" + pos.accexeide, pos.acctipviscod);
                break;
            case '4    ':
                motorVisibilidad("#" + pos.accexeide, pos.acctipviscod);
                break;
            case '5    ':
                motorVisibilidad("#" + pos.accexeide, pos.acctipviscod);
                break;
        }
    }

    pos = null;excepciones = null;i = null;
}

function aplicarPermisos() {
    var permisos = dataTracking.user.outpermisosOut;
    var pos;
    for(var i = 0; i < permisos.length; i++){
        pos = permisos[i];
        switch(pos.accpercod){
            case '1    ':
                accesoTracking(pos.acctipviscod);
                break;
            case '2    ':
                motorVisibilidad(".consulta", pos.acctipviscod);
                break;
            case '3    ':
                motorVisibilidad(".modificacion", pos.acctipviscod);
                break;
            case '4    ':
                motorVisibilidad(".creacion", pos.acctipviscod);
                break;
            case '5    ':
                motorVisibilidad(".borrado", pos.acctipviscod);
                break;
        }
    }
    permisos = null; pos = null; i = null;

}

// VERIFICA SI TIENE PERMISOS 
function accesoTracking(acceso) {
    switch (acceso) {
        case '2    ':
            document.write("<B>NO TIENE PERMISOS PARA VISUALIZAR</B>");
            document.write('<script type="text/undefined">');
            document.execCommand("Stop"); 
            break;
        case '3    ':
            document.write("<B>ACCESO DENEGADO</B>");
            document.write('<script type="text/undefined">');
            document.execCommand("Stop");
            break;
    }
}

function motorVisibilidad(clase, acceso) {
    switch (acceso) {
        case '2    ':
            $(clase).hide();
            break;
        case '3    ':
            $(clase).prop("disable",true);
            break;
    }
}

function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
}

function genNavigation(div, dest_codigo_orden) {

    var ul = "<ul style='-webkit-padding-start: 0px; padding-start: 0px;'>"
    for (var i = 0; i < dest_codigo_orden.length; i++) {
        if (i + 1 == dest_codigo_orden.length) {
            ul += "<li class='navLiHorizontal'><span class='LastnavAHorizontal'>"
            + dest_codigo_orden[i]["LABEL"] 
            + "</span></li>"
        }else{
            //ul += "<li class='navLiHorizontal'><a href='#' class='navAHorizontal'>" + codigo_orden.substr(14 * i, 14) + "</a></li>"
            ul += "<li class='navLiHorizontal'><a href='#' class='navAHorizontal' data='" + JSON.stringify(dest_codigo_orden[i]) + "'>"
            + dest_codigo_orden[i]["LABEL"] 
            + "</a></li>"            
        }

    }

    div.html(ul);
    div.find("a").on({
        click: function () {
            // Handle click...
            var data = JSON.parse($(this).attr("data"));
            data = getFromWsPkg(data["DEST_CODIGO_ORDEN"], data["ENTREGADO"]);
            paquetes[paquetes.length] = data;
            $(".mainPaqueteOpen").each(function () {
                if ($(this).attr("CODIGO_ORDEN") == data["CODIGO_ORDEN"]) {
                    $(this).find("#tbDetPaquetes").smartTable("destroy");
                    $(this).find("#tbDetPaquetes").remove();
                    $(this).dialog("destroy");
                    $(this).remove();
                    return false;
                }
            });
            createDialogDetPaq("-1," + (paquetes.length-1), paquetes.length-1);
            data = null;
        }
    });    
    ul = null;
}

// SI IMPRESION ES 0 ES PARA ACUSE Y SI ES 1 ES PARA EL LABEL
function printPdf(codigo_orden, impresion) {

    dataTracking.batch = ajaxrequest("getJsonBatch");    
    var part_codigo = codigo_orden.substr(0, 14);
    var prefijo = part_codigo.substr(0, 3);
    var counter = parseInt(part_codigo.substr(3, 11),10);
    var band = false;
    for(var i = 0; i < dataTracking.batch.length; i++){
        if (dataTracking.batch[i].PREFIJO == prefijo) {
            if (counter == dataTracking.batch[i].CONTADOR) {
                band = true;
            }            
            break;
        }
    }

    if(band){
        messageBox("El batch de este paquete aun no esta cerrado por lo que no puede ser imprimido.");

        part_codigo = null;
        prefijo = null;
        counter = null;
        band = null;
        return;

    }

    $("#divPrintTracking").attr("codigo_orden", codigo_orden).attr("impresion", impresion).dialog("open");
    part_codigo = null;
    prefijo = null;
    counter = null;
    band = null;
}


function printDocument(documentId) {

    //Wait until PDF is ready to print    
    if (typeof document.getElementById(documentId).print == 'undefined') {

        setTimeout(function () { printDocument(documentId); }, 1000);

    } else {

        var x = document.getElementById(documentId);
        x.print();
        x = null;
    }
}



function callReportPdf(url, id_report) {

    // NOTA: SI CAMBIAN EL NOMBRE DEL PARAMETRO AQUI DEBE SER CAMBIADO TAMBIEN.

    var src = url + "&rs:Command=Render&rs:Format=PDF&ReportId=" + id_report;
    window.open(src, "Imprimir");
    iframe = null;
    src = null;
}


function repeatChar(ch, num) {
    //var rst = null;
    var rst = "";
    if (num > 0) {
        for (var i = 0; i < num; i++) {
            rst += ch;
        }
    }
    i = null;
    return rst;
}

function searchID(request, response) {
    var term = request.term; var projects = [];
    var data = 0;
    if (term.indexOf("  ") == -1 && ((term.substring(0, 1) < 48 || term.substring(0, 1) > 57) && term.substring(0, 1) < 96 || term.substring(0, 1) > 105)) {
        if (term.length <= 6) {
            projects[projects.length] = {
                value: "03" + repeatChar("0", 6 - term.length) + term,
                label: "03" + repeatChar("0", 6 - term.length) + term,
                desc: "MPP"
            };
        }
        projects[projects.length] =
			        {
			            value: "04" + repeatChar("0", 12 - term.length) + term,
			            label: "04" + repeatChar("0", 12 - term.length) + term,
			            desc: "PBS"
			        };
        response(projects);
    } else if (term.indexOf("  ") != -1) {
        term = term.replace(/  /, "");
        if (term in cache) {
            response(cache[term]);
            return;
        } else {
            try {
                if ((term.substring(0, 2) == "03" || term.substring(0, 2) == "04") && ((term.length == 8 || term.length == 14 || this.element.val().indexOf('-') != -1))) {
                    data = WebSvcTrackingSoapHttpPort_prcBusquedainf(term, 'CRT');
                } else {
                    data = WebSvcTrackingSoapHttpPort_prcBusquedainf(term, ' ');
                }
            } catch (e) {

                return;
            }
            if (data == undefined) {
                return;
            }
            
            data = data.outinformantesOut;
            data = data.slice(0, 10);
            data[data.length] = { nuevocte: true };
            projects = data;
            cache[term] = projects;
            response(projects);
        }
    }
    data = null; projects = null; term = null;
    this.element.removeClass("ui-autocomplete-loading");
    return true;
}

function validate_fields(data, key) {
    var data_ = {};
    data = data.toString();
    if (data == "-1") {
        data_["error"] = 1;
        data_["message"] = " El campo " + key + " es requerido. Favor selecionar."; 
    } else if (data == "") {
        data_["error"] = 1;
        data_["message"] = " El campo " + key + " es requerido. Favor ingresar.";
    }
    return data_;
}

function SetCaretAtEnd(elem, len) {
    var elemLen = len;
    // For IE Only
    if (document.selection) {
        // Set focus
        elem.focus();
        // Use IE Ranges
        var oSel = document.selection.createRange();
        // Reset position to 0 & then set at end
        oSel.moveStart('character', -elemLen);
        oSel.moveStart('character', elemLen);
        oSel.moveEnd('character', 0);
        oSel.select();
        oSel = null;
    }
    else if (elem.selectionStart || elem.selectionStart == '0') {
        // Firefox/Chrome
        elem.selectionStart = elemLen;
        elem.selectionEnd = elemLen;
        elem.focus();
    } // if
    elemLen = null;
}