﻿var timeOutMSG = "Su Consulta a demorado mas del tiempo esperado, favor verificar los datos.";
var timeOutTime = 10000;

function invokeAsync(url, xmlDoc, callback) {
    url = url.replace("http://login.universal.com.do:7778", SisacserviceURL);
    var req = null;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    callback(req.responseXML);
                }
            }
        }
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type", "text/xml");
        req.timeout = timeOutTime;
        req.ontimeout = function () { alert(timeOutMSG); }

        req.send(xmlDoc);
    }

}

function invokeSync(url, xmlDoc) {

    url = url.replace("http://login.universal.com.do:7778", SisacserviceURL);
    var req = null; if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (req) {
        req.open("POST", url, false);
        req.setRequestHeader("Content-Type", "text/xml");
        req.timeout = timeOutTime;
        req.ontimeout = function () { alert(timeOutMSG); }
        req.send(xmlDoc);
        return req.responseXML;
    }

}