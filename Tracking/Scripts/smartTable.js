﻿(function ($) {
    $.widget("ui.smartTable", {
        options: {
            data: [],
            css: [
                {
                    name: "font-size",
                    value: "8.7pt"
                },
                {
                    name: "font-family",
                    value: "Cambria"
                }
            ],
            index: "-1",
            print: true,
            object: 0,
            columns: [],
            toolBox: [],
            height: 300,
            width: "",
            sWidth: [],
            onlyView: false,
            instace: false,
            searchVar: false,
            searchAdvanced: false,
            codigo: "",
            dataTable: true,
            id: "",
            classMouseView: "mouseEnterView",
            searchView: false,
            id_sub_area: -1,
            deleteRow: false,
        },

        _create: function () {
            var element = this.element;
            var css = this.options.css;
            var date_ = new Date();
            var $this = this;
            this.options.id = date_.getTime() + Math.floor(Math.random() * 123);

            for (var y = 0; y < css.length; y++) {
                element.css(css[y].name, css[y].value);
            }
            if (this.options.index != "-2" && this.options.print == true) {
                var div = $this.element.find("#smartTablePrint").css("font-size", "8.5pt");
                var obj = $("<button>Acuse</button>").addClass("consulta print").button({
                    icons: {
                        primary: "ui-icon-print"
                    }
                }).click(function () {
                    printPdf($this.options.codigo, 0);
                });
                div.append(obj);
                obj = $("<button>Label</button>").addClass("consulta print").button({
                    icons: {
                        primary: "ui-icon-print"
                    }
                }).click(function () {
                    printPdf($this.options.codigo, 1);
                });
                div.append(obj);

                div = null; obj = null;
                //this.addPrint();
            }
            //this.element.find("#tbSmart").css("width", "1000px");            
            this.element.find("#contTbSmart").find("table:eq(0)").prop("id", this.options.id);
            this.createTheader();
            if (this.options.dataTable) {
                this.addDataTable();
            } else {
                this.element.find("#" + this.options.id).css("width", "100%");
            }
            if (!this.options.onlyView && this.options.index != "-2" || this.options.searchView) {

                var Objects = this.options.toolBox;
                var divToolBox = this.element.find("#toolBox");
                var divRigth = $("<div class='crearNuevoDivSmartTable'></div>");
                var divLeft = $("<div></div>");
                var p = $("<div></div>");
                var pos;
                var obj;

                for (var i = 0; i < Objects.length; i++) {
                    pos = Objects[i];
                    switch (pos.type) {
                        case "SEARCH":
                            p.append("<span class='fontDecorate'>Buscar</span>");
                            obj = $("<input type='text' class='searchSmartTable consulta'></input>");
                            obj.autocomplete({
                                minLength: 4,
                                delay: 2250,
                                source: function (request, response) {
                                    var term = $.trim(request.term);
                                    var data;
                                    if (!$this.options.searchVar) {
                                        $('#divPaqActuales').find('#divHistorialPaqAct').hide();
                                        if (term == "") {
                                            data = ajaxrequest("getPkg", { CODIGO_ORDEN: "" });
                                        } else {
                                            data = ajaxrequest("buscarDocumentos",
                                        {
                                            TERM: term,
                                            CODIGO_ORDEN: $this.options.codigo,
                                            ID_SUB_AREA: $this.options.id_sub_area
                                        });
                                        }
                                        // VARIABLE GLOBAL DE PANTALLA PRINCIPAL
                                        paquetes = data;
                                    } else {
                                        if (term == "") {
                                            data = ajaxrequest("getPkgChildrens",
                                        {
                                            CODIGO_ORDEN: $this.options.codigo,
                                            ID_SUB_AREA: $this.options.id_sub_area
                                        });
                                        } else {
                                            data = ajaxrequest("buscarDocumentos",
                                        {
                                            TERM: term,
                                            CODIGO_ORDEN: $this.options.codigo,
                                            ID_SUB_AREA: $this.options.id_sub_area
                                        });
                                        }
                                    }
                                    $this.reCreateTbl();
                                    $this.fillTbody(data);
                                    $this.addDataTable();

                                    data = null; term = null;
                                    return;
                                },
                                focus: function (event, ui) {
                                    return false;
                                },
                                select: function (event, ui) {
                                    return false;
                                }
                            });
                            p.append(obj);

                            if ($this.options.searchAdvanced) {
                                obj = $("<button>...</button>").css({ "margin-left": "3px", "height": "20px" }).button().click(function () {
                                    $("#divAdvSearch").dialog("open");
                                });
                                p.append(obj);
                                obj = $("<button>Refrescar</button>").css({ "margin-left": "3px", "height": "20px" }).button().click(function () {
                                    $this.element.find(".searchSmartTable").val("").autocomplete("search", "    ");
                                    //                                data = ajaxrequest("getPkg", { CODIGO_ORDEN: "" });
                                    //                                $this.fillTbody(data);
                                });
                                p.append(obj);
                            }
                            break;

                        case "CREAR":
                            obj = $("<span>Crear Nuevo</span>");
                            obj.click(crearNuevoDocumento);
                            divRigth.append(obj);
                            break;

                    }
                }
                divLeft.append(p);
                divToolBox.append(divRigth);
                divToolBox.append(divLeft);

                divToolBox = null;
                divRigth = null;
                divLeft = null;
                p = null;
                pos = null;
                obj = null;
                i = null;
                Objects = null;
                //this.createToolBox(this.options.toolBox);
            }
            if (this.options.data.length > 0) {
                this.fillTbody(this.options.data);
            }
            css = null;
            element = null;
            date_ = null;
        },

        createTheader: function () {
            var $this = this;
            var thead = this.element.find("#" + this.options.id).children("thead");
            thead.addClass("ui-widget-header");
            //thead.children().remove();
            var columns = this.options.columns;
            var pos;
            var tr = $("<tr></tr>");
            var td;
            var obj;

            if (!$this.options.onlyView) {
                if ($this.options.object == 0) {
                    obj = $("<input type='checkbox' class='stBodyChkBox' />").click(function (e) {
                        $(this).attr('evt', false);
                        if ($(this).prop('checked')) {
                            $this.selectAllRows();
                        } else {
                            $this.unSelectAllRows();
                        }
                    });
                } else if ($this.options.object == 1) {
                    obj = "";
                    //obj = $("<input type='radio' disabled='disabled' />");
                }
                tr.append($("<th align='left'></th>").append(obj));
            }

            // COLUMNA DE ELIMINAR
            if ($this.options.deleteRow) {
                obj = "";
                tr.append($("<th align='left'></th>").append(obj));
            }

            for (var i = 0; i < columns.length; i++) {
                pos = columns[i];
                td = $("<th align='left'></th>").append(pos.label);
                tr.append(td);
            }            

            thead.append(tr);

            thead = null;
            columns = null;
            pos = null;
            tr = null;
            td = null;
            obj = null;
            i = null;
        },

        resize: function (height, index) {
            this.element.find(".dataTables_scrollBody").css('height', height);
        },

        fillTbody: function (data) {

            //this.reCreateTbl();
            if (data.length < 0) {
                return;
            }
            if (this.options.onlyView) { // PARA CREAR TABLA DE VISUALIZACION
                this._fillTbodyView(data);
            } else if (this.options.object == 0) { // PARA CREAR TABLA CON CHECKBOX
                this._fillCheckTbody(data);
            } else if (this.options.object == 1) { // PARA CREAR TABLA CON RADIO
                this._fillRadioBody(data);
            }
            // COPIA LA TABLA CREADA

            //this.addDataTable();

        },
        // CREA BODY PARA UNA TABLA CON RADIO BUTTONS
        _fillRadioBody: function (data) {
            var $this = this;
            this.options.data = data;
            var tbody = this.element.find("#" + this.options.id).children("tbody");
            var index = this.options.index;
            tbody.empty();
            var html = "";
            for (var i = 0; i < data.length; i++) {
                //tbody.append(this._fnAddRowCheck(data[i], index + "," + i));
                //html += this.__fnAddRowRadio(data[i], index + "," + i);
                tbody[0].appendChild(this.__fnAddRowRadio(data[i], index + "," + i));
            }
            //tbody[0].innerHTML = html;
            tbody.find(":radio.stBodyChkBox").on({
                click: function () {
                    // Handle click...
                    $this._checkClick.call(this, $this);

                }
            });
            tbody.find("tr").on({
                mouseenter: function () {
                    // Handle mouseenter...
                    $(this).addClass('mouseEnter');
                },
                mouseleave: function () {
                    // Handle mouseleave...
                    $(this).removeClass('mouseEnter');
                },
                click: function () {
                    // Handle click...
                    var that = this;
                    setTimeout(function () {
                        var dblclick = parseInt($(that).data('double'), 10);
                        if (dblclick > 0) {
                            $(that).data('double', dblclick - 1);
                            that = null;
                        } else {
                            $this._rowClick.call(that, $this);
                        }
                        dblclick = null;
                    }, 300);
                },
                dblclick: function () {
                    $(this).data('double', 2);
                }
            });

            tbody = null; index = null; html = null; i = null;
        },

        __fnAddRowRadio: function (data, index) {
            var columns = this.options.columns;
            var newtr = document.createElement("tr");
            var newtd = document.createElement("td");
            newtd.style.cssText = 'padding: 1px 18px 1px 10px;';
            newtd.innerHTML = "<input type='radio' name='radio' class='stBodyChkBox' />";
            newtr.appendChild(newtd);

            for (var j = 0; j < columns.length; j++) {
                newtd = document.createElement("td");
                newtd.innerHTML = "<td>" + data[columns[j].key] + "</td>";
                newtr.appendChild(newtd);
            }
            columns = null; newtd = null;
            newtr.setAttribute('INDEX', index);
            newtr.setAttribute('data', JSON.stringify(data));
            newtr.className = 'context-menu-one';

            newtd = null;
            columns = null;
            return newtr;
        },


        _fillCheckTbody: function (data) {
            var $this = this;
            this.options.data = data;
            var div = this.element.find("#contTbSmart");
            div.empty();
            var index = this.options.index;
            var html = "";
            for (var i = 0; i < data.length; i++) {
                html += this._fnAddRowCheck(data[i], index + "," + i);
            }
            i = null;
            html = "<table id='" + this.options.id + "' style='width: 100%;' >"
            + "<thead></thead>"
            + "<tbody>" + html + "</tbody>" + "</table>";
            div[0].innerHTML = html;
            var tbody = this.element.find("#" + this.options.id).children("tbody");

            tbody.find(":checkbox.stBodyChkBox").on({
                click: function () {
                    // Handle click...
                    $this._checkClick.call(this, $this);

                }
            });
            tbody.find("tr").on({
                mouseenter: function () {
                    // Handle mouseenter...
                    $(this).addClass('mouseEnter');
                },
                mouseleave: function () {
                    // Handle mouseleave...
                    $(this).removeClass('mouseEnter');
                },
                click: function () {
                    // Handle click...
                    var that = this;
                    setTimeout(function () {
                        var dblclick = parseInt($(that).data('double'), 10);
                        if (dblclick > 0) {
                            $(that).data('double', dblclick - 1);
                            that = null;
                        } else {
                            $this._rowClick.call(that, $this);
                        }
                        dblclick = null;
                    }, 300);

                },
                dblclick: function () {
                    $(this).data('double', 2);
                    $this._rowDblClick.call(this, $this);
                }
            });
            this.createTheader();
            this.addMenuContext();

            pos = null; tbody = null; index = null;
            div = null; html = null; i = null;

        },



        _fnAddRowCheck: function (data, index) {


            var tr = "";
            var columns = this.options.columns;
            if (data["ENTREGADO"] == 0) {
                tr += "<td style='padding: 1px 18px 1px 10px;'><input type='checkbox' class='stBodyChkBox' /></td>";
            } else {
                tr += "<td style='padding: 1px 18px 1px 10px;'><span></span></td>";
            }
            for (var j = 0; j < columns.length; j++) {
                tr += "<td>" + data[columns[j].key] + "</td>";
            }
            tr = "<tr class='context-menu-one' INDEX='" + index + "'>" + tr + "</tr>";
            columns = null;
            return tr;
        },

        // CREA EL BODY DE UNA TABLA DE VISUALIZACION
        _fillTbodyView: function (data) {

            var $this = this;
            this.options.data = data;
            var div = this.element.find("#contTbSmart");
            div.children().remove();
            var index = this.options.index;
            var html = "";
            for (var i = 0; i < data.length; i++) {
                html += this._fnAddRowView(data[i], index + "," + i);
            }
            html = "<table id='" + this.options.id + "' style='width: 100%;' >"
            + "<thead></thead>"
            + "<tbody>" + html + "</tbody>" + "</table>";
            div[0].innerHTML = html;
            var tbody = this.element.find("#" + this.options.id).children("tbody");

            tbody.find("tr").attr("classMouse", this.options.classMouseView).on({
                mouseenter: function () {
                    // Handle mouseenter...
                    $(this).addClass($(this).attr("classMouse"));
                },
                mouseleave: function () {
                    // Handle mouseleave...
                    $(this).removeClass($(this).attr("classMouse"));
                },
                click: function () {
                    // Handle click...
                    var that = this;
                    setTimeout(function () {
                        var dblclick = parseInt($(that).data('double'), 10);
                        if (dblclick > 0) {
                            $(that).data('double', dblclick - 1);
                            that = null;
                        }
                        dblclick = null;
                    }, 300);
                },
                dblclick: function () {
                    $(this).data('double', 2);
                    $this._rowDblClickView.call(this, $this);
                }
            });

            this.createTheader();

            index = null; html = null; div = null;
            pos = null; tbody = null; i = null;
        },

        // CREA LA FILA Y LA RETORNA PARA LLENAR UNA TABLA DE VISUALIZACION
        _fnAddRowView: function (data, index) {
            var tr = "";
            var columns = this.options.columns;
            for (var j = 0; j < columns.length; j++) {
                tr += "<td>" + data[columns[j].key] + "</td>";
            }
            tr = "<tr INDEX='" + index + "'>" + tr + "</tr>";
            columns = null;
            return tr;
        },

        // ADICIONA UNA FILA AL FINAL DE LA TABLA
        //        addRow: function (data, index) {
        //            var tr;
        //            if (this.options.onlyView) { // PARA CREAR TABLA DE VISUALIZACION
        //                tr = this._fnAddRowView(data);
        //            } else if (this.options.object == 0) { // PARA CREAR TABLA CON CHECKBOX
        //                tr = this._fnAddRowCheck(data, index);
        //            } else if (this.options.object == 1) { // PARA CREAR TABLA CON RADIO
        //                tr = this._fnAddRowRadio(data);
        //            }
        //            return tr;
        //        },

        //        _radioClick: function ($this) {
        //            var row = $(this).parent().parent();
        //            row.parent().children().each(function () {
        //                if ($(this).hasClass("rowSelected")) {
        //                    $(this).removeClass("rowSelected");
        //                }
        //            });
        //            row.addClass("rowSelected");
        //            row.attr("evt", "false");
        //            row = null;
        //        },

        _checkClick: function ($this) {
            var row = $(this).parent().parent();
            if ($(this).prop('checked')) {
                if ($this.options.object == 1) {
                    $this.unSelectAllRows();
                }
                row.addClass("rowSelected");
                //$this.selectRow(row);
            } else {
                row.removeClass("rowSelected");
                //$this.unSelectRow(row);
            }
            row.attr("evt", "false");
            row = null;
        },

        _rowClick: function ($this) {
            if ($(this).attr('evt') == 'false') {
                $(this).attr('evt', 'true');
                return;
            }
            if (!$(this).hasClass("rowSelected")) {
                $this.unSelectAllRows();
                if ($this.options.object == 1) {
                    $this.unSelectAllRows();
                }
                $(this).addClass("rowSelected");
                //$this.selectRow($(this));
                $(this).find(".stBodyChkBox").prop("checked", true);
            } else {
                $(this).removeClass("rowSelected");
                //$this.unSelectRow($(this));
                $(this).find(".stBodyChkBox").prop("checked", false);
            }
            $this._trigger("rowClick", null, { row: $(this) });
        },

        _rowDblClick: function ($this) {
            $this._trigger("rowDblClick", null, { row: $(this) });
        },

        _rowDblClickView: function ($this) {
            $this._trigger("rowDblClickView", null, { row: $(this) });
        },

        addRowToBody: function (data) {
            var $this = this;
            var allData = [];
            allData = this.options.data;
            allData[allData.length] = data;
            otrosPaquetes = allData;
            this.options.data = allData;
            var columns = this.options.columns;
            allData = [];
            if ($this.options.deleteRow) {
                allData[allData.length] = "<a href='#' style='color: #034af3;'>Descartar</a>";
            }

            for (var j = 0; j < columns.length; j++) {
                allData[allData.length] = data[columns[j].key];
            }
            
            this.element.find("#" + this.options.id).dataTable().fnAddData(allData);
            // SE UTILIZA LA VARIABLE COLUMN PARA GUARDAR LA FILA AGREGADA
            var columns = this.element.find("#" + this.options.id).children("tbody").find("tr:last");
            allData = this.options.classMouseView;
            columns.attr("INDEX", this.options.index + "," + columns.index())
            .attr("classMouse", this.options.classMouseView)
            .on({
                mouseenter: function () {
                    // Handle mouseenter...
                    $(this).addClass($(this).attr("classMouse"));
                },
                mouseleave: function () {
                    // Handle mouseleave...
                    $(this).removeClass($(this).attr("classMouse"));
                },
                click: function () {
                    // Handle click...
                    var that = this;
                    setTimeout(function () {
                        var dblclick = parseInt($(that).data('double'), 10);
                        if (dblclick > 0) {
                            $(that).data('double', dblclick - 1);
                            that = null;
                        }
                        dblclick = null;
                    }, 300);
                },
                dblclick: function () {
                    $(this).data('double', 2);
                    $this._rowDblClickView.call(this, $this);
                }
            });

            // CLICK A CELDA DE DESCARTAR
            if ($this.options.deleteRow) {
                columns.children(":eq(0)").children(":eq(0)").on({
                    click: function () {
                        $this.options.data.splice($(this).parent().parent().index(),1);
                        otrosPaquetes = $this.options.data;
                        $this.element.find("#" + $this.options.id).dataTable().fnDeleteRow($(this).parent().parent().index());
                    }
                });
            }


            columns = null; allData = null;

        },

        //        selectRow: function (row) {
        //            if (this.options.object == 1) {
        //                this.unSelectAllRows();
        //            }
        //            row.addClass("rowSelected");
        //        },

        //        unSelectRow: function (row) {
        //            row.removeClass("rowSelected");
        //        },

        selectAllRows: function () {
            this.element.find("#" + this.options.id).children("tbody").children().each(function () {
                if (!$(this).hasClass("rowSelected") && $(this).find(".stBodyChkBox").length > 0) {
                    $(this).find('.stBodyChkBox').prop("checked", true);
                    $(this).addClass("rowSelected");
                }
            });
        },

        unSelectAllRows: function () {
            this.element.find("#" + this.options.id).children("tbody").children().each(function () {
                if ($(this).hasClass("rowSelected")) {
                    $(this).find('.stBodyChkBox').prop("checked", false);
                    $(this).removeClass("rowSelected");
                }
            });
        },

        addDataTable: function () {
            if (this.options.sWidth.length <= 0) {
                var width = [];
                var columns = this.options.columns;
                if (!this.options.onlyView) {
                    width[width.length] = { "sWidth": "1%" };
                }
                if (this.options.deleteRow) {
                    width[width.length] = { "sWidth": "1%" };
                }                
                for (var i = 0; i < columns.length; i++) {
                    width[width.length] = { "sWidth": columns[i].width };
                }                
                i = null;
                this.options.sWidth = width;
                columns = null;
                width = null;
            }
            //if ($.fn.DataTable.fnIsDataTable(tb)) {
            //$(this).dataTable().fnDestroy();
            //}
            this.element.find("#" + this.options.id).dataTable({
                "bJQueryUI": false
					, "bFilter": false
                    , "bPaginate": false
                    , "bDestroy": true
					, "bSort": false
					, "bInfo": false
					, "bAutoWidth": false
					, "bLengthChange": false
                    , "oLanguage": {
                        "sEmptyTable": "    "
                    }
					, "sScrollY": this.options.height
                    , "sScrollX": "100%"
                //, "sScrollXInner": "98%"
                    , "aoColumns": this.options.sWidth
                    , "bDestroy": true

            });
            tb = null;

        },

        reCreateTbl: function () {
            this.options.data = [];
            var contTb = this.element.find("#contTbSmart");
            contTb.empty();
            this.element.find("#contTbSmart").find("table:eq(0)").prop("id", this.options.id);
            contTb.html("<table id='" + this.options.id + "' style='width:100%;'><thead></thead><tbody></tbody></table>");
            this.createTheader();
            contTb = null;
        },

        addMenuContext: function () {
            var $this = this;
            $($this.element).contextMenu({
                selector: '.context-menu-one',
                build: function ($trigger, e) {
                    var documentos = $this.getDataRowsSelected($this);
                    if (documentos.length > 1) {
                        $trigger.data('disableCmd', false);

                    } else {
                        $trigger.data('disableCmd', true);

                    }
                    if (documentos.length > 0) {
                        $trigger.data('disableCp', true);
                    } else {
                        $trigger.data('disableCp', false);
                    }
                    documentos = null;
                    return {
                        callback: function (key, options) {
                            //                    var m = "clicked: " + key;
                            //                    window.console && console.log(m) || alert(m);

                            switch (key) {
                                case 'crearPaquete':
                                    openCrearPackage($this.getDataRowsSelected($this), $this.options.index);
                                    break;
                                case 'verDocumento':
                                    verDocumento(getPackByIndex(this.attr("index")));
                                    break;
                                case 'verCliente':
                                    verCliente(getPackByIndex(this.attr("index")));
                                    break;
                            }


                        },
                        items: {
                            "crearPaquete": { name: "Crear Paquete", icon: "createPackage", disabled: function (key, opt) {
                                // this references the trigger element
                                return !this.data('disableCp');
                            }
                            },
                            "sep1": "---------",
                            "verDocumento": { name: "Ver Documento", icon: "viewDocument", disabled: function (key, opt) {
                                // this references the trigger element
                                return !this.data('disableCmd');
                            }
                            },
                            "verCliente": { name: "Ver Cliente", icon: "viewCustomer", disabled: function (key, opt) {
                                // this references the trigger element
                                return !this.data('disableCmd');
                            }
                            }
                        }
                    };
                }
            });
        },

        getDataRowsSelected: function ($this) {
            var rowsSelected = [];
            $this.element.find("#" + this.options.id).children("tbody").children().each(function () {
                if ($(this).hasClass("rowSelected") && $(this).find(".stBodyChkBox").length > 0) {
                    rowsSelected[rowsSelected.length] = $this.getPackByIndex($(this).attr("INDEX"));
                }
            });
            $this = null;
            return rowsSelected;
        },

        getPackByIndex: function (index) {
            //var data = this.options.data;
            var data = paquetes;

            //REMPLAZA EL -1 DE LA CADENA INDEX, LUEGO CONVIERTE EN ARREGLO
            var indexArray = index.replace("-1,", "").split(",");
            // BUSCA LA PRIMERA POSICION
            var pos = data[indexArray[0]];
            for (var i = 1; i < indexArray.length; i++) {
                pos = pos.childrens[indexArray[i]];
            }
            i = null;
            data = null;
            indexArray = null;
            return pos;
        },

        destroy: function () {

            $(this.element).contextMenu('destroy');
            this._destroy();
        },

        _init: function () {

            // call open if this instance should be open by default
        },

        _setOption: function (key, value) {
            this.options[key] = value;

            switch (key) {
                case "something":
                    // perform some additional logic if just setting the new
                    // value in this.options is not enough. 
                    break;
            }
        }
    });


})(jQuery);

