﻿function firstChildElement(node) {
    if (!node) return null; var child = node.firstChild;
    while (child) {
        if (child.nodeType == 1)
            return child;
        child = child.nextSibling;
    }
    return null;
}

function nextSiblingElement(node) {
    if (!node) return null; var sibling = node.nextSibling;
    while (sibling) {
        if (sibling.nodeType == 1)
            return sibling;
        sibling = sibling.nextSibling;
    }
    return null;
}

function getText(node) {
    if (!node) return null; var text = '';
    var child = node.firstChild;
    while (child) {
        if (child.nodeType == 3) {
            text = text + child.nodeValue;
        }
        child = child.nextSibling;
    }
    return text;
}

function invokeSync(url, xmlDoc) {
    var req = null; if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("POST", url, false);
        req.setRequestHeader("Content-Type", "text/xml");
        req.send(xmlDoc);
        return req.responseXML;
    }
}

function invokeAsync(url, xmlDoc, callback) {
    var req = null;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    callback(req.responseXML);
                }
            }
        }
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type", "text/xml");
        req.send(xmlDoc);
    }
}

function createNewDocument() {
    var xmlDoc = null;
    if (document.implementation && document.implementation.createDocument) {
        xmlDoc = document.implementation.createDocument("", "", null);
    } else if (window.ActiveXObject) {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    }
    return xmlDoc;
}

function createElementNS(xmlDoc, namespace, localName) {
    var element = null;
    if (typeof xmlDoc.createElementNS != 'undefined') {
        element = xmlDoc.createElementNS(namespace, localName);
    }
    else if (typeof xmlDoc.createNode != 'undefined') {
        if (namespace) {
            element = xmlDoc.createNode(1, localName, namespace);
        } else {
            element = xmlDoc.createElement(localName);
        }
    }
    return element;
}

function localName(element) {
    if (element.localName)
        return element.localName;
    else
        return element.baseName;
}



function WebSvcTrackingSoapHttpPort_prcBusquedainf(_inEntrada, _inTipoentrada) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcBusquedainfElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inEntrada');
    paramEl.appendChild(xmlDoc.createTextNode(_inEntrada));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipoentrada');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipoentrada));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcBusquedainf_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcTrackingSoapHttpPort_prcBusquedainfAsync(_inEntrada, _inTipoentrada, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcBusquedainfElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inEntrada');
    paramEl.appendChild(xmlDoc.createTextNode(_inEntrada));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipoentrada');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipoentrada));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcBusquedainf_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcTrackingSoapHttpPort_prcDirtrack(_inNatide, _inTipper, _inConcontrato, _inConfamilia, _inConbeneficiario, _inTipodest, _inCant) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcDirtrackElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNatide');
    paramEl.appendChild(xmlDoc.createTextNode(_inNatide));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipper');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipper));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConcontrato');
    paramEl.appendChild(xmlDoc.createTextNode(_inConcontrato));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConfamilia');
    paramEl.appendChild(xmlDoc.createTextNode(_inConfamilia));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConbeneficiario');
    paramEl.appendChild(xmlDoc.createTextNode(_inConbeneficiario));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipodest');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipodest));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inCant');
    paramEl.appendChild(xmlDoc.createTextNode(_inCant));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcDirtrack_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcTrackingSoapHttpPort_prcDirtrackAsync(_inNatide, _inTipper, _inConcontrato, _inConfamilia, _inConbeneficiario, _inTipodest, _inCant, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcDirtrackElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNatide');
    paramEl.appendChild(xmlDoc.createTextNode(_inNatide));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipper');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipper));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConcontrato');
    paramEl.appendChild(xmlDoc.createTextNode(_inConcontrato));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConfamilia');
    paramEl.appendChild(xmlDoc.createTextNode(_inConfamilia));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConbeneficiario');
    paramEl.appendChild(xmlDoc.createTextNode(_inConbeneficiario));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipodest');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipodest));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inCant');
    paramEl.appendChild(xmlDoc.createTextNode(_inCant));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcDirtrack_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcTrackingSoapHttpPort_prcActdireccion(_inCodigousuario, _inAppkey, _inIdioma, _inInumide, _inStrtipper, _inDirecciones) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcActdireccionElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inCodigousuario');
    paramEl.appendChild(xmlDoc.createTextNode(_inCodigousuario));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inAppkey');
    paramEl.appendChild(xmlDoc.createTextNode(_inAppkey));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inIdioma');
    paramEl.appendChild(xmlDoc.createTextNode(_inIdioma));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inInumide');
    paramEl.appendChild(xmlDoc.createTextNode(_inInumide));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inStrtipper');
    paramEl.appendChild(xmlDoc.createTextNode(_inStrtipper));
    parameterParent.appendChild(paramEl);
    WebSvcTrackingSoapHttpPort_serialize_arrayof_DireccionesObjUser(xmlDoc, parameterParent, _inDirecciones, '', 'inDirecciones');
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcActdireccion_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcTrackingSoapHttpPort_prcActdireccionAsync(_inCodigousuario, _inAppkey, _inIdioma, _inInumide, _inStrtipper, _inDirecciones, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcActdireccionElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inCodigousuario');
    paramEl.appendChild(xmlDoc.createTextNode(_inCodigousuario));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inAppkey');
    paramEl.appendChild(xmlDoc.createTextNode(_inAppkey));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inIdioma');
    paramEl.appendChild(xmlDoc.createTextNode(_inIdioma));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inInumide');
    paramEl.appendChild(xmlDoc.createTextNode(_inInumide));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inStrtipper');
    paramEl.appendChild(xmlDoc.createTextNode(_inStrtipper));
    parameterParent.appendChild(paramEl);
    WebSvcTrackingSoapHttpPort_serialize_arrayof_DireccionesObjUser(xmlDoc, parameterParent, _inDirecciones, '', 'inDirecciones');
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcActdireccion_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcTrackingSoapHttpPort_prcReem(_inNumree) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcReemElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNumree');
    paramEl.appendChild(xmlDoc.createTextNode(_inNumree));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcReem_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcTrackingSoapHttpPort_prcReemAsync(_inNumree, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcReemElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNumree');
    paramEl.appendChild(xmlDoc.createTextNode(_inNumree));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcReem_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcTrackingSoapHttpPort_prcDatoscartera(_inNumfactura) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcDatoscarteraElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNumfactura');
    paramEl.appendChild(xmlDoc.createTextNode(_inNumfactura));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcDatoscartera_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcTrackingSoapHttpPort_prcDatoscarteraAsync(_inNumfactura, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcDatoscarteraElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNumfactura');
    paramEl.appendChild(xmlDoc.createTextNode(_inNumfactura));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcDatoscartera_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcTrackingSoapHttpPort_prcCuemed(_inNumrad, _inSucursal) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcCuemedElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNumrad');
    paramEl.appendChild(xmlDoc.createTextNode(_inNumrad));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inSucursal');
    paramEl.appendChild(xmlDoc.createTextNode(_inSucursal));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcCuemed_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcTrackingSoapHttpPort_prcCuemedAsync(_inNumrad, _inSucursal, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcCuemedElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNumrad');
    paramEl.appendChild(xmlDoc.createTextNode(_inNumrad));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inSucursal');
    paramEl.appendChild(xmlDoc.createTextNode(_inSucursal));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcCuemed_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcTrackingSoapHttpPort_prcInfgenafi(_inNatide, _inTipper, _inConcontrato, _inConfamilia, _inConbeneficiario, _inTipodest) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcInfgenafiElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNatide');
    paramEl.appendChild(xmlDoc.createTextNode(_inNatide));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipper');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipper));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConcontrato');
    paramEl.appendChild(xmlDoc.createTextNode(_inConcontrato));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConfamilia');
    paramEl.appendChild(xmlDoc.createTextNode(_inConfamilia));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConbeneficiario');
    paramEl.appendChild(xmlDoc.createTextNode(_inConbeneficiario));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipodest');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipodest));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcInfgenafi_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcTrackingSoapHttpPort_prcInfgenafiAsync(_inNatide, _inTipper, _inConcontrato, _inConfamilia, _inConbeneficiario, _inTipodest, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'prcInfgenafiElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inNatide');
    paramEl.appendChild(xmlDoc.createTextNode(_inNatide));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipper');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipper));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConcontrato');
    paramEl.appendChild(xmlDoc.createTextNode(_inConcontrato));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConfamilia');
    paramEl.appendChild(xmlDoc.createTextNode(_inConfamilia));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inConbeneficiario');
    paramEl.appendChild(xmlDoc.createTextNode(_inConbeneficiario));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'inTipodest');
    paramEl.appendChild(xmlDoc.createTextNode(_inTipodest));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcInfgenafi_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcTrackingTest/WebSvcTrackingSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcTrackingSoapHttpPort_serialize_arrayof_DireccionesObjUser(xmlDoc, parentEl, adt, namespaceURI, localPart) {
    for (var i = 0; i < adt.length; i++) {
        var elementEl = createElementNS(xmlDoc, namespaceURI, localPart);
        parentEl.appendChild(elementEl);
        WebSvcTrackingSoapHttpPort_serialize_DireccionesObjUser(xmlDoc, elementEl, adt[i], namespaceURI);
    }
}

function WebSvcTrackingSoapHttpPort_serialize_DireccionesObjUser(xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.stelefono) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'stelefono');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.stelefono));
    }
    if (adt.sdireccion) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'sdireccion');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.sdireccion));
    }
    if (adt.scodsucursal) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scodsucursal');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scodsucursal));
    }
    if (adt.scodciudad) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scodciudad');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scodciudad));
    }
    if (adt.scodbarrio) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scodbarrio');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scodbarrio));
    }
    if (adt.scodtipoubicacion) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scodtipoubicacion');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scodtipoubicacion));
    }
    if (adt.scodareatelefono1) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scodareatelefono1');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scodareatelefono1));
    }
    if (adt.semailfacturas) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'semailfacturas');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.semailfacturas));
    }
    if (adt.semailcobros) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'semailcobros');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.semailcobros));
    }
    if (adt.scodareatelefono2) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scodareatelefono2');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scodareatelefono2));
    }
    if (adt.sbeeper) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'sbeeper');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.sbeeper));
    }
    if (adt.scodareatelefono3) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scodareatelefono3');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scodareatelefono3));
    }
    if (adt.semailcomision) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'semailcomision');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.semailcomision));
    }
    if (adt.snombreubicacion) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'snombreubicacion');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.snombreubicacion));
    }
    if (adt.scelular) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scelular');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scelular));
    }
    if (adt.scodtipodireccion) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'scodtipodireccion');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.scodtipodireccion));
    }
    if (adt.sdecripcion) {
        partEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcTracking.wsdl', 'sdecripcion');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.sdecripcion));
    }
}

function WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcBusquedainf_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outinformantesOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outinformantesOut') {
            resultsObject.outinformantesOut[resultsObject.outinformantesOut.length] = WebSvcTrackingSoapHttpPort_deserialize_SisacinformantesObjUser(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcDirtrack_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outdireccionesOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outdireccionesOut') {
            resultsObject.outdireccionesOut[resultsObject.outdireccionesOut.length] = WebSvcTrackingSoapHttpPort_deserialize_TrackingdireccionesObjUser(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcActdireccion_Out(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outstrtipperOut') {
            resultsObject.outstrtipperOut = getText(child);
        }
        if (localName(child) == 'outnumeroidentificacionOut') {
            resultsObject.outnumeroidentificacionOut = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcReem_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outatencionesOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outvalorpagadoOut') {
            resultsObject.outvalorpagadoOut = getText(child);
        }
        if (localName(child) == 'outcodplanOut') {
            resultsObject.outcodplanOut = getText(child);
        }
        if (localName(child) == 'outfechaaprobacionOut') {
            resultsObject.outfechaaprobacionOut = getText(child);
        }
        if (localName(child) == 'outbancoOut') {
            resultsObject.outbancoOut = getText(child);
        }
        if (localName(child) == 'outnumcontratoOut') {
            resultsObject.outnumcontratoOut = getText(child);
        }
        if (localName(child) == 'outatencionesOut') {
            resultsObject.outatencionesOut[resultsObject.outatencionesOut.length] = WebSvcTrackingSoapHttpPort_deserialize_SisacatencionesObjUser(child);
        }
        if (localName(child) == 'outvalorsolicitadoOut') {
            resultsObject.outvalorsolicitadoOut = getText(child);
        }
        if (localName(child) == 'outfechapagoOut') {
            resultsObject.outfechapagoOut = getText(child);
        }
        if (localName(child) == 'outfecharadicacionOut') {
            resultsObject.outfecharadicacionOut = getText(child);
        }
        if (localName(child) == 'outnombreOut') {
            resultsObject.outnombreOut = getText(child);
        }
        if (localName(child) == 'outclaseOut') {
            resultsObject.outclaseOut = getText(child);
        }
        if (localName(child) == 'outproductoOut') {
            resultsObject.outproductoOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outcasobizagiOut') {
            resultsObject.outcasobizagiOut = getText(child);
        }
        if (localName(child) == 'outnumfamiliaOut') {
            resultsObject.outnumfamiliaOut = getText(child);
        }
        if (localName(child) == 'outidentificacionOut') {
            resultsObject.outidentificacionOut = getText(child);
        }
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outcodasesorOut') {
            resultsObject.outcodasesorOut = getText(child);
        }
        if (localName(child) == 'outnumafiliadoOut') {
            resultsObject.outnumafiliadoOut = getText(child);
        }
        if (localName(child) == 'outnumeroOut') {
            resultsObject.outnumeroOut = getText(child);
        }
        if (localName(child) == 'outestadoOut') {
            resultsObject.outestadoOut = getText(child);
        }
        if (localName(child) == 'outsucursalOut') {
            resultsObject.outsucursalOut = getText(child);
        }
        if (localName(child) == 'outtipoplanOut') {
            resultsObject.outtipoplanOut = getText(child);
        }
        if (localName(child) == 'outnumreembolsoOut') {
            resultsObject.outnumreembolsoOut = getText(child);
        }
        if (localName(child) == 'outobservacionOut') {
            resultsObject.outobservacionOut = getText(child);
        }
        if (localName(child) == 'outnomasesorOut') {
            resultsObject.outnomasesorOut = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcDatoscartera_Out(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outtipodocumentoOut') {
            resultsObject.outtipodocumentoOut = getText(child);
        }
        if (localName(child) == 'outcorreosinscritosOut') {
            resultsObject.outcorreosinscritosOut = getText(child);
        }
        if (localName(child) == 'outimprimereporteOut') {
            resultsObject.outimprimereporteOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outbalanceOut') {
            resultsObject.outbalanceOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outfechamotorenvioOut') {
            resultsObject.outfechamotorenvioOut = getText(child);
        }
        if (localName(child) == 'outncfOut') {
            resultsObject.outncfOut = getText(child);
        }
        if (localName(child) == 'outpagosautomaticosOut') {
            resultsObject.outpagosautomaticosOut = getText(child);
        }
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outfechaemisionOut') {
            resultsObject.outfechaemisionOut = getText(child);
        }
        if (localName(child) == 'outfechalimiteOut') {
            resultsObject.outfechalimiteOut = getText(child);
        }
        if (localName(child) == 'outnumeroOut') {
            resultsObject.outnumeroOut = getText(child);
        }
        if (localName(child) == 'outvigenciaOut') {
            resultsObject.outvigenciaOut = getText(child);
        }
        if (localName(child) == 'outconceptoOut') {
            resultsObject.outconceptoOut = getText(child);
        }
        if (localName(child) == 'outmontoOut') {
            resultsObject.outmontoOut = getText(child);
        }
        if (localName(child) == 'outobservacionOut') {
            resultsObject.outobservacionOut = getText(child);
        }
        if (localName(child) == 'outcorreomotorenvioOut') {
            resultsObject.outcorreomotorenvioOut = getText(child);
        }
        if (localName(child) == 'outperiodoOut') {
            resultsObject.outperiodoOut = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcCuemed_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outestadosOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outfechafacturaOut') {
            resultsObject.outfechafacturaOut = getText(child);
        }
        if (localName(child) == 'outestadosOut') {
            resultsObject.outestadosOut[resultsObject.outestadosOut.length] = WebSvcTrackingSoapHttpPort_deserialize_SisaccuemedestObjUser(child);
        }
        if (localName(child) == 'outncfOut') {
            resultsObject.outncfOut = getText(child);
        }
        if (localName(child) == 'outnumfacturaOut') {
            resultsObject.outnumfacturaOut = getText(child);
        }
        if (localName(child) == 'outretencionfuenteOut') {
            resultsObject.outretencionfuenteOut = getText(child);
        }
        if (localName(child) == 'outtotalvalorglosasOut') {
            resultsObject.outtotalvalorglosasOut = getText(child);
        }
        if (localName(child) == 'outvalornetopagarOut') {
            resultsObject.outvalornetopagarOut = getText(child);
        }
        if (localName(child) == 'outfechaOut') {
            resultsObject.outfechaOut = getText(child);
        }
        if (localName(child) == 'outcantidaddevueltasOut') {
            resultsObject.outcantidaddevueltasOut = getText(child);
        }
        if (localName(child) == 'outfecharadicacionOut') {
            resultsObject.outfecharadicacionOut = getText(child);
        }
        if (localName(child) == 'outfechareingresoOut') {
            resultsObject.outfechareingresoOut = getText(child);
        }
        if (localName(child) == 'outcantidaddevueltasdefOut') {
            resultsObject.outcantidaddevueltasdefOut = getText(child);
        }
        if (localName(child) == 'outtelefonofaxOut') {
            resultsObject.outtelefonofaxOut = getText(child);
        }
        if (localName(child) == 'outsectorOut') {
            resultsObject.outsectorOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outvalorbrutoOut') {
            resultsObject.outvalorbrutoOut = getText(child);
        }
        if (localName(child) == 'outcantidadatencionesOut') {
            resultsObject.outcantidadatencionesOut = getText(child);
        }
        if (localName(child) == 'outvalorverificacionOut') {
            resultsObject.outvalorverificacionOut = getText(child);
        }
        if (localName(child) == 'outvalordevueltasdefOut') {
            resultsObject.outvalordevueltasdefOut = getText(child);
        }
        if (localName(child) == 'outvalordevueltasOut') {
            resultsObject.outvalordevueltasOut = getText(child);
        }
        if (localName(child) == 'outemailOut') {
            resultsObject.outemailOut = getText(child);
        }
        if (localName(child) == 'outcantidadverificacionOut') {
            resultsObject.outcantidadverificacionOut = getText(child);
        }
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outtelefonoprincipalOut') {
            resultsObject.outtelefonoprincipalOut = getText(child);
        }
        if (localName(child) == 'outvaloratencionesOut') {
            resultsObject.outvaloratencionesOut = getText(child);
        }
        if (localName(child) == 'outestadoOut') {
            resultsObject.outestadoOut = getText(child);
        }
        if (localName(child) == 'outtelefonoauxiliarOut') {
            resultsObject.outtelefonoauxiliarOut = getText(child);
        }
        if (localName(child) == 'outdireccionOut') {
            resultsObject.outdireccionOut = getText(child);
        }
        if (localName(child) == 'outnumradicacionOut') {
            resultsObject.outnumradicacionOut = getText(child);
        }
        if (localName(child) == 'outciudadOut') {
            resultsObject.outciudadOut = getText(child);
        }
        if (localName(child) == 'outtotalcalculadoOut') {
            resultsObject.outtotalcalculadoOut = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_WebSvcTrackingUser_prcInfgenafi_Out(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outnomempleadorOut') {
            resultsObject.outnomempleadorOut = getText(child);
        }
        if (localName(child) == 'outideempleadorOut') {
            resultsObject.outideempleadorOut = getText(child);
        }
        if (localName(child) == 'outnombreOut') {
            resultsObject.outnombreOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outaseclaveOut') {
            resultsObject.outaseclaveOut = getText(child);
        }
        if (localName(child) == 'outfamiliaOut') {
            resultsObject.outfamiliaOut = getText(child);
        }
        if (localName(child) == 'outcontratoOut') {
            resultsObject.outcontratoOut = getText(child);
        }
        if (localName(child) == 'outasetipperOut') {
            resultsObject.outasetipperOut = getText(child);
        }
        if (localName(child) == 'outidentificacionOut') {
            resultsObject.outidentificacionOut = getText(child);
        }
        if (localName(child) == 'outtipperempleadorOut') {
            resultsObject.outtipperempleadorOut = getText(child);
        }
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outaseideOut') {
            resultsObject.outaseideOut = getText(child);
        }
        if (localName(child) == 'outnumideempleadorOut') {
            resultsObject.outnumideempleadorOut = getText(child);
        }
        if (localName(child) == 'outnumafiliadoOut') {
            resultsObject.outnumafiliadoOut = getText(child);
        }
        if (localName(child) == 'outnomprestadorOut') {
            resultsObject.outnomprestadorOut = getText(child);
        }
        if (localName(child) == 'outtipperprestadorOut') {
            resultsObject.outtipperprestadorOut = getText(child);
        }
        if (localName(child) == 'outideprestadorOut') {
            resultsObject.outideprestadorOut = getText(child);
        }
        if (localName(child) == 'outcodprestadorOut') {
            resultsObject.outcodprestadorOut = getText(child);
        }
        if (localName(child) == 'outnomasesorOut') {
            resultsObject.outnomasesorOut = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_SisacinformantesObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'nombre') {
            resultsObject.nombre = getText(child);
        }
        if (localName(child) == 'color') {
            resultsObject.color = getText(child);
        }
        if (localName(child) == 'tipopersona') {
            resultsObject.tipopersona = getText(child);
        }
        if (localName(child) == 'tipoinformante') {
            resultsObject.tipoinformante = getText(child);
        }
        if (localName(child) == 'codtipoinformante') {
            resultsObject.codtipoinformante = getText(child);
        }
        if (localName(child) == 'identificacion') {
            resultsObject.identificacion = getText(child);
        }
        if (localName(child) == 'tipoid') {
            resultsObject.tipoid = getText(child);
        }
        if (localName(child) == 'consecutivo') {
            resultsObject.consecutivo = getText(child);
        }
        if (localName(child) == 'codigo') {
            resultsObject.codigo = getText(child);
        }
        if (localName(child) == 'segmento') {
            resultsObject.segmento = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_TrackingdireccionesObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'codsucursal') {
            resultsObject.codsucursal = getText(child);
        }
        if (localName(child) == 'nombreubicacion') {
            resultsObject.nombreubicacion = getText(child);
        }
        if (localName(child) == 'codciudad') {
            resultsObject.codciudad = getText(child);
        }
        if (localName(child) == 'sucprincipal') {
            resultsObject.sucprincipal = getText(child);
        }
        if (localName(child) == 'barrio') {
            resultsObject.barrio = getText(child);
        }
        if (localName(child) == 'codtipodireccion') {
            resultsObject.codtipodireccion = getText(child);
        }
        if (localName(child) == 'codareatelefono3') {
            resultsObject.codareatelefono3 = getText(child);
        }
        if (localName(child) == 'emailcomision') {
            resultsObject.emailcomision = getText(child);
        }
        if (localName(child) == 'telefono') {
            resultsObject.telefono = getText(child);
        }
        if (localName(child) == 'codareatelefono1') {
            resultsObject.codareatelefono1 = getText(child);
        }
        if (localName(child) == 'sucursal') {
            resultsObject.sucursal = getText(child);
        }
        if (localName(child) == 'emailfacturas') {
            resultsObject.emailfacturas = getText(child);
        }
        if (localName(child) == 'celular') {
            resultsObject.celular = getText(child);
        }
        if (localName(child) == 'codprovincia') {
            resultsObject.codprovincia = getText(child);
        }
        if (localName(child) == 'codareatelefono2') {
            resultsObject.codareatelefono2 = getText(child);
        }
        if (localName(child) == 'codsuc') {
            resultsObject.codsuc = getText(child);
        }
        if (localName(child) == 'provincia') {
            resultsObject.provincia = getText(child);
        }
        if (localName(child) == 'codbarrio') {
            resultsObject.codbarrio = getText(child);
        }
        if (localName(child) == 'decripcion') {
            resultsObject.decripcion = getText(child);
        }
        if (localName(child) == 'emailcobros') {
            resultsObject.emailcobros = getText(child);
        }
        if (localName(child) == 'codtipoubicacion') {
            resultsObject.codtipoubicacion = getText(child);
        }
        if (localName(child) == 'beeper') {
            resultsObject.beeper = getText(child);
        }
        if (localName(child) == 'direccion') {
            resultsObject.direccion = getText(child);
        }
        if (localName(child) == 'ciudad') {
            resultsObject.ciudad = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_SisacatencionesObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'nombreafiliado') {
            resultsObject.nombreafiliado = getText(child);
        }
        if (localName(child) == 'servicio') {
            resultsObject.servicio = getText(child);
        }
        if (localName(child) == 'identificacionafiliado') {
            resultsObject.identificacionafiliado = getText(child);
        }
        if (localName(child) == 'procedimientos') {
            resultsObject.procedimientos = WebSvcTrackingSoapHttpPort_deserialize_SisacprocedimientosList(child);
        }
        if (localName(child) == 'diagnostico') {
            resultsObject.diagnostico = getText(child);
        }
        if (localName(child) == 'identificacionprestador') {
            resultsObject.identificacionprestador = getText(child);
        }
        if (localName(child) == 'tipoatencion') {
            resultsObject.tipoatencion = getText(child);
        }
        if (localName(child) == 'origen') {
            resultsObject.origen = getText(child);
        }
        if (localName(child) == 'nombreprestador') {
            resultsObject.nombreprestador = getText(child);
        }
        if (localName(child) == 'numeroatencion') {
            resultsObject.numeroatencion = getText(child);
        }
        if (localName(child) == 'fechaatencion') {
            resultsObject.fechaatencion = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_SisaccuemedestObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'concepto') {
            resultsObject.concepto = getText(child);
        }
        if (localName(child) == 'valor') {
            resultsObject.valor = getText(child);
        }
        if (localName(child) == 'estado') {
            resultsObject.estado = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_SisacprocedimientosList(valueEl) {
    var resultsObject = {};
    resultsObject.array = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'array') {
            resultsObject.array[resultsObject.array.length] = WebSvcTrackingSoapHttpPort_deserialize_SisacprocedimientosObjUser(child);
        }
    }
    return resultsObject;
}

function WebSvcTrackingSoapHttpPort_deserialize_SisacprocedimientosObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'codprocedimiento') {
            resultsObject.codprocedimiento = getText(child);
        }
        if (localName(child) == 'totalbeneficiario') {
            resultsObject.totalbeneficiario = getText(child);
        }
        if (localName(child) == 'nombreprocedimiento') {
            resultsObject.nombreprocedimiento = getText(child);
        }
        if (localName(child) == 'valorunitario') {
            resultsObject.valorunitario = getText(child);
        }
        if (localName(child) == 'porcentajereconocido') {
            resultsObject.porcentajereconocido = getText(child);
        }
        if (localName(child) == 'valorfactura') {
            resultsObject.valorfactura = getText(child);
        }
        if (localName(child) == 'valorsistema') {
            resultsObject.valorsistema = getText(child);
        }
    }
    return resultsObject;
}
