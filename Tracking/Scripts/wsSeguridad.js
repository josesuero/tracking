﻿function firstChildElement(node) {
    if (!node) return null; var child = node.firstChild;
    while (child) {
        if (child.nodeType == 1)
            return child;
        child = child.nextSibling;
    }
    return null;
}

function nextSiblingElement(node) {
    if (!node) return null; var sibling = node.nextSibling;
    while (sibling) {
        if (sibling.nodeType == 1)
            return sibling;
        sibling = sibling.nextSibling;
    }
    return null;
}

function getText(node) {
    if (!node) return null; var text = '';
    var child = node.firstChild;
    while (child) {
        if (child.nodeType == 3) {
            text = text + child.nodeValue;
        }
        child = child.nextSibling;
    }
    return text;
}

function invokeSync(url, xmlDoc) {
    var req = null; if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("POST", url, false);
        req.setRequestHeader("Content-Type", "text/xml");
        req.send(xmlDoc);
        return req.responseXML;
    }
}

function invokeAsync(url, xmlDoc, callback) {
    var req = null;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    callback(req.responseXML);
                }
            }
        }
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type", "text/xml");
        req.send(xmlDoc);
    }
}

function createNewDocument() {
    var xmlDoc = null;
    if (document.implementation && document.implementation.createDocument) {
        xmlDoc = document.implementation.createDocument("", "", null);
    } else if (window.ActiveXObject) {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    }
    return xmlDoc;
}

function createElementNS(xmlDoc, namespace, localName) {
    var element = null;
    if (typeof xmlDoc.createElementNS != 'undefined') {
        element = xmlDoc.createElementNS(namespace, localName);
    }
    else if (typeof xmlDoc.createNode != 'undefined') {
        if (namespace) {
            element = xmlDoc.createNode(1, localName, namespace);
        } else {
            element = xmlDoc.createElement(localName);
        }
    }
    return element;
}

function localName(element) {
    if (element.localName)
        return element.localName;
    else
        return element.baseName;
}



function WebSvcAccesoSoapHttpPort_prcAccrolsel(_inAccsiscod) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccrolselElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccsiscod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccsiscod));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccrolsel_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcAccesoSoapHttpPort_prcAccrolselAsync(_inAccsiscod, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccrolselElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccsiscod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccsiscod));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccrolsel_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcAccesoSoapHttpPort_prcAccperrolexcsel(_inAccsiscod, _inAccrolcod, _inAccseccod) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccperrolexcselElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccsiscod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccsiscod));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccrolcod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccrolcod));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccseccod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccseccod));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccperrolexcsel_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcAccesoSoapHttpPort_prcAccperrolexcselAsync(_inAccsiscod, _inAccrolcod, _inAccseccod, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccperrolexcselElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccsiscod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccsiscod));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccrolcod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccrolcod));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccseccod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccseccod));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccperrolexcsel_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcAccesoSoapHttpPort_prcAccusrpersec(_inAccsiscod, _inAccseccod, _inAccusrcod) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccusrpersecElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccsiscod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccsiscod));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccseccod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccseccod));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccusrcod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccusrcod));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccusrpersec_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcAccesoSoapHttpPort_prcAccusrpersecAsync(_inAccsiscod, _inAccseccod, _inAccusrcod, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccusrpersecElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccsiscod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccsiscod));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccseccod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccseccod));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccusrcod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccusrcod));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccusrpersec_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcAccesoSoapHttpPort_prcAccsecsel(_inAccsiscod) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccsecselElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccsiscod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccsiscod));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccsecsel_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcAccesoSoapHttpPort_prcAccsecselAsync(_inAccsiscod, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccsecselElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'inAccsiscod');
    paramEl.appendChild(xmlDoc.createTextNode(_inAccsiscod));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccsecsel_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcAccesoSoapHttpPort_prcAccsissel() {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccsisselElement');
    body.appendChild(parameterParent);
    var responseDoc = invokeSync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccsissel_Out(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function WebSvcAccesoSoapHttpPort_prcAccsisselAsync(callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://com/universal/WebSvcAcceso.wsdl', 'prcAccsisselElement');
    body.appendChild(parameterParent);
    var resultsProcessor = function (responseDoc) {
        var resultsObj = null;
        body = firstChildElement(responseDoc.documentElement);
        if (localName(body) != 'Body') {
            body = nextSiblingElement(body);
        }

        var resultEl = firstChildElement(body);
        resultEl = firstChildElement(resultEl);
        if (resultEl)
            resultObj = WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccsissel_Out(resultEl);
        else
            resultObj = null;
        callback(resultObj);
    }
    invokeAsync('http://login.universal.com.do:7778/WebSvcAccesoTest/WebSvcAccesoSoapHttpPort', xmlDoc, resultsProcessor);
}

function WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccrolsel_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outaccrolOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outaccrolOut') {
            resultsObject.outaccrolOut[resultsObject.outaccrolOut.length] = WebSvcAccesoSoapHttpPort_deserialize_AccrolObjUser(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccperrolexcsel_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outaccexcOut = [];
    resultsObject.outaccperrolOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outaccexcOut') {
            resultsObject.outaccexcOut[resultsObject.outaccexcOut.length] = WebSvcAccesoSoapHttpPort_deserialize_AccexeObjUser(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outaccperrolOut') {
            resultsObject.outaccperrolOut[resultsObject.outaccperrolOut.length] = WebSvcAccesoSoapHttpPort_deserialize_AccperrolObjUser(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccusrpersec_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outpermisosOut = [];
    resultsObject.outexcepcionesOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outnombreOut') {
            resultsObject.outnombreOut = getText(child);
        }
        if (localName(child) == 'outareacodOut') {
            resultsObject.outareacodOut = getText(child);
        }
        if (localName(child) == 'outareanomOut') {
            resultsObject.outareanomOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outsubareacodOut') {
            resultsObject.outsubareacodOut = getText(child);
        }
        if (localName(child) == 'outpermisosOut') {
            resultsObject.outpermisosOut[resultsObject.outpermisosOut.length] = WebSvcAccesoSoapHttpPort_deserialize_AccperObjUser(child);
        }
        if (localName(child) == 'outemailOut') {
            resultsObject.outemailOut = getText(child);
        }
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outtipousuarioOut') {
            resultsObject.outtipousuarioOut = getText(child);
        }
        if (localName(child) == 'outexcepcionesOut') {
            resultsObject.outexcepcionesOut[resultsObject.outexcepcionesOut.length] = WebSvcAccesoSoapHttpPort_deserialize_AccexeObjUser(child);
        }
        if (localName(child) == 'outestadoOut') {
            resultsObject.outestadoOut = getText(child);
        }
        if (localName(child) == 'outsubareanomOut') {
            resultsObject.outsubareanomOut = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccsecsel_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outaccsecOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outaccsecOut') {
            resultsObject.outaccsecOut[resultsObject.outaccsecOut.length] = WebSvcAccesoSoapHttpPort_deserialize_AccsecObjUser(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_WebSvcAccesoUser_prcAccsissel_Out(valueEl) {
    var resultsObject = {};
    resultsObject.outaccsisOut = [];
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'outscodejecucioninterfaceOut') {
            resultsObject.outscodejecucioninterfaceOut = getText(child);
        }
        if (localName(child) == 'outsdescmensajeOut') {
            resultsObject.outsdescmensajeOut = getText(child);
        }
        if (localName(child) == 'outscodmensajeOut') {
            resultsObject.outscodmensajeOut = getText(child);
        }
        if (localName(child) == 'outaccsisOut') {
            resultsObject.outaccsisOut[resultsObject.outaccsisOut.length] = WebSvcAccesoSoapHttpPort_deserialize_AccsisObjUser(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_AccrolObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'accrolcod') {
            resultsObject.accrolcod = getText(child);
        }
        if (localName(child) == 'accrolnom') {
            resultsObject.accrolnom = getText(child);
        }
        if (localName(child) == 'acctiprolcod') {
            resultsObject.acctiprolcod = getText(child);
        }
        if (localName(child) == 'accsiscod') {
            resultsObject.accsiscod = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_AccexeObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'accexeide') {
            resultsObject.accexeide = getText(child);
        }
        if (localName(child) == 'acctipobjcod') {
            resultsObject.acctipobjcod = getText(child);
        }
        if (localName(child) == 'acctipviscod') {
            resultsObject.acctipviscod = getText(child);
        }
        if (localName(child) == 'accexecod') {
            resultsObject.accexecod = getText(child);
        }
        if (localName(child) == 'accexenom') {
            resultsObject.accexenom = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_AccperrolObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'accrolcod') {
            resultsObject.accrolcod = getText(child);
        }
        if (localName(child) == 'acctipviscod') {
            resultsObject.acctipviscod = getText(child);
        }
        if (localName(child) == 'accseccod') {
            resultsObject.accseccod = getText(child);
        }
        if (localName(child) == 'accpercod') {
            resultsObject.accpercod = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_AccperObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'acctipviscod') {
            resultsObject.acctipviscod = getText(child);
        }
        if (localName(child) == 'accpercod') {
            resultsObject.accpercod = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_AccsecObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'accsiscod') {
            resultsObject.accsiscod = getText(child);
        }
        if (localName(child) == 'accsecnom') {
            resultsObject.accsecnom = getText(child);
        }
        if (localName(child) == 'accseccod') {
            resultsObject.accseccod = getText(child);
        }
        if (localName(child) == 'accsecdes') {
            resultsObject.accsecdes = getText(child);
        }
    }
    return resultsObject;
}

function WebSvcAccesoSoapHttpPort_deserialize_AccsisObjUser(valueEl) {
    var resultsObject = {};
    for (var child = firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'accsiscod') {
            resultsObject.accsiscod = getText(child);
        }
        if (localName(child) == 'accsisnom') {
            resultsObject.accsisnom = getText(child);
        }
    }
    return resultsObject;
}