﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="Tracking.UploadFile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    $(function () {
        var obj = $("#object_to_upload");
        var pos;
            
        for(var j = 0; j < dataTracking.typesDoc.length; j++){
            pos = dataTracking.typesDoc[j];
            if ($.inArray(dataTracking.user.outsubareacodOut, pos.ID_SUB_AREA.split(",")) != -1 && pos.ARCHIVO == 1
            || (pos.ID_SUB_AREA == -1 && pos.ARCHIVO == 1)) {
                obj.append("<option value=" + pos.ID + ">" + pos.NOMBRE + "</option>");
            }                            
        }

    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" action="UploadFile.aspx" runat="server">
        <div>
        <table>
            <tr>
                <td>
                    <asp:FileUpload ID="FileUpload1" name="file" runat="server" />
                </td>
                <td>
                    <select id="object_to_upload" name="type_document"></select>
                </td>
                <td>
                    <input type="submit" id="send" value="Upload"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Text="Resultado: "></asp:Label>
                </td>
            </tr>
        </table>
        </div>
<%--<asp:GridView ID="GridView1" runat="server" onrowdatabound="GridView1_RowDataBound">
</asp:GridView>--%>
    </form>
</asp:Content>
