﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tracking.Core;
using Jayrock.Json;
using System.Configuration;
using Tracking.WsSeguridad;
using Jayrock.Json.Conversion;
using TrackingFile;
using System.IO;

namespace Tracking
{
    public partial class UploadFile : System.Web.UI.Page
    {
        private cgdata cgdata;
        private tracking tracking;
        private string id_user;
        private JsonObject dataUser;
        private string tipo_documento;

        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "";
            if (Request.Form.AllKeys.Contains("type_document") && FileUpload1.HasFile && Request.Form.Count != 0)
            {
                tipo_documento = Request.Form["type_document"];
                proccess_upload();
            }
            else if (Request.Form.Count != 0)
            {
                Label1.Text = "Resultado: Verifique el archivo o que el documento seleccionado sea el correcto.";
            }
            HttpResponse.RemoveOutputCacheItem("/UploadFile.aspx");
            FileUpload1 = null;
        }
        public JsonObject getDataUserFromWs(string id_user_)
        {

            JsonObject obj = (JsonObject)Session["wsDataUser"];
            if (Session["wsDataUser"] != null)
            {
                return obj;
            }
            WebSvcAcceso ws = new WebSvcAcceso();
            WebSvcAccesoUser_prcAccusrpersec_Out user_Out = ws.prcAccusrpersec("1    ", "1    ", id_user_);
            obj = (JsonObject)JsonConvert.Import(JsonConvert.ExportToString(user_Out));
            obj.Put("documentos_area", tracking.getDocumentosArea(int.Parse(user_Out.outsubareacodOut)));
            obj.Put("id", id_user_);
            Session["wsDataUser"] = obj;
            return obj;
        }

        public string whoame()
        {
            string user = ConfigurationManager.AppSettings["debug"];
            if (user == "")
            {
                user = System.Threading.Thread.CurrentPrincipal.Identity.Name.Replace("SEGUROS\\", "");
            }
            return user;
        }

        private void proccess_upload()
        {

            Dictionary<string, object>[] documentos = null;           
            
            // LECTURA DE ARCHIVO
            try
            {
                cgdata = new cgdata();
                tracking = new tracking(cgdata);
                id_user = whoame();
                dataUser = (JsonObject)getDataUserFromWs(id_user);
                string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string fileLocation = Server.MapPath("~/App_Data/" + fileName);
                FileUpload1.SaveAs(fileLocation);
                string extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
                documentos = IOFile.toDictionary(fileLocation, extension, tipo_documento);

            }
            catch (Exception ex)
            {
                Label1.Text = "Resultado: " + ex.Message + " / " + ex.StackTrace;
                tracking.removeCache("reglas");
                ScriptManager.RegisterClientScriptBlock
                (
                    this,
                    this.GetType(),
                    "success",
                    "alert('Error al intentar cargar el archivo.Resultado de error: Formato de archivo incorrecto." + ex.Message + " / " + ex.StackTrace + "');window.location.assign('"
                    + ResolveUrl("UploadFile.aspx") + "');",
                    true
                );
            }

            // CARGA DE DOCUMENTOS A LA BD
            try
            {

                cgdata.connection.Open();
                cgdata.BeginTransaction();

                Dictionary<string, object> dataPaquete;
                Dictionary<string, string> dataPaq;
                Dictionary<string, string> direccion_;

                // REGLA FIJA PARA DOCUMENTOS MANUALES O SUBIDOS POR CARGA DE ARCHIVO

                JsonObject rule = (JsonObject)((JsonArray)tracking.getReglas()["-1"])[0];
                JsonArray levels = (JsonArray)rule["niveles"];
                JsonObject level = new JsonObject();
                levels.Add(level);
                string dest_cod_tipo = "";
                
                // DESTINATARIO FINAL
                Dictionary<string,string> dest_final;
                string id_other_dest = "";

                Dictionary<string, object> batc = (Dictionary<string, object>)tracking.getAllBatch()[rule["batch"].ToString()];
                int cont = tracking.aumentarBatch(int.Parse(batc["ID"].ToString()));
                tracking.removeCache("batchs");

                // NOTA: SI CAMBIA EL LARGO DEL CODIGO ORDEN, TAMBIEN DEBE SER CAMBIADO AQUI
                string batch_code = batc["PREFIJO"].ToString() + cont.ToString().PadLeft(11, '0');

                string id_sub_area = dataUser["outsubareacodOut"].ToString();

                JsonObject package;

                for (int i = 0; i < documentos.Length; i++)
                {

                    dataPaquete = documentos[i];
                    dataPaquete.Add("ID_USUARIO", id_user);
                    dataPaquete.Add("ID_AREA", id_sub_area);
                    dataPaquete.Add("COD_PAQUETE", "");

                    // INSERTA DESTINATARIO FINAL
                    if (dataPaquete.ContainsKey("DESTINATARIO_FINAL"))
                    {
                        dest_final = (Dictionary<string,string>) dataPaquete["DESTINATARIO_FINAL"];
                        id_other_dest = tracking.insertDestinatario(
                            dest_final["CODIGO"],
                            dest_final["NOMBRE"],
                            dest_final["TIPO"],
                            dest_final["TIPO_PERSONA"]
                        ) + ",";
                    }                  

                    // VERIFICA SI DIRECCION ESTA EN EL ARCHIVO O SERA CONSULTADA AL SERVICIO
                    if (dataPaquete.ContainsKey("DATA_DIRECCION"))
                    {
                        direccion_ = (Dictionary<string, string>)dataPaquete["DATA_DIRECCION"];
                    }
                    else
                    {
                        dataPaq = dataPaquete.ToDictionary(k => k.Key, k => k.Value.ToString());
                        direccion_ = tracking.wsDireccion(dataPaq);
                        dataPaquete.Add("DATA_DIRECCION", direccion_);
                    }

                    dest_cod_tipo = dataPaquete["DESTINATARIO"].ToString();

                    level["tipo"] = "campo";
                    level["valor"] = dest_cod_tipo + "CODIGO";
                    levels[levels.Length - 1] = level;

                    rule["niveles"] = levels;
                    rule["destinatario"] = dest_cod_tipo;

                    dataPaquete = tracking.applyRulesDocManual(dataPaquete, rule, batch_code);

                    //INSERTA DESTINATARIO
                    int id_dest = tracking.insertDestinatario(dataPaquete[dest_cod_tipo + "CODIGO"].ToString(), dataPaquete[dest_cod_tipo + "NOMBRE"].ToString(), dataPaquete[dest_cod_tipo + "TIPO"].ToString(), dataPaquete[dest_cod_tipo + "TIPO_PERSONA"].ToString());

                    // INSERTA LA DIRECCION            
                    int id_dir = tracking.insertDireccion(id_dest, direccion_["codciudad"], direccion_["codprovincia"], direccion_["codbarrio"], direccion_["codtipodireccion"], direccion_["direccion"], direccion_["telefono"]);

                    // INSERT DEL PAQUETE
                    package = tracking.insertPackage(dataPaquete["COD_ORDEN"].ToString(), dataPaquete["COD_PAQUETE"].ToString()
                    , dataPaquete["NUM_REF"].ToString(), dataPaquete["ID_AREA"].ToString(), dataPaquete["ID_OBJETO"].ToString()
                    , int.Parse(dataPaquete["ID_TIPO_OBJETO"].ToString()), "1", dataPaquete["ID_SUCURSAL"].ToString(), id_dir, id_dest, id_dest + "," + id_other_dest, id_user, dataPaquete["NOTES"].ToString());

                }
                tracking.removeCache("reglas");
                tracking.insertLog("-1", 1, id_user, "Creado via Subida de Archivo", batch_code, id_sub_area);
                cgdata.commitTransaction();
                cgdata.connection.Close();
                ScriptManager.RegisterClientScriptBlock
                (
                    this,
                    this.GetType(),
                    "success",
                    "alert('Ejecucion exitosa. Se han cargado los documentos correctamente.');window.location.assign('"
                    + ResolveUrl("UploadFile.aspx") + "');",
                    true
                );
                Label1.Text = "Resultado: Ejecucion exitosa";
            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                tracking.removeCache("reglas");
                ScriptManager.RegisterClientScriptBlock
                (
                    this,
                    this.GetType(),
                    "success",
                    "alert('Error al intentar cargar el archivo.Resultado de error: "  + ex.Message + " / " + ex.StackTrace + "');window.location.assign('"
                    + ResolveUrl("UploadFile.aspx") + "');",
                    true
                );
                Label1.Text = "Resultado: Existen datos incorrectos en el archivo, favor verificar. " + ex.Message + " / " + ex.StackTrace;
            }

        }

    }
}
