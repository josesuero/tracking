﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.DataAccess.Client;
using Jayrock.Json;
using System.Reflection;
using Jayrock.Json.Conversion;
using System.Configuration;
using System.ComponentModel;
using System.DirectoryServices;
using Tracking.Core;
using Tracking.WsSeguridad;
using System.Web.SessionState;
using Tracking.Clases_Globales;
using System.Threading;


namespace Tracking
{
    public class ajaxserver : IHttpHandler, IRequiresSessionState
    {

        public bool IsReusable
        {
            get { return false; }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public void ProcessRequest(HttpContext context)
        {
            try
            {

                Type t = typeof(ajaxfunctions);
                ajaxfunctions ajaxfuncs = new ajaxfunctions(context);
                MethodInfo mi = t.GetMethod(context.Request["requesttype"]);
                object[] args = new object[1];
                JsonObject obj = new JsonObject();
                string data = context.Request["data"];
                if (data != null)
                {
                    data = data.Replace("\n", "\\n");
                    data = data.Replace("\r", "\\r");
                }
                if (data != null && (data.StartsWith("{") || data.StartsWith("[")))
                    obj = (JsonObject)JsonConvert.Import(data);
                args[0] = obj;
                mi.Invoke(ajaxfuncs, args);
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    context.Response.Write("Error: " + e.Message);
                }
                else
                {
                    context.Response.Write("Error: " + e.InnerException.Message + e.InnerException.StackTrace);
                }
            }
        }
    }

    #region ajaxfunctions
    public class ajaxfunctions
    {

        public cgdata cgdata;
        public tracking tracking;
        HttpContext context;
        public string id_user;
        public JsonObject dataUser;

        public ajaxfunctions()
        {

        }

        public ajaxfunctions(HttpContext context)
        {
            this.context = context;
            cgdata = new cgdata();
            tracking = new tracking(cgdata);
            id_user = whoame();
            JsonObject data = new JsonObject();
            dataUser = (JsonObject)getDataUserFromWs(id_user);
        }

        // RETORNA EL ID DEL USUARIO LOGUEADO
        public string whoame()
        {
            string user = ConfigurationManager.AppSettings["debug"];
            if (user == "")
            {
                user = System.Threading.Thread.CurrentPrincipal.Identity.Name.Replace("SEGUROS\\", "");
            }
            return user;
        }
        public void userData(JsonObject data)
        {
            //JsonObject name = new JsonObject();
            //name.Put("name", GetFullName("SEGUROS\\" + data["user"]));
            //context.Response.Write(name);
            write(GetFullName((string)data["user"]));
        }

        public JsonObject userData()
        {
            return (JsonObject)JsonConvert.Import(GetFullName(id_user));
        }

        public string GetFullName(string strLogin)
        {
            string connection = "LDAP://su02dc.seguros.local";
            DirectorySearcher dssearch = new DirectorySearcher(connection);

            dssearch.Filter = "(sAMAccountName=" + strLogin + ")";

            SearchResult sresult = dssearch.FindOne();

            DirectoryEntry dsresult = sresult.GetDirectoryEntry();

            JsonObject userdata = new JsonObject();
            userdata.Put("id", strLogin);
            //userdata.Put("name", dsresult.Properties["givenName"][0].ToString() + " " + dsresult.Properties["sn"][0].ToString());
            //userdata.Put("email", dsresult.Properties["mail"][0].ToString());
            return userdata.ToString();

        }

        #region "Utilities"

        /// <summary>
        /// Funcion convierte un objeto a Json
        /// </summary>
        /// <param name="obj">Objeto a convertir a Json</param>
        /// <returns></returns>
        public String toJSON(object obj)
        {
            return JsonConvert.ExportToString(obj);
        }

        public JsonObject getDataUserFromWs(string id_user_)
        {

            JsonObject obj = (JsonObject)context.Session["wsDataUser"];
            if (context.Session["wsDataUser"] != null)
            {
                return obj;
            }
            WebSvcAcceso ws = new WebSvcAcceso();
            WebSvcAccesoUser_prcAccusrpersec_Out user_Out = ws.prcAccusrpersec("1    ", "1    ", id_user_);
            obj = (JsonObject)JsonConvert.Import(JsonConvert.ExportToString(user_Out));
            if (obj["outscodmensajeOut"].ToString() != "06666")
            {
                obj.Put("documentos_area", tracking.getDocumentosArea(int.Parse(user_Out.outsubareacodOut)));
            }            
            obj.Put("id", id_user_);
            context.Session["wsDataUser"] = obj;
            return obj;
        }

        /// <summary>
        /// Devuelve un objeto a la pagina
        /// </summary>
        /// <param name="str">Objeto a devolver</param>
        private void write(object str)
        {
            if (this.context == null)
            {
                return;
            }
            this.context.Response.Write(str.ToString());
        }

        #endregion

        #region "Tracking Methods"

        // RETORNA LAS PROVINCIAS CON SUS CIUDADES CORRESPONDIENTES
        // Y LAS CIUDADES CON LOS SECTORES QUE LE CORRESPONDEN
        public JsonArray getFromServerProvincias()
        {
            JsonArray provincias = tracking.getProvincias();
            return provincias;

            //JsonObject provincia = new JsonObject();
            //JsonArray ciudades = new JsonArray();
            //JsonObject ciudad = new JsonObject();
            //JsonArray sectores = new JsonArray();

            //for (int i = 0; i < provincias.Length; i++)
            //{
            //    provincia = (JsonObject)provincias[i];
            //    ciudades = tracking.getCiudades(provincia["codprovincia"].ToString());
            //    for (int j = 0; j < ciudades.Length; j++)
            //    {
            //        ciudad = (JsonObject)ciudades[j];
            //        sectores = tracking.getSectores(ciudad["codciudad"].ToString());
            //        // SE LE AGREGAN LOS SECTORES QUE PERTENECEN A CADA CIUDAD
            //        ciudad["sectores"] = sectores;
            //        ciudades[j] = ciudad;
            //    }
            //    // SE LE AGREGAN LAS CIUDADES QUE PERTENECEN A CADA PROVINCIA
            //    provincia["ciudades"] = ciudades;
            //    provincias[i] = provincia;
            //}

            //return provincias;
        }

        public JsonObject getDataProvincia(JsonObject provincia)
        {
            JsonArray ciudades = new JsonArray();
            JsonObject ciudad = new JsonObject();
            JsonArray sectores = new JsonArray();
            ciudades = tracking.getCiudades(provincia["codprovincia"].ToString());
            for (int j = 0; j < ciudades.Length; j++)
            {
                ciudad = (JsonObject)ciudades[j];
                sectores = tracking.getSectores(ciudad["codciudad"].ToString());
                // SE LE AGREGAN LOS SECTORES QUE PERTENECEN A CADA CIUDAD
                ciudad["sectores"] = sectores;
                ciudades[j] = ciudad;
            }
            // SE LE AGREGAN LAS CIUDADES QUE PERTENECEN A CADA PROVINCIA
            provincia["ciudades"] = ciudades;
            return provincia;
        }

        #endregion

        #region "Tracking Methods Ajax"

        // RETORNA DATA NECESARIA PARA CARGAR TRACKING
        public void dataLoadTracking(JsonObject data)
        {

            string from = data["from"].ToString();
            JsonObject dataTracking = new JsonObject();
            dataTracking["user"] = dataUser;

            // usado para desarrollo
            if(from == "codeconverter"){
                write(dataTracking);
                return;
            }

            if (dataUser["outscodmensajeOut"].ToString() == "06666")
            {                
                write(dataTracking);
                return;
            }
            dataTracking["typesDoc"] = tracking.getTypesDoc();
            dataTracking["status"] = (JsonArray)JsonConvert.Import(JsonConvert.ExportToString(tracking.getStatusPkg().Tables[0].Rows));
            dataTracking["provincias"] = getFromServerProvincias();
            dataTracking["areas"] = tracking.getAreas();
            dataTracking["sucursales"] = tracking.getSucursales();
            dataTracking["tipos_destinatarios"] = tracking.getTiposDestinatario();

            switch (from)
            {
                case "default":
                    dataTracking["packages"] = tracking.getUserPkg(id_user, dataUser["outsubareacodOut"].ToString(), "", 0);
                    dataTracking["batch"] = tracking.getJsonBatchs(dataUser["outsubareacodOut"].ToString());
                    break;
                case "reportes":

                    break;
            }

            write(dataTracking);
        }


        // RETORNA DATA DE DIRECCION
        public void getProvincias(JsonObject data) {
            write(getFromServerProvincias());
        }

        // RETORNA DATA DE DIRECCION
        public void getProvincia(JsonObject data)
        {
            write(getDataProvincia(data));
        }


        // RETORNA DATA DEL USUARIO
        public void getDataUser(JsonObject data)
        {
            write(getDataUserFromWs(id_user));
        }

        //public void searchCustomer(JsonObject data) {
        //    parameters.add("in_entrada", "string", data["filter"], "in");
        //    parameters.add("in_tipoentrada", "string", "", "in");
        //    SISACINFORMANTES_LIST sisacList = new SISACINFORMANTES_LIST();
        //    parameters.add("out_informantes", "obj", sisacList, "out", "SISAC.SISACINFORMANTES_LIST");
        //    parameters.add("out_scodejecucioninterface ", "string", "", "out", null, 1000);
        //    parameters.add("out_scodmensaje", "string", "", "out", null, 1000);
        //    parameters.add("out_sdescmensaje", "string", "", "out", null, 1000);
        //    cgdata.connection.Open();
        //    cgdata.procedureNoquery("sisac.websvcsisac_pkg.prc_busquedainf", parameters.toArray());
        //    cgdata.connection.Close();            
        //    write(toJSON(parameters.getParameter(2).Value));
        //}


        // CONSULTA Y RETORNA LOS TIPOS DOCUMENTO A LA PAGINA

        //
        public void getTypesDoc(JsonObject data)
        {
            JsonArray arr = tracking.getTypesDoc();
            write(arr);
        }

        // RETORNA PKG QUE LE CORRESPONDE AL USUARIO LOGUEADO
        public void getPkg(JsonObject data)
        {
            // NOTA: ESTA FUNCION SOLO SE UTILIZA EN LOADPKG DE JAVASCRIPT DE TRACKING
            // POR TAL RAZON ES TA FIJO EL DATO DE ENTREGADO
            // NOTA: enviar prefijo desde la pagina
            JsonArray arr = tracking.getUserPkg(id_user, dataUser["outsubareacodOut"].ToString(), data["CODIGO_ORDEN"].ToString(), 0);
            write(arr);
        }


        // RETORNA CODIGO DE NUEVO DESTINATARIO
        public void get_codigo_nuevo_dest(JsonObject data) {
            write(tracking.get_codigo_nuevo_dest());
        }

        // PAQUETES EN MANOS DE OTRAS PERSONAS
        public void getPkgFromOthers(JsonObject data)
        {
            
            JsonObject error = new JsonObject();         
            int type = 0;
            string codigo_orden = "";
            try {
                //AQUI SE DESCOMPRIME EL CODIGO ORDEN
                try
                {
                    codigo_orden = ConverterToBase64.Decompress(data["CODIGO"].ToString());

                }
                catch (Exception ex)
                {
                    type = 1;
                    error["ERROR"] = 1;
                    error["MENSAJE"] = "Codigo incorrecto. Verifique codigo o si lector de codigo de barras esta defectuoso.";
                    throw ex;
                }
                //string codigo_orden = data["CODIGO"].ToString();
                JsonObject arr = new JsonObject();
                JsonArray arr_agrupar = new JsonArray();
                
                if (data["AGRUPAR"] != null && data["AGRUPAR"].ToString().Equals("1"))
                {
                    arr_agrupar = tracking.getUserPkg(id_user, dataUser["outsubareacodOut"].ToString(), codigo_orden, 0, 0);
                    arr = (JsonObject)(arr_agrupar)[0];
                    if(arr_agrupar.Length == 0){
                        type = 1;
                        error["ERROR"] = 1;
                        error["MENSAJE"] = "Documento o Paquete no existe o ya fue entregado y no permite realizar transacciones sobre el.";
                        throw new Exception();
                    }else if(!arr["ID_SUB_AREA"].ToString().Equals(dataUser["outsubareacodOut"].ToString())){
                        type = 1;
                        error["ERROR"] = 1;
                        error["MENSAJE"] = "Paquete que intenta agrupar no esta en su area. Favor verificar.";
                        throw new Exception();
                    }                  
                }
                else {
                    arr = tracking.getPkgFromOthers(id_user, codigo_orden, (Dictionary<string, string>)dataUser["documentos_area"]);
                }
                
                write(arr);
            }catch(Exception ex){
                if(type != 1){
                    error["ERROR"] = 1;                    
                    error["MENSAJE"] = ex.Message + " / " + ex.StackTrace;      
                }
                write(error);
            }
        }


        public void getFromWsPkg(JsonObject data)
        {
            JsonObject obj = tracking.getFromWsPkg(id_user, data["CODIGO"].ToString(), int.Parse(data["ENTREGADO"].ToString()));
            write(obj);
        }

        // BUSCA UN PAQUETE QUE CORRESPONDA AL CRITERIO DE BUSQUEDAD
        public void buscarDocumentos(JsonObject data)
        {
            write(tracking.buscarDocumento(data["TERM"].ToString(), data["CODIGO_ORDEN"].ToString(), data["ID_SUB_AREA"].ToString()));
        }

        // BUSCA UN PAQUETE QUE CORRESPONDA AL CRITERIO DE BUSQUEDAD
        public void busquedadAvanzada(JsonObject data)
        {
            write(tracking.busquedadAvanzada(data, id_user, dataUser["outsubareacodOut"].ToString()));
        }

        // AGRUPA LOS PAQUETES QUE ESTAN EN EL KEY "CHILDRENS"
        // DE LA VARIABLE DATA, EN UN NUEVO BATCH
        public void crearPaquete(JsonObject data)
        {
            try
            {
                cgdata.connection.Open();
                cgdata.BeginTransaction();
                JsonObject result = tracking.crearPaquete(id_user, dataUser["outsubareacodOut"].ToString(), data["nota"].ToString(), (JsonArray)data["childrens"], (Dictionary<string, string>)dataUser["documentos_area"]);

                if (result["ERROR"].ToString() != "1")
                {
                    cgdata.commitTransaction();                                    
                }
                cgdata.connection.Close();
                write(result);
            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                cgdata.connection.Close();
                write(ex.Message);
                write(ex.StackTrace);
            }
        }

        // CREA UN NUEVO DOCUMENTO Y DEVUELVE EL ID A LA PAGINA
        public void crearDocumento(JsonObject data)
        {
            try
            {
                cgdata.connection.Open();
                cgdata.BeginTransaction();

                Dictionary<string, object> dataPaquete = new Dictionary<string, object>();
                Dictionary<string, string> dataPaq;

                JsonObject package = new JsonObject();

                //OBJETOS
                JsonObject informacionDoc = (JsonObject)data["controles"];

                Dictionary<string, string> direccion_ = new Dictionary<string, string>();
                JsonObject direccion = (JsonObject)data["direction"];
                JsonObject receiver;
                JsonObject final_receiver;

                // ID DE DESTINATARIO FINAL CUANDO SE ENVIA INTERMEDIARIO
                string id_other_dest = "";
                if (data.Contains("destinatario_via"))
                {
                    receiver = (JsonObject)data["destinatario_via"];
                    final_receiver = (JsonObject)data["receiver"];
                    id_other_dest = tracking.insertDestinatario(
                        final_receiver["consecutivo"].ToString(),
                        final_receiver["nombre"].ToString(),
                        final_receiver["codtipoinformante"].ToString(),
                        final_receiver["tipopersona"].ToString()
                    ) + ",";
                    

                }
                else {
                    receiver = (JsonObject)data["receiver"];

                }
                 
               
                // LLENA LOS DATOS DEL DOCUMENTO EN EL DICCIONARIO DATA PAQUETE

                foreach (string prop in informacionDoc.Names)
                {
                    dataPaquete.Add(prop, informacionDoc[prop].ToString());
                }

                string dest_cod_tipo = receiver["codtipoinformante"].ToString();

                dataPaquete.Add("DESTINATARIO", dest_cod_tipo);
                dataPaquete.Add(dest_cod_tipo + "CODIGO", receiver["consecutivo"].ToString());
                dataPaquete.Add(dest_cod_tipo + "NOMBRE", receiver["nombre"].ToString());
                dataPaquete.Add(dest_cod_tipo + "TIPO", receiver["codtipoinformante"].ToString());
                dataPaquete.Add(dest_cod_tipo + "TIPO_PERSONA", receiver["tipopersona"].ToString());
                dataPaquete.Add("ID_USUARIO", id_user);
                dataPaquete.Add("ID_SISTEMA_ORIGEN", "1");                
                dataPaquete.Add("ID_AREA", dataUser["outsubareacodOut"].ToString());
                dataPaquete.Add("ID_CONTRATO", " ");
                dataPaquete.Add("ID_FAMILIA", " ");

                // DIRECCION
                if (data.Contains("destinatario_via"))
                {
                    dataPaq = dataPaquete.ToDictionary(k => k.Key, k => k.Value.ToString());
                    direccion_ = tracking.wsDireccion(dataPaq);
                }
                else
                {
                    // DIRECCION
                    foreach (string prop in direccion.Names)
                    {
                        
                        direccion_.Add(prop, direccion[prop].ToString());
                    }
                }
                // SE AGREGA DATA DE DIRECCION
                dataPaquete.Add("ID_SUCURSAL", direccion_["codsucursal"]);
                dataPaquete.Add("DATA_DIRECCION", direccion_);

                // REGLA FIJA PARA DOCUMENTOS MANUALES O SUBIDOS POR CARGA DE ARCHIVO
                JsonObject rule1 = (JsonObject)((JsonArray)tracking.getReglas()["-1"])[0];
                JsonObject rule = (JsonObject)JsonConvert.Import(rule1.ToString());
                Dictionary<string, object> batc = (Dictionary<string, object>)tracking.getAllBatch()[rule["batch"].ToString()];
                int cont = tracking.aumentarBatch(int.Parse(batc["ID"].ToString()));
                tracking.removeCache("batchs");
                // NOTA: SI CAMBIA EL LARGO DEL CODIGO ORDEN, TAMBIEN DEBE SER CAMBIADO AQUI
                string batch_code = batc["PREFIJO"].ToString() + cont.ToString().PadLeft(11, '0');
                
                JsonArray levels = (JsonArray)rule["niveles"];
                JsonObject level = new JsonObject();
                level["tipo"] = "campo";
                level["valor"] = dest_cod_tipo + "CODIGO";
                levels.Add(level);

                rule["niveles"] = levels;
                rule["destinatario"] = dest_cod_tipo;

                dataPaquete = tracking.applyRulesDocManual(dataPaquete, rule, batch_code);
                tracking.insertLog("-1", 1, id_user, "Se creo batch manualmente via tracking.", batch_code, dataPaquete["ID_AREA"].ToString());

                //INSERTA DESTINATARIO
                int id_dest = tracking.insertDestinatario(dataPaquete[dest_cod_tipo + "CODIGO"].ToString(), dataPaquete[dest_cod_tipo + "NOMBRE"].ToString(), dataPaquete[dest_cod_tipo + "TIPO"].ToString(), dataPaquete[dest_cod_tipo + "TIPO_PERSONA"].ToString());
                // INSERTA LA DIRECCION     
                int id_dir = tracking.insertDireccion(id_dest, direccion_["codciudad"], direccion_["codprovincia"], direccion_["codbarrio"], direccion_["codtipodireccion"], direccion_["direccion"], direccion_["telefono"]);                

                // INSERT DEL PAQUETE
                package = tracking.insertPackage(dataPaquete["COD_ORDEN"].ToString(), dataPaquete["COD_PAQUETE"].ToString()
                , dataPaquete["NUM_REF"].ToString(), dataPaquete["ID_AREA"].ToString(), dataPaquete["ID_OBJETO"].ToString()
                , int.Parse(dataPaquete["ID_TIPO_OBJETO"].ToString()), "1", dataPaquete["ID_SUCURSAL"].ToString(), id_dir, id_dest, id_dest + "," + id_other_dest, id_user, dataPaquete["NOTES"].ToString());

                cgdata.commitTransaction();
                cgdata.connection.Close();
                write(1);


            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                write(ex.Message);
                write(ex.StackTrace);
            }
        }

        // PERMITE RECIBIR UN PAQUETE
        public void recibirPaquete(JsonObject data)
        {
            string nota = data["nota"].ToString();
            string id_usr = data["id_user"].ToString();
            int id_status = int.Parse(data["id_status"].ToString());          
            string message = "";
            int error = 0;
            JsonArray paquetes = (JsonArray)data["paquetes"];
            JsonObject paquete;
            JsonObject dict = new JsonObject();
            //Dictionary<string, string> dataPaquete;
            System.Web.Script.Serialization.JavaScriptSerializer json;

            try
            {
                cgdata.connection.Open();
                cgdata.BeginTransaction();
                for (int i = 0; i < paquetes.Length; i++)
                {
                    paquete = (JsonObject)paquetes[i];
                    paquete.Remove("childrens");
                    paquete["id_status"] = id_status;
                    paquete["id_user"] = id_usr;
                    paquete["id_sub_area_user"] = dataUser["outsubareacodOut"].ToString();
                    // SI EL STATUS CON QUE SE INTENTA REALIZAR LA TRANSACCION ES DESCARTADO = 10
                    // ES NECESARIO QUE EL PAQUETE ESTE EN LA MISMA AREA
                    if (id_status == 10)
                    {
                        if (paquete["ID_SUB_AREA"].ToString() == dataUser["outsubareacodOut"].ToString())
                        {
                            tracking.recibirPaquete(paquete["CODIGO_ORDEN"].ToString(), id_usr, dataUser["outsubareacodOut"].ToString(), id_status, nota);
                        }
                        else
                        {
                            message = "Para descartar un paquete es necesario que esten en su area, favor verificar el paquete de codigo orden: " + paquete["CODIGO_ORDEN"].ToString() + ".";
                            error = 1;
                            throw new Exception();
                        }
                    }
                    else if (id_status == 6)
                    {
                        if (paquete["ID_SUB_AREA"].ToString() == dataUser["outsubareacodOut"].ToString())
                        {
                            tracking.recibirPaquete(paquete["CODIGO_ORDEN"].ToString(), id_usr, dataUser["outsubareacodOut"].ToString(), id_status, nota);
                        }
                        else
                        {
                            message = "Para asignar un paquete es necesario que esten en su area, favor verificar el paquete de codigo orden: " + paquete["CODIGO_ORDEN"].ToString() + ".";
                            error = 1;
                            throw new Exception();
                        }
                    }
                    else if (id_status == 5)
                    {
                        if (paquete["ID_SUB_AREA"].ToString() != dataUser["outsubareacodOut"].ToString())
                        {
                            tracking.recibirPaquete(paquete["CODIGO_ORDEN"].ToString(), id_usr, dataUser["outsubareacodOut"].ToString(), id_status, nota);
                        }
                        else
                        {
                            message = "Se esta intentando recibir un paquete que ya fue recibido en su area. Favor verificar el paquete de codigo orden: " + paquete["CODIGO_ORDEN"].ToString() + ".";
                            error = 1;
                            throw new Exception();
                        }
                    }
                    else
                    {
                        tracking.recibirPaquete(paquete["CODIGO_ORDEN"].ToString(), id_usr, dataUser["outsubareacodOut"].ToString(), id_status, nota);
                    }
                    // APLICANDO NOTIFICACIONES
                    //json = new System.Web.Script.Serialization.JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
                    //dict = (Dictionary<string, object>)json.DeserializeObject(JsonConvert.ExportToString(paquete));
                    //dataPaquete = dict.ToDictionary(k => k.Key, k => k.Vale == null ? "" : k.Value.ToString());
                    
                    //tracking.threadNotificaciones(id_usr, dataUser["outsubareacodOut"].ToString(), paquete, id_status);
                    //t.Start(id_usr, dataUser["outsubareacodOut"].ToString(), paquete, id_status);
                    //tracking.aplicarNotificaciones(id_usr, dataUser["outsubareacodOut"].ToString(), paquete, id_status);
                    tracking.noThreadNotificaciones(id_usr, dataUser["outsubareacodOut"].ToString(), paquete, id_status);
                }
                cgdata.commitTransaction();
                cgdata.connection.Close();
                write(1);
            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                if (error == 1)
                {
                    write(message);
                    //write(0);
                }
                else
                {
                    write(ex.Message);
                    write(ex.StackTrace);
                }

            }
        }        

        // INSERT HISTORIAL PKG
        public void insertLog(JsonObject data)
        {
            try
            {
                cgdata.connection.Open();
                JsonObject pkg = (JsonObject)data["package"];
                JsonObject dataLog = (JsonObject)data["dataLog"];
                tracking.insertLog(pkg["ID"].ToString(), int.Parse(pkg["STATUS"].ToString()), id_user, dataLog["nota"].ToString(), pkg["CODIGO_ORDEN"].ToString(), dataUser["outsubareacodOut"].ToString());
                cgdata.connection.Close();
                write("1");
            }
            catch (Exception ex)
            {
                write(ex.Message);
                write(ex.StackTrace);
            }
            finally
            {
                cgdata.connection.Close();
            }
        }

        // REGISTRA NUEVA NOTA
        public void newNote(JsonObject data)
        {
            try
            {
                cgdata.connection.Open();
                tracking.newNote(data, id_user);
                write("1");
            }
            catch (Exception ex)
            {
                write(ex.Message);
                write(ex.StackTrace);
            }
            finally
            {
                cgdata.connection.Close();
            }
        }

        //RETORNA HISTORIAL DE UN PAQUETE
        public void getLogPkg(JsonObject data)
        {
            JsonObject pkg = (JsonObject)data["package"];
            JsonArray arr = tracking.getLogPkg(int.Parse(pkg["ID"].ToString()), pkg["CODIGO_ORDEN"].ToString());
            write(arr);
        }

        //  CONSULTA Y LUEGO RETORNA LAS NOTAS DE UN HISTORIAL
        public void getNotas(JsonObject data)
        {
            JsonArray arr = tracking.getNotas(int.Parse(data["id_hist"].ToString()));
            write(arr);
        }

        // RETORNA EL DETALLE DE UN PAQUETE
        public void getDetPkg(JsonObject data)
        {
            JsonObject detalle = tracking.getDetPkg(data, id_user, data["ID_SUB_AREA"].ToString());
            if (detalle.Contains("error"))
            {
                write("Error: Direccion incorrecta del destinatario.");
            }
            else {
                write(detalle);
            }
            
        }

        // RETORNA LOS PAQUETES HIJOS DE UN PAQUETE
        public void getPkgChildrens(JsonObject data)
        {
            write(tracking.getUserPkg(id_user, data["ID_SUB_AREA"].ToString(), data["CODIGO_ORDEN"].ToString(), int.Parse(data["ENTREGADO"].ToString())));
        }

        //RETORNA LAS DIRECCIONES EXISTENTES EN TRACKING DE UN DESTINATARIO O CTE
        public void getDirDest(JsonObject data)
        {
            cgdata.connection.Open();
            JsonArray dir = tracking.getDirDest(data["cod"].ToString(), data["tipo"].ToString(), data["tipo_persona"].ToString());
            cgdata.connection.Close();
            write(dir);

        }

        // CAMBIO DE DIRECCION
        public void cambioDireccion(JsonObject data)
        {
            try
            {
                cgdata.connection.Open();
                cgdata.BeginTransaction();
                tracking.cambioDireccion(data, id_user, dataUser["outsubareacodOut"].ToString());
                cgdata.commitTransaction();
                cgdata.connection.Close();
                write(1);
            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                write(ex.StackTrace + " / " + ex.Message);
            }
        }

        // CAMBIO DE DESTINATARIO
        public void cambioDestinatario(JsonObject data)
        {

            JsonObject result_data = new JsonObject();
            try
            {
                cgdata.connection.Open();
                cgdata.BeginTransaction();

                Dictionary<string, string> dataPaq = new Dictionary<string, string>();
                Dictionary<string, string> direccion_ = new Dictionary<string, string>();
                JsonObject direccion = (JsonObject)data["direction"];
                JsonObject receiver;
                JsonObject final_receiver;

                // ID DE DESTINATARIO FINAL CUANDO SE ENVIA INTERMEDIARIO
                string id_other_dest = "";
                if (data.Contains("destinatario_via"))
                {
                    receiver = (JsonObject)data["destinatario_via"];
                    final_receiver = (JsonObject)data["receiver"];
                    id_other_dest = tracking.insertDestinatario(
                        final_receiver["consecutivo"].ToString(),
                        final_receiver["nombre"].ToString(),
                        final_receiver["codtipoinformante"].ToString(),
                        final_receiver["tipopersona"].ToString()
                    ) + ",";
                }
                else
                {
                    receiver = (JsonObject)data["receiver"];
                }

                //INSERTA DESTINATARIO
                int id_dest = tracking.insertDestinatario(receiver["consecutivo"].ToString(), receiver["nombre"].ToString(), receiver["codtipoinformante"].ToString(), receiver["tipopersona"].ToString());

                string dest_cod_tipo = receiver["codtipoinformante"].ToString();

                // DIRECCION
                if (data.Contains("destinatario_via"))
                {
                    dataPaq.Add("DESTINATARIO", dest_cod_tipo);
                    dataPaq.Add(dest_cod_tipo + "CODIGO", receiver["consecutivo"].ToString());
                    dataPaq.Add(dest_cod_tipo + "NOMBRE", receiver["nombre"].ToString());
                    dataPaq.Add(dest_cod_tipo + "TIPO", receiver["codtipoinformante"].ToString());
                    dataPaq.Add(dest_cod_tipo + "TIPO_PERSONA", receiver["tipopersona"].ToString());
                    dataPaq.Add("ID_CONTRATO", " ");
                    dataPaq.Add("ID_FAMILIA", " ");
                    direccion_ = tracking.wsDireccion(dataPaq);
                }
                else
                {
                    // DIRECCION
                    foreach (string prop in direccion.Names)
                    {
                        direccion_.Add(prop, direccion[prop].ToString());
                    }
                }
                // INSERTA LA DIRECCION            
                int id_dir = tracking.insertDireccion(id_dest, direccion_["codciudad"], direccion_["codprovincia"], direccion_["codbarrio"], direccion_["codtipodireccion"], direccion_["direccion"], direccion_["telefono"]);                

                Equivalencias eq = new Equivalencias();
                string codigo = receiver["consecutivo"].ToString();
                // NOTA: SI CAMBIA EL LARGO DEL CODIGO ORDEN, DEBE SER CAMBIANDO AQUI TAMBIEN
                string part_codigo_orden = eq[dest_cod_tipo + "CODIGO"] + codigo.PadLeft(11, '0');
                string codigo_orden_new = data["CODIGO_ORDEN"].ToString();
                codigo_orden_new = codigo_orden_new.Substring(0, codigo_orden_new.Length - 14);
                codigo_orden_new = codigo_orden_new + part_codigo_orden;
                tracking.cambioDestinatario(data, id_dest, id_dest + "," + id_other_dest, id_dir, codigo_orden_new, data["notes"].ToString(), id_user, dataUser["outsubareacodOut"].ToString());
                cgdata.commitTransaction();
                cgdata.connection.Close();
                result_data["ERROR"] = 0;
                result_data["CODIGO_ORDEN"] = codigo_orden_new;
                write(result_data);
            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                result_data["ERROR"] = 1;
                result_data["MESSAGE"] = ex.StackTrace + " / " + ex.Message;
                write(result_data);
            }
        }

        // RETORNA TODOS LOS BATCH
        public void getJsonBatch(JsonObject data)
        {
            write(tracking.getJsonBatchs(dataUser["outsubareacodOut"].ToString()));
        }

        //INCREMETAR EL CONTADOR DE UN BATCH
        public void aumentarBatch(JsonObject data)
        {
            try
            {
                cgdata.connection.Open();
                cgdata.BeginTransaction();
                tracking.aumentarBatch(int.Parse(data["ID"].ToString()));
                cgdata.commitTransaction();
                cgdata.connection.Close();
                // RETORNA TODOS LOS BATCH LUEGO DE QUE FUE AUMENTADO
                write(tracking.getJsonBatchs());
            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                write(ex.StackTrace + " / " + ex.Message);
            }
        }

        // PROCESO DE IMPRESION - TRACKING

        public void process_Print(JsonObject data)
        {
            try
            {

                cgdata.connection.Open();
                cgdata.BeginTransaction();
                Random rnd = new Random();
                string id_report = DateTime.Now.ToString("MMddyyyyHHmmssfff") + rnd.Next(999) + rnd.Next(9999);
                process_print_(data, id_report, null);
                cgdata.commitTransaction();
                cgdata.connection.Close();
                data["MESSAGE"] = "";
                data["ERROR"] = 0;
                data["id_report"] = id_report;
                write(data);

            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                data["MESSAGE"] = "No pudo ejecutarse la impresion correctamente." + ex.Message + " / " + ex.StackTrace;
                data["ERROR"] = 1;                
            }

        }

        // PROCESO PARA CARGAR DATOS A LA TABLA TEMPORAL
        private void process_print_(JsonObject data, string id_report, string id_acuse)
        {

            string tipo = data["TIPO_IMPRESION"].ToString();
            string impresion = data["IMPRESION"].ToString();
            string codigo_orden = data["CODIGO_ORDEN"].ToString();
            int nivel = int.Parse(data["NIVEL"].ToString());
            

            JsonArray objetos;
            JsonObject objeto;

            JsonObject data_;
            data_ = new JsonObject();
            data_["TIPO_IMPRESION"] = tipo;            
            data_["NIVEL"] = nivel;

            // SI ES 0 = PAQUETES, SI ES 1 = DOCUMENTOS
            if (tipo == "0")
            {
                // LABELS DE PAQUETE FINAL O PENULTIMO NIVEL
                if (nivel == 2)
                {
                    objetos = tracking.getPaquetesFinal(id_user, dataUser["outsubareacodOut"].ToString(), codigo_orden, 1);
                    data_["TIPO_IMPRESION"] = "1";
                }
                // LABEL DE PAQUETE ACTUAL
                else if (nivel == 0 || (nivel == 1 && impresion == "0"))
                {
                    objetos = tracking.getUserPkg(id_user, dataUser["outsubareacodOut"].ToString(), codigo_orden, -1, 0);
                    data_["NIVEL"] = "1";
                }
                // LABEL DE PAQUETES CONTENIDO EN EL PAQUETE ACTUAL DEL CODIGO ORDEN
                else
                {
                    objetos = tracking.getUserPkg(id_user, dataUser["outsubareacodOut"].ToString(), codigo_orden, -1, nivel);                    
                }

                // SI ES 0 = ACUSE, Y SI ES 1 = LABEL
                if (impresion == "0") {
                    
                    data_["IMPRESION"] = "1";

                    for (int i = 0; i < objetos.Length; i++)
                    {
                        objeto = (JsonObject)objetos[i];
                        objeto = setDestinatioFinal(objeto);
                        //destinatarios = (JsonArray)objeto["DATA_CODIGO_ORDEN"];                        
                        data_["CODIGO_ORDEN"] = objeto["CODIGO_ORDEN"];
                        process_print_(data_, id_report, insert_objeto_report(objeto, id_report, id_acuse).ToString());
                    }

                }else{
                    
                    for (int i = 0; i < objetos.Length; i++)
                    {
                        objeto = (JsonObject)objetos[i];
                        objeto = setDestinatioFinal(objeto);
                        //destinatarios = (JsonArray)objeto["DATA_CODIGO_ORDEN"];
                        insert_objeto_report(objeto, id_report, id_acuse);
                    }
                
                }

            }
            else
            {
                // SI ES 0 = ACUSE, Y SI ES 1 = LABEL
                if (impresion == "0")
                {

                    data_["IMPRESION"] = "1";

                    objetos = tracking.getUserPkg(id_user, dataUser["outsubareacodOut"].ToString(), codigo_orden, -1, 0);
                    for (int i = 0; i < objetos.Length; i++)
                    {
                        objeto = (JsonObject)objetos[i];
                        objeto = setDestinatioFinal(objeto);
                        //destinatarios = (JsonArray)objeto["DATA_CODIGO_ORDEN"];
                        id_acuse = insert_objeto_report(objeto, id_report, id_acuse).ToString();                        
                    }

                }
                objetos = tracking.getDocumentosPrint(id_user, dataUser["outsubareacodOut"].ToString(), codigo_orden);
                for (int i = 0; i < objetos.Length; i++)
                {
                    objeto = (JsonObject)objetos[i];
                    objeto = setDestinatioFinal(objeto);
                    insert_objeto_report(objeto, id_report, id_acuse);    
                
                }
            }
        }

        private string insert_objeto_report(JsonObject objeto, string id_report, string id_acuse) {

            //objeto["DEST_ID_FINAL"] = objeto["DEST_ID"];
            //objeto["DEST_COD_FINAL"] = objeto["DEST_COD"];
            //objeto["DEST_NAME_FINAL"] = objeto["DEST_NAME"];
            //objeto["DEST_ID_TIPO_FINAL"] = objeto["DEST_ID_TIPO"];
            //objeto["DEST_TIPO_COD_FINAL"] = objeto["DEST_TIPO_COD"];
            //objeto["DEST_TIPO_NOM_FINAL"] = objeto["DEST_TIPO_NOM"];
            //objeto["DEST_TIPO_PERSONA_FINAL"] = objeto["DEST_TIPO_PERSONA"];
            objeto["ID_ACUSE"] = id_acuse;
            return tracking.insert_tmp_report(objeto, id_report, id_user);

        }

        private JsonObject setDestinatioFinal(JsonObject objeto) {

            JsonArray destinatarios;
            JsonObject destinatario;
            // VARIABLE DE VERIFICACION SI EL DOCUMENTO TIENE UN DESTINATARIO FINAL
            // DE ACUERDO AL TIPO DE DOCUMENTO QUE ES
            // EN CASO DE TENER UNO, BAND CAMBIARA A = FALSE
            // EN CASO DE QUE NO, BAND CONTINUARA TRUE Y SE ASUMIRA QUE EL DESTINATARIO FINAL ES EL VIA
            bool band = true;
            if (objeto["FINAL"].ToString() == "0")
            {
                objeto["DEST_ID_FINAL"] = objeto["DEST_ID"];
                objeto["DEST_COD_FINAL"] = objeto["DEST_COD"];
                objeto["DEST_NAME_FINAL"] = objeto["DEST_NAME"];
                objeto["DEST_ID_TIPO_FINAL"] = objeto["DEST_ID_TIPO"];
                objeto["DEST_TIPO_COD_FINAL"] = objeto["DEST_TIPO_COD"];
                objeto["DEST_TIPO_NOM_FINAL"] = objeto["DEST_TIPO_NOM"];
                objeto["DEST_TIPO_PERSONA_FINAL"] = objeto["DEST_TIPO_PERSONA"];
                if (tracking.Verificar_Prox_Nivel(objeto["CODIGO_ORDEN"].ToString(), 1) == 1)
                {
                    objeto["ID_DIRECCION"] = objeto["ID_DIR_LAST_DOC"];
                }            
            }else{

                destinatarios = tracking.getDestinatariosPaquete(int.Parse(objeto["ID"].ToString()));
                for (int j = 0; j < destinatarios.Length; j++)
                {
                    destinatario = (JsonObject)destinatarios[j];
                    if (destinatario["DEST_TIPO_COD"].ToString() == objeto["DESTINATARIO_FINAL"].ToString())
                    {
                        band = false;
                        objeto["DEST_ID_FINAL"] = destinatario["DEST_ID"];
                        objeto["DEST_COD_FINAL"] = destinatario["DEST_COD"];
                        objeto["DEST_NAME_FINAL"] = destinatario["DEST_NAME"];
                        objeto["DEST_ID_TIPO_FINAL"] = destinatario["DEST_ID_TIPO"];
                        objeto["DEST_TIPO_COD_FINAL"] = destinatario["DEST_TIPO_COD"];
                        objeto["DEST_TIPO_NOM_FINAL"] = destinatario["DEST_TIPO_NOM"];
                        objeto["DEST_TIPO_PERSONA_FINAL"] = destinatario["DEST_TIPO_PERSONA"];
                        //objeto["DEST_ID"] = destinatario["DEST_ID"];
                        //objeto["DEST_COD"] = destinatario["DEST_COD"];
                        //objeto["DEST_NAME"] = destinatario["DEST_NAME"];
                        //objeto["DEST_ID_TIPO"] = destinatario["DEST_ID_TIPO"];
                        //objeto["DEST_TIPO_COD"] = destinatario["DEST_TIPO_COD"];
                        //objeto["DEST_TIPO_NOM"] = destinatario["DEST_TIPO_NOM"];
                        //objeto["DEST_TIPO_PERSONA"] = destinatario["DEST_TIPO_PERSONA"];
                        break;
                    }
                }
                if (band)
                {
                    objeto["DEST_ID_FINAL"] = objeto["DEST_ID"];
                    objeto["DEST_COD_FINAL"] = objeto["DEST_COD"];
                    objeto["DEST_NAME_FINAL"] = objeto["DEST_NAME"];
                    objeto["DEST_ID_TIPO_FINAL"] = objeto["DEST_ID_TIPO"];
                    objeto["DEST_TIPO_COD_FINAL"] = objeto["DEST_TIPO_COD"];
                    objeto["DEST_TIPO_NOM_FINAL"] = objeto["DEST_TIPO_NOM"];
                    objeto["DEST_TIPO_PERSONA_FINAL"] = objeto["DEST_TIPO_PERSONA"];
                }
            }           
            return objeto;

        }

        #endregion


    }
    #endregion

}