﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Tracking.Objetos_DB;
using Jayrock.Json;
using Tracking.Core;

namespace Tracking.wsTracking
{
    /// <summary>
    /// Summary description for wsConsPkg
    /// </summary>
    [WebService(Namespace = "/Tracking/wsTracking/wsConsPkg.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class wsConsPkg : System.Web.Services.WebService
    {
//        const string consPkgDescription = @"
//        <table>
//            <tr>
//                <td>Summary:</td><td>Consultar Paquete By ID Tracking</td>
//            </tr>
//            <tr>
//                <td>Parameters:</td><td>&nbsp;</td>
//            </tr>
//            <tr>
//                <td>id_paquete:</td><td>ID del Paquete que se desea consultar.</td>
//            </tr>
//        </table>";
        public tracking tracking;
        public cgdata cgdata;

        [WebMethod(Description = "Consulta detalle de un Paquete")]
        public PaqueteDetalle consPkg(string codigo, string id_user)
        {
            cgdata = new cgdata();
            tracking = new tracking(cgdata);
            PaqueteDetalle pkg = new PaqueteDetalle();
            // CONSULTA PAQUETE NO ENTREGADOS
            JsonObject paquete = tracking.getFromWsPkg(id_user, codigo, 0);
            DestinatarioPaq[] destinatarios;
            Direccion direccion;

            if (paquete["FINAL"].ToString() == "1")
            {
                JsonArray jsonDest = tracking.getDestinatariosPaquete(int.Parse(paquete["ID"].ToString()));
                JsonObject jsonDir = tracking.getDirPkg(int.Parse(paquete["ID_DIRECCION"].ToString()));
                destinatarios = destinatarioToArrayClass(jsonDest);
                direccion = direccionToClass(jsonDir);
            }
            else {
                destinatarios = new DestinatarioPaq[0];
                direccion = new Direccion();
            }  

            foreach (var prop in pkg.GetType().GetProperties())
            {
                if (prop.Name == "DESTINATARIOS")
                {
                    prop.SetValue(pkg, destinatarios, null);
                }
                else if (prop.Name == "DIRECCION")
                {
                    prop.SetValue(pkg, direccion, null);

                }else{
                    prop.SetValue(pkg, paquete[prop.Name].ToString(), null); 
                }
                
                //data.Add(prop.Name, prop.GetValue(paquete, null).ToString());
            }
            return pkg;
        }

        public DestinatarioPaq[] destinatarioToArrayClass(JsonArray destinatarios)
        {

            DestinatarioPaq[] destinatarios_ = new DestinatarioPaq[destinatarios.Length];
            DestinatarioPaq destinatario;
            JsonObject dest;

            for (int i = 0; i < destinatarios.Length; i++)
            {
                destinatario = new DestinatarioPaq();
                dest = (JsonObject)destinatarios[i];
                foreach (var prop in destinatario.GetType().GetProperties())
                {
                    prop.SetValue(destinatario, dest[prop.Name].ToString(), null);
                }
                destinatarios_[i] = destinatario;
            }

            return destinatarios_;
        }

        public Direccion direccionToClass(JsonObject direccion)
        {

            Direccion direccion_ = new Direccion();
            foreach (var prop in direccion_.GetType().GetProperties())
            {
                prop.SetValue(direccion_, direccion[prop.Name].ToString(), null);
            }

            return direccion_;
        }
        
    }
    
}
