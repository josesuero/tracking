﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Tracking.Objetos_DB;
using Tracking.Core;
using Jayrock.Json;
using System.Threading;
using Tracking.WsMain;


namespace Tracking.wsTracking
{
    /// <summary>
    /// Summary description for wsCreatePkg
    /// </summary>
    [WebService(Namespace = "/Tracking/wsTracking/wsCreatePkg.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class wsCreatePkg : System.Web.Services.WebService
    {
        public cgdata cgdata;
        public tracking tracking;
        public JsonArray reglas;


        public wsCreatePkg() {
            
        }

        // ES UTILIZADO POR EL WS DE INTEGRACION Y ESTE LLAMA EL METODO PRINCIPAL DE INSERTAR PAQUETE
        public JsonObject insertPackage(Dictionary<string, object> destinatario, Destinatario[] destinatarios)
        {
            JsonObject package = new JsonObject();                
            string dest = destinatario["DESTINATARIO"].ToString();
            Destinatario destinatario_;
            int id_dest = -1;
            int id_ = -1;
            string l_destinatarios = "";
                
            for (int i = 0; i < destinatarios.Length; i++)
            {
                destinatario_ = destinatarios[i];

                id_ = tracking.insertDestinatario(destinatario_.CODIGO, destinatario_.NOMBRE, destinatario_.TIPO, destinatario_.TIPO_PERSONA);
                l_destinatarios += id_ + ",";
                if (dest == destinatario_.TIPO)
                {
                    id_dest = id_;
                }
            }               

            //int id_dest = tracking.insertDestinatario(destinatario[dest + "CODIGO"], destinatario[dest + "NOMBRE"], destinatario[dest + "TIPO"], destinatario[dest + "TIPO_PERSONA"]);

            //WebSvcTrackingUser_prcDirtrack_Out wsDir = (WebSvcTrackingUser_prcDirtrack_Out)destinatario["DATA_DIRECCION"];
            //int id_dir = id_dir = tracking.insertDireccion(id_dest, wsDir.outdireccionesOut[0].codciudad, wsDir.outdireccionesOut[0].codprovincia, wsDir.outdireccionesOut[0].codbarrio, wsDir.outdireccionesOut[0].codtipodireccion, wsDir.outdireccionesOut[0].direccion);

            Dictionary<string, string> wsDir = (Dictionary<string, string>)destinatario["DATA_DIRECCION"];
            int id_dir = tracking.insertDireccion(id_dest, wsDir["codciudad"], wsDir["codprovincia"],wsDir["codbarrio"]
            , wsDir["codtipodireccion"], wsDir["direccion"], wsDir["telefono"]);

            //int id_dir = tracking.wsDireccion(destinatario, id_dest);

            // INSERTA LA DIRECCION.
            //int id_dir = int.Parse(direccion["id"].ToString());
            //if (id_dir == -1)                {
            //    // TODO : CODTIPODIRECION ENVIARLO QUE NO ESTA EN LA PANTALLA
            //    id_dir = tracking.insertDireccion(id_dest, direccion["codciudad"].ToString(), direccion["codprovincia"].ToString(), direccion["codbarrio"].ToString(), "1", direccion["direccion"].ToString());
            //}
            package = tracking.insertPackage(destinatario["COD_ORDEN"].ToString(), destinatario["COD_PAQUETE"].ToString()
            , destinatario["NUM_REF"].ToString(), destinatario["ID_AREA"].ToString(), destinatario["ID_OBJETO"].ToString()
            , int.Parse(destinatario["ID_TIPO_OBJETO"].ToString()), destinatario["ID_SISTEMA_ORIGEN"].ToString(),
            destinatario["ID_SUCURSAL"].ToString(), id_dir, id_dest, l_destinatarios, destinatario["ID_USUARIO"].ToString()
            , destinatario["NOTES"].ToString(), destinatario["NO_CASO"].ToString(), destinatario["ID_CONTRATO"].ToString());            
            
            return package;

        }

        [WebMethod(Description = "Crear Paquete Tracking")]
        public PaqueteOut CreatePkg(Paquete paquete, Destinatario[] destinatarios)
        {
            // CRONOMETRO DE  TIEMPO DE DURACION DE PROCESO DE GENERACION DE CODIGO ORDEN
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            PaqueteOut pkgOut = new PaqueteOut();
            Dictionary<string, object> data;

            cgdata = new cgdata();
            tracking = new tracking(cgdata);
            
            try
            {

                cgdata.connection.Open();
                cgdata.BeginTransaction();
                //OBJETOS
                // seleccionar destinatarios    
                data = tracking.applyRules(paquete, destinatarios);
                if (data.ContainsKey("EJECUCION") == true)
                {
                    cgdata.rollBackTran();
                    pkgOut.CODIGO = "";
                    pkgOut.CODIGO_ORDEN = "";
                    pkgOut.EJECUCION = 0;
                    pkgOut.MESSAGE = data["MESSAGE"].ToString();
                    return pkgOut;
                }
                //HILO QUE INSERTA EN LA BASE DE DATOS
                //Thread t = new Thread (new ParameterizedThreadStart(insertPackage));
                //t.Start (destinatario);              

                JsonObject dataResult = insertPackage(data, destinatarios);
                //pkgOut.ID = int.Parse(package["id_pkg"].ToString());
                pkgOut.CODIGO = dataResult["codigo"].ToString();
                pkgOut.CODIGO_ORDEN = dataResult["cod_orden"].ToString();
                pkgOut.EJECUCION = 1;
                pkgOut.MESSAGE = "Ejecucion exitosa";
                // commint
                cgdata.commitTransaction();
                cgdata.connection.Close();
            }
            catch (Exception ex){
                if(cgdata != null && cgdata.transaction != null){
                    cgdata.rollBackTran();
                }                
                pkgOut.CODIGO = "";
                pkgOut.CODIGO_ORDEN = "";
                pkgOut.EJECUCION = 0;
                pkgOut.MESSAGE = ex.Message + " / " + ex.StackTrace;
            }
            sw.Stop();
            pkgOut.TIME = sw.ElapsedMilliseconds.ToString();
            return pkgOut;
        }

    }

    
}
