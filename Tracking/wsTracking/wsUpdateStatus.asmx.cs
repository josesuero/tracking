﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Tracking.Core;
using Jayrock.Json;
using Jayrock.Json.Conversion;
using Tracking.Objetos_DB;

namespace Tracking.wsTracking
{
    /// <summary>
    /// Summary description for wsUpdateStatus
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class wsUpdateStatus : System.Web.Services.WebService
    {

//        const string consPkgDescription = @"
//        <table>
//            <tr>
//                <td>Summary:</td><td>Actualizar paquete</td>
//            </tr>
//            <tr>
//                <td>Parameters:</td><td>&nbsp;</td>
//            </tr>
//            <tr>
//                <td>:</td><td>ID del Paquete que se desea consultar.</td>
//            </tr>
//        </table>";
        public tracking tracking;
        public cgdata cgdata;

        [WebMethod(Description = "updatePaquete")]
        public PaqueteOut updatePkg(string codigo, string id_user, string id_sub_area, int id_status, string nota)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            cgdata = new cgdata();
            tracking = new tracking(cgdata);
            Dictionary<string, string> documentos_area = tracking.getDocumentosArea(int.Parse(id_sub_area));
            JsonObject paquete = tracking.getPkgFromOthers(id_user, codigo, documentos_area);
            PaqueteOut pkgOut = new PaqueteOut();

            if(paquete["ERROR"].ToString() == "1"){
                pkgOut.CODIGO = paquete["CODIGO"].ToString();
                pkgOut.CODIGO_ORDEN = paquete["CODIGO_ORDEN"].ToString();
                pkgOut.EJECUCION = 0;
                pkgOut.MESSAGE = paquete["MENSAJE"].ToString();
                return pkgOut;
            }

            //Dictionary<string, object> dict;
            //Dictionary<string, string> dataPaquete;
            //System.Web.Script.Serialization.JavaScriptSerializer json;

            try
            {
                cgdata.connection.Open();
                cgdata.BeginTransaction();
                paquete.Remove("childrens");
                paquete["id_status"] = id_status;
                paquete["id_user"] = id_user;
                paquete["id_sub_area_user"] = id_sub_area;
                tracking.recibirPaquete(paquete["CODIGO_ORDEN"].ToString(), id_user, id_sub_area, id_status, nota);

                // APLICANDO NOTIFICACIONES
                //json = new System.Web.Script.Serialization.JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
                //dict = (Dictionary<string, object>)json.DeserializeObject(JsonConvert.ExportToString(paquete));
                //dataPaquete = dict.ToDictionary(k => k.Key, k => k.Value == null ? "" : k.Value.ToString());
                //tracking.aplicarNotificaciones(id_user, id_sub_area,dataPaquete, id_status);
                tracking.noThreadNotificaciones(id_user, id_sub_area, paquete, id_status);
                cgdata.commitTransaction();

                pkgOut.CODIGO = paquete["CODIGO"].ToString();
                pkgOut.CODIGO_ORDEN = paquete["CODIGO_ORDEN"].ToString();
                pkgOut.EJECUCION = 1;
                pkgOut.MESSAGE = "Ejecucion exitosa";
            }
            catch (Exception ex)
            {
                cgdata.rollBackTran();
                pkgOut.CODIGO = paquete["CODIGO"].ToString();
                pkgOut.CODIGO_ORDEN = paquete["CODIGO_ORDEN"].ToString();
                pkgOut.EJECUCION = 0;
                pkgOut.MESSAGE = ex.Message + " / " + ex.StackTrace;
            }

            sw.Stop();
            pkgOut.TIME = sw.ElapsedMilliseconds.ToString();
            return pkgOut;
        }
    }
}
