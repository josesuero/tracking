﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.DataAccess.Client;


namespace Tracking.Core
{
    public class Parameters
    {
        public List<OracleParameter> parameters;
        public enum TiposDatos { 
            integer = OracleDbType.Int32,
            strings = OracleDbType.Varchar2,
            fecha = OracleDbType.Date,
            tabla = OracleDbType.RefCursor,
            arrayInt = OracleDbType.Array,
            obj = OracleDbType.Object
            
        };
        //public enum TipoDatos {
        //    integer, strings, number
        //}
        public static Dictionary<string, OracleDbType> type = new Dictionary<string, OracleDbType>() { 
            {"int",OracleDbType.Int32},
            {"string",OracleDbType.Varchar2},
            {"date",OracleDbType.Date},
            {"obj",OracleDbType.Object}

        };
        
        public static Dictionary<string, ParameterDirection> direction = new Dictionary<string, ParameterDirection>() { 
            {"in",ParameterDirection.Input},            
            {"out",ParameterDirection.Output},
            {"returnValue",ParameterDirection.ReturnValue},
            {"inOut",ParameterDirection.InputOutput}
        };
        public Parameters (){
            parameters = new List<OracleParameter>();
        }        

        public void addIn(string parameterName, object value, TiposDatos dType)
        {
            add(parameterName, value, ParameterDirection.Input, null, -1, dType); 
        }

        //public void addIn(string parameterName, object value, int size)
        //{
        //    add(parameterName, value, ParameterDirection.Input, null, size, ""); 
        //}
        

        public void addOut(string parameterName, object value, TiposDatos dType)
        {
            add(parameterName, value, ParameterDirection.Output, null, -1, dType); 
        }

        //public void addOut(string parameterName, object value, int size)
        //{
        //    add(parameterName, value, ParameterDirection.Output, null, size, ""); ;
        //}

        //public void addReturnVal(string parameterName, object value)
        //{
        //    add(parameterName, value, ParameterDirection.ReturnValue, null, -1, null); 
        //}

        //public void addReturnVal(string parameterName, object value, int size)
        //{
        //    add(parameterName, value, ParameterDirection.ReturnValue, null, size,"");
        //}
        public void addInOut(string parameterName, object value, TiposDatos dType)
        {
            add(parameterName, value, ParameterDirection.InputOutput, null, -1, dType);
        }

        //public void add(string parameterName, object value, TiposDatos dType)
        //{
        //    add(parameterName, value, ParameterDirection.Input, null, -1, dType);
        //}
        public void add(string parameterName, object value, ParameterDirection dir, string UdtTypeName, int size, TiposDatos dType) {
            add(parameterName, value, dir, UdtTypeName, size, dType, OracleCollectionType.None);
        }

        public void add(string parameterName, object value, ParameterDirection dir, string UdtTypeName, int size, TiposDatos dType, OracleCollectionType collection)
        {
            
            OracleParameter ora = new OracleParameter();            
            ora.ParameterName = parameterName;
            ora.Direction = dir;
            ora.OracleDbType = (OracleDbType)dType;
            if (value != null)
                ora.Value = value;
               
            if (UdtTypeName != null)
                ora.UdtTypeName = UdtTypeName;
            if (collection != OracleCollectionType.None)
                ora.CollectionType = collection;
            if (size != -1)
                ora.Size = size;
            parameters.Add(ora);            
        }

        public OracleParameter[] toArray()
        {
            return parameters.ToArray();
        }

        public OracleParameter getByIndex(int index) {
            return parameters.ElementAt<OracleParameter>(index);
        }

        public OracleParameter getByName(string name)
        {
            return parameters.Find(
                delegate(OracleParameter p)
                {
                    return p.ParameterName == name;
                }
            );
        }
        public void removeParams() {
            parameters.Clear();
        }
    }
}