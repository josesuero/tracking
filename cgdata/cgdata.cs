﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.DataAccess.Client;

namespace Tracking.Core
{
    public class cgdata
    {
        public OracleConnection connection;
        public OracleTransaction transaction;

        public cgdata() {
            connection = connect();
            transaction = null;
        }

        public OracleConnection connect() {
            string connstr = System.Configuration.ConfigurationManager.AppSettings["connstr"];
            OracleConnection dbconn = new OracleConnection(connstr);
            return dbconn;
        }

        public void BeginTransaction() {
            transaction = connection.BeginTransaction();
        }

        public void rollBackTran() {
            transaction.Rollback();
            transaction = null;
        }

        public void commitTransaction() {
            transaction.Commit();
            transaction = null;
        }

        public DataSet query(string query) {
            return this.query(query, null,transaction);
        }

        public DataSet query(string query, OracleParameter[] oraParams){
            return this.query(query, oraParams,transaction);
        }
        public DataSet query(string query, OracleParameter[] oraParams, OracleTransaction tran)
        {
            DataSet ds = new DataSet();
            OracleCommand cmd = new OracleCommand(query, connection);
            if (oraParams != null)
                cmd.Parameters.AddRange(oraParams);
            cmd.Transaction = tran;            
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.SelectCommand = cmd;
            da.Fill(ds);
            return ds;
        }

        public void noquery(string sqlstr)
        {
            this.noquery(sqlstr, null, transaction);
        }

        public void noquery(string sqlstr, OracleParameter[] oraParams)
        {
            this.noquery(sqlstr, oraParams, transaction);
        }

        public void noquery(string sqlstr, OracleParameter[] oraParams, OracleTransaction tran)
        {
            OracleCommand cmd = new OracleCommand(sqlstr, connection);
            cmd.Transaction = tran;                
            if (oraParams != null)
                cmd.Parameters.AddRange(oraParams);                
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        public object scalar(String sqlstr)
        {
            return scalar(sqlstr, null, transaction);
        }
        public object scalar(String sqlstr, OracleParameter[] oraParams)
        {
            return scalar(sqlstr, oraParams, transaction);
        }
        public object scalar(String sqlstr, OracleParameter[] oraParams, OracleTransaction sqltran)
        {
            OracleCommand cmd = new OracleCommand(sqlstr, connection);
            if (sqltran != null)
                cmd.Transaction = sqltran;
            if (oraParams != null)
                cmd.Parameters.AddRange(oraParams);
            return cmd.ExecuteScalar();
        }

        private OracleCommand storedprocedure(string procedure, OracleParameter[] oraParams)
        {
            OracleCommand cmd = new OracleCommand(procedure, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            //OracleCommandBuilder.DeriveParameters(cmd);
            if (oraParams != null)
                cmd.Parameters.AddRange(oraParams);
            return cmd;
        }

        public DataSet procedureDataSet(string procedure, OracleParameter[] oraParams)
        {
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(storedprocedure(procedure, oraParams));
            da.Fill(ds);
            return ds;
        }

        public object procedureScalar(string procedure, OracleParameter[] oraParams)
        {
            try
            {
                connection.Open();
                OracleCommand comm = storedprocedure(procedure, oraParams);
                return comm.ExecuteScalar();
            }
            finally
            {
                connection.Close();
            }
        }

        public void procedureNoquery(string procedure, OracleParameter[] oraParams) {
            OracleCommand comm = storedprocedure(procedure, oraParams);
            comm.ExecuteNonQuery();
        }

    }
}